import sconeClient.*;
import java.lang.String;
import java.util.*;
import java.io.*;


public class parseEvents {
    public static  String server ="localhost";
    public static int port =5000 ;
    public static String sendToScone(String query){
	String result = "nil";
	try{
	    SconeClientXML sc = new SconeClientXML(server, port);
	    try{
		 result = sc.evalLisp(query);
		System.out.println(result);
	    }catch(Exception e){
		System.out.println(e);
	    }
	    sc.close();
	}catch(Exception e){
	    System.out.println(e);
	}
	return result;
    }
    public static void main(String args[]){
	String evt, date, device, status;

	FileInputStream fis;
	DataInputStream din;
	try{
	    fis = new FileInputStream("config");
	    din = new DataInputStream(fis);
	    try { 
		//server=din.readLine();
		//port = Integer.parseInt(din.readLine());
		din.close();
		//System.out.println("Host: "+server+" Port: "+ port);
		//sendToScone("(load \"/home/mjsantof/repo/mariajose.santofimia/tesis/CMU/Scone/validation/scone-definitions.lisp\")");
	    } catch (IOException e) {  
		System.out.println("Error while reading the config file");
	    }  
	    
	}catch(Exception ex){
	    System.out.println("A config file should be provided with the Scone Server connection information");
	}
	
	try{
	    fis = new FileInputStream(args[0]);
	    din = new DataInputStream(fis);
	    while(din.available()!=0){
		evt = din.readLine();
		int beginIndex=0;
		int endIndex =0;
		if((endIndex = evt.indexOf(" "))>0){
		    date = evt.substring(beginIndex, endIndex);
		    beginIndex = endIndex;
		    endIndex = evt.indexOf(" ", beginIndex+1);
		    device = evt.substring(beginIndex+1, endIndex);
		    beginIndex = endIndex;
		    status = evt.substring(beginIndex+1);
		    //System.out.println(date + " "+device+ " "+ status);
		    String action  = getAction(device, status);
		    String query = "(new-event-indv {"+date+" "+device+" "+status+"} "+action+")";
		    
		    //System.out.println(query);
		    sendToScone(query);
		}
		
	    }
	    	    
	}catch(IOException ex){
	    System.out.println("Error, an event file must be provided as an argument");
	}

    }
   
    public static boolean sensingDevice(String device){
	String query = "(parent-wire (lookup-element {"+device+"}))";
	if(sendToScone(query).equals("{events:ambient-sensor}"))
	    return true;
	else
	    return false;

    }
    public static String getAction(String device, String status){
	String query, sensedUnit;
	int beginIndex=0;
	if(sensingDevice(device)){
	    sensedUnit = sendToScone("(get-the-x-of-y {sensed unit} {"+device+"})");
	    beginIndex = sensedUnit.indexOf(":");
	    sensedUnit = sensedUnit.substring(beginIndex+1, sensedUnit.length()-1);
	    status = "{Measure "+ status + " "+sensedUnit+"}";
	    //  System.out.println("Status: "+ status);
	    query = "(get-action (lookup-element "+status+") {sensed-by-event} {"+device+"})";
	}
	else
	    query = "(get-action {"+status+"} {caused-by-event} {"+device+"})";
	
	//	System.out.println(query);
	return sendToScone(query);
    }

}

