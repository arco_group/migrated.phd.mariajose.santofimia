package jadex.examples.MASCS.Perceptor;

import java.util.*;
import jadex.runtime.*;


/**
 *  Stack blocks according to the target configuration.
 */

public class FaceRecognitionPlan	extends Plan
{
    //-------- attributes --------
    
    /** The desired target configuration. */

    
    //-------- constructors --------
    
    /**
     *  Create a new plan.
     */
    public FaceRecognitionPlan()
    {

    }
    
    //-------- methods --------
    
    /**
     *  The plan body.
     */
    public void body()
    {
    }
}
