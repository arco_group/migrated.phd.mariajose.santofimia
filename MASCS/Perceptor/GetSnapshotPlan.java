package jadex.examples.MASCS.Perceptor;

import java.util.*;
import jadex.runtime.*;


/**
 *  Stack blocks according to the target configuration.
 */

public class GetSnapshotPlan	extends Plan
{
    //-------- attributes --------
    
    /** The desired target configuration. */

    
    //-------- constructors --------
    
    /**
     *  Create a new plan.
     */
    public GetSnapshotPlan()
    {

    }
    
    //-------- methods --------
    
    /**
     *  The plan body.
     */
    public void body()
    {
    }
}
