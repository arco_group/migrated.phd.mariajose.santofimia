package jadex.examples.MASCS.Perceptor;

import java.util.*;
import jadex.runtime.*;


/**
 *  Stack blocks according to the target configuration.
 */

public class ClassifyDamagePlan	extends Plan
{
    //-------- attributes --------
    
    /** The desired target configuration. */

    
    //-------- constructors --------
    
    /**
     *  Create a new plan.
     */
    public ClassifyDamagePlan()
    {

    }
    
    //-------- methods --------
    
    /**
     *  The plan body.
     */
    public void body()
    {
    }
}
