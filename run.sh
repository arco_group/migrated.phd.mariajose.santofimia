#!/bin/bash

### CONFIGURATION VARIABLES ###
PATH_TO_SCONE=/home/mjsantof/home/mariajose.santofimia/tesis/CMU/Scone
PATH_TO_JADEX=/home/mjsantof/tesis/jadex/jadex-0.96/src
PATH_TO_DUMMIES=/home/mjsantof/home/mariajose.santofimia/tesis/CMU/Scone/dummy

# Action to be done: compile, start or stop
ACTION=$1

### PROCESS THE COMMAND LINE ARGS ###
#if no action is supplied, exit and print usage

if [ "$ACTION" = "" ]; then
    echo "Usage: ./run.sh {compile | start | stop} "
    exit
fi

# Now we have everything we need.  Let's get ready to start the server.
if [ "$ACTION" = "start" ]; then
## STARTING SCONE
    cd $PATH_TO_SCONE/server/scone-server-release-1.0
    ./start-server 5000 localhost

## STARTING ICEBOX
    cd $PATH_TO_DUMMIES
    icebox --Ice.Config=config.icebox & 
    icestormadmin --Ice.Config=config.icestorm -e "create alarms"
    
## STARTING DUMMIES
    cd $PATH_TO_DUMMIES/presence
    python subscriptor.py &
    python presenceDummy.py &
    cd $PATH_TO_DUMMIES/faceDetector
    ./server &
    cd $PATH_TO_DUMMIES/frameGrabber
    ./server &
    cd $PATH_TO_DUMMIES/FacialRecognition
    ./server &
    
## STARTING JADEX
    cd $PATH_TO_JADEX
    java jadex.adapter.standalone.Platform &
    exit
fi
if [ "$ACTION" = "stop" ]; then
    ## STOPING SCONE
    cd $PATH_TO_SCONE/server/scone-server-release-1.0
    ./stop-server    
    exit
fi