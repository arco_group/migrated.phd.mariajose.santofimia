module Events
{
	struct Event {
	       string type;
	};
	interface EventMonitor {
		void report (Event e);  
	};
};
