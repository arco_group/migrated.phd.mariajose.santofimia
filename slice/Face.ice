module Face {
   interface Detector {
   	void faceDetector();
   };
   interface Recognizer {
   	int faceRecognizer();
   };
};
