;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; 
;;; Load the Scone engine files and the bootstrap knowledge.
;;;
;;; Author: Scott E. Fahlman
;;; ***************************************************************************

;;; Copyright (C) 2003-2007, Carnegie Mellon University.

;;; The Scone software is made available to the public under the CPL
;;; 1.0 Open Source license.  A copy of this license is distributed
;;; with the software.  The license can also be found at URL
;;; <http://www.opensource.org/licenses/cpl.php>.

;;; The Scone engine may incoporate some fragments of the NETL2
;;; system, developed by Scott E. Fahlman for IBM Coporation between
;;; June 2001 and May 2003.  IBM holds the copyright on NETL2 and has
;;; made that software available to the author under the CPL 1.0 Open
;;; Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under contract number NBCHD030010.
;;; Any opinions, findings and conclusions or recommendations expressed in
;;; this material are those of the author(s) and do not necessarily reflect
;;; the views of DARPA or the Department of Interior-National Business
;;; Center (DOI-NBC).

;;; Additional support for the development of Scone and related applications
;;; has been provided by generous research grants from Cisco Systems Inc. and
;;; Google Inc.

;;; ***************************************************************************

;;; This file defines the SCONE function that starts up some user-specified
;;; version of SCONE, and sets things up so that we will by default get the
;;; matching KB files.

;;; NOTE: Contains some installation-specific pathnames.  If you move this
;;; directory, be sure to update the pathnames.

;;; NOTE: We want a single version of the SCONE function that will
;;; work for newer versions of SCONE, which create a separate "SCONE"
;;; package, and older versions that do not create a separate
;;; package. And if we try to read in a SCONE::FOO symbol when the
;;; "SCONE" package does not exist, we get an error.  So the function
;;; below unfortunately employs some evil voodoo in order to deal with
;;; this.  Sorry!

(defvar *version*)
(defvar *default-kb-pathname*)

(declaim (ftype (function (string &key (:verbose boolean)))
		load-kb))

(defun scone (&optional (version "current"))
  (setq *version* version)
  (setq *default-kb-pathname* 
    (format nil "/home/mjsantof/tesis/CMU/Scone/scone/0.7x/kb/anonymous.lisp"
	    *version*))
  (load (format nil "/home/mjsantof/tesis/CMU/Scone/scone/0.7x/engine"
		*version*))
  ;; If we're using a Scone engine that creates a separate scone package,
  ;; get into that package.
  (when (find-package :scone)
    (in-package :scone)
    (set (intern "*DEFAULT-KB-PATHNAME*") cl-user::*default-kb-pathname*))
  (funcall (intern "LOAD-KB") "bootstrap")
  (values))

(format t "~2%;;; Call (scone \"some-version-name\") to start Scone.~%")

