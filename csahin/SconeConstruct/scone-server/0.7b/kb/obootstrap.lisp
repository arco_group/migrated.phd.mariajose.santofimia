;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; Bootstrap KB definitions.
;;;
;;; Author: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2003-2006, Carnegie Mellon University.

;;; The Scone software and documentation are made available to the
;;; public under the CPL 1.0 Open Source license.  A copy of this
;;; license is distributed with the software.  The license can also be
;;; found at URL <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone
;;; software, you agree to be bound by the terms and conditions set
;;; forth in the CPL 1.0 Open Source License.  If you do not agree to
;;; these terms and conditions, or if they are not legally applicable
;;; in the jurisdiction where such use takes place, then you may not
;;; use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by
;;; Scott E. Fahlman for IBM Coporation between June 2001 and May
;;; 2003.  IBM holds the copyright on NETL2 and has made that software
;;; available to the author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under contract number NBCHD030010.
;;; Any opinions, findings and conclusions or recommendations expressed in
;;; this material are those of the author(s) and do not necessarily reflect
;;; the views of DARPA or the Department of Interior-National Business
;;; Center (DOI-NBC).
;;; ***************************************************************************
;;;
;;; Load this before any other KB files.

;;; This file contains the initial bootstrap structures for the Scone
;;; Knowledge Base.  This includes concepts such as {thing} and {set} that
;;; are essential to the proper working of the Scone engine.

;;; Some of the elements contained in this file are stored in global
;;; variables such as *thing* and *set* so that engine functions can easily
;;; refer to them.  These variables are declared in the engine file.

;;; This file necessarily contains some forward references.
(setq *defer-unknown-connections* t)

;;; Suppress error checking for this file.
(setq *no-checking* t)

(in-namespace "common")

;;; Must put this first so that deferred connection is fixed later.
(setq *has-link* {has link})

;;; This is the top node of the type hierarchy.  Everything is a {thing}.
;;; Note that the parent of {thing} is {thing} -- top of the tree.
(setq *thing*
      (new-type {thing} {thing}
		:context {universal}))

;;; Create the CONTEXT type.
(new-type {context} {intangible}
	  :context {universal})

;;; The universal context is always on. Statements in this context are
;;; always active, and entities in this context always exist.
(setq *universal*
      (new-indv {universal} {context}
		:context {universal}
		:english :adj-noun))

;;; This is the default context, representing actual reality.
(setq *general*
      (new-indv {general} {universal}
		:context {universal}
		:english :adj-noun))

;;; The following definitions are in the {universal} context.
(in-context {universal})

;;; Parent node for relations.
(setq *relation*
      (new-type {relation} {intangible}))


;;; Numbers types.

(setq *number* (new-type {number} {intangible}))

(setq *integer*
      (new-type {integer} {exact number}
		:english '("whole number" "counting number")))

(setq *ratio*
      (new-type {ratio} {exact number}
		:english '("fraction")))

(setq *float*
      (new-type {floating-point number} {inexact number}
		:english '("float"
			   :adj-noun
			   "real")))

;;; Strings.
(setq *string*
      (new-type {string} {intangible}))

;;; Functions.
(setq *function*
      (new-type {function} {intangible}))

;;; Lisp Defstructs.
(setq *struct*
      (new-type {struct} {intangible}))

;;; Parent nodes for built-in link types.
(new-type {link} {relation})

(setq *is-a-link*
      (new-type {is-a link} {link}))

(setq *is-not-a-link*
      (new-type {is-not-a link} {link}))

(setq *eq-link*
      (new-type {eq link} {link}))

(setq *not-eq-link*
      (new-type {not-eq link} {link}))

(setq *map-link*
      (new-type {map link} {link}))

(setq *not-map-link*
      (new-type {not-map link} {link}))

(setq *has-link*
      (new-type {has link} {link}))

(setq *has-no-link*
      (new-type {has-no link} {link}))

(setq *cancel-link*
      (new-type {cancel link} {link}))

(setq *split*
      (new-type {split link} {link}))

(setq *complete-split*
      (new-type {complete split link} {split link}))


;;; Now we can create some splits among types created earlier.

(new-split-subtypes {thing}
		    '(({tangible} :adj)
		      ({intangible} :adj)))

(new-complete-split-subtypes {number}
  '({exact number} {inexact number}))

(new-split '({integer} {ratio}))

;;; Define sets.
(setq *set*
      (new-type {set} {intangible}
		:english '("group"
			   "collection"
			   "bunch")))

;;; Useful contants.
(setq *zero* {0})
(setq *one* {1})
(setq *two* {2})

(process-deferred-connections)

;;; Set the context to {general}.
(in-context {general})

;;; Now we can add some role nodes.

;;; Almost any THING can have a set of parts.  The interpetation of PART
;;; may vary a bit between classes like TYPEWRITER, TEXAS, and THURSDAY,
;;; but all of these fall under this PART role.
(new-type-role {part} {thing} {thing}
	       :may-have t)

;;; Every set has a set of MEMBERs.
(new-type-role {member} {set} {thing})

;;; Every set has a cardinality, which is an integer.
(setq *cardinality*
      (new-indv-role {cardinality} {set} {integer}
		     :english '("number of members")))

;;; Define empty and non-empty sets.
(new-complete-split-subtypes
 {set}
 '({empty set}
   {non-empty set})
 :context {universal})

(setq *empty-set* (lookup-element {empty set}))

(setq *non-empty-set* (lookup-element {non-empty set}))

;;; Note the last element created by this file.
(setq *last-bootstrap-element* *last-element*)
