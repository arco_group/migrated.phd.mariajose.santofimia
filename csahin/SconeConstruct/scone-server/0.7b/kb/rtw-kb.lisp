;;;; -------------------------------------------------------------------------
;;;; Read the Web Ontology
;;;;
;;;; Author: Ben Lambert (benlambert@cmu.edu)
;;;; Last Update: 3-31-06
;;;;
;;;; This a draft ontology is for the Read the Web projects.  The ontology is 
;;;; based on the scenario discussed in class on 3-9-06.  See the class Website
;;;; for the original pictures of the blackboard:
;;;; http://www.cs.cmu.edu/afs/cs.cmu.edu/project/theo-21/www/index.html
;;;;
;;;;
;;;; Table of Contents:
;;;; - Publications (a la DBLP)
;;;; - Academic Concepts
;;;;    - Research Areas
;;;;    - Conferences
;;;;    - Departments
;;;;    - Academic relations
;;;; - Microbiology
;;;; - Read The Web relations
;;;; - Example instantiations (with MaryS and JoeJ)
;;;;
;;;;
;;;; -------------------------------------------------------------------------



;; Load Radar first
;;(load-kb "radar")


;(in-namespace "rtw" :include "common")


;;;; -------------------------------------------------------------------------
;;;; PUBLICATIONS

;;;; These definitions for publications are designed to be compatible with DBLP
;;;; a computer science publication database.  XML data for DBLP can be found:

;;;; http://www.informatik.uni-trier.de/~ley/db/
;;;; OR a subset at:
;;;; http://www.cs.washington.edu/research/xmldatasets/www/repository.html

(new-type {publication} {information object})  ;; build this down from RADAR's info object

;; 8 types of publications defined in DBLP
(new-type {article} {publication})
(new-type {inproceedings} {publication})
(new-type {proceedings} {publication})
(new-type {book} {publication})
(new-type {incollection} {publication})
(new-type {phdthesis} {publication})
(new-type {mastersthesis} {publication})
(new-type {www} {publication})


;; These are just a few of the "roles" (or fields) potentially associated with each DBLP publication
;; All of the following may be available for a given publication (according to the XML DTD): 
;  author|editor|title|booktitle|pages|year|address|journal|volume|number|month|url|ee|cdrom|cite|publisher|note|crossref|isbn|series|school|chapter

(new-type-role {author} {publication} {common:person} :may-have t)
(new-type-role {editor} {publication} {common:person} :may-have t)
(new-indv-role {pub year} {publication} {common:year} :may-have t)
(new-indv-role {journal} {publication} {publication} :may-have t)
(new-indv-role {pub month} {publication} {common:month} :may-have t)
(new-indv-role {pub url} {publication} {common:url} :may-have t)
(new-type-role {publisher} {publication} {common:thing} :may-have t)
(new-type-role {pub series} {publication} {common:thing} :may-have t)
(new-type-role {pub school} {publication} {common:school} :may-have t)

;; This role is not in DBLP but we may want to associate papers in proceedings with
;; the conference that they appear in 
(new-indv-role {venue} {inproceedings} {conference})





;;;; -------------------------------------------------------------------------
;;;; ACADEMIC CONCEPTS

;;;; These concepts augment some of the academic definitions already in the Radar KB

;; Concepts such as: person, male, female, student, and faculty that were
;; discussed in class already exist in the RADAR and Core KBs

;; CMU is already defined in the Radar KB, define Pitt
(new-indv {University of Pittsburgh} {common:university})
(english {University of Pittsburgh}  "Pitt")  ;;a.k.a "Pitt"


;; RESEARCH AREAS

;; We need the concept of academic topics and research areas, we can associate these
;; with people (faculty and students), conferences, departments, papers, etc.

(new-type {academic topic} {thing} :english '("field" "topic" "research area"))

;; We need computer science and biology for the scenario discussed in class
(new-type {computer science} {academic topic} :english '("CS"))
(new-type {biology} {academic topic} :english '("bio"))

;; We can further subdivide broad fields into finer-grained research areas.
(new-type {machine learning} {computer science} :english '("ML"))
(new-type {knowledge representation} {computer science} :english '("KR"))
(new-type {tissue engineering} {biology})

;; University affiliated people may have research areas
(new-type-role {research area} {university person} {academic topic} :may-have t) 

;; CONFERENCES

;; Most of the concepts for conferences (location, date, organizer, etc.) 
;; are already in the Radar KB.  The only thing we'll add is a research area for conferences
;; and a relation between people and the conferences they attend
(new-indv-role {conference topic} {conference} {academic topic})

(new-relation {attends conference} :a-inst-of {person} :b-inst-of {conference})


;; DEPARTMENTS

;; Universities have departments
(new-type-role {department} {university} {organization})

;; Departments have a topic, faculty, and students
(new-type-role {department research area} {department} {academic topic})
(new-type-role {department faculty} {department} {faculty member})
(new-type-role {department student} {department} {student})

;; Most university affiliated people have a "home" department (maybe not in SCS)
(new-type-role {home department} {university person} {department})

;; Define CS and Bio Depts. for CMU and Pitt
(new-indv {Pitt Biology Department} {department})
(x-is-a-y-of-z {biology} {department research area} {Pitt Biology Department})
(x-is-a-y-of-z {Pitt Biology Department} {department} {University of Pittsburgh})

(new-indv {Pitt CS Department} {department})
(x-is-a-y-of-z {computer science} {department research area} {Pitt CS Department})
(x-is-a-y-of-z {Pitt CS Department} {department} {University of Pittsburgh})

(new-indv {CMU Biology Department} {department})
(x-is-a-y-of-z {biology} {department research area} {CMU Biology Department})
(x-is-a-y-of-z {CMU Biology Department} {department} {Carnegie Mellon})

(new-indv {CMU CS Department} {department}) ;; CSD
(x-is-a-y-of-z {computer science} {department research area} {CMU CS Department})
(x-is-a-y-of-z {CMU CS Department} {department} {Carnegie Mellon})



;; ACADEMIC RELATIONS

(new-relation {collaborates with} :a-inst-of {person} :b-inst-of {person} :symmetric t)

;; coauthor is a sub-type of collaborates with
(new-relation {coauthor with} :parent {collaborates with} :symmetric t)

;; so is adviser of (except it's not symmetric) and have type restrictions
(new-relation {adviser of} :a-inst-of {faculty member} :b-inst-of {student} :parent {collaborates with})



;;;; -------------------------------------------------------------------------
;;;; MICROBIOLOGY


;; Need a top-level concept for this?  Not exactly tangible.  Subliminal?
(new-type {microscopic} {thing})
(new-type {microbiological element} {microscopic})

;; A few biological and chemical concepts we'll need
(new-type {protein} {microbiological element})
(new-type {gene} {microbiological element})
(new-type {metabolic pathway} {microbiological element})
(new-type {molecule} {microscopic})
(new-type {atom} {microscopic})
(new-is-a {protein} {molecule})

;; One of the biological relations we probably want to look for
(new-relation {inhibits} :a-inst-of {microbiological element} :b-inst-of {metabolic pathway})

;; We also need a concept of location with a cell
(new-type {cellular location} {place})
(new-indv {mitochondrion} {cellular location})
(new-indv {nucleus} {cellular location})
(new-indv {Golgi} {cellular location})


;;;; -------------------------------------------------------------------------
;;;; READ THE WEB RELATIONS


;; For Vitor, Mohit, Richard, and Einat's group to use
(new-relation {co-occurs with} :a-inst-of {thing} :b-inst-of {thing}) ;;symmetric?

;; Any information object (eg. book, paper, email) can mention anything (e.g person, conference, etc.)
(new-relation {mentions} :a-inst-of {information object} :b-inst-of {thing})

;; If we want to extract birthdays we need to assert that people have birthdays
;; While we're at it we can give all creatures that are "born" birthdays (e.g. my dogs b-day)

(new-indv-role {birthday} {common:animal} {common:day})




;;;; -------------------------------------------------------------------------
;;;; SOME EXAMPLE INSTANTIATIONS (based on the example we did in class)


;; Scott
(new-indv {Scott Fahlman} {professor})
(x-is-a-y-of-z {knowledge representation} {research area} {Scott Fahlman})
(x-is-a-y-of-z {CMU CS Department} {home department} {Scott Fahlman})

;; Prof. MaryS @ Pitt Bio
(new-indv {MaryS} {faculty member} :english '("Mary" "Prof. S"))
(x-is-a-y-of-z {MaryS} {department faculty} {Pitt Biology Department})

;; A paper of Mary's
(new-indv {Example Paper 1} {inproceedings})
(x-is-a-y-of-z {MaryS} {author} {Example Paper 1})

;; Some background about Mary
(new-is-a {MaryS} {female})
(new-indv {April 5 1960} {day})
(x-is-the-y-of-z {April 5 1960} {birthday} {MaryS})

;; Some microbiological things that Mary likes
(new-indv {Krebs Cycle} {metabolic pathway})
(new-indv {Succinyl-CoA} {molecule})

;; I think this is actually true
(new-statement {Succinyl-CoA} {inhibits} {Krebs Cycle})

;; ATP is made in mitochondrion
(new-statement {Succinyl-CoA} {located at} {mitochondrion})
(new-statement {Krebs Cycle} {located at} {mitochondrion})


;; One way to make these biological entities topics to study is to just put
;; an is-a link to {academic topic}.  Alternatively we could make a new
;; concept that represents the topic.
(new-is-a {Krebs Cycle} {academic topic})
(x-is-a-y-of-z {Krebs Cycle} {research area} {MaryS})


;; A conference for Mary
(new-indv {ISMB 2005} {conference} :english '("International Society for Computational Biology 2005")) 

(x-is-the-y-of-z {MaryS} {person in charge} {ISMB 2005}) ;; Mary is the chair
(x-is-the-y-of-z {ISMB 2005} {venue} {Example Paper 1})  ;; and her paper got accepted

;; The paper mentions Krebs
(new-statement {Example Paper 1} {mentions} {Krebs Cycle})

;; a grad student for Mary
(new-indv {JoeJ} {graduate student})

(new-statement {MaryS} {adviser of} {JoeJ})

(x-is-a-y-of-z {JoeJ} {department student} {Pitt Biology Department})


;; How do we want to use Medline id's??  MeSH terms? as aliases?
(english {Example Paper 1} "MEDLINE# 12345678")
(english {Krebs Cycle} "MeSH term XYZ")
