;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; 
;;; Input converters for foreign data formats
;;;
;;; Author: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2005, Carnegie Mellon University.

;;; The Scone software and documentation are made available to the
;;; public under the CPL 1.0 Open Source license.  A copy of this
;;; license is distributed with the software.  The license can also be
;;; found at URL <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone
;;; software, you agree to be bound by the terms and conditions set
;;; forth in the CPL 1.0 Open Source License.  If you do not agree to
;;; these terms and conditions, or if they are not legally applicable
;;; in the jurisdiction where such use takes place, then you may not
;;; use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by
;;; Scott E. Fahlman for IBM Coporation between June 2001 and May
;;; 2003.  IBM holds the copyright on NETL2 and has made that software
;;; available to the author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under contract numbers NBCHC030029 and
;;; NBCHC030030.  Any opinions, findings and conclusions or recommendations
;;; expressed in this material are those of the author(s) and do not
;;; necessarily reflect the views of DARPA or the Department of
;;; Interior-National Business Center (DOI-NBC).
;;; ***************************************************************************


;;; Outlook data for People/Contacts, dumped as a Comma-Separated Values
;;; (CSV) file.

(defun parse-CSV-record (s n)
  "N is the maximum number of fields in a CSV record.  S is a character
   input stream for the CSV file, positioned at the start of a record.
   Suck in the comma-separated fields one by one, stuffing them into the
   successive slots of an array.  If there are too many fields in the
   record signal an error.  If too few, pad with NIL.  Return the array.
   The second return value is T if we hit the end of file, NIL otherwise."
  (let* ((array (make-array n :initial-element nil)))
    ;; Extract no more than N fields.
    (do ((record 0 (1+ record)))
	((>= record n)
	 (error "Too many fields in CSV record."))
      ;; Read the first char and see at what it is.
      (let ((c (read-char s nil 'end)))
	(case c
	  ;; Hit end of file.  Return the record array and T.
	  (end
	   (return-from parse-csv-record (values array t)))
	  ;; Newline.  Return the array.
	  (#\newline
	   (return-from parse-csv-record (values array nil)))
	  ;; Comma.  Null field, so just skip it.
	  (#\, nil)
	  ;; Double-quote.  Read a quoted field.
	  (#\"
	   (multiple-value-bind (string eor eof)
				(parse-quoted-CSV-field s)
	     (setf (aref array record) string)
	     (when eor
	       (return-from parse-CSV-record
			    (values array eof)))))
	  ;; Anything else, read as a regular field.
	  (t (unread-char c s)
	     (multiple-value-bind (string eor eof)
				  (parse-CSV-field s)
	       (setf (aref array record) string)
	       (when eor
		 (return-from parse-CSV-record
			      (values array eof))))))))))

(defun parse-CSV-field (s)
  "Read one non-quoted CVS field from stream S.  Returns the value as a
   string.  The second return value is T if we hit the end of the record.
   The third is T if we hit the end of the file."
  (let ((charlist nil)
	(string nil)
	(eor nil)
	(eof nil))
    (loop
      (let ((c (read-char s nil 'end)))
	(case c
	  ;; Hit end of file.  Exit loop, end of file.
	  (end
	   (setq eor t)
	   (setq eof t)
	   (return))
	  ;; Hit end of record.  Exit loop, end of record.
	  (#\newline
	   (setq eor t)
	   (return))
	  ;; Hit comma.  Exit loop.
	  (#\, (return))
	  ;; Anything else, accumulate character for output string.
	  (t (push c charlist)))))
    ;; Turn char list to string.
    (setq string (coerce (nreverse charlist) 'string))
    ;; For non-quoted fields, strip surrounding whitespace.
    (setq string (string-trim '(#\Space #\Tab) string))
    ;; Return string, end of record, and end of file.
    (values string eor eof)))

(defun parse-quoted-CSV-field (s)
  "Read one quoted CVS field from stream S.  Assume that the opening
   double-quote has already been read.  Returns the value as a string.  The
   second return value is T if we hit the end of the record.  The third is
   T if we hit the end of the file."
  (let ((charlist nil)
	(string nil)
	(eor nil)
	(eof nil))
    (loop
      (let ((char (read-char s nil 'end)))
	(case char
	  ;; Hit end of file.  This shouldn't happen.
	  (end
	   (error "Hit end of file within quoted field."))
	  ;; Hit a double quote.
	  (#\"
	   ;; Check for second double-quote, used to add one double-quote
	   ;; within a quoted string.
	   (cond ((char= (peek-char nil s nil #\x) #\")
		  ;; Consume the second double quote.
		  (read-char s)
		  ;; Accumulate one double-quote into the string.
		  (push #\" charlist))
		 ;; Only one double-quote.  See what's next.
		 (t (let ((next (read-char s nil 'end)))
		      (case next
			(end
			 (setq eor t)
			 (setq eof t)
			 (return))
			(#\newline
			 (setq eor t)
			 (return))
			(#\,
			 (return))
			(t (break) (error "Garbage after quoted field.")))))))
	  ;; If any other char, accumulate it for output string.
	  (t (push char charlist)))))
    ;; Turn char list to string.
    (setq string (coerce (nreverse charlist) 'string))
    ;; Return string, end of record, and end of file.
    (values string eor eof)))

(defun smerge (array &rest indices)
  "INDICES is a list whose elements may be strings or integers.  If an
   integer, it is an index into ARRAY.  That array element should be either
   a string or NIL.  Return a new string that is a concatenation of all the
   non-null, non-empty argument strings, in order.  Normally we separate
   each of these substrings by a space, but if we encounter a string in the
   INDICES list, that becomes the new separator in sticky fashion."
  (let ((substrings nil)	; Accumulate output string here.
	(separator " ")		; Separator to use before next substring.
	(item nil))		; Item found in the array.
    ;; Iterate over indices, looking for substrings to use.
    (dolist (i indices)
      (etypecase i
	;; If an integer, it points into ARRAY.
	(integer
	 ;; See if it's non-null and non-empty.
	 (when (and (setq item (aref array i))
		    (> (length item) 0))
	   ;; Unless this is the first item, push the separator.
	   (when substrings (push separator substrings))
	   ;; Then push the entry.
	   (push item substrings)))
	;; If not an index, should be a separating string.
	(string (setq separator i))))
    ;; Now create and return the string or NIL.
    (when substrings
      (apply #'concatenate (cons 'string (nreverse substrings))))))

(defmacro assemble-arg (label &rest indices)
  "Macro to build an argument for NEW-RADAR-PERSON.  Uses ARRAY, STRING,
   and ARGLIST from caller."
  `(when (setq string (smerge array ,@indices))
     (when ,label (push ,label arglist))
     (push string arglist)))

(defun parse-outlook-contacts (filename &optional (skip 1))
  "Assume that FILENAME designates a file that is a dump of contact records
   from Outlook in comma-separated-value (CSV) format.  Each record (line)
   in the file is a set of up to 92 comma-separated fields, followed by a
   newline.  Read these records one by one, generating a call to
   NEW-RADAR-PERSON for each well-formed record.  If any components of the
   filename are missing, these are supplied from *DEFAULT-KB-PATHNAME*.
   The optional SKIP argument may be used to skip some lines in the file
   before processing.  Returns the number of records successfully
   processed."
  (let* ((pathname
	  (merge-pathnames (pathname filename) *default-kb-pathname*))
	 (count 0))
    (with-open-file (s pathname :direction :input)
      (dotimes (i skip)
	(unless (read-line s nil nil)
	  (return-from parse-outlook-contacts 0)))
      (loop
	(multiple-value-bind (array eof)
			     (parse-csv-record s 92)
	  (let* ((arglist nil)
		 (string nil))
	    ;; Assemble full name.
	    (assemble-arg nil 1 2 3 4)
	    ;; Do the rest only if there's a full name.
	    (when arglist
	      (assemble-arg :employer 5)
	      (assemble-arg :home-address ", " 15 16 17 18 19 20 21)
	      (assemble-arg :business-address ", " 5 6 8 9 10 11 12 13 14)
	      (assemble-arg :home-phone-number 37)
	      (assemble-arg :office-phone-number 31)
	      (assemble-arg :fax-phone-number 30)
	      (assemble-arg :cell-phone-number 40)
	      (assemble-arg :email-address 57)
	      (assemble-arg :web-page 91)
	      (assemble-arg :gender 66)
	      ;; Call the person-creation function in Scone.
	      (apply #'new-radar-person (nreverse arglist))
	      ;; Count successful creations.
	      (incf count)))
	  ;; If this record terminated with end-of-file, quit.
	  (when eof (return-from parse-outlook-contacts count)))))))


#|
Rosetta stone for Outlook Contact Fields:

00 "Title"
01 "First Name"
02 "Middle Name"
03 "Last Name"
04 "Suffix"
05 "Company"
06 "Department"
07 "Job Title"
08 "Business Street"
09 "Business Street 2"
10 "Business Street 3"
11 "Business City"
12 "Business State"
13 "Business Postal Code"
14 "Business Country"
15 "Home Street"
16 "Home Street 2"
17 "Home Street 3"
18 "Home City"
19 "Home State"
20 "Home Postal Code"
21 "Home Country"
22 "Other Street"
23 "Other Street 2"
24 "Other Street 3"
25 "Other City"
26 "Other State"
27 "Other Postal Code"
28 "Other Country"
29 "Assistant's Phone"
30 "Business Fax"
31 "Business Phone"
32 "Business Phone 2"
33 "Callback"
34 "Car Phone"
35 "Company Main Phone"
36 "Home Fax"
37 "Home Phone"
38 "Home Phone 2"
39 "ISDN"
40 "Mobile Phone"
41 "Other Fax"
42 "Other Phone"
43 "Pager"
44 "Primary Phone"
45 "Radio Phone"
46 "TTY/TDD Phone"
47 "Telex"
48 "Account"
49 "Anniversary"
50 "Assistant's Name"
51 "Billing Information"
52 "Birthday"
53 "Business Address PO Box"
54 "Categories"
55 "Children"
56 "Directory Server"
57 "E-mail Address"
58 "E-mail Type"
59 "E-mail Display Name"
60 "E-mail 2 Address"
61 "E-mail 2 Type"
62 "E-mail 2 Display Name"
63 "E-mail 3 Address"
64 "E-mail 3 Type"
65 "E-mail 3 Display Name"
66 "Gender"
67 "Government ID Number"
68 "Hobby"
69 "Home Address PO Box"
70 "Initials"
71 "Internet Free Busy"
72 "Keywords"
73 "Language"
74 "Location"
75 "Manager's Name"
76 "Mileage"
77 "Notes"
78 "Office Location"
79 "Organizational ID Number"
80 "Other Address PO Box"
81 "Priority"
82 "Private"
83 "Profession"
84 "Referred By"
85 "Sensitivity"
86 "Spouse"
87 "User 1"
88 "User 2"
89 "User 3"
90 "User 4"
91 "Web Page"
|#


