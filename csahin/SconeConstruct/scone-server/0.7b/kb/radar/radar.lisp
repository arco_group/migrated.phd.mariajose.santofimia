;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; 
;;; Radar Project Ontology
;;;
;;; Author: Scone version by Scott E. Fahlman, based in part on version by
;;;         Anthony Tomasic
;;; ***************************************************************************
;;; Copyright (C) 2005-2006, Carnegie Mellon University.

;;; The Scone software and documentation are made available to the
;;; public under the CPL 1.0 Open Source license.  A copy of this
;;; license is distributed with the software.  The license can also be
;;; found at URL <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone
;;; software, you agree to be bound by the terms and conditions set
;;; forth in the CPL 1.0 Open Source License.  If you do not agree to
;;; these terms and conditions, or if they are not legally applicable
;;; in the jurisdiction where such use takes place, then you may not
;;; use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by
;;; Scott E. Fahlman for IBM Coporation between June 2001 and May
;;; 2003.  IBM holds the copyright on NETL2 and has made that software
;;; available to the author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under contract number NBCHD030010.
;;; Any opinions, findings and conclusions or recommendations expressed in
;;; this material are those of the author(s) and do not necessarily reflect
;;; the views of DARPA or the Department of Interior-National Business
;;; Center (DOI-NBC).
;;; ***************************************************************************
;;;

(in-context {general})

;;; For now, just put this in the common namespace.
(in-namespace "common")

;;; ------------------------------------------------------------------------
;;; Piecewise Linear Functions

;;; We need a new primitive object type to handle the piecewise linear
;;; functions that some Radar groups use to represent preferences.

(defstruct (plf)
  "Define a Lisp DEFSTRUCT to hold the PLF (piecewise linear functions) that are used
   in various parts of Radar to represent such things as meeting-time preferences."
  ;; Two parallel vectors of equal length."
  (v1)
  (v2))

(defmacro plf-node? (e)
  "Predicate to test whether E is a PLF-node."
  `(and (struct-node? ,e)
	(typep (internal-name ,e) 'plf)))

(defvar *plf*
  (new-type {plf} {struct}
	    :english '(:iname "piecewise linear function")))

(defun new-plf-node (value &key (parent *plf*))
  "Given a PLF object as the VALUE argument, create and return a new PLF
   node..  If an object that is EQ to this PLF already exists, just return
   it."
  (check-type value plf)
  (new-primitive-node value parent *struct-hashtable*))

(defun new-plf (v1 v2)
  "Given two vectors, V1 and V2, make sure we have fresh, non-shared
   copies.  Create the PLF structure.  Then create and return the PLF
   node."
  (let* ((v1-copy (copy-seq v1))
	 (v2-copy (copy-seq v2))
	 (struct (make-plf :v1 v1-copy :v2 v2-copy)))
    (new-plf-node struct)))

;;; ------------------------------------------------------------------------
;;; Information objects

;;; An information object is some blob of information itself, not the
;;; physical manifestation.

(new-type {information object} {thing})

(new-split '({information object} {physical object}))

;;; Define some (disjoint) types of information object.
(new-split-subtypes {information object}
		    '({text object}
		      {image object}
		      {audio object}
		      {video object}
		      {executable object}
		      {binary data object}))

;;; A file is more about packaging than content.
(new-type {file} {information object})

;;; Define some file types and relate them to information object types.

(new-intersection-type {text file}
		       '({text object} {file}))

(new-intersection-type {image file}
		       '({image object} {file}))

(new-intersection-type {audio file}
		       '({audio object} {file}))

(new-intersection-type {video file}
		       '({video object} {file}))

(new-intersection-type {executable file}
		       '({executable object} {file}))

(new-intersection-type {binary file}
		       '({binary data object} {file}))

;;; A string is a kind of text object.
(new-is-a {string} {text object})

;;; Some subtypes of string.
(new-split-subtypes
 {string}
 '(({url} "web page" "web address")
   ({Email address} "E-mail address" "internet address" "net address")
   ({date-time expression} "timestamp" "date" "time" "time expression")
   ({street address} "mailing address" "physical mail address" "postal address")
   ({phone number} "telephone number")))

;;; A message is a kind of information object.
(new-type {message} {information object})

;;; Some types of message.
(new-intersection-type {voice message}
		       '({audio object} {message}))

(new-intersection-type {video message}
		       '({video object} {message}))

(new-intersection-type {text message}
		       '({text object} {message}))

(new-split-subtypes {text message}
		    '({electronic message}
		      {hardcopy message}))

(new-split-subtypes 
 {electronic message}
 '(({Email message} "E-mail message" "Email" "E-mail")
   ({instant message} "IM" "chat message")))

;;; A message has one sender and any number of recipients.
(new-indv-role {message sender} {message} {person}
	       :english '(:role "sender"))

(new-type-role {message recipient} {message} {person}
	       :english '(:role "recipient"))

(new-type-role {message To recipient} {message}
	       {message recipient})

(new-type-role {message CC recipient} {message}
	       {message recipient})

(new-type-role {message BCC recipient} {message}
	       {message recipient})

;;; An Email message has a set of fields.
(new-type-role {message field} {message} {information object})
(x-is-a-y-of-z {message field} {part} {message})

;;; The fields of an Email message.
(new-type-role {Email message field} {email message} {string})
(x-is-the-y-of-z {Email message field} {message field} {Email message})

(new-indv-role {Email From field} {Email message} {Email address}
	       :english '(:role "From field" "From address"))

(new-type-role {Email To field} {Email message} {Email address}
	       :english '(:role "To field" "To address"))

(new-type-role {Email CC field} {Email message} {Email address}
	       :english '(:role "CC field" "CC address"))

(new-type-role {Email BCC field} {Email message} {Email address}
	       :english '(:role "BCC field" "BCC address"))

(new-type-role {Email Reply-To field} {Email message} {Email address}
	       :english '(:role "Reply-To field" "Reply-To address"
				"Reply field" "Reply address"))

(new-indv-role {Email Subject field} {Email message} {string}
	       :english '(:role "Subject field" "Subject"))

(new-indv-role {Email DATE field} {Email Message}
	       {date-time expression}
	       :english '(:role "timestamp" "date" "send date"))

(new-indv-role {Email Body field} {Email message} {string}
	       :english '(:role "Body" "Body field"))

(new-type-role {Email attachment} {Email message} {file}
	       :english '(:role "attachment" "enclosure"))

;;; ------------------------------------------------------------------------
;;; Aspects of Person

;;; Parts of the person's name.  Doesn't fit all cases/cultures.
(new-indv-role {first name} {person}
	       {string}
	       :english '(:role "given name" "Christian name"))

(new-indv-role {last name} {person}
	       {string}
	       :english '(:role "family name" "surname"))

(new-indv-role {middle name} {person}
	       {string} :may-have t)

;;; This is for stuff like "Sr.", "Jr.", "III", etc.
(new-indv-role {name suffix} {person}
	       {string} :may-have t
	       :english '(:role "suffix"))

(new-indv-role {nickname} {person}
	       {string} :may-have t
	       :english "nickname")

;;; A person may have an employer.
(new-indv-role {employer} {person}
	       {legal entity}
	       :may-have t)

;;; Addresses, home and business.

;;; %%% Add functions to parse an address and relate to geographic
;;; entities.  For now it's just an uninterpreted string.

(new-type-role {person address} {person} {street address}
	       :english '(:role "address" "street address"
				"mailing address" "physical mail address"
				"postal address"))

(new-indv-role {home address} {person} {person address})
(new-indv-role {business address} {person} {person address})

;;; Phone numbers.
(new-type-role {person phone number} {person}
	       {phone number}
	       :english '("phone number" "telephone number"))
(new-indv-role {home phone number} {person}
	       {person phone number})
(new-indv-role {cell phone number} {person}
	       {person phone number})
(new-indv-role {office phone number} {person}
	       {person phone number}
	       :may-have t
	       :english '("business phone number"))
(new-indv-role {lab phone number} {person}
	       {person phone number}
	       :may-have t)
(new-indv-role {fax phone number} {person}
	       {person phone number} :may-have t)
(new-indv-role {primary phone number} {person}
	       {person phone number})

;;; Email address
(new-indv-role {person email address} {person}
	       {email address}
	       :english '(:role "Email address" "E-mail address"
				"Email" "E-mail"))

;;; %%% What is this?  The person's PGP key?
(new-indv-role {cryptmail} {person}
	       {string} :may-have t)

;;; Personal web page.
(new-indv-role {personal web page} {person}
	       {url}
	       :english '(:role "url" "web page" "personal web page"))

;;; Bio is some kind of text object, proibably a file or string.
(new-indv-role {biographical note} {person}
	       {text object}  :may-have t
	       :english '(:role "bio" "biography" "biographical note"))

;;; A person may have a picture.
(new-indv-role {person picture} {person}
	       {image file}  :may-have t
	       :english '(:role "picture" "photo" "mug shot"))

;;; Contact information is a collection of other roles.
(new-type-role {contact information} {person}
	       {string})

(new-is-a {person phone number} {contact information})
(new-is-a {person email address} {contact information})
(new-is-a {personal web page} {contact information})
(new-is-a {person address} {contact information})

;;; In the Radar world, some people are VIPs.
(new-type {vip} {person})
(new-type {conference vip} {vip})
(new-type {university vip} {vip})

;;; ------------------------------------------------------------------------
;;; Inter-Personal Relations

(new-relation {friend of}
	      :a-inst-of {person}
	      :b-inst-of {person}
	      :symmetric t)

(new-relation {co-worker of}
	      :a-inst-of {person}
	      :b-inst-of {person}
	      :symmetric t)

(new-relation {family member of}
	      :a-inst-of {person}
	      :b-inst-of {person}
	      :symmetric t)

;;; This may be either formal or informal status.
(new-relation {higher status than}
	      :a-inst-of {person}
	      :b-inst-of {person}
	      :transitive t
	      :english '(:relation
			 :inverse-relation "lower status than"))

;;; This is meant to be a formal ranking according to the rules of some
;;; hierarchical organization, such as the military or a university.  It
;;; implies higher status.

(new-relation {higher rank than}
	      :a-inst-of {person}
	      :b-inst-of {person}
	      :transitive t)

(new-statement (find-wire :a {higher rank than})
	       {higher status than}
	       (find-wire :b {higher rank than}))

(new-relation {reports to}
	      :a-inst-of {person}
	      :b-inst-of {person})

(new-statement (find-wire :b {reports to})
	       {higher status than}
	       (find-wire :a {reports to}))

;;; ------------------------------------------------------------------------
;;; Subtypes of Person

;;; Some people use (and are known to) Radar, and some don't.
(new-complete-split-subtypes
 {person}
 '(({radar user} "Radar person")
   ({not radar user} "non-Radar-user" "non-Radar person")))

;;; Radar may need to represent which users are real and which are simulated,
;;; though perhaps Radar shouldn't know this.

;;; %%% Anthony's ontology includes "dead" as an option here.  What's that
;;; about?  If he means "dead" in the usual sense, that's a split with
;;; "living" not with "real"/"simulated".

(new-split-subtypes {person}
		    '({real person} {simulated person}))

;;; Some types of people you find around a university.

(new-type {university person} {person}
	  :english '(:adj-noun "academic"))

(new-split-subtypes
 {university person}
 '(({faculty member} :noun :adj "faculty")
   ({post-doc} "post-doctoral fellow" "post-doctoral research associate")
   {visitor} 
   {student}
   ({staff member} :noun :adj "staff")))

;;; This type may overlap with faculty or staff.
(new-type {university administrator} {university person})

;;; Types of staff.
(new-split-subtypes {staff member}
		    '({technical staff member}
		      {administrative staff member}))

(new-split-subtypes 
 {technical staff member}
 '(({staff programmer} "research programmer" "programmer")
   {technical writer}
   {data wrangler}
   {user studies assistant}))

(new-split-subtypes
 {administrative staff member}
 '({departmental administrator}
   ({secretary} "executive assistant" "assistant")
   {student coordinator}
   {business manager}))

(new-split-subtypes
 {student}
 '(({undergraduate student}  :adj-noun "undergradaute" "undergrad")
   ({graduate student} "grad student")
   {special student}))

;;; Hierarchy of professitude.

(new-split-subtypes
 {faculty member}
 '(({professor-rank} :adj)
   ({associate-professor-rank} :adj)
   ({assistant-professor-rank} :adj)))

(new-statement {professor-rank}
	       {higher rank than}
	       {associate-professor-rank})

(new-statement {associate-professor-rank}
	       {higher rank than}
	       {assistant-professor-rank})

(new-statement {assistant-professor-rank}
	       {higher rank than}
	       {post-doc})

(new-statement {post-doc}
	       {higher rank than}
	       {graduate student})

(new-statement {graduate student}
	       {higher rank than}
	       {undergraduate student})

;;; NOTE:  Some of the below is CMU-specific.  Leave it that way for now.

(new-split-subtypes {faculty member}
		    '({tenure-track faculty member}
		      {research faculty member}
		      {teaching faculty member}
		      {systems faculty member}))

(english {tenure-track faculty member}
	 :adj "tenure-track")
(english {research faculty member}
	 :adj "research" "research-track" :noun "research scientist")
(english {teaching faculty member}
	 :adj "teaching" "teaching-track" :noun "lecturer" )
(english {systems faculty member}
	 :adj "systems" "systems-track" :noun "systems scientist")

(new-intersection-type
 {professor}
 '({tenure-track faculty member} {professor-rank})
 :english '("full professor"))

(new-intersection-type
 {associate professor}
 '({tenure-track faculty member} {associate-professor-rank}))

(new-intersection-type
 {assistant professor}
 '({tenure-track faculty member} {assistant-professor-rank}))

(new-intersection-type
 {research professor}
 '({research faculty member} {professor-rank})
 :english '("resarch full professor" "full research professor"))

(new-intersection-type
 {research associate professor}
 '({research faculty member} {associate-professor-rank})
 :english '("associate research professor"))

(new-intersection-type
 {research scientist}
 '({research faculty member} {assistant-professor-rank}))

(new-intersection-type
 {teaching professor}
 '({teaching faculty member} {professor-rank})
 :english '("teaching full professor" "full teaching professor"))

(new-intersection-type
 {teaching associate professor}
 '({teaching faculty member} {associate-professor-rank})
 :english '("associate teaching professor"))

(new-intersection-type
 {lecturer}
 '({teaching faculty member} {assistant-professor-rank}))

(new-intersection-type
 {principal systems scientist}
 '({systems faculty member} {professor-rank}))

(new-intersection-type
 {senior systems scientist}
 '({systems faculty member} {associate-professor-rank}))

(new-intersection-type
 {systems scientist}
 '({systems faculty member} {assistant-professor-rank}))


;;; ------------------------------------------------------------------------
;;; Aspects of Radar User

(new-indv-role {radar user login id} {radar user}
	       {string}
	       :english '(:role "login id" "user id"
				"RADAR login id" "RADAR user id"))

(new-indv-role {radar user password} {radar user}
	       {string}
	       :english '(:role "password" "RADAR password"))

(new-split-subtypes {radar user}
		    '({external radar user}
		      {internal radar user}
		      {DARPA radar user}))

(new-type {evaluation radar user} {internal radar user})

(english {external radar user} :adj "external")
(english {internal radar user} :adj "internal")
(english {external radar user} :adj "DARPA")
(english {external radar user} :adj "evaluation" :noun "evaluator")


;;; ------------------------------------------------------------------------
;;; Gathering/Event

;;; A gathering is any planned event that involves multiple people.
(new-type {gathering} {event})

;;; A gathering has some participants.
(new-type-role {gathering participants} {gathering}
	       {person})

(new-indv-role {person in charge} {gathering}
	       {person}
	       :english '(:role "host"))

;;; Normally we only know the attendance after the fact.
(new-indv-role {attendance} {gathering}
	       {integer})

;;; Current estimate of attendance.
(new-indv-role {estimated attendance} {gathering}
	       {integer}
	       :may-have t)

;;; What equipment do we want/need?
(new-type-role {requested equipment} {gathering}
	       {physical object}
	       :may-have t)

;;; What equipment is committed?
(new-type-role {committed equipment} {gathering}
	       {physical object}
	       :may-have t)

;;; For the purpose of Radar STP, the preferred time is a Piecewise Linear
;;; Function.  This is too restrictive for the real world.
(new-indv-role {preferred time} {gathering}
	       {plf})

(new-split-subtypes {gathering}
		    '({open gathering}
		      {group gathering}
		      {invitation-only gathering}))


;;; A meeting is a specific kind of gathering.
(new-type {meeting} {gathering})

(new-split-subtypes {meeting}
		    '({one-time meeting} {recurring meeting}))

(new-split-subtypes {recurring meeting}
		    '({daily meeting}
		      {weekly meeting}
		      {monthly meeting}
		      {annual meeting}
		      {occasional meeting}))


(new-type {conference planning meeting}
	  {invitation-only gathering})

(new-split-subtypes {conference planning meeting}
		    '({program committee meeting}
		      {local arangements meeting}))

;;; University classes.

(new-type {university class} {meeting}
	  :english '("class"))

(get-the-x-role-of-y {person in charge} {university class}
		     :iname {class teacher}
		     :english '(:role "teacher" "instructor"
				      "lecturer"))

(new-indv-role {university class university}
	       {university class}
	       {university}
	       :english '(:role "university"))

(new-type-role {university class student}
	       {university class}
	       {student}
	       :english '(:role "student"))

(new-split-subtypes {university class}
		    '({graduate class} {undergraduate class}))

(new-is-a
 (get-the-x-role-of-y {university class student}
		      {graduate class})
 {graduate student})

(new-is-a
 (get-the-x-role-of-y {university class student}
		      {undergraduate class})
 {undergraduate student})

;;; Conference Events

(new-type {conference} {gathering})

(new-type-role {conference event} {conference}
	       {gathering})

(x-is-a-y-of-z {conference event}
	       {part}
	       {conference})	       

(new-indv-role {conference event title} {conference event} {string}
	       :may-have t)

(new-indv-role {conference event abstract} {conference event} {string}
	       :may-have t)

;;; Some types of conference event.
(new-split-subtypes {conference event}
		    '({technical presentation} {workshop} {tutorial}
		      {poster session}
		      {administrative} {exhibit} {party}
		      {panel} {conference break} {conference meal}
		      {conference social event} 
		      {conference meeting}))



(new-split-subtypes {technical presentation}
		    '({technical paper session}
		      {demo}
		      {invited talk}
		      {keynote talk}))

(new-type {plenary} {technical presentation})

(new-indv-role {speaker} {technical presentation} {person})

(new-split-subtypes {conference break}
		    '({catered break}
		      {non-catered break}))

(new-split-subtypes {conference meal}
		    '({conference breakfast}
		      {conference lunch}
		      {conference dinner}
		      {conference banquet}))

(new-split-subtypes {conference meal}
		    '({buffet meal}
		      {served meal }
		      {boxed meal}))

;; //??? -BEL
(new-is-a {conference event} {compound event})


;;; Some relations relating to events.

;;; State that two events should not conflict.  The C argument is a
;;; numeric strength in the range 0-1.

(new-relation {should not conflict with}
	      :a-inst-of {event}
	      :b-inst-of {event}
	      :c-inst-of {number})

;;; State that there is overlap among the would-be attendees of two
;;; gatherings.  The C argument is a floating-point number in the range
;;; 0-1.  This may be used to deduce "should not conflict with".

(new-relation {gathering participants overlap}
	      :a-inst-of {gathering}
	      :b-inst-of {gathering}
	      :c-inst-of {number})


;;; Non-Gathering Events

;;; Note: Mike Pool's ontology lists the service actions as resources, but
;;; I think that's confusing.

(new-type {service event} {event})
(new-type {service action} {action})
(new-is-a {service action} {service event})

(new-split-subtypes
 {service action}
 '(({clean room} :action)
   ({prepare document} :action)
   ({service equipment} :action)
   ({set up equipment} :action)
   ({prepare food} :action)
   ({set up food service} :action)
   ({set up room} :action)
   ({deliver goods} :action)
   ({register conference attendees} :action)
   ({prepare budget} :action)
   ({disseminate information} :action)
   ({plan event} :action)
   ({revise budget} :action)
   ({revise event plan} :action)))

(new-type {communication event} {event})
(new-type {communication action} {action})
(new-is-a {communication action} {communication event})

(new-split-subtypes {communication event}
		    '({Email exchange}
		      {face-to-face conversation}
		      {instant messaging exchange}
		      {postal communication}
		      {telephone conversation}))

;;; ------------------------------------------------------------------------
;;; Event Equipment

;;; A physical object needed for an event to be successful.
(new-type {event equipment} {physical object}
	  :english '("equipment" "physical resource"))

;;; Use a piecewise linear function to represent equipment availability as
;;; a function of time.
(new-indv-role {equipment available time} {event equipment}
	      {plf})

;;; Distinguish betweeen portable equipment and equipment in fixed location,
;;; such as a projector mounted in a room.  Usually we don't consider moving 
;;; fixed equipment.

(new-complete-split-subtypes
 {event equipment}
 '(({portable equipment} :adj "portable" "movable")
   ({fixed equipment} :adj "fixed" "immovable" "built-in")))

;;; Distinguish between equipment we own or can get for free and equipment
;;; we must pay for.

(new-complete-split-subtypes
 {event equipment}
 '(({no-cost equipment} :noun :adj "no-cost" "free" "included")
   ({rental equipment} :noun
    :adj "rental" "charged-for" "leased" "rented")))

;;; Rental equipment has a price.  For now, assume that this is just a
;;; price per hour, though we may have to put in a more complicated scheme
;;; later.
(new-indv-role {equipment price per hour} {rental equipment}
	       {currency measure})

;;; Assume that fixed equipment is no-cost.
(new-is-a {fixed equipment} {no-cost equipment})

;;; Some types of equipment
(new-split-subtypes {event equipment}
		    '({PA system}
		      {microphone}
		      {projector}
		      {computer}
		      {telephone set}
		      {furniture}))

(new-split-subtypes {microphone}
		    '({wireless microphone}
		      {wired microphone}))

(new-split-subtypes {projector}
		    '({slide projector}
		      {transparency projector}
		      {computer projector}))

(english {slide projector} "35mm projector" "35mm slide projector")
(english {transparency projector} "overhead projector" "overhead")
(english {computer projector} "data projector" "digital projector")

(new-split-subtypes {computer}
		    '({desktop computer}
		      {laptop computer}))

(english {desktop computer} "workstation")
(english {laptop computer} "notebook computer" "notebook")

;;; Add some properties of computer.

(new-split-subtypes {computer}
		    '({windows computer}
		      {linux computer}
		      {macintosh computer}))

(new-indv-role {main memory size} {computer}
	       {information measure}
	       :english '(:role "memory size" "memory capcity"))

(new-indv-role {hard disk size} {computer}
	       {information measure}
	       :english '(:role "disk size" "disk capacity"))

(new-indv-role {CPU speed} {computer}
	       {frequency measure}
	       :english '(:role "speed" "clock speed"))

;;; Furniture

(new-split-subtypes {furniture}
		    '({chair}
		      {table}
		      {podium}))

;;; Most furniture is movable, though with some difficulty.
(new-is-a {furniture} {portable equipment})

;;; Some types of chair you might find around a conference.
(new-split-subtypes {chair}
		    '({folding chair}
		      {stacking chair}
		      {office chair}
		      {soft chair}
		      {chair-desk}
		      {built-in seat}))

;;; Some types of table you might find around a conference.
(new-split-subtypes {table}
		    '({conference table}
		      {classroom folding table}
		      {serving table}
		      {built-in table}))


;;; You can't move the built-in stuff.
(new-is-not-a {built-in seat} {portable equipment})
(new-is-a {built-in seat} {fixed equipment})
(new-is-not-a {built-in table} {portable equipment})
(new-is-a {built-in table} {fixed equipment})

;;; ------------------------------------------------------------------------
;;; Locations

;;; Define building complex.
(new-type {building complex} {place})

;;; Some types of building complex.
(new-split-subtypes {building complex}
		    '({university campus}
		      {resort}
		      {office park}
		      {commercial development}))

;;; Define building.
(new-type {building} {place})

;;; Some types of building.
(new-split-subtypes {building}
		    '({hotel}
		      {university academic building}
		      {university mixed-use building}
		      {university dormitory}
		      {office building}
		      {store}
		      {commercial building}))

;;; Floor in a building.
(new-type {floor} {place})
(new-indv-role {floor building} {floor} {building})

;;; Floors have a numerical index, an integer that may be negative.
;;; The U.S. convention is that the ground floor is 1, so basements are
;;; 0, -1 etc.  At CMU, "ground floor" is loosely defined.
(new-indv-role {floor index} {floor}
	       {integer})


;;; Define room.  In Radar, much of our focus is on rooms.
(new-type {room} {place})

;;; Aspects of room.

(new-indv-role {room building complex} {room}
	       {building complex}
	       :may-have t
	       :english '("building complex" "complex"))

(new-indv-role {room building} {room}
	       {building}
	       :english '("building"))

(new-indv-role {room floor} {room}
	       {floor}
	       :english '("floor"))

;;; %%% After we fix role inheritance, put part-of relations on the above.
;;; %%% Right now, it wouldn't work correctly.

;;; Room dimensions.
;;; Eugene's convention is that length is the larger of the two dimensions.

(new-indv-role {room length} {room}
	       {length measure}
	       :english "length")

(new-indv-role {room width} {room}
	       {length measure}
	       :english "width")

(new-indv-role {room ceiling height} {room}
	       {length measure}
	       :english '("height" "ceiling height"))

;;; The area may not be the product of length and width if the room is not
;;; rectangular.
(new-indv-role {room area} {room}
	       {area measure}
	       :english "area")

(new-indv-role {room window area} {room}
	       {area measure}
	       :english "window area")

;;; Room capacities by use/configuration.
;;; A capacity of 0 means that the room is unsuitable for that use.

(new-indv-role {room auditorium capacity} {room}
	       {integer}
	       :english "auditorium capacity")

(new-indv-role {room classroom capacity} {room}
	       {integer}
	       :english "classroom capacity")

(new-indv-role {room conference capacity} {room}
	       {integer}
	       :english "conference capacity")

(new-indv-role {room U-shape capacity} {room}
	       {integer}
	       :english "U-shape capacity")

(new-indv-role {room square capacity} {room}
	       {integer}
	       :english "square capacity")

(new-indv-role {room reception capacity} {room}
	       {integer}
	       :english "reception capacity")

(new-indv-role {room banquet capacity} {room}
	       {integer}
	       :english "banquet capacity")

(new-indv-role {room cafe capacity} {room}
	       {integer}
	       :english "cafe capacity")

(new-indv-role {room poster capacity} {room}
	       {integer}
	       :english "poster capacity")

(new-indv-role {room demo capacity} {room}
	       {integer}
	       :english "demo capacity")


;;; Use a PLF to show room availability as a function of time.
;;; The values might all be +1 or -1.
(new-indv-role {room availability} {room}
	       {plf}
	       :english '(:role "availability"))

;;; Some rooms have wireless internet.
(new-type {room with wireless internet} {room})

;;; Some rooms have a built-in projector.
(new-type {room with projector} {room})

;;; Various outlets in a room.

(new-type-role {room electrical outlets} {room}
	       {integer}
	       :english '("electrical outlets" "power outlets"
			  "AC outlets"))

(new-indv-role {room electrical capacity} {room}
	       {power measure}
	       :english '(:role "electrical capacity"
				"power capacity" "outlet capacity"
				"available power" "available watts"))

(new-type-role {room ethernet outlets} {room}
	       {integer}
	       :english '("ethernet outlets"
			  "internet outlets" "network outlets"
			  "wired network outlets"))

(new-type-role {room telephone outlets} {room}
	       {integer}
	       :english '("telephone outlets" "phone outlets"))

;;; Assume we want to express room desirability on a numerical scale.
(new-indv-role {room desirability} {room}
	       {number})

(new-split-subtypes {room}
		    '({meeting room}
		      {classroom}
		      {auditorium}
		      {office}
		      {lab}
		      {rest room}
		      {kitchen}
		      {dining room}
		      {multi-purpose room}
		      {closet}
		      {storage room}
		      {coat room}
		      {reception area}
		      {lounge}))

(english {rest room} "restroom" "toilet" "wash room" "lavatory" "WC")

(new-split-subtypes {rest room}
		    '({men's rest room}
		      {women's rest room}
		      {unisex rest room}))

(english {men's rest room} :adj "men's")
(english {men's rest room} :adj "women's" "ladies" "ladies'")

(new-type-role {toilet} {rest room}
	       {man-made object})

(new-type-role {urinal} {men's rest room}
	       {man-made object})

(new-type-role {sink} {rest room}
	       {man-made object})

(new-type-role {shower} {rest room}
	       {man-made object}
	       :may-have t)
	       

(new-type {handicapped rest room} {rest room}
	  :english '(:noun :adj "handicapped" "accessible"))

(new-type {air-conditioned room} {room}
	  :english '(:noun :adj "air-conditioned"
			    "climate controlled"))

;;; ------------------------------------------------------------------------
;;; Tasks

(new-type {task} {action})

(new-type-role {subtask} {task} {task})

(x-is-a-y-of-z {subtask} {part} {task})

;;; ---------------------------------------------------------------------------
;;; Convenience functions to create new instance-level knowledge in Radar.

;;; When we import instance descriptions from SQL databases, we may want to
;;; remember the numerical index.

(new-indv-role {person index} {person} {integer})
(new-indv-role {room index} {room} {integer})
(new-indv-role {gathering index} {conference event} {integer})
(new-indv-role {presentation index} {technical presentation} {integer})

;;; Some odd weight attribute in the radar SQL files.
(new-indv-role {radar weight} {person} {integer})


;;; Function for creating a person for Radar.

;;; Somewhat kludgy function for parsing a full name.

(defvar *suffixes* '("Jr." "Sr." "Junior" "Senior" "Jr" "Sr"
		     "I" "II" "III" "IV"))

(defun parse-full-name-suffix (name)
  "Returns two values: the suffix string (or nil) and the residual name
   string with suffix removed."
  (let* ((p (and (typep name 'string)
		 ;; Find the last space in the name.
		 (position #\  name :from-end t)))
	 (suffix nil)
	 (residue nil))
    (when p
      (setq suffix (subseq name (+ p 1)))
      (setq residue 
	    (string-right-trim '(#\, #\  )
			       (subseq name 0 p))))
    (if (member suffix *suffixes* :test #'string= )
      (values suffix residue)
      (values nil name))))

(defun parse-full-name (name)
  "Parse a full name according to traditional English conventions,
   returning five values: success, first, middle (could be nil), last,
   suffix (could be nil).  If the parse is believed to be successful,
   success will be T, else NIL."
  ;; Remove any leading or trailing spaces.
  (setq name (string-trim '(#\ ) name))
  ;; Remove any known suffix.
  (multiple-value-bind (suffix residue)
		       (parse-full-name-suffix name)
    ;; Find the first and last spaces (might be the same).
    (let ((p1 (position #\  residue))
	  (p2 (position #\  residue :from-end t))
	  (first nil)
	  (middle nil)
	  (last nil))
      ;; If no space, return nil.  Else, pick off first and last names.
      (when p1 
	(setq first (subseq residue 0 p1))
	(setq last (subseq residue (+ p2 1)))
	;; If there are at least two spaces, the stuff between the first
	;; and last is the middle name.
	(unless (= p1 p2)
	  (setq middle (subseq residue (+ p1 1) p2)))
	(values t first middle last suffix)))))

(defmacro save-role (role-name owner)
  `(when ,role-name
     (x-is-the-y-of-z
      ,role-name
      (lookup-element-test ,(substitute #\  #\- (symbol-name role-name)))
      ,owner)))

(defmacro save-string-role (role-name owner)
  `(when ,role-name
     (x-is-the-y-of-z
      (new-string ,role-name)
      (lookup-element-test ,(substitute #\  #\- (symbol-name role-name)))
      ,owner)))

(defun new-radar-person
       (full-name	       ; The person's full name (string).
	&key
	(generate-names t)     ; If explicitly NIL, don't try to derive names.
	types		       ; Supertype (or list of them) for this person.
	person-index	       ; Integer index of this person's DB entry.
	radar-weight           ; An integer.
	first-name	       ; Person's first name.
	middle-name	       ; Person's middle name or initial.
	last-name	       ; Person's last name.
	name-suffix	       ; Name suffix: "Jr." "III", etc.
	nickname	       ; Primary nickname (string).
	other-names	       ; List of alternative names.
	home-address	       ; Home address (string).
	business-address       ; Business address (string).
	primary-phone-number   ; Preferred phone number.
	home-phone-number      ; Home phone number. (string)
	office-phone-number    ; Office phone number. (string)
	cell-phone-number      ; Cell phone number (string).
	lab-phone-number       ; Lab phone number (string).
	fax-phone-number       ; Fax phone number (string).
	email-address	       ; Email address (string).
	web-page	       ; Web page (URL string).
	vip		       ; A value of T says that this person is a VIP.
	gender)		       ; "male" or "female"
  "Define a new person for the Radar KB.  The required FULL-NAME arg a
   string which becomes the internal name for this indv node.  :TYPES is a
   parent class, such as {professor}, or a list of parent classes.  The other
   args are as shown.
   
   The parts of a person's name can usually be extracted from the
   FULL-NAME string if your name follows the usual English rules.  However,
   you can provide these components if you think that the software may get
   this wrong.  The :OTHER-NAMES argument is a list of alternative names for
   this person.  The system will attempt to produce some alternative names
   using parts of the FULL-NAME string, unless the user specifically says
   :GENERATE-NAMES NIL."
  (check-type full-name string)
  (let* ((iname (make-element-iname :string full-name))
	 (person (or (lookup-element-pred iname)
		     (new-indv iname {radar user}))))
    ;;; Set up all the supertypes the user provided.
    (unless (typep types 'list) (setq types (list types)))
    (dolist (type types)
      (new-is-a person type))
    (when nickname
      (x-is-the-y-of-z (new-string nickname)
		       {nickname}
		       person)
      (push nickname other-names))
    ;;; If the user does not supply components of the name, try to
    ;;; extract them.
    (multiple-value-bind (ok first middle last suffix)
			 (parse-full-name full-name)
      (when ok
	(unless first-name (setq first-name first))
	(unless middle-name (setq middle-name middle))
	(unless last-name (setq last-name last))
	(unless name-suffix (setq name-suffix suffix))))
    (when generate-names
      (when last-name (push last-name other-names))
      (when first-name (push first-name other-names))
      (when (and first-name last-name)
	(push (format nil "~A ~A" first-name last-name) other-names))
      (when (and nickname last-name)
	(push (format nil "~A ~A" nickname last-name) other-names)))
    (apply #'english (cons person other-names))
    (when person-index
      (x-is-the-y-of-z person-index {person index} person))
    (when radar-weight
      (x-is-the-y-of-z radar-weight {radar weight} person))
    ;; Now save the name components and other roles.
    (save-string-role first-name person)
    (save-string-role middle-name person)
    (save-string-role last-name person)
    (save-string-role name-suffix person)
;    (save-role employer person)
    (save-string-role home-address person)
    (save-string-role business-address person)
    (save-string-role primary-phone-number person)
    (save-string-role home-phone-number person)
    (save-string-role office-phone-number person)
    (save-string-role cell-phone-number person)
    (save-string-role lab-phone-number person)
    (save-string-role fax-phone-number person)
    (when web-page
      (x-is-the-y-of-z (new-string web-page)
		       {personal web page}
		       person))
    (when email-address
      (x-is-the-y-of-z (new-string email-address)
		       {person email address}
		       person))
    (when vip (new-is-a person {vip}))
    (when (and gender
	       (setq gender
		     (lookup-element-predicate gender))
	       (or (eq gender (lookup-element {male}))
		   (eq gender (lookup-element {female}))))
      (new-is-a person gender))
    person))

(defun new-radar-room   
       (name		       ; Name of the room (string).
	&key
	other-names	       ; List of alterantive names.
	types		       ; Supertype (or list of them) for this room.
	room-index	       ; Integer index of this room's DB entry.
	building-complex       ; Building complex containing this room.
	building	       ; Building containing this room.
	floor		       ; Must be of type {floor}.
	length		       ; Length measure
	width		       ; Length measure.
	ceiling-height	       ; Length measure.
	area		       ; Area measure.
	window-area	       ; Area measure.
	auditorium-capacity    ; Integer.
	classroom-capacity     ; Integer.
	conference-capacity    ; Integer.
	U-shape-capacity       ; Integer.
	square-capacity	       ; Integer.
	reception-capacity     ; Integer.
	banquet-capacity       ; Integer.
	cafe-capacity	       ; Integer.
	poster-capacity	       ; Integer.
	demo-capacity	       ; Integer.
	room-availability      ; PLF.
	electrical-outlets     ; Integer.
	electrical-capacity    ; Power measure.
	ethernet-outlets       ; Integer.
	telephone-outlets      ; Integer.
	desirability	       ; Number.
	has-wireless	       ; If T, room has wireless internet.
	has-projector)	       ; If T, room has a built-in projector.
  "Define a new room for the Radar KB.  The required NAME arg is a string
   which becomes the internal name for this indv node.  :TYPES is a parent
   class, such as {auditorium}, or list of parent classes."
  (check-type name string)
  (let* ((iname (make-element-iname :string name))
	 (room (or (lookup-element-pred iname)
		   (new-indv iname {room}))))
    ;; Set up all the supertypes the user provided.
    (dolist (type types)
      (new-is-a room type))
    (apply #'english (cons room other-names))
    ;; Now save the various roles.
    (when room-index
      (x-is-the-y-of-z room-index {room index} room))
    (when building-complex
      (x-is-the-y-of-z building-complex {room building complex} room))
    (when building
      (x-is-the-y-of-z building {room building} room))
    ;; %%% Should find/create a floor if we get an integer here.  
    (when floor
      (x-is-the-y-of-z floor {room floor} room))
    (when length
      (x-is-the-y-of-z length {room length} room))
    (when width
      (x-is-the-y-of-z width {room width} room))
    (when ceiling-height
      (x-is-the-y-of-z ceiling-height {room ceiling height} room))
    (when area
      (x-is-the-y-of-z area {room area} room))
    (when window-area
      (x-is-the-y-of-z window-area {room window area} room))
    (when auditorium-capacity
      (x-is-the-y-of-z auditorium-capacity {room auditorium capacity} room))
    (when classroom-capacity
      (x-is-the-y-of-z classroom-capacity {room classroom capacity} room))
    (when conference-capacity
      (x-is-the-y-of-z conference-capacity {room conference capacity} room))
    (when U-shape-capacity
      (x-is-the-y-of-z U-shape-capacity {room U-shape capacity} room))
    (when square-capacity
      (x-is-the-y-of-z square-capacity {room square capacity} room))
    (when reception-capacity
      (x-is-the-y-of-z reception-capacity {room reception capacity} room))
    (when banquet-capacity
      (x-is-the-y-of-z banquet-capacity {room banquet capacity} room))
    (when cafe-capacity
      (x-is-the-y-of-z cafe-capacity {room cafe capacity} room))
    (when poster-capacity
      (x-is-the-y-of-z poster-capacity {room poster capacity} room))
    (when demo-capacity
      (x-is-the-y-of-z demo-capacity {room demo capacity} room))
    (when room-availability
      (x-is-the-y-of-z room-availability {room availability} room))
    (when electrical-outlets
      (x-is-the-y-of-z electrical-outlets {room electrical outlets} room))
    (when electrical-capacity
      (x-is-the-y-of-z electrical-capacity {room electrical capacity} room))
    (when ethernet-outlets
      (x-is-the-y-of-z ethernet-outlets {room ethernet outlets} room))
    (when telephone-outlets
      (x-is-the-y-of-z telephone-outlets {room telephone outlets} room))
    (when desirability
      (x-is-the-y-of-z desirability {room desirability} room))
    (when has-wireless
      (new-is-a room {room with wireless internet}))
    (when has-projector
      (new-is-a room {room with projector}))
    room))

(defun new-radar-gathering   
       (name		       ; Name of the gathering (string).
	&key
	other-names	       ; List of alterantive names.
	types		       ; Supertype (or list of them) for this gathering.
	gathering-index	       ; Integer index of this gathering's DB entry.
	title		       ; Title of the gathering (string).
	person-in-charge       ; An existing person node.
	location)	       ; An existing location node.
  "Define a new gathering for the Radar KB.  The required NAME arg is a string
   which becomes the internal name for this indv node.  :TYPES is a parent
   class, such as {workshop}, or a list of supertypes."
  (check-type name string)
  (let* ((iname (make-element-iname :string name))
	 (gathering (or (lookup-element-pred iname)
			(new-indv iname {conference event}))))
    ;; Set up all the supertypes the user provided.
    (dolist (type types)
      (new-is-a gathering type))
    (apply #'english (cons gathering other-names))
    ;; Now save the various roles.
    (when gathering-index
      (x-is-the-y-of-z gathering-index {gathering index} gathering))
    (when title
      (x-is-the-y-of-z (new-string title)
		       {conference event title} gathering))
    (when person-in-charge
      (x-is-the-y-of-z person-in-charge {person in charge} gathering))
    (when location
      ;(x-is-the-y-of-z location {location of event} gathering))
      (x-is-the-y-of-z location {location} gathering))
    gathering))

(defun new-radar-presentation   
       (name		       ; Name of the presentation (string).
	&key
	other-names	       ; List of alterantive names.
	types		       ; Supertype (or list) for this presentation.
	presentation-index     ; Integer index of this presentation's DB entry.
	subevent-of	       ; Another event that contains this one.
	speaker		       ; An existing person node.
	abstract)	       ; Presentation abstract (string).
  "Define a new presentation for the Radar KB.  The required NAME arg is a
   string which becomes the internal name for this indv node."
  (check-type name string)
  (let* ((iname (make-element-iname :string name))
	 (presentation (or (lookup-element-pred iname)
			   (new-indv iname {technical presentation}))))
    ;; Set up all the supertypes the user provided.
    (dolist (type types)
      (new-is-a presentation type))
    (apply #'english (cons presentation other-names))
    ;; Now save the various roles.
    (when presentation-index
      (x-is-the-y-of-z presentation-index {presentation index} presentation))
    (when subevent-of
      (x-is-a-y-of-z presentation {subevent} subevent-of))
    (when speaker
      (x-is-the-y-of-z speaker {speaker} presentation))
    (when abstract
      (x-is-the-y-of-z (new-string abstract)
		       {conference event abstract} presentation))
    presentation))


