;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; 
;;; This file contains fixes to the Scone-RADAR dictionary, found
;;; after wargaming Fall 2005
;;;
;;; Author: Alicia Tribble
;;; ***************************************************************************
;;; Copyright (C) 2003, Carnegie Mellon University.

;;; The Scone software and documentation are made available to the
;;; public under the CPL 1.0 Open Source license.  A copy of this
;;; license is distributed with the software.  The license can also be
;;; found at URL <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone
;;; software, you agree to be bound by the terms and conditions set
;;; forth in the CPL 1.0 Open Source License.  If you do not agree to
;;; these terms and conditions, or if they are not legally applicable
;;; in the jurisdiction where such use takes place, then you may not
;;; use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by
;;; Scott E. Fahlman for IBM Coporation between June 2001 and May
;;; 2003.  IBM holds the copyright on NETL2 and has made that software
;;; available to the author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under Contract No. NBCHC030029 and
;;; NBCHD030010.  Any opinions, findings and conclusions or recommendations
;;; expressed in this material are those of the author(s) and do not
;;; necessarily reflect the views of DARPA or the Department of
;;; Interior-National Business Center (DOI-NBC).
;;; ***************************************************************************
;;;


;;; Remove EN tags for false hits
;;
(unregister-definition "HOPE"	 {Hope Ezzard} )
(unregister-definition "MINOR"   {Patricia Minor})
(unregister-definition "FALL"	 {Tina Fall})
(unregister-definition "CAR"	  {Brian Car})
(unregister-definition "FREE"	  {no-cost equipment})
(unregister-definition "INTERNAL"	{internal radar user})
(unregister-definition "EXTERNAL" 	{external radar user})
(unregister-definition "SYSTEMS"	{systems faculty member})
(unregister-definition "RESEARCH"	{research faculty member})
(unregister-definition "TITLE"	        {person title})
(unregister-definition "WE"		{Wednesday})
(unregister-definition "BIT"	        {bit})
(unregister-definition "US"	        {microsecond})
(unregister-definition "MS"		{millisecond})
(unregister-definition "SQ IN"		{square inch})
(unregister-definition "IN"		{inch})
(unregister-definition "PART"		{part})
(unregister-definition "SET"		{set})
(unregister-definition "REAL"		{floating-point number})
(unregister-definition "THING"		{thing})
(unregister-definition "HAS"		{has link})

;; Add EN for some missing morph. variants
(english {office} "offices")
(english {projector} "projectors")
(english {conference social event} "social event")
(english {nickname} "aka")
(english {nickname} "aka title")
(english {conference lunch} "lunch")

;; Add some missing elements
(new-indv {telephone} {furniture}
          :english '("telephone" "telephones" "phones"))
(new-indv {web site} {information object}
          :english '("website" "web site"))
