;;; Generated automatically from files ardra/people.www.sql ardra/locations.www.sql ardra/eventtypes.www.sql ardra/events.www.sql ardra/presentations.www.sql
;;; on Fri Jul 15 19:39:40 2005
;;;

;;; Hand-Edited Elements for Bootstrapping ;;;
(new-indv {UC} {building})
(new-indv {Flagstaff} {building})
(new-indv {Stever} {building})
(new-indv {Mehrabian} {building})


;;; Generated automatically from file ardra/people.www.sql
(new-radar-person "Jonathon Robertson"
	:first-name "Jonathon"
	:radar-weight 100
	:last-name "Robertson"
	:person-index 1
	:primary-phone-number "412 281 1346"
	:email-address "jrobertson@ardra.org"
	:vip t
)

(new-radar-person "Francine Allen"
	:first-name "Francine"
	:radar-weight 100
	:last-name "Allen"
	:person-index 2
	:primary-phone-number "412 281 1348"
	:vip t
	:email-address "fra@ardra.org"
)

(new-radar-person "Tom Whittenburg"
	:first-name "Tom"
	:radar-weight 100
	:last-name "Whittenburg"
	:person-index 3
	:primary-phone-number "412 281 1350"
	:vip t
	:email-address "tlwhiten@ardra.org"
)

(new-radar-person "Jan Wilson"
	:first-name "Jan"
	:radar-weight 100
	:last-name "Wilson"
	:person-index 4
	:primary-phone-number "412 281 1352"
	:vip t
	:email-address "janice@cs.cmu.edu"
)

(new-radar-person "Michael Winslow"
	:first-name "Michael"
	:radar-weight 100
	:last-name "Winslow"
	:person-index 5
	:primary-phone-number "412 281 1354"
	:vip t
	:email-address "mrwins@ardra.org"
)

(new-radar-person "Rob Anderson"
	:first-name "Rob"
	:radar-weight 100
	:last-name "Anderson"
	:person-index 6
	:primary-phone-number "412 281 1356"
	:vip t
	:email-address "roa@cs.cmu.edu"
)

(new-radar-person "Spence Pierro"
	:first-name "Spence"
	:radar-weight 100
	:last-name "Pierro"
	:person-index 7
	:primary-phone-number "412 281 1358"
	:vip t
	:email-address "spierro@ardra.org"
)

(new-radar-person "Natalie Cohee"
	:first-name "Natalie"
	:radar-weight 100
	:last-name "Cohee"
	:person-index 8
	:primary-phone-number "412 281 1360"
	:vip t
	:email-address "ncohen@dot.gov"
)

(new-radar-person "Julie Coil"
	:first-name "Julie"
	:radar-weight 50
	:last-name "Coil"
	:person-index 9
	:primary-phone-number "412 281 1362"
	:vip nil
	:email-address "jcoyle@pittsburgh.flagstaff.com"
)

(new-radar-person "Meredith Lorenz"
	:first-name "Meredith"
	:radar-weight 50
	:last-name "Lorenz"
	:person-index 10
	:primary-phone-number "412 281 1364"
	:vip nil
	:email-address "lorenze@pittsburgh.flagstaff.com"
)

(new-radar-person "Carl Spencer"
	:first-name "Carl"
	:radar-weight 50
	:last-name "Spencer"
	:person-index 11
	:primary-phone-number "412 281 1366"
	:vip nil
	:email-address "cas@cmu.edu"
)

(new-radar-person "Art Furr"
	:first-name "Art"
	:radar-weight 50
	:last-name "Furr"
	:person-index 12
	:primary-phone-number "412 281 1368"
	:vip nil
	:email-address "afurr@cmu.edu"
)

(new-radar-person "Blake Randal"
	:first-name "Blake"
	:radar-weight 100
	:last-name "Randal"
	:person-index 13
	:primary-phone-number "412 281 1370"
	:vip t
	:email-address "bor@cs.cmu.edu"
)

(new-radar-person "Sam Katterson"
	:first-name "Sam"
	:radar-weight 25
	:last-name "Katterson"
	:person-index 14
	:primary-phone-number "412 281 1372"
	:vip nil
	:email-address "skatters@ardra.org"
)

(new-radar-person "Brad Cruise"
	:first-name "Brad"
	:radar-weight 50
	:last-name "Cruise"
	:person-index 15
	:primary-phone-number "412 281 1374"
	:vip nil
	:email-address "bcruise@ardra.org"
)

(new-radar-person "Andrea Manson"
	:first-name "Andrea"
	:radar-weight 100
	:last-name "Manson"
	:person-index 16
	:primary-phone-number "412 281 1376"
	:vip t
	:email-address "amanson@ardra.org"
)

(new-radar-person "Steph Waggenman"
	:first-name "Steph"
	:radar-weight 25
	:last-name "Waggenman"
	:person-index 17
	:primary-phone-number "412 281 1378"
	:vip nil
	:email-address "swaggenme@ardra.org"
)

(new-radar-person "Elaine Simpson"
	:first-name "Elaine"
	:radar-weight 25
	:last-name "Simpson"
	:person-index 18
	:primary-phone-number "412 281 1380"
	:vip nil
	:email-address "ems@ardra.org"
)

(new-radar-person "Elizabeth McCall"
	:first-name "Elizabeth"
	:radar-weight 25
	:last-name "McCall"
	:person-index 19
	:primary-phone-number "412 281 1382"
	:vip nil
	:email-address "lam@ardra.org"
)

(new-radar-person "Agatha Daniels"
	:first-name "Agatha"
	:radar-weight 25
	:last-name "Daniels"
	:person-index 20
	:primary-phone-number "412 281 1384"
	:vip nil
	:email-address "awest@ardra.org"
)

(new-radar-person "Virgina Baker"
	:first-name "Virgina"
	:radar-weight 25
	:last-name "Baker"
	:person-index 21
	:primary-phone-number "412 281 1386"
	:vip nil
	:email-address "vlb@ardra.org"
)

(new-radar-person "Sophie Cooper"
	:first-name "Sophie"
	:radar-weight 25
	:last-name "Cooper"
	:person-index 22
	:primary-phone-number "412 281 1388"
	:vip nil
	:email-address "stc@ardra.org"
)

(new-radar-person "Isabelle Russel"
	:first-name "Isabelle"
	:radar-weight 25
	:last-name "Russel"
	:person-index 23
	:primary-phone-number "412 281 1390"
	:vip nil
	:email-address "imr@ardra.org"
)

(new-radar-person "Miles Ritter"
	:first-name "Miles"
	:radar-weight 25
	:last-name "Ritter"
	:person-index 24
	:primary-phone-number "412 281 1392"
	:vip nil
	:email-address "mmr@ardra.org"
)

(new-radar-person "Ryan McPeak"
	:first-name "Ryan"
	:radar-weight 25
	:last-name "McPeak"
	:person-index 25
	:primary-phone-number "412 281 1394"
	:vip nil
	:email-address "rmcpeak@ardra.org"
)

(new-radar-person "Hollie Fereday"
	:first-name "Hollie"
	:radar-weight 25
	:last-name "Fereday"
	:person-index 26
	:primary-phone-number "412 281 1396"
	:vip nil
	:email-address "hfereday@ardra.org"
)

(new-radar-person "Jack Fishbein"
	:first-name "Jack"
	:radar-weight 25
	:last-name "Fishbein"
	:person-index 27
	:primary-phone-number "412 281 1398"
	:vip nil
	:email-address "jfishbein@ardra.org"
)

(new-radar-person "Walter Evenson"
	:first-name "Walter"
	:radar-weight 25
	:last-name "Evenson"
	:person-index 28
	:primary-phone-number "412 281 1400"
	:vip nil
	:email-address "wevenso@ardra.org"
)

(new-radar-person "Gerald Buckner"
	:first-name "Gerald"
	:radar-weight 25
	:last-name "Buckner"
	:person-index 29
	:primary-phone-number "412 281 1402"
	:vip nil
	:email-address "jbuckner@ardra.org"
)

(new-radar-person "Marie Isner"
	:first-name "Marie"
	:radar-weight 25
	:last-name "Isner"
	:person-index 30
	:primary-phone-number "412 281 1404"
	:vip nil
	:email-address "mei@ardra.org"
)

(new-radar-person "Mary Eisner"
	:first-name "Mary"
	:radar-weight 25
	:last-name "Eisner"
	:person-index 31
	:primary-phone-number "412 281 1406"
	:vip nil
	:email-address "meisner@ardra.org"
)

(new-radar-person "Louis Kabalero"
	:first-name "Louis"
	:radar-weight 25
	:last-name "Kabalero"
	:person-index 32
	:primary-phone-number "412 281 1408"
	:vip nil
	:email-address "lkabalero@ardra.org"
)

(new-radar-person "Vincent McMurray"
	:first-name "Vincent"
	:radar-weight 25
	:last-name "McMurray"
	:person-index 33
	:primary-phone-number "412 281 1410"
	:vip nil
	:email-address "vmcmurray@ardra.org"
)

(new-radar-person "Valerie Kidd"
	:first-name "Valerie"
	:radar-weight 25
	:last-name "Kidd"
	:person-index 34
	:primary-phone-number "412 281 1412"
	:vip nil
	:email-address "vkidd@ardra.org"
)

(new-radar-person "Beatrice Rodriguez"
	:first-name "Beatrice"
	:radar-weight 25
	:last-name "Rodriguez"
	:person-index 35
	:primary-phone-number "412 281 1414"
	:vip nil
	:email-address "brodriguez@ardra.org"
)

(new-radar-person "Sigfried Calloway"
	:first-name "Sigfried"
	:radar-weight 25
	:last-name "Calloway"
	:person-index 36
	:primary-phone-number "412 281 1416"
	:vip nil
	:email-address "scalloway@ardra.org"
)

(new-radar-person "Sandra Nubanks"
	:first-name "Sandra"
	:radar-weight 25
	:last-name "Nubanks"
	:person-index 37
	:primary-phone-number "412 281 4562"
	:vip nil
	:email-address "snubanks@ardra.org"
)

(new-radar-person "William Alexander"
	:first-name "William"
	:radar-weight 25
	:last-name "Alexander"
	:person-index 38
	:primary-phone-number "412 281 4564"
	:vip nil
	:email-address "walex@ardra.org"
)

(new-radar-person "Fabian Monoker"
	:first-name "Fabian"
	:radar-weight 25
	:last-name "Monoker"
	:person-index 39
	:primary-phone-number "412 281 4566"
	:vip nil
	:email-address "fmono@ardra.org"
)

(new-radar-person "Thomas Bouvier"
	:first-name "Thomas"
	:radar-weight 25
	:last-name "Bouvier"
	:person-index 40
	:primary-phone-number "412 281 4568"
	:vip nil
	:email-address "thomas@ardra.org"
)

(new-radar-person "Elizabeth McManis"
	:first-name "Elizabeth"
	:radar-weight 25
	:last-name "McManis"
	:person-index 41
	:primary-phone-number "412 281 4670"
	:vip nil
	:email-address "emcma@ardra.org"
)

(new-radar-person "Gary Abramowitz"
	:first-name "Gary"
	:radar-weight 25
	:last-name "Abramowitz"
	:person-index 42
	:primary-phone-number "412 281 4672"
	:vip nil
	:email-address "gabram@ardra.org"
)

(new-radar-person "Vivek Kahn"
	:first-name "Vivek"
	:radar-weight 25
	:last-name "Kahn"
	:person-index 43
	:primary-phone-number "412 281 4674"
	:vip nil
	:email-address "vivekk@ardra.org"
)

(new-radar-person "Yoo Fong"
	:first-name "Yoo"
	:radar-weight 25
	:last-name "Fong"
	:person-index 44
	:primary-phone-number "412 281 4676"
	:vip nil
	:email-address "yoof@ardra.org"
)

(new-radar-person "Rachel Greensburg"
	:first-name "Rachel"
	:radar-weight 25
	:last-name "Greensburg"
	:person-index 45
	:primary-phone-number "412 281 4678"
	:vip nil
	:email-address "regreen@ardra.org"
)

(new-radar-person "Steve Adams"
	:first-name "Steve"
	:radar-weight 25
	:last-name "Adams"
	:person-index 46
	:primary-phone-number "412 281 4680"
	:vip nil
	:email-address "sadams@ardra.org"
)

(new-radar-person "Alexander Gallatin"
	:first-name "Alexander"
	:radar-weight 25
	:last-name "Gallatin"
	:person-index 47
	:primary-phone-number "412 281 4682"
	:vip nil
	:email-address "alexg@ardra.org"
)

(new-radar-person "Vivica Wagner"
	:first-name "Vivica"
	:radar-weight 25
	:last-name "Wagner"
	:person-index 48
	:primary-phone-number "412 281 4684"
	:vip nil
	:email-address "vwagner@ardra.org"
)

(new-radar-person "Jason O\'Reilly"
	:first-name "Jason"
	:radar-weight 25
	:last-name "O\'Reilly"
	:person-index 49
	:primary-phone-number "412 281 4686"
	:vip nil
	:email-address "jasono@ardra.org"
)

(new-radar-person "Shriya Shaw"
	:first-name "Shriya"
	:radar-weight 25
	:last-name "Shaw"
	:person-index 50
	:primary-phone-number "412 281 4688"
	:vip nil
	:email-address "sshaw@ardra.org"
)

(new-radar-person "Becky Green"
	:first-name "Becky"
	:radar-weight 25
	:last-name "Green"
	:person-index 51
	:primary-phone-number "412 281 4690"
	:vip nil
	:email-address "rgreen@ardra.org"
)

(new-radar-person "Maggie Foxenreiter"
	:first-name "Maggie"
	:radar-weight 25
	:last-name "Foxenreiter"
	:person-index 52
	:primary-phone-number "412 281 4692"
	:vip nil
	:email-address "mfox@ardra.org"
)

(new-radar-person "Ben Steigerwald"
	:first-name "Ben"
	:radar-weight 25
	:last-name "Steigerwald"
	:person-index 53
	:primary-phone-number "412 281 4694"
	:vip nil
	:email-address "bsteiger@ardra.org"
)

(new-radar-person "Andrew Schwartz"
	:first-name "Andrew"
	:radar-weight 25
	:last-name "Schwartz"
	:person-index 54
	:primary-phone-number "412 281 4696"
	:vip nil
	:email-address "aschwartz@ardra.org"
)

(new-radar-person "Martha Pialino"
	:first-name "Martha"
	:radar-weight 25
	:last-name "Pialino"
	:person-index 55
	:primary-phone-number "412 281 4698"
	:vip nil
	:email-address "mpialino@ardra.org"
)

(new-radar-person "Colleen Undranko"
	:first-name "Colleen"
	:radar-weight 25
	:last-name "Undranko"
	:person-index 56
	:primary-phone-number "412 281 4700"
	:vip nil
	:email-address "cundra@ardra.org"
)

(new-radar-person "Eric Musacelo"
	:first-name "Eric"
	:radar-weight 25
	:last-name "Musacelo"
	:person-index 57
	:primary-phone-number "412 281 4702"
	:vip nil
	:email-address "emusa@ardra.org"
)

(new-radar-person "Arnold Wickner"
	:first-name "Arnold"
	:radar-weight 25
	:last-name "Wickner"
	:person-index 58
	:primary-phone-number "412 281 4704"
	:vip nil
	:email-address "awickner@ardra.org"
)

(new-radar-person "Brianna Smith"
	:first-name "Brianna"
	:radar-weight 25
	:last-name "Smith"
	:person-index 59
	:primary-phone-number "412 281 4706"
	:vip nil
	:email-address "bsmith@ardra.org"
)

(new-radar-person "Sonal Malhotra"
	:first-name "Sonal"
	:radar-weight 25
	:last-name "Malhotra"
	:person-index 60
	:primary-phone-number "412 281 4708"
	:vip nil
	:email-address "smalh@ardra.org"
)

(new-radar-person "Tal Havartza"
	:first-name "Tal"
	:radar-weight 25
	:last-name "Havartza"
	:person-index 61
	:primary-phone-number "412 281 4710"
	:vip nil
	:email-address "thava@ardra.org"
)

(new-radar-person "Susan Mikhal"
	:first-name "Susan"
	:radar-weight 25
	:last-name "Mikhal"
	:person-index 62
	:primary-phone-number "412 281 4712"
	:vip nil
	:email-address "susanm@ardra.org"
)

(new-radar-person "Ian Liluso"
	:first-name "Ian"
	:radar-weight 25
	:last-name "Liluso"
	:person-index 63
	:primary-phone-number "412 281 4714"
	:vip nil
	:email-address "ianlilu@ardra.org"
)

(new-radar-person "Ruben Salada"
	:first-name "Ruben"
	:radar-weight 25
	:last-name "Salada"
	:person-index 64
	:primary-phone-number "412 281 4716"
	:vip nil
	:email-address "rsala@ardra.org"
)

(new-radar-person "Nina Riazo"
	:first-name "Nina"
	:radar-weight 25
	:last-name "Riazo"
	:person-index 65
	:primary-phone-number "412 281 4718"
	:vip nil
	:email-address "ninar@ardra.org"
)

(new-radar-person "Gina Wilson"
	:first-name "Gina"
	:radar-weight 25
	:last-name "Wilson"
	:person-index 66
	:primary-phone-number "412 281 4720"
	:vip nil
	:email-address "ginaw@ardra.org"
)

(new-radar-person "Brian Car"
	:first-name "Brian"
	:radar-weight 25
	:last-name "Car"
	:person-index 67
	:primary-phone-number "412 281 4722"
	:vip nil
	:email-address "bcar@ardra.org"
)

(new-radar-person "Timothy Margilois"
	:first-name "Timothy"
	:radar-weight 25
	:last-name "Margilois"
	:person-index 68
	:primary-phone-number "412 281 4724"
	:vip nil
	:email-address "tmargil@ardra.org"
)

(new-radar-person "Jun Lee"
	:first-name "Jun"
	:radar-weight 25
	:last-name "Lee"
	:person-index 69
	:primary-phone-number "412 281 4726"
	:vip nil
	:email-address "jlee@ardra.org"
)

(new-radar-person "April Towers"
	:first-name "April"
	:radar-weight 25
	:last-name "Towers"
	:person-index 70
	:primary-phone-number "412 281 4728"
	:vip nil
	:email-address "atowers@ardra.org"
)

(new-radar-person "Ashuk Wendorf"
	:first-name "Ashuk"
	:radar-weight 25
	:last-name "Wendorf"
	:person-index 71
	:primary-phone-number "412 281 4730"
	:vip nil
	:email-address "awendorf@ardra.org"
)

(new-radar-person "Samantha Swick"
	:first-name "Samantha"
	:radar-weight 25
	:last-name "Swick"
	:person-index 72
	:primary-phone-number "412 281 6512"
	:vip nil
	:email-address "sswick@ardra.org"
)

(new-radar-person "Susana Fariola"
	:first-name "Susana"
	:radar-weight 25
	:last-name "Fariola"
	:person-index 73
	:primary-phone-number "412 281 6514"
	:vip nil
	:email-address "sfariola@ardra.org"
)

(new-radar-person "Andrew Delafonte"
	:first-name "Andrew"
	:radar-weight 25
	:last-name "Delafonte"
	:person-index 74
	:primary-phone-number "412 281 6516"
	:vip nil
	:email-address "adelafon@ardra.org"
)

(new-radar-person "Frank Ionola"
	:first-name "Frank"
	:radar-weight 25
	:last-name "Ionola"
	:person-index 75
	:primary-phone-number "412 281 6518"
	:vip nil
	:email-address "fionola@ardra.org"
)

(new-radar-person "Suzanne Marilios"
	:first-name "Suzanne"
	:radar-weight 25
	:last-name "Marilios"
	:person-index 76
	:primary-phone-number "412 281 6520"
	:vip nil
	:email-address "smarilios@ardra.org"
)

(new-radar-person "Tina Fall"
	:first-name "Tina"
	:radar-weight 25
	:last-name "Fall"
	:person-index 77
	:primary-phone-number "412 281 6522"
	:vip nil
	:email-address "tgf@ardra.org"
)

(new-radar-person "Ed Ivanovich"
	:first-name "Ed"
	:radar-weight 25
	:last-name "Ivanovich"
	:person-index 78
	:primary-phone-number "412 281 6524"
	:vip nil
	:email-address "eivanovich@ardra.org"
)

(new-radar-person "Westley Qualter"
	:first-name "Westley"
	:radar-weight 25
	:last-name "Qualter"
	:person-index 79
	:primary-phone-number "412 281 6526"
	:vip nil
	:email-address "qualter@ardra.org"
)

(new-radar-person "Ilise Nebuto"
	:first-name "Ilise"
	:radar-weight 25
	:last-name "Nebuto"
	:person-index 80
	:primary-phone-number "412 281 6558"
	:vip nil
	:email-address "ilisen@ardra.org"
)

(new-radar-person "Dennis Johnson"
	:first-name "Dennis"
	:radar-weight 25
	:last-name "Johnson"
	:person-index 81
	:primary-phone-number "412 281 6560"
	:vip nil
	:email-address "dlj@ardra.org"
)

(new-radar-person "Andy Sewally"
	:first-name "Andy"
	:radar-weight 25
	:last-name "Sewally"
	:person-index 82
	:primary-phone-number "412 281 6562"
	:vip nil
	:email-address "asewally@ardra.org"
)

(new-radar-person "Peter Havater"
	:first-name "Peter"
	:radar-weight 25
	:last-name "Havater"
	:person-index 83
	:primary-phone-number "412 281 6564"
	:vip nil
	:email-address "phavater@ardra.org"
)

(new-radar-person "Yong Qian"
	:first-name "Yong"
	:radar-weight 25
	:last-name "Qian"
	:person-index 84
	:primary-phone-number "412 281 6566"
	:vip nil
	:email-address "yquian@ardra.org"
)

(new-radar-person "Ola Neuen"
	:first-name "Ola"
	:radar-weight 25
	:last-name "Neuen"
	:person-index 85
	:primary-phone-number "412 281 6568"
	:vip nil
	:email-address "neuen@ardra.org"
)

(new-radar-person "Cathy Algowitz"
	:first-name "Cathy"
	:radar-weight 25
	:last-name "Algowitz"
	:person-index 86
	:primary-phone-number "412 281 6570"
	:vip nil
	:email-address "calgowitz@ardra.org"
)

(new-radar-person "Helmet Uter"
	:first-name "Helmet"
	:radar-weight 25
	:last-name "Uter"
	:person-index 87
	:primary-phone-number "412 281 6572"
	:vip nil
	:email-address "helmet@ardra.org"
)

(new-radar-person "Petra Svetski"
	:first-name "Petra"
	:radar-weight 25
	:last-name "Svetski"
	:person-index 88
	:primary-phone-number "412 281 6574"
	:vip nil
	:email-address "svetski@ardra.org"
)

(new-radar-person "Nina Idiz"
	:first-name "Nina"
	:radar-weight 25
	:last-name "Idiz"
	:person-index 89
	:primary-phone-number "412 281 6576"
	:vip nil
	:email-address "nidiz@ardra.org"
)

(new-radar-person "Jana Padamanabahn"
	:first-name "Jana"
	:radar-weight 25
	:last-name "Padamanabahn"
	:person-index 90
	:primary-phone-number "412 281 6578"
	:vip nil
	:email-address "padamana@ardra.org"
)

(new-radar-person "Mark Oriolos"
	:first-name "Mark"
	:radar-weight 25
	:last-name "Oriolos"
	:person-index 91
	:primary-phone-number "412 281 6580"
	:vip nil
	:email-address "moriolos@ardra.org"
)

(new-radar-person "Dana Johanson"
	:first-name "Dana"
	:radar-weight 25
	:last-name "Johanson"
	:person-index 92
	:primary-phone-number "412 281 6582"
	:vip nil
	:email-address "dwj@ardra.org"
)

(new-radar-person "Wanda Teven"
	:first-name "Wanda"
	:radar-weight 25
	:last-name "Teven"
	:person-index 93
	:primary-phone-number "412 281 6584"
	:vip nil
	:email-address "teven@ardra.org"
)

(new-radar-person "Larry Elry"
	:first-name "Larry"
	:radar-weight 25
	:last-name "Elry"
	:person-index 94
	:primary-phone-number "412 281 6586"
	:vip nil
	:email-address "lelry@ardra.org"
)

(new-radar-person "Deana Gona"
	:first-name "Deana"
	:radar-weight 25
	:last-name "Gona"
	:person-index 95
	:primary-phone-number "412 281 6588"
	:vip nil
	:email-address "dgona@ardra.org"
)

(new-radar-person "Fiona Mansala"
	:first-name "Fiona"
	:radar-weight 25
	:last-name "Mansala"
	:person-index 96
	:primary-phone-number "412 281 6590"
	:vip nil
	:email-address "fmans@ardra.org"
)

(new-radar-person "Ed Farvich"
	:first-name "Ed"
	:radar-weight 25
	:last-name "Farvich"
	:person-index 97
	:primary-phone-number "412 281 6592"
	:vip nil
	:email-address "emf@ardra.org"
)

(new-radar-person "Renee O\'Laughlin"
	:first-name "Renee"
	:radar-weight 25
	:last-name "O\'Laughlin"
	:person-index 98
	:primary-phone-number "412 281 6594"
	:vip nil
	:email-address "olaughlin@ardra.org"
)

(new-radar-person "Pete Fava"
	:first-name "Pete"
	:radar-weight 25
	:last-name "Fava"
	:person-index 99
	:primary-phone-number "412 281 6596"
	:vip nil
	:email-address "pfava@ardra.org"
)

(new-radar-person "Zachary Tonazu"
	:first-name "Zachary"
	:radar-weight 25
	:last-name "Tonazu"
	:person-index 100
	:primary-phone-number "412 281 6598"
	:vip nil
	:email-address "tonazu@ardra.org"
)

(new-radar-person "Michael Reese"
	:first-name "Michael"
	:radar-weight 25
	:last-name "Reese"
	:person-index 101
	:primary-phone-number "412 281 6600"
	:vip nil
	:email-address "mreese@ardra.org"
)

(new-radar-person "Tina Sutter"
	:first-name "Tina"
	:radar-weight 25
	:last-name "Sutter"
	:person-index 102
	:primary-phone-number "412 281 6602"
	:vip nil
	:email-address "tlsutter@ardra.org"
)

(new-radar-person "Kelly Jovanovich"
	:first-name "Kelly"
	:radar-weight 25
	:last-name "Jovanovich"
	:person-index 103
	:primary-phone-number "412 281 6604"
	:vip nil
	:email-address "jovanovich@ardra.org"
)

(new-radar-person "Chian Lee"
	:first-name "Chian"
	:radar-weight 25
	:last-name "Lee"
	:person-index 104
	:primary-phone-number "412 281 6606"
	:vip nil
	:email-address "chian@ardra.org"
)

(new-radar-person "Shawanda North"
	:first-name "Shawanda"
	:radar-weight 25
	:last-name "North"
	:person-index 105
	:primary-phone-number "412 281 6608"
	:vip nil
	:email-address "shawanda@ardra.org"
)

(new-radar-person "Jack Petersen"
	:first-name "Jack"
	:radar-weight 25
	:last-name "Petersen"
	:person-index 106
	:primary-phone-number "412 281 6610"
	:vip nil
	:email-address "jpeterse@ardra.org"
)

(new-radar-person "Don Henry"
	:first-name "Don"
	:radar-weight 25
	:last-name "Henry"
	:person-index 107
	:primary-phone-number "412 281 4802"
	:vip nil
	:email-address "dhenry@ardra.org"
)

(new-radar-person "Abigail Reeves"
	:first-name "Abigail"
	:radar-weight 25
	:last-name "Reeves"
	:person-index 108
	:primary-phone-number "412 281 4804"
	:vip nil
	:email-address "abireeves@ardra.org"
)

(new-radar-person "Calla Diller"
	:first-name "Calla"
	:radar-weight 25
	:last-name "Diller"
	:person-index 109
	:primary-phone-number "412 281 4806"
	:vip nil
	:email-address "calla@ardra.org"
)

(new-radar-person "Courtney Charles"
	:first-name "Courtney"
	:radar-weight 25
	:last-name "Charles"
	:person-index 110
	:primary-phone-number "412 281 4808"
	:vip nil
	:email-address "courtneyd@ardra.org"
)

(new-radar-person "Katrina Aronza"
	:first-name "Katrina"
	:radar-weight 25
	:last-name "Aronza"
	:person-index 111
	:primary-phone-number "412 281 4810"
	:vip nil
	:email-address "katrinaa@ardra.org"
)

(new-radar-person "Laura Timdale"
	:first-name "Laura"
	:radar-weight 25
	:last-name "Timdale"
	:person-index 112
	:primary-phone-number "412 281 4812"
	:vip nil
	:email-address "laurat2@ardra.org"
)

(new-radar-person "Charles Derunn"
	:first-name "Charles"
	:radar-weight 25
	:last-name "Derunn"
	:person-index 113
	:primary-phone-number "412 281 4814"
	:vip nil
	:email-address "cderunn@ardra.org"
)

(new-radar-person "Michael Faye"
	:first-name "Michael"
	:radar-weight 25
	:last-name "Faye"
	:person-index 114
	:primary-phone-number "412 281 4816"
	:vip nil
	:email-address "mfaye@ardra.org"
)

(new-radar-person "Bob Armstrong"
	:first-name "Bob"
	:radar-weight 25
	:last-name "Armstrong"
	:person-index 115
	:primary-phone-number "412 281 4818"
	:vip nil
	:email-address "robertarm@ardra.org"
)

(new-radar-person "Richard Holley"
	:first-name "Richard"
	:radar-weight 25
	:last-name "Holley"
	:person-index 116
	:primary-phone-number "412 281 4820"
	:vip nil
	:email-address "rholley@ardra.org"
)

(new-radar-person "Hollis Carrow"
	:first-name "Hollis"
	:radar-weight 25
	:last-name "Carrow"
	:person-index 117
	:primary-phone-number "412 281 4822"
	:vip nil
	:email-address "hollis@ardra.org"
)

(new-radar-person "Becky Anupira"
	:first-name "Becky"
	:radar-weight 25
	:last-name "Anupira"
	:person-index 118
	:primary-phone-number "412 281 4824"
	:vip nil
	:email-address "bta@ardra.org"
)

(new-radar-person "Vincent Almoore"
	:first-name "Vincent"
	:radar-weight 25
	:last-name "Almoore"
	:person-index 119
	:primary-phone-number "412 281 4826"
	:vip nil
	:email-address "vga4@ardra.org"
)

(new-radar-person "Taylor Killian"
	:first-name "Taylor"
	:radar-weight 25
	:last-name "Killian"
	:person-index 120
	:primary-phone-number "412 281 4828"
	:vip nil
	:email-address "killian@ardra.org"
)

(new-radar-person "Scott Scheurman"
	:first-name "Scott"
	:radar-weight 25
	:last-name "Scheurman"
	:person-index 121
	:primary-phone-number "412 281 4830"
	:vip nil
	:email-address "scheurman@ardra.org"
)

(new-radar-person "Laron Dale"
	:first-name "Laron"
	:radar-weight 25
	:last-name "Dale"
	:person-index 122
	:primary-phone-number "412 281 4832"
	:vip nil
	:email-address "led@ardra.org"
)

(new-radar-person "Jarrod Michaels"
	:first-name "Jarrod"
	:radar-weight 25
	:last-name "Michaels"
	:person-index 123
	:primary-phone-number "412 281 4834"
	:vip nil
	:email-address "jarrod@ardra.org"
)

(new-radar-person "Melissa Crosby"
	:first-name "Melissa"
	:radar-weight 25
	:last-name "Crosby"
	:person-index 124
	:primary-phone-number "412 281 4836"
	:vip nil
	:email-address "mcrosby@ardra.org"
)

(new-radar-person "Donald Allman"
	:first-name "Donald"
	:radar-weight 25
	:last-name "Allman"
	:person-index 125
	:primary-phone-number "412 281 4838"
	:vip nil
	:email-address "dallman@ardra.org"
)

(new-radar-person "Julia Gonzalez"
	:first-name "Julia"
	:radar-weight 25
	:last-name "Gonzalez"
	:person-index 126
	:primary-phone-number "412 281 4840"
	:vip nil
	:email-address "jlgonzalez@ardra.org"
)

(new-radar-person "Natalia Kinski"
	:first-name "Natalia"
	:radar-weight 25
	:last-name "Kinski"
	:person-index 127
	:primary-phone-number "412 281 4842"
	:vip nil
	:email-address "nekinski@ardra.org"
)

(new-radar-person "Theodore Callman"
	:first-name "Theodore"
	:radar-weight 25
	:last-name "Callman"
	:person-index 128
	:primary-phone-number "412 281 4844"
	:vip nil
	:email-address "tcallman@ardra.org"
)

(new-radar-person "Eli Dorunda"
	:first-name "Eli"
	:radar-weight 25
	:last-name "Dorunda"
	:person-index 129
	:primary-phone-number "412 281 4846"
	:vip nil
	:email-address "elid@ardra.org"
)

(new-radar-person "Federico Muniz"
	:first-name "Federico"
	:radar-weight 25
	:last-name "Muniz"
	:person-index 130
	:primary-phone-number "412 281 4848"
	:vip nil
	:email-address "federico@ardra.org"
)

(new-radar-person "Cara Ride"
	:first-name "Cara"
	:radar-weight 25
	:last-name "Ride"
	:person-index 131
	:primary-phone-number "412 281 4850"
	:vip nil
	:email-address "cmride@ardra.org"
)

(new-radar-person "Bradley Noriega"
	:first-name "Bradley"
	:radar-weight 25
	:last-name "Noriega"
	:person-index 132
	:primary-phone-number "412 281 4852"
	:vip nil
	:email-address "bam@ardra.org"
)

(new-radar-person "Tom Thoreau"
	:first-name "Tom"
	:radar-weight 25
	:last-name "Thoreau"
	:person-index 133
	:primary-phone-number "412 281 4854"
	:vip nil
	:email-address "thoreau@ardra.org"
)

(new-radar-person "Henry David"
	:first-name "Henry"
	:radar-weight 25
	:last-name "David"
	:person-index 134
	:primary-phone-number "412 281 4856"
	:vip nil
	:email-address "hbdavid@ardra.org"
)

(new-radar-person "Juliet Frerking"
	:first-name "Juliet"
	:radar-weight 25
	:last-name "Frerking"
	:person-index 135
	:primary-phone-number "412 281 4858"
	:vip nil
	:email-address "frerking@ardra.org"
)

(new-radar-person "Thomas Collins"
	:first-name "Thomas"
	:radar-weight 25
	:last-name "Collins"
	:person-index 136
	:primary-phone-number "412 281 4860"
	:vip nil
	:email-address "tcollins@ardra.org"
)

(new-radar-person "Britt Daniel"
	:first-name "Britt"
	:radar-weight 25
	:last-name "Daniel"
	:person-index 137
	:primary-phone-number "412 281 4862"
	:vip nil
	:email-address "brittd@ardra.org"
)

(new-radar-person "Vicenzo Calabreze"
	:first-name "Vicenzo"
	:radar-weight 25
	:last-name "Calabreze"
	:person-index 138
	:primary-phone-number "412 281 4864"
	:vip nil
	:email-address "vicenzo@ardra.org"
)

(new-radar-person "Teo Ciccoli"
	:first-name "Teo"
	:radar-weight 25
	:last-name "Ciccoli"
	:person-index 139
	:primary-phone-number "412 281 4866"
	:vip nil
	:email-address "teo@ardra.org"
)

(new-radar-person "Lupe Bucca"
	:first-name "Lupe"
	:radar-weight 25
	:last-name "Bucca"
	:person-index 140
	:primary-phone-number "412 281 4868"
	:vip nil
	:email-address "lbucca@ardra.org"
)

(new-radar-person "Brenda Smallman"
	:first-name "Brenda"
	:radar-weight 25
	:last-name "Smallman"
	:person-index 141
	:primary-phone-number "412 281 4870"
	:vip nil
	:email-address "bsmallman@ardra.org"
)

(new-radar-person "Louisa Hollands"
	:first-name "Louisa"
	:radar-weight 25
	:last-name "Hollands"
	:person-index 142
	:primary-phone-number "412 281 4872"
	:vip nil
	:email-address "lhollands@ardra.org"
)

(new-radar-person "Jennifer Morris"
	:first-name "Jennifer"
	:radar-weight 25
	:last-name "Morris"
	:person-index 143
	:primary-phone-number "412 281 4874"
	:vip nil
	:email-address "jlmorris@ardra.org"
)

(new-radar-person "Angelica Antonias"
	:first-name "Angelica"
	:radar-weight 25
	:last-name "Antonias"
	:person-index 144
	:primary-phone-number "412 281 4876"
	:vip nil
	:email-address "ata@ardra.org"
)

(new-radar-person "Penelope Benold"
	:first-name "Penelope"
	:radar-weight 25
	:last-name "Benold"
	:person-index 145
	:primary-phone-number "412 281 4878"
	:vip nil
	:email-address "penelope@ardra.org"
)

(new-radar-person "Stuart Milton"
	:first-name "Stuart"
	:radar-weight 25
	:last-name "Milton"
	:person-index 146
	:primary-phone-number "412 281 4880"
	:vip nil
	:email-address "semilton@ardra.org"
)

(new-radar-person "Patricia Minor"
	:first-name "Patricia"
	:radar-weight 25
	:last-name "Minor"
	:person-index 147
	:primary-phone-number "412 281 4882"
	:vip nil
	:email-address "pminor@ardra.org"
)

(new-radar-person "Staffan Bjorn"
	:first-name "Staffan"
	:radar-weight 25
	:last-name "Bjorn"
	:person-index 148
	:primary-phone-number "412 281 4884"
	:vip nil
	:email-address "staffan@ardra.org"
)

(new-radar-person "Patrick Fitzsimmons"
	:first-name "Patrick"
	:radar-weight 25
	:last-name "Fitzsimmons"
	:person-index 149
	:primary-phone-number "412 281 4886"
	:vip nil
	:email-address "fitzsimm@ardra.org"
)

(new-radar-person "Jeffrey Lester"
	:first-name "Jeffrey"
	:radar-weight 25
	:last-name "Lester"
	:person-index 150
	:primary-phone-number "412 281 4888"
	:vip nil
	:email-address "jlester@ardra.org"
)

(new-radar-person "Lucas Zettle"
	:first-name "Lucas"
	:radar-weight 25
	:last-name "Zettle"
	:person-index 151
	:primary-phone-number "412 281 4890"
	:vip nil
	:email-address "zettle@ardra.org"
)

(new-radar-person "Lisa Sweeney"
	:first-name "Lisa"
	:radar-weight 25
	:last-name "Sweeney"
	:person-index 152
	:primary-phone-number "412 281 4892"
	:vip nil
	:email-address "lsweeney@ardra.org"
)

(new-radar-person "Luke Moyer"
	:first-name "Luke"
	:radar-weight 25
	:last-name "Moyer"
	:person-index 153
	:primary-phone-number "412 281 4894"
	:vip nil
	:email-address "lmoyer@ardra.org"
)

(new-radar-person "Michelle Wooldridge"
	:first-name "Michelle"
	:radar-weight 25
	:last-name "Wooldridge"
	:person-index 154
	:primary-phone-number "412 281 4896"
	:vip nil
	:email-address "mew@ardra.org"
)

(new-radar-person "John Connor"
	:first-name "John"
	:radar-weight 25
	:last-name "Connor"
	:person-index 155
	:primary-phone-number "412 281 4898"
	:vip nil
	:email-address "jlc@ardra.org"
)

(new-radar-person "Mitchell Bradbury"
	:first-name "Mitchell"
	:radar-weight 25
	:last-name "Bradbury"
	:person-index 156
	:primary-phone-number "412 281 4990"
	:vip nil
	:email-address "mitchellb@ardra.org"
)

(new-radar-person "Motohiko Haguchi"
	:first-name "Motohiko"
	:radar-weight 25
	:last-name "Haguchi"
	:person-index 157
	:primary-phone-number "412 281 4992"
	:vip nil
	:email-address "motohiko@ardra.org"
)

(new-radar-person "Patti Minel"
	:first-name "Patti"
	:radar-weight 25
	:last-name "Minel"
	:person-index 158
	:primary-phone-number "412 281 4994"
	:vip nil
	:email-address "pminel@ardra.org"
)

(new-radar-person "Hope Ezzard"
	:first-name "Hope"
	:radar-weight 25
	:last-name "Ezzard"
	:person-index 159
	:primary-phone-number "412 281 4996"
	:vip nil
	:email-address "ezzard@ardra.org"
)

(new-radar-person "Johannes Copper"
	:first-name "Johannes"
	:radar-weight 25
	:last-name "Copper"
	:person-index 160
	:primary-phone-number "412 281 4998"
	:vip nil
	:email-address "jpcopper@ardra.org"
)

(new-radar-person "Matushi Sakaguchi"
	:first-name "Matushi"
	:radar-weight 25
	:last-name "Sakaguchi"
	:person-index 161
	:primary-phone-number "412 281 5000"
	:vip nil
	:email-address "sakaguchi@ardra.org"
)

(new-radar-person "Iris Johnson"
	:first-name "Iris"
	:radar-weight 25
	:last-name "Johnson"
	:person-index 162
	:primary-phone-number "412 281 5002"
	:vip nil
	:email-address "ijohnson@ardra.org"
)

(new-radar-person "Lauren Sherman"
	:first-name "Lauren"
	:radar-weight 25
	:last-name "Sherman"
	:person-index 163
	:primary-phone-number "412 281 5004"
	:vip nil
	:email-address "lsherman@ardra.org"
)

(new-radar-person "Lucinda Hollister"
	:first-name "Lucinda"
	:radar-weight 25
	:last-name "Hollister"
	:person-index 164
	:primary-phone-number "412 281 5006"
	:vip nil
	:email-address "lucinda@ardra.org"
)

(new-radar-person "Peter Ferguson"
	:first-name "Peter"
	:radar-weight 25
	:last-name "Ferguson"
	:person-index 165
	:primary-phone-number "412 281 5008"
	:vip nil
	:email-address "pferguson@ardra.org"
)

(new-radar-person "James Sontag"
	:first-name "James"
	:radar-weight 25
	:last-name "Sontag"
	:person-index 166
	:primary-phone-number "412 281 5010"
	:vip nil
	:email-address "jpsontag@ardra.org"
)

(new-radar-person "Miho Sakato"
	:first-name "Miho"
	:radar-weight 25
	:last-name "Sakato"
	:person-index 167
	:primary-phone-number "412 281 5012"
	:vip nil
	:email-address "mihos@ardra.org"
)

(new-radar-person "Iban Zong"
	:first-name "Iban"
	:radar-weight 25
	:last-name "Zong"
	:person-index 168
	:primary-phone-number "412 281 5014"
	:vip nil
	:email-address "iban@ardra.org"
)

(new-radar-person "Samuel Johns"
	:first-name "Samuel"
	:radar-weight 25
	:last-name "Johns"
	:person-index 169
	:primary-phone-number "412 281 5016"
	:vip nil
	:email-address "sjohns@ardra.org"
)

(new-radar-person "Laura Schnieder"
	:first-name "Laura"
	:radar-weight 25
	:last-name "Schnieder"
	:person-index 170
	:primary-phone-number "412 281 5018"
	:vip nil
	:email-address "lbschnieder@ardra.org"
)

(new-radar-person "Sunala Rodriguez"
	:first-name "Sunala"
	:radar-weight 25
	:last-name "Rodriguez"
	:person-index 171
	:primary-phone-number "412 281 5020"
	:vip nil
	:email-address "sunala@ardra.org"
)

(new-radar-person "Frank Luin"
	:first-name "Frank"
	:radar-weight 25
	:last-name "Luin"
	:person-index 172
	:primary-phone-number "412 281 5022"
	:vip nil
	:email-address "frluin@ardra.org"
)

(new-radar-person "David Kingsley"
	:first-name "David"
	:radar-weight 25
	:last-name "Kingsley"
	:person-index 173
	:primary-phone-number "412 281 5024"
	:vip nil
	:email-address "dbkingsley@ardra.org"
)

(new-radar-person "Andrew Akins"
	:first-name "Andrew"
	:radar-weight 25
	:last-name "Akins"
	:person-index 174
	:primary-phone-number "412 281 5026"
	:vip nil
	:email-address "aakins@ardra.org"
)

(new-radar-person "Russell Piper"
	:first-name "Russell"
	:radar-weight 25
	:last-name "Piper"
	:person-index 175
	:primary-phone-number "412 281 5028"
	:vip nil
	:email-address "rpiper@ardra.org"
)

(new-radar-person "William Bernstein"
	:first-name "William"
	:radar-weight 25
	:last-name "Bernstein"
	:person-index 176
	:primary-phone-number "412 281 5030"
	:vip nil
	:email-address "wbernste@ardra.org"
)

(new-radar-person "Donald Hartfield"
	:first-name "Donald"
	:radar-weight 25
	:last-name "Hartfield"
	:person-index 177
	:primary-phone-number "412 281 5032"
	:vip nil
	:email-address "dnhart@ardra.org"
)

(new-radar-person "Norman Beatty"
	:first-name "Norman"
	:radar-weight 25
	:last-name "Beatty"
	:person-index 178
	:primary-phone-number "412 281 5034"
	:vip nil
	:email-address "nbeatty@ardra.org"
)

(new-radar-person "Kieth deCorta"
	:first-name "Kieth"
	:radar-weight 25
	:last-name "deCorta"
	:person-index 179
	:primary-phone-number "412 281 5036"
	:vip nil
	:email-address "decorta@ardra.org"
)

(new-radar-person "Harold Mientras"
	:first-name "Harold"
	:radar-weight 25
	:last-name "Mientras"
	:person-index 180
	:primary-phone-number "412 281 5038"
	:vip nil
	:email-address "hmientras@ardra.org"
)

(new-radar-person "Stanley Boznik"
	:first-name "Stanley"
	:radar-weight 25
	:last-name "Boznik"
	:person-index 181
	:primary-phone-number "412 281 5040"
	:vip nil
	:email-address "boznik@ardra.org"
)

(new-radar-person "Jeanne Santos"
	:first-name "Jeanne"
	:radar-weight 25
	:last-name "Santos"
	:person-index 182
	:primary-phone-number "412 281 5042"
	:vip nil
	:email-address "jeannes@ardra.org"
)

(new-radar-person "David Rodgerson"
	:first-name "David"
	:radar-weight 25
	:last-name "Rodgerson"
	:person-index 183
	:primary-phone-number "412 281 5044"
	:vip nil
	:email-address "dlr@ardra.org"
)

(new-radar-person "John Constable"
	:first-name "John"
	:radar-weight 25
	:last-name "Constable"
	:person-index 184
	:primary-phone-number "412 281 5046"
	:vip nil
	:email-address "jconstable@ardra.org"
)

(new-radar-person "Guillermo Credon"
	:first-name "Guillermo"
	:radar-weight 25
	:last-name "Credon"
	:person-index 185
	:primary-phone-number "412 281 5048"
	:vip nil
	:email-address "guillermo@ardra.org"
)

(new-radar-person "Nigel Contorso"
	:first-name "Nigel"
	:radar-weight 25
	:last-name "Contorso"
	:person-index 186
	:primary-phone-number "412 281 5050"
	:vip nil
	:email-address "nigelc@ardra.org"
)

(new-radar-person "Jenny Santiago"
	:first-name "Jenny"
	:radar-weight 25
	:last-name "Santiago"
	:person-index 187
	:primary-phone-number "412 281 5052"
	:vip nil
	:email-address "jls2@ardra.org"
)

(new-radar-person "Christine Johnston"
	:first-name "Christine"
	:radar-weight 25
	:last-name "Johnston"
	:person-index 188
	:primary-phone-number "412 281 5054"
	:vip nil
	:email-address "cjohnston@ardra.org"
)

(new-radar-person "Emily Halwizer"
	:first-name "Emily"
	:radar-weight 25
	:last-name "Halwizer"
	:person-index 189
	:primary-phone-number "412 281 5056"
	:vip nil
	:email-address "halwizer@ardra.org"
)

(new-radar-person "Dennis Parrott"
	:first-name "Dennis"
	:radar-weight 25
	:last-name "Parrott"
	:person-index 190
	:primary-phone-number "412 281 5058"
	:vip nil
	:email-address "dparrott@ardra.org"
)

(new-radar-person "Bob Halstear"
	:first-name "Bob"
	:radar-weight 25
	:last-name "Halstear"
	:person-index 191
	:primary-phone-number "412 281 5060"
	:vip nil
	:email-address "halstear@ardra.org"
)

(new-radar-person "Daniel Hendridge"
	:first-name "Daniel"
	:radar-weight 25
	:last-name "Hendridge"
	:person-index 192
	:primary-phone-number "412 281 5062"
	:vip nil
	:email-address "dhendridge@ardra.org"
)

(new-radar-person "Andreas Schaeffer"
	:first-name "Andreas"
	:radar-weight 25
	:last-name "Schaeffer"
	:person-index 193
	:primary-phone-number "412 281 5064"
	:vip nil
	:email-address "andreass@ardra.org"
)

(new-radar-person "Gene Hawkings"
	:first-name "Gene"
	:radar-weight 25
	:last-name "Hawkings"
	:person-index 194
	:primary-phone-number "412 281 5066"
	:vip nil
	:email-address "hawkings@ardra.org"
)

(new-radar-person "Troy Costales"
	:first-name "Troy"
	:radar-weight 25
	:last-name "Costales"
	:person-index 195
	:primary-phone-number "412 281 5068"
	:vip nil
	:email-address "troy@ardra.org"
)

(new-radar-person "Phillip Hardunn"
	:first-name "Phillip"
	:radar-weight 25
	:last-name "Hardunn"
	:person-index 196
	:primary-phone-number "412 281 5070"
	:vip nil
	:email-address "pmhardunn@ardra.org"
)

(new-radar-person "Fred Nampton"
	:first-name "Fred"
	:radar-weight 25
	:last-name "Nampton"
	:person-index 197
	:primary-phone-number "412 281 5072"
	:vip nil
	:email-address "fnampton@ardra.org"
)

(new-radar-person "Raquel Diano"
	:first-name "Raquel"
	:radar-weight 25
	:last-name "Diano"
	:person-index 198
	:primary-phone-number "412 281 5074"
	:vip nil
	:email-address "raquel@ardra.org"
)

(new-radar-person "John Drexel"
	:first-name "John"
	:radar-weight 25
	:last-name "Drexel"
	:person-index 199
	:primary-phone-number "412 281 5076"
	:vip nil
	:email-address "jdrex@ardra.org"
)

(new-radar-person "Debbie Cotton"
	:first-name "Debbie"
	:radar-weight 25
	:last-name "Cotton"
	:person-index 200
	:primary-phone-number "412 281 5078"
	:vip nil
	:email-address "dfcotton@ardra.org"
)

(new-radar-person "Elise Steiner"
	:first-name "Elise"
	:radar-weight 25
	:last-name "Steiner"
	:person-index 201
	:primary-phone-number "412 281 5080"
	:vip nil
	:email-address "elises@ardra.org"
)

(new-radar-person "Hammond Hughes"
	:first-name "Hammond"
	:radar-weight 25
	:last-name "Hughes"
	:person-index 202
	:primary-phone-number "412 281 5082"
	:vip nil
	:email-address "hammond@ardra.org"
)

(new-radar-person "Gina Bertolucci"
	:first-name "Gina"
	:radar-weight 25
	:last-name "Bertolucci"
	:person-index 203
	:primary-phone-number "412 281 5084"
	:vip nil
	:email-address "bertolucci@ardra.org"
)

(new-radar-person "Mark Abokowicz"
	:first-name "Mark"
	:radar-weight 25
	:last-name "Abokowicz"
	:person-index 204
	:primary-phone-number "412 281 5086"
	:vip nil
	:email-address "abokowicz@ardra.org"
)

(new-radar-person "Dan Murray"
	:first-name "Dan"
	:radar-weight 25
	:last-name "Murray"
	:person-index 205
	:primary-phone-number "412 281 5088"
	:vip nil
	:email-address "dgmurray@ardra.org"
)

(new-radar-person "Donald Reeves"
	:first-name "Donald"
	:radar-weight 25
	:last-name "Reeves"
	:person-index 206
	:primary-phone-number "412 281 5090"
	:vip nil
	:email-address "donaldr@ardra.org"
)

(new-radar-person "Henry Watson"
	:first-name "Henry"
	:radar-weight 25
	:last-name "Watson"
	:person-index 207
	:primary-phone-number "412 281 5092"
	:vip nil
	:email-address "hmw@ardra.org"
)

(new-radar-person "Phillipe Defrano"
	:first-name "Phillipe"
	:radar-weight 25
	:last-name "Defrano"
	:person-index 208
	:primary-phone-number "412 281 5094"
	:vip nil
	:email-address "philliped@ardra.org"
)

(new-radar-person "Judith Payne"
	:first-name "Judith"
	:radar-weight 25
	:last-name "Payne"
	:person-index 209
	:primary-phone-number "412 281 5096"
	:vip nil
	:email-address "jepayne@arda.com"
)

(new-radar-person "Sean Martin"
	:first-name "Sean"
	:radar-weight 25
	:last-name "Martin"
	:person-index 210
	:primary-phone-number "412 281 2563"
	:vip nil
	:email-address "smartin@ardra.org"
)

(new-radar-person "Howard Richards"
	:first-name "Howard"
	:radar-weight 25
	:last-name "Richards"
	:person-index 211
	:primary-phone-number "412 281 2565"
	:vip nil
	:email-address "howard@ardra.org"
)

(new-radar-person "Casper Miles"
	:first-name "Casper"
	:radar-weight 25
	:last-name "Miles"
	:person-index 212
	:primary-phone-number "412 281 2567"
	:vip nil
	:email-address "cmiles@ardra.org"
)

(new-radar-person "Francine Stimmel"
	:first-name "Francine"
	:radar-weight 25
	:last-name "Stimmel"
	:person-index 213
	:primary-phone-number "412 281 2569"
	:vip nil
	:email-address "frstimmel@ardra.org"
)

(new-radar-person "Terrence Porteus"
	:first-name "Terrence"
	:radar-weight 25
	:last-name "Porteus"
	:person-index 214
	:primary-phone-number "412 281 2571"
	:vip nil
	:email-address "tporteus@ardra.org"
)

(new-radar-person "Dino Smith"
	:first-name "Dino"
	:radar-weight 25
	:last-name "Smith"
	:person-index 215
	:primary-phone-number "412 281 2573"
	:vip nil
	:email-address "dinos@ardra.org"
)

(new-radar-person "Christina Santiago"
	:first-name "Christina"
	:radar-weight 25
	:last-name "Santiago"
	:person-index 216
	:primary-phone-number "412 281 2575"
	:vip nil
	:email-address "csantiago@ardra.org"
)

(new-radar-person "Charline Spring"
	:first-name "Charline"
	:radar-weight 25
	:last-name "Spring"
	:person-index 217
	:primary-phone-number "412 281 2577"
	:vip nil
	:email-address "cspring@ardra.org"
)

(new-radar-person "Jordan Taylor"
	:first-name "Jordan"
	:radar-weight 25
	:last-name "Taylor"
	:person-index 218
	:primary-phone-number "412 281 2579"
	:vip nil
	:email-address "jordant@ardra.org"
)

(new-radar-person "David Thompson"
	:first-name "David"
	:radar-weight 25
	:last-name "Thompson"
	:person-index 219
	:primary-phone-number "412 281 2581"
	:vip nil
	:email-address "dthomps@ardra.org"
)

(new-radar-person "Deborah Young"
	:first-name "Deborah"
	:radar-weight 25
	:last-name "Young"
	:person-index 220
	:primary-phone-number "412 281 2583"
	:vip nil
	:email-address "dyoung@ardra.org"
)

(new-radar-person "Aaron Stein"
	:first-name "Aaron"
	:radar-weight 25
	:last-name "Stein"
	:person-index 221
	:primary-phone-number "412 281 2585"
	:vip nil
	:email-address "asteing@ardra.org"
)

(new-radar-person "Jonathon Miller"
	:first-name "Jonathon"
	:radar-weight 25
	:last-name "Miller"
	:person-index 222
	:primary-phone-number "412 281 2587"
	:vip nil
	:email-address "jmiller@ardra.org"
)

(new-radar-person "Eli Montrose"
	:first-name "Eli"
	:radar-weight 25
	:last-name "Montrose"
	:person-index 223
	:primary-phone-number "412 281 2589"
	:vip nil
	:email-address "elim@ardra.org"
)

(new-radar-person "Patti Stevens"
	:first-name "Patti"
	:radar-weight 25
	:last-name "Stevens"
	:person-index 224
	:primary-phone-number "412 281 2591"
	:vip nil
	:email-address "pattis@ardra.org"
)

(new-radar-person "Margaret Hilldale"
	:first-name "Margaret"
	:radar-weight 25
	:last-name "Hilldale"
	:person-index 225
	:primary-phone-number "412 281 2593"
	:vip nil
	:email-address "mhilldale@ardra.org"
)

(new-radar-person "George Peterson"
	:first-name "George"
	:radar-weight 25
	:last-name "Peterson"
	:person-index 226
	:primary-phone-number "412 281 2595"
	:vip nil
	:email-address "gpeterso@ardra.org"
)

(new-radar-person "Estelle Giles"
	:first-name "Estelle"
	:radar-weight 25
	:last-name "Giles"
	:person-index 227
	:primary-phone-number "412 281 2597"
	:vip nil
	:email-address "estelle@ardra.org"
)

(new-radar-person "Lillie Furman"
	:first-name "Lillie"
	:radar-weight 25
	:last-name "Furman"
	:person-index 228
	:primary-phone-number "412 281 2599"
	:vip nil
	:email-address "lfurma@ardra.org"
)

(new-radar-person "Arthur Peters"
	:first-name "Arthur"
	:radar-weight 25
	:last-name "Peters"
	:person-index 229
	:primary-phone-number "412 281 2601"
	:vip nil
	:email-address "apeters@ardra.org"
)

(new-radar-person "Violo Watkins"
	:first-name "Violo"
	:radar-weight 25
	:last-name "Watkins"
	:person-index 230
	:primary-phone-number "412 281 2603"
	:vip nil
	:email-address "watkins@ardra.org"
)

(new-radar-person "Mitchell Blakeslee"
	:first-name "Mitchell"
	:radar-weight 25
	:last-name "Blakeslee"
	:person-index 231
	:primary-phone-number "412 281 2605"
	:vip nil
	:email-address "blakeslee@ardra.org"
)

(new-radar-person "Lisa Albough"
	:first-name "Lisa"
	:radar-weight 25
	:last-name "Albough"
	:person-index 232
	:primary-phone-number "412 281 2607"
	:vip nil
	:email-address "lalbough@ardra.org"
)

(new-radar-person "Jeffrey Winslow"
	:first-name "Jeffrey"
	:radar-weight 25
	:last-name "Winslow"
	:person-index 233
	:primary-phone-number "412 281 2609"
	:vip nil
	:email-address "jwinslow@ardra.org"
)

(new-radar-person "Charlotte Carlson"
	:first-name "Charlotte"
	:radar-weight 25
	:last-name "Carlson"
	:person-index 234
	:primary-phone-number "412 281 2611"
	:vip nil
	:email-address "ccarlson@ardra.org"
)

(new-radar-person "Margie Bradshaw"
	:first-name "Margie"
	:radar-weight 25
	:last-name "Bradshaw"
	:person-index 235
	:primary-phone-number "412 281 2613"
	:vip nil
	:email-address "mbradsha@ardra.org"
)

(new-radar-person "Frank Tribeza"
	:first-name "Frank"
	:radar-weight 25
	:last-name "Tribeza"
	:person-index 236
	:primary-phone-number "412 281 2615"
	:vip nil
	:email-address "ftribeza@ardra.org"
)

(new-radar-person "Mathias Croft"
	:first-name "Mathias"
	:radar-weight 25
	:last-name "Croft"
	:person-index 237
	:primary-phone-number "412 281 2617"
	:vip nil
	:email-address "mcroft@ardra.org"
)

(new-radar-person "Jerome Walters"
	:first-name "Jerome"
	:radar-weight 25
	:last-name "Walters"
	:person-index 238
	:primary-phone-number "412 281 2619"
	:vip nil
	:email-address "jwalters@ardra.org"
)

(new-radar-person "Patrick Stanwix"
	:first-name "Patrick"
	:radar-weight 25
	:last-name "Stanwix"
	:person-index 239
	:primary-phone-number "412 281 2621"
	:vip nil
	:email-address "pstanwix@ardra.org"
)

(new-radar-person "Tricia McMurray"
	:first-name "Tricia"
	:radar-weight 25
	:last-name "McMurray"
	:person-index 240
	:primary-phone-number "412 281 2623"
	:vip nil
	:email-address "mcmurray@ardra.org"
)

(new-radar-person "Elizabeth Stanton"
	:first-name "Elizabeth"
	:radar-weight 25
	:last-name "Stanton"
	:person-index 241
	:primary-phone-number "412 281 2625"
	:vip nil
	:email-address "estanton@ardra.org"
)

(new-radar-person "Garret Mitchell"
	:first-name "Garret"
	:radar-weight 25
	:last-name "Mitchell"
	:person-index 242
	:primary-phone-number "412 281 2627"
	:vip nil
	:email-address "gmitchell@ardra.org"
)

(new-radar-person "Nina Murray"
	:first-name "Nina"
	:radar-weight 25
	:last-name "Murray"
	:person-index 243
	:primary-phone-number "412 281 2629"
	:vip nil
	:email-address "nina@ardra.org"
)

(new-radar-person "Nolan Wagner"
	:first-name "Nolan"
	:radar-weight 25
	:last-name "Wagner"
	:person-index 244
	:primary-phone-number "412 281 2631"
	:vip nil
	:email-address "nwagner@ardra.org"
)

(new-radar-person "Gina Cargiullo"
	:first-name "Gina"
	:radar-weight 25
	:last-name "Cargiullo"
	:person-index 245
	:primary-phone-number "412 281 2633"
	:vip nil
	:email-address "cargiullo@ardra.org"
)

(new-radar-person "Dorren Waits"
	:first-name "Dorren"
	:radar-weight 25
	:last-name "Waits"
	:person-index 246
	:primary-phone-number "412 281 3132"
	:vip nil
	:email-address "dwaits@ardra.org"
)

(new-radar-person "Adam Constanza"
	:first-name "Adam"
	:radar-weight 25
	:last-name "Constanza"
	:person-index 247
	:primary-phone-number "412 281 3133"
	:vip nil
	:email-address "constanza@ardra.org"
)

(new-radar-person "Shin Yi"
	:first-name "Shin"
	:radar-weight 25
	:last-name "Yi"
	:person-index 248
	:primary-phone-number "412 281 3134"
	:vip nil
	:email-address "shin@ardra.org"
)

(new-radar-person "Trevor Masse"
	:first-name "Trevor"
	:radar-weight 25
	:last-name "Masse"
	:person-index 249
	:primary-phone-number "412 281 3135"
	:vip nil
	:email-address "tmasse@ardra.org"
)

(new-radar-person "Leonard Church"
	:first-name "Leonard"
	:radar-weight 25
	:last-name "Church"
	:person-index 250
	:primary-phone-number "412 281 3136"
	:vip nil
	:email-address "lchurch@ardra.org"
)

(new-radar-person "Tommy Hollis"
	:first-name "Tommy"
	:radar-weight 25
	:last-name "Hollis"
	:person-index 251
	:primary-phone-number "412 281 3137"
	:vip nil
	:email-address "thollis@ardra.org"
)

(new-radar-person "Karen Duchamp"
	:first-name "Karen"
	:radar-weight 25
	:last-name "Duchamp"
	:person-index 252
	:primary-phone-number "412 281 3138"
	:vip nil
	:email-address "duchamp@ardra.org"
)

(new-radar-person "Kris Vaughan"
	:first-name "Kris"
	:radar-weight 25
	:last-name "Vaughan"
	:person-index 253
	:primary-phone-number "412 281 3139"
	:vip nil
	:email-address "klv@ardra.org"
)

(new-radar-person "Jimmy Contoro"
	:first-name "Jimmy"
	:radar-weight 25
	:last-name "Contoro"
	:person-index 254
	:primary-phone-number "412 281 3140"
	:vip nil
	:email-address "jcontor@ardra.org"
)

(new-radar-person "China Elliot"
	:first-name "China"
	:radar-weight 25
	:last-name "Elliot"
	:person-index 255
	:primary-phone-number "412 281 3141"
	:vip nil
	:email-address "chinae@ardra.org"
)

(new-radar-person "Eva Masters"
	:first-name "Eva"
	:radar-weight 25
	:last-name "Masters"
	:person-index 256
	:primary-phone-number "412 281 3142"
	:vip nil
	:email-address "emasters@ardra.org"
)

(new-radar-person "Brad Mann"
	:first-name "Brad"
	:radar-weight 25
	:last-name "Mann"
	:person-index 257
	:primary-phone-number "412 281 3143"
	:vip nil
	:email-address "bmann@ardra.org"
)

(new-radar-person "Victoria Johnson"
	:first-name "Victoria"
	:radar-weight 25
	:last-name "Johnson"
	:person-index 258
	:primary-phone-number "412 281 3144"
	:vip nil
	:email-address "vej@ardra.org"
)

(new-radar-person "Stephanie Gabel"
	:first-name "Stephanie"
	:radar-weight 25
	:last-name "Gabel"
	:person-index 259
	:primary-phone-number "412 281 3145"
	:vip nil
	:email-address "sgabel@ardra.org"
)

(new-radar-person "Angelo Stienhardt"
	:first-name "Angelo"
	:radar-weight 25
	:last-name "Stienhardt"
	:person-index 260
	:primary-phone-number "412 281 3146"
	:vip nil
	:email-address "steinhardt@ardra.org"
)

(new-radar-person "David McCullar"
	:first-name "David"
	:radar-weight 25
	:last-name "McCullar"
	:person-index 261
	:primary-phone-number "412 281 3147"
	:vip nil
	:email-address "mccullar@ardra.org"
)

(new-radar-person "Seth Kubicek"
	:first-name "Seth"
	:radar-weight 25
	:last-name "Kubicek"
	:person-index 262
	:primary-phone-number "412 281 3148"
	:vip nil
	:email-address "kubicek@ardra.org"
)

(new-radar-person "Steven Bose"
	:first-name "Steven"
	:radar-weight 25
	:last-name "Bose"
	:person-index 263
	:primary-phone-number "412 281 3149"
	:vip nil
	:email-address "slbose@ardra.org"
)

(new-radar-person "Jason Hogan"
	:first-name "Jason"
	:radar-weight 25
	:last-name "Hogan"
	:person-index 264
	:primary-phone-number "412 281 3150"
	:vip nil
	:email-address "jmhogan@ardra.org"
)

(new-radar-person "Jason Rios"
	:first-name "Jason"
	:radar-weight 25
	:last-name "Rios"
	:person-index 265
	:primary-phone-number "412 281 3151"
	:vip nil
	:email-address "jlwrios@ardra.org"
)

(new-radar-person "Lynn Barclay"
	:first-name "Lynn"
	:radar-weight 25
	:last-name "Barclay"
	:person-index 266
	:primary-phone-number "412 281 3152"
	:vip nil
	:email-address "lbarc@ardra.org"
)

(new-radar-person "Katie Graham"
	:first-name "Katie"
	:radar-weight 25
	:last-name "Graham"
	:person-index 267
	:primary-phone-number "412 281 3153"
	:vip nil
	:email-address "kgraham@ardra.org"
)

(new-radar-person "Sarah Chesney"
	:first-name "Sarah"
	:radar-weight 25
	:last-name "Chesney"
	:person-index 268
	:primary-phone-number "412 281 3154"
	:vip nil
	:email-address "schesney@ardra.org"
)

(new-radar-person "Jane Liebrock"
	:first-name "Jane"
	:radar-weight 25
	:last-name "Liebrock"
	:person-index 269
	:primary-phone-number "412 281 3155"
	:vip nil
	:email-address "jjl@ardra.org"
)

(new-radar-person "Boomer Butler"
	:first-name "Boomer"
	:radar-weight 25
	:last-name "Butler"
	:person-index 270
	:primary-phone-number "412 281 3156"
	:vip nil
	:email-address "boom@ardra.org"
)

(new-radar-person "Austin Jaffe"
	:first-name "Austin"
	:radar-weight 25
	:last-name "Jaffe"
	:person-index 271
	:primary-phone-number "412 281 3157"
	:vip nil
	:email-address "austinj@ardra.org"
)

(new-radar-person "Joel Nilene"
	:first-name "Joel"
	:radar-weight 25
	:last-name "Nilene"
	:person-index 272
	:primary-phone-number "412 281 3158"
	:vip nil
	:email-address "jonile@ardra.org"
)

(new-radar-person "Miriam Dunn"
	:first-name "Miriam"
	:radar-weight 25
	:last-name "Dunn"
	:person-index 273
	:primary-phone-number "412 281 3159"
	:vip nil
	:email-address "mdunn@ardra.org"
)

(new-radar-person "Austin Parton"
	:first-name "Austin"
	:radar-weight 25
	:last-name "Parton"
	:person-index 274
	:primary-phone-number "412 281 3160"
	:vip nil
	:email-address "aparton@ardra.org"
)

(new-radar-person "Ashley Guinne"
	:first-name "Ashley"
	:radar-weight 25
	:last-name "Guinne"
	:person-index 275
	:primary-phone-number "412 281 3161"
	:vip nil
	:email-address "agguinne@ardra.org"
)

(new-radar-person "Taylor Childress"
	:first-name "Taylor"
	:radar-weight 25
	:last-name "Childress"
	:person-index 276
	:primary-phone-number "412 281 3162"
	:vip nil
	:email-address "tchildre@ardra.org"
)

(new-radar-person "Leah Davies"
	:first-name "Leah"
	:radar-weight 25
	:last-name "Davies"
	:person-index 277
	:primary-phone-number "412 281 3163"
	:vip nil
	:email-address "ldavies@ardra.org"
)

(new-radar-person "Trevor Rodgriguez"
	:first-name "Trevor"
	:radar-weight 25
	:last-name "Rodgriguez"
	:person-index 278
	:primary-phone-number "412 281 3164"
	:vip nil
	:email-address "trevor@ardra.org"
)

(new-radar-person "Dan Gold"
	:first-name "Dan"
	:radar-weight 25
	:last-name "Gold"
	:person-index 279
	:primary-phone-number "412 281 3165"
	:vip nil
	:email-address "dgold@ardra.org"
)

(new-radar-person "Ellen Green"
	:first-name "Ellen"
	:radar-weight 25
	:last-name "Green"
	:person-index 280
	:primary-phone-number "412 281 3166"
	:vip nil
	:email-address "ewg@ardra.org"
)

(new-radar-person "Lorraine Baker"
	:first-name "Lorraine"
	:radar-weight 25
	:last-name "Baker"
	:person-index 281
	:primary-phone-number "412 281 3167"
	:vip nil
	:email-address "lbaker@ardra.org"
)

(new-radar-person "Sam Ketterson"
	:first-name "Sam"
	:radar-weight 25
	:last-name "Ketterson"
	:person-index 282
	:vip nil
)

(new-radar-person "Stuart Weitzmann"
	:first-name "Stuart"
	:radar-weight 100
	:last-name "Weitzmann"
	:person-index 283
	:vip t
	:email-address "sweitzmann@gmail.com"
)

;;; Generated automatically from file ardra/locations.www.sql
(new-radar-room "Connan"
	:room-index 1
	:building {UC}
)

(new-radar-room "McConomy Auditorium"
	:room-index 2
	:building {UC}
)

(new-radar-room "UC 1987"
	:room-index 3
	:building {UC}
)

(new-radar-room "Pake"
	:room-index 4
	:building {UC}
)

(new-radar-room "Dowd"
	:room-index 5
	:building {UC}
)

(new-radar-room "Rangos 1/2"
	:room-index 6
	:building {UC}
)

(new-radar-room "Rangos 3"
	:room-index 8
	:building {UC}
)

(new-radar-room "Gym"
	:room-index 9
	:building {UC}
)

(new-radar-room "Conference A"
	:room-index 10
	:building {Flagstaff}
)

(new-radar-room "Conference B"
	:room-index 11
	:building {Flagstaff}
)

(new-radar-room "Conference C"
	:room-index 12
	:building {Flagstaff}
)

(new-radar-room "Conference D"
	:room-index 13
	:building {Flagstaff}
)

(new-radar-room "Conference E"
	:room-index 14
	:building {Flagstaff}
)

(new-radar-room "Conference Foyer"
	:room-index 16
	:building {Flagstaff}
)

(new-radar-room "Frick"
	:room-index 17
	:building {Flagstaff}
)

(new-radar-room "Phipps"
	:room-index 18
	:building {Flagstaff}
)

(new-radar-room "Oliver"
	:room-index 19
	:building {Flagstaff}
)

(new-radar-room "Vandergrift"
	:room-index 20
	:building {Flagstaff}
)

(new-radar-room "Laughlin"
	:room-index 21
	:building {Flagstaff}
)

(new-radar-room "Heinz"
	:room-index 22
	:building {Flagstaff}
)

(new-radar-room "Parkview West"
	:room-index 23
	:building {Flagstaff}
)

(new-radar-room "Parkview West Foyer"
	:room-index 24
	:building {Flagstaff}
)

(new-radar-room "Parkview East"
	:room-index 25
	:building {Flagstaff}
)

(new-radar-room "Carnegie I"
	:room-index 26
	:building {Flagstaff}
)

(new-radar-room "Carnegie II"
	:room-index 27
	:building {Flagstaff}
)

(new-radar-room "William Penn Ballroom"
	:room-index 28
	:building {Flagstaff}
)

(new-radar-room "Three Rivers"
	:room-index 29
	:building {Flagstaff}
)

(new-radar-room "Sternwheeler"
	:room-index 30
	:building {Flagstaff}
)

(new-radar-room "Riverboat"
	:room-index 31
	:building {Flagstaff}
)

(new-radar-room "Oakmont"
	:room-index 32
	:building {Flagstaff}
)

(new-radar-room "Fox Chapel"
	:room-index 33
	:building {Flagstaff}
)

(new-radar-room "Churchill"
	:room-index 34
	:building {Flagstaff}
)

(new-radar-room "Mt. Lebanon"
	:room-index 35
	:building {Flagstaff}
)

(new-radar-room "Sewickley"
	:room-index 36
	:building {Flagstaff}
)

(new-radar-room "Shadyside"
	:room-index 37
	:building {Flagstaff}
)

(new-radar-room "Paneled/Oval"
	:room-index 38
	:building {Flagstaff}
)

(new-radar-room "Grant Suite A"
	:room-index 39
	:building {Flagstaff}
)

(new-radar-room "Grant Suite B"
	:room-index 40
	:building {Flagstaff}
)

(new-radar-room "Grant Suite C"
	:room-index 41
	:building {Flagstaff}
)

(new-radar-room "Grant Suite Bar"
	:room-index 42
	:building {Flagstaff}
)

(new-radar-room "Grant Suite Foyer"
	:room-index 43
	:building {Flagstaff}
)

(new-radar-room "Grand Ballroom"
	:room-index 44
	:building {Flagstaff}
)

(new-radar-room "Urban"
	:room-index 46
	:building {Flagstaff}
)

(new-radar-room "Monongahela"
	:room-index 47
	:building {Flagstaff}
)

(new-radar-room "Allegheny"
	:room-index 48
	:building {Flagstaff}
)

(new-radar-room "Parlor D"
	:room-index 49
	:building {Flagstaff}
)

(new-radar-room "Parlor G"
	:room-index 50
	:building {Flagstaff}
)

(new-radar-room "Parlor E/F"
	:room-index 51
	:building {Flagstaff}
)

(new-radar-room "Sky"
	:room-index 52
	:building {Flagstaff}
)

(new-radar-room "Stever 1250"
	:room-index 53
	:building {Stever}
)

(new-radar-room "Stever 1260"
	:room-index 54
	:building {Stever}
)

(new-radar-room "Stever 1265"
	:room-index 55
	:building {Stever}
)

(new-radar-room "Stever 1270"
	:room-index 56
	:building {Stever}
)

(new-radar-room "Stever 1340"
	:room-index 57
	:building {Stever}
)

(new-radar-room "Stever 1345"
	:room-index 58
	:building {Stever}
)

(new-radar-room "Stever 1350"
	:room-index 59
	:building {Stever}
)

(new-radar-room "Stever 1420"
	:room-index 60
	:building {Stever}
)

(new-radar-room "Stever 1435"
	:room-index 61
	:building {Stever}
)

(new-radar-room "Stever 1440"
	:room-index 62
	:building {Stever}
)

(new-radar-room "Stever 2255"
	:room-index 63
	:building {Stever}
)

(new-radar-room "Stever 2260"
	:room-index 64
	:building {Stever}
)

(new-radar-room "Stever 2355"
	:room-index 65
	:building {Stever}
)

(new-radar-room "Stever 2420"
	:room-index 66
	:building {Stever}
)

(new-radar-room "Stever 2455"
	:room-index 67
	:building {Stever}
)

(new-radar-room "Stever 2450 A"
	:room-index 68
	:building {Stever}
)

(new-radar-room "Stever 2450 B"
	:room-index 69
	:building {Stever}
)

(new-radar-room "Stever 2450 C"
	:room-index 70
	:building {Stever}
)

(new-radar-room "Costa Auditorium"
	:room-index 71
	:building {Stever}
)

(new-radar-room "Dowd Auditorium"
	:room-index 72
	:building {Stever}
)

(new-radar-room "Landis Auditorium"
	:room-index 73
	:building {Stever}
)

(new-radar-room "Reception Foyer"
	:room-index 74
	:building {Stever}
)

(new-radar-room "Reception I"
	:room-index 75
	:building {Stever}
)

(new-radar-room "Reception II"
	:room-index 76
	:building {Stever}
)

(new-radar-room "Reception III/IV"
	:room-index 77
	:building {Stever}
)

(new-radar-room "grass"
	:room-index 79
)

(new-radar-room "McKenna"
	:room-index 80
	:building {UC}
)

(new-radar-room "Peter"
	:room-index 81
	:building {UC}
)

(new-radar-room "Wright"
	:room-index 82
	:building {UC}
)

(new-radar-room "Caffee"
	:room-index 89
	:building {UC}
)

(new-radar-room "Danforth"
	:room-index 90
	:building {UC}
)

(new-radar-room "Schatz Atrium"
	:room-index 91
	:building {UC}
)

(new-radar-room "Schatz Dining Room"
	:room-index 92
	:building {UC}
)

(new-radar-room "Skibo Coffeehouse"
	:room-index 93
	:building {UC}
)

(new-radar-room "Mehrabian 1001"
	:room-index 94
	:building {Mehrabian}
)

(new-radar-room "Mehrabian 1501"
	:room-index 95
	:building {Mehrabian}
)

(new-radar-room "Mehrabian 2005"
	:room-index 96
	:building {Mehrabian}
)

(new-radar-room "Mehrabian 2015"
	:room-index 97
	:building {Mehrabian}
)

(new-radar-room "Mehrabian 2505"
	:room-index 98
	:building {Mehrabian}
)

(new-radar-room "Mehrabian 2515"
	:room-index 99
	:building {Mehrabian}
)

;;; Generated automatically from file ardra/eventtypes.www.sql
;;; Generated automatically from file ardra/events.www.sql
(new-radar-gathering "Workshop 1a"
	:title "Intermodal Passenger Screening"
	:location {grass}
	:person-in-charge {Angelo Stienhardt}
	:gathering-index 1
	:types '( {workshop} {open gathering} )
)

(new-radar-gathering "Workshop 1b"
	:title "Intermodal Passenger Screening"
	:location {grass}
	:person-in-charge {Angelo Stienhardt}
	:gathering-index 2
	:types '( {workshop} {open gathering} )
)

(new-radar-gathering "Workshop 1c"
	:title "Intermodal Passenger Screening"
	:location {grass}
	:person-in-charge {Angelo Stienhardt}
	:gathering-index 3
	:types '( {workshop} {open gathering} )
)

(new-radar-gathering "Workshop 1d"
	:title "Intermodal Passenger Screening"
	:location {grass}
	:person-in-charge {Angelo Stienhardt}
	:gathering-index 4
	:types '( {workshop} {open gathering} )
)

(new-radar-gathering "Workshop 2a"
	:title "Advances in Roadway Maintenance"
	:location {grass}
	:person-in-charge {Steve Adams}
	:gathering-index 5
	:types '( {workshop} {open gathering} )
)

(new-radar-gathering "Workshop 2b"
	:title "Advances in Roadway Maintenance"
	:location {grass}
	:person-in-charge {Steve Adams}
	:gathering-index 6
	:types '( {workshop} {open gathering} )
)

(new-radar-gathering "Workshop 2c"
	:title "Advances in Roadway Maintenance"
	:location {grass}
	:person-in-charge {Steve Adams}
	:gathering-index 7
	:types '( {workshop} {open gathering} )
)

(new-radar-gathering "Workshop 2d"
	:title "Advances in Roadway Maintenance"
	:location {grass}
	:person-in-charge {Steve Adams}
	:gathering-index 8
	:types '( {workshop} {open gathering} )
)

(new-radar-gathering "Tutorial 1a"
	:title "Multi-Agency Fare Collection"
	:location {grass}
	:person-in-charge {Ben Steigerwald}
	:gathering-index 9
	:types '( {tutorial} {open gathering} )
)

(new-radar-gathering "Tutorial 1b"
	:title "Multi-Agency Fare Collection"
	:location {grass}
	:person-in-charge {Ben Steigerwald}
	:gathering-index 10
	:types '( {tutorial} {open gathering} )
)

(new-radar-gathering "Tutorial 2a"
	:title "The ADA and Terminal Design"
	:location {grass}
	:person-in-charge {Martha Pialino}
	:gathering-index 11
	:types '( {tutorial} {open gathering} )
)

(new-radar-gathering "Tutorial 2b"
	:title "The ADA and Terminal Design"
	:location {grass}
	:person-in-charge {Martha Pialino}
	:gathering-index 12
	:types '( {tutorial} {open gathering} )
)

(new-radar-gathering "Tutorial 3a"
	:title "Does Thruway Congestion Matter?"
	:location {grass}
	:person-in-charge {Colleen Undranko}
	:gathering-index 13
	:types '( {tutorial} {open gathering} )
)

(new-radar-gathering "Tutorial 3b"
	:title "Does Thruway Congestion Matter?"
	:location {grass}
	:person-in-charge {Colleen Undranko}
	:gathering-index 14
	:types '( {tutorial} {open gathering} )
)

(new-radar-gathering "Tutorial 4a"
	:title "Variable Toll Collection"
	:location {grass}
	:person-in-charge {Eric Musacelo}
	:gathering-index 15
	:types '( {tutorial} {open gathering} )
)

(new-radar-gathering "Tutorial 4b"
	:title "Variable Toll Collection"
	:location {grass}
	:person-in-charge {Eric Musacelo}
	:gathering-index 16
	:types '( {tutorial} {open gathering} )
)

(new-radar-gathering "Plenary 1"
	:title "ARDRA Presidential Address"
	:location {grass}
	:person-in-charge {Andrea Manson}
	:gathering-index 17
	:types '( {plenary} {open gathering} )
)

(new-radar-gathering "Keynote"
	:title "Advanced Transportation and Market Forces"
	:location {grass}
	:person-in-charge {Stuart Weitzmann}
	:gathering-index 18
	:types '( {keynote} {open gathering} )
)

(new-radar-gathering "Demo A1"
	:title "Collision Warning Systems"
	:location {grass}
	:person-in-charge {Michael Winslow}
	:gathering-index 19
	:types '( {demo} {open gathering} )
)

(new-radar-gathering "Panel A1"
	:title "Legal Aspects of Adaptive Cruise Control"
	:location {grass}
	:person-in-charge {David McCullar}
	:gathering-index 20
	:types '( {panel} {open gathering} )
)

(new-radar-gathering "Panel C1"
	:title "Hours of Service and Fatigue"
	:location {grass}
	:person-in-charge {Seth Kubicek}
	:gathering-index 21
	:types '( {panel} {open gathering} )
)

(new-radar-gathering "Panel T1"
	:title "Contactless Smart Cards"
	:location {grass}
	:person-in-charge {Steven Bose}
	:gathering-index 22
	:types '( {panel} {open gathering} )
)

(new-radar-gathering "Poster C1"
	:title "Commercial Vehicle Operations"
	:location {grass}
	:person-in-charge {Tom Whittenburg}
	:gathering-index 23
	:types '( {poster session} {open gathering} )
)

(new-radar-gathering "Poster M1"
	:title "Mixed Posters"
	:location {grass}
	:person-in-charge {Tom Whittenburg}
	:gathering-index 24
	:types '( {poster session} {open gathering} )
)

(new-radar-gathering "Session A1"
	:title "Multimedia Telematics"
	:location {grass}
	:person-in-charge {Jason Hogan}
	:gathering-index 25
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session C1"
	:title "In-Vehicle Information Systems"
	:location {grass}
	:person-in-charge {Jason Rios}
	:gathering-index 26
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session P1"
	:title "Highway Design and Local Communities"
	:location {grass}
	:person-in-charge {Lynn Barclay}
	:gathering-index 27
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session P2"
	:title "Successful Planning Strategies"
	:location {grass}
	:person-in-charge {Katie Graham}
	:gathering-index 28
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session T1"
	:title "Multimodal Station Design"
	:location {grass}
	:person-in-charge {Laura Timdale}
	:gathering-index 29
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session T2"
	:title "Bus Rapid Transit"
	:location {grass}
	:person-in-charge {Sarah Chesney}
	:gathering-index 30
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Plenary 2"
	:title "Federal Advanced Transportation Programs"
	:location {grass}
	:person-in-charge {Cara Ride}
	:gathering-index 31
	:types '( {plenary} {open gathering} )
)

(new-radar-gathering "Demo M1"
	:title "Driver Monitoring Systems"
	:location {grass}
	:person-in-charge {Michael Winslow}
	:gathering-index 32
	:types '( {demo} {open gathering} )
)

(new-radar-gathering "Demo T1"
	:title "Passenger Information Systems"
	:location {grass}
	:person-in-charge {Michael Winslow}
	:gathering-index 33
	:types '( {demo} {open gathering} )
)

(new-radar-gathering "Panel A2"
	:title "Driver Distraction"
	:location {grass}
	:person-in-charge {Jane Liebrock}
	:gathering-index 34
	:types '( {panel} {open gathering} )
)

(new-radar-gathering "Panel O1"
	:title "55 or 65?"
	:location {grass}
	:person-in-charge {Boomer Butler}
	:gathering-index 35
	:types '( {panel} {open gathering} )
)

(new-radar-gathering "Poster P1"
	:title "Planning"
	:location {grass}
	:person-in-charge {Tom Whittenburg}
	:gathering-index 36
	:types '( {poster session} {open gathering} )
)

(new-radar-gathering "Session A2"
	:title "Cooperative Cruise Control"
	:location {grass}
	:person-in-charge {Austin Jaffe}
	:gathering-index 37
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session A3"
	:title "Safety Systems 1"
	:location {grass}
	:person-in-charge {Joel Nilene}
	:gathering-index 38
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session C2"
	:title "Drowsy Driving"
	:location {grass}
	:person-in-charge {Miriam Dunn}
	:gathering-index 39
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session M1"
	:title "Maize-Wing Best Papers"
	:location {grass}
	:person-in-charge {Sam Ketterson}
	:gathering-index 40
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session O1"
	:title "Incident Detection"
	:location {grass}
	:person-in-charge {Austin Parton}
	:gathering-index 41
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session T3"
	:title "Collision Warning Systems for Buses"
	:location {grass}
	:person-in-charge {Ashley Guinne}
	:gathering-index 42
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session T4"
	:title "Increasing Ridership via AVL"
	:location {grass}
	:person-in-charge {Taylor Childress}
	:gathering-index 43
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Plenary 3"
	:title "Why ITS Matters"
	:location {grass}
	:person-in-charge {Jonathon Robertson}
	:gathering-index 44
	:types '( {plenary} {open gathering} )
)

(new-radar-gathering "Demo M2"
	:title "In-Vehicle Entertainment Systems"
	:location {grass}
	:person-in-charge {Michael Winslow}
	:gathering-index 45
	:types '( {demo} {open gathering} )
)

(new-radar-gathering "Panel O2"
	:title "High Occupancy Vehicle Lanes"
	:location {grass}
	:person-in-charge {Leah Davies}
	:gathering-index 46
	:types '( {panel} {open gathering} )
)

(new-radar-gathering "Poster A1"
	:title "Automobiles"
	:location {grass}
	:person-in-charge {Tom Whittenburg}
	:gathering-index 47
	:types '( {poster session} {open gathering} )
)

(new-radar-gathering "Session A4"
	:title "Safety Systems 2"
	:location {grass}
	:person-in-charge {Trevor Rodgriguez}
	:gathering-index 48
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session M2"
	:title "Potpourri"
	:location {grass}
	:person-in-charge {Dan Gold}
	:gathering-index 49
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session C3"
	:title "Electronic Braking Systems"
	:location {grass}
	:person-in-charge {Ellen Green}
	:gathering-index 50
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session C4"
	:title "Tanker Trucks"
	:location {grass}
	:person-in-charge {Lorraine Baker}
	:gathering-index 51
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Session O2"
	:title "Truck Inspection"
	:location {grass}
	:person-in-charge {Stephanie Gabel}
	:gathering-index 52
	:types '( {technical paper session} {open gathering} )
)

(new-radar-gathering "Registration and Breakfast Mon"
	:title "Registration and Breakfast Monday"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 53
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Registration and Breakfast Tue"
	:title "Registration and Breakfast Tuesday"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 54
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Registration and Breakfast Wed"
	:title "Registration and Breakfast Wednesday"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 55
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Registration and Breakfast Thu"
	:title "Registration and Breakfast Thursday"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 56
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Conference Committee Mon"
	:title "Conference Committee Monday"
	:location {grass}
	:person-in-charge {Jonathon Robertson}
	:gathering-index 57
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Conference Committee Tue"
	:title "Conference Committee Tuesday"
	:location {grass}
	:person-in-charge {Jonathon Robertson}
	:gathering-index 58
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Conference Committee Wed"
	:title "Conference Committee Wednesday"
	:location {grass}
	:person-in-charge {Jonathon Robertson}
	:gathering-index 59
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Conference Committee Thu"
	:title "Conference Committee Thursday"
	:location {grass}
	:person-in-charge {Jonathon Robertson}
	:gathering-index 60
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Break Mon AM"
	:title "Break Monday AM"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 61
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Break Mon PM"
	:title "Break Monday PM"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 62
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Break Tue AM"
	:title "Break Tuesday AM"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 63
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Break Tue PM"
	:title "Break Tuesday PM"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 64
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Break Wed AM"
	:title "Break Wednesday AM"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 65
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Break Wed PM"
	:title "Break Wednesday PM"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 66
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Break Thu AM"
	:title "Break Thursday AM"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 67
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Break Thu PM"
	:title "Break Thursday PM"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 68
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Exhibits Setup"
	:title "Exhibits Setup"
	:location {grass}
	:person-in-charge {Natalie Cohee}
	:gathering-index 69
	:types '( {exhibit} {open gathering} )
)

(new-radar-gathering "Exhibits Tue"
	:title "Exhibits Tuesday"
	:location {grass}
	:person-in-charge {Natalie Cohee}
	:gathering-index 70
	:types '( {exhibit} {open gathering} )
)

(new-radar-gathering "Exhibits Wed"
	:title "Exhibits Wednesday"
	:location {grass}
	:person-in-charge {Natalie Cohee}
	:gathering-index 71
	:types '( {exhibit} {open gathering} )
)

(new-radar-gathering "Exhibits Thu"
	:title "Exhibits Thursday"
	:location {grass}
	:person-in-charge {Natalie Cohee}
	:gathering-index 72
	:types '( {exhibit} {open gathering} )
)

(new-radar-gathering "Exhibit Teardown"
	:title "Exhibit Teardown"
	:location {grass}
	:person-in-charge {Natalie Cohee}
	:gathering-index 73
	:types '( {exhibit} {open gathering} )
)

(new-radar-gathering "Exec Comm"
	:title "Exec Comm"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 75
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Special Interest 1"
	:title "Special Interest 1"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 76
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Special Interest 2"
	:title "Special Interest 2"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 77
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Special Interest 3"
	:title "Special Interest 3"
	:location {grass}
	:person-in-charge {Spence Pierro}
	:gathering-index 78
	:types '( {administrative} {open gathering} )
)

(new-radar-gathering "Student Social"
	:title "Student Social"
	:location {grass}
	:person-in-charge {Michael Winslow}
	:gathering-index 79
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Exhibitor Social 1"
	:title "Exhibitor Social 1"
	:location {grass}
	:person-in-charge {Eva Masters}
	:gathering-index 80
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Exhibitor Social 2"
	:title "Exhibitor Social 2"
	:location {grass}
	:person-in-charge {Brad Mann}
	:gathering-index 81
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Exhibitor Social 3"
	:title "Exhibitor Social 3"
	:location {grass}
	:person-in-charge {Victoria Johnson}
	:gathering-index 82
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Banquet"
	:title "Banquet"
	:location {grass}
	:person-in-charge {Michael Winslow}
	:gathering-index 83
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Reception"
	:title "Reception"
	:location {grass}
	:person-in-charge {Michael Winslow}
	:gathering-index 84
	:types '( {conference social event} {open gathering} )
)

(new-radar-gathering "Sponsor Social"
	:title "Sponsor Social"
	:location {grass}
	:person-in-charge {China Elliot}
	:gathering-index 85
	:types '( {conference social event} {open gathering} )
)

;;; Generated automatically from file ardra/presentations.www.sql
(new-radar-presentation "The Benefits of Seamless Transfers"
	:abstract "This paper addresses the good things that come from quick"
	:presentation-index 1
	:speaker {Sandra Nubanks}
	:subevent-of {Workshop 1a}
)

(new-radar-presentation "Bioterrorism and Large Scale Terminals"
	:abstract "Testing was done to see how feasible an escape from a large terminal would be in the case of an attack. The paper analyzes what can be done to prevent bioterrorism in terminals"
	:presentation-index 2
	:speaker {William Alexander}
	:subevent-of {Workshop 1a}
)

(new-radar-presentation "Spaceports: The Next Step"
	:abstract "This paper addresses how passengers will be monitored for boarding shuttles for space takeoff. More so than any other form of transportation"
	:presentation-index 3
	:speaker {Fabian Monoker}
	:subevent-of {Workshop 1b}
)

(new-radar-presentation "Legal Issues in Multi-Agency Teminals"
	:abstract "This study addresses cases that have been presented in regards to the advertising techniques and the company disputes of any nature that have occurred when many agents occupy the same terminal. Oftentimes the disputes have been about passenger handling when passengers use both agencies and sometimes unscrupulous tactics are used to gain more customers for one agency or another. What can be done? The paper discusses which terminals respond best to passenger to agency relationships and what are most common problems."
	:presentation-index 4
	:speaker {Thomas Bouvier}
	:subevent-of {Workshop 1b}
)

(new-radar-presentation "Data Privacy Concerns"
	:abstract "Especially in the world of the airline system the privacy of the passenger is risked for the sake of security. In the post 9/11 world we must be careful to maintain the balance between customer privacy and data that is needed to allow for safe travel. This study focuses on what data is largely accepted as relevant and which materials seem to be the work of profiling or even sometimes used more for advertising purposes than security ones. Proposes solutions as to who can access the passenger information and how to keep it sealed."
	:presentation-index 5
	:speaker {Elizabeth McManis}
	:subevent-of {Workshop 1c}
)

(new-radar-presentation "Advances in Metal Detectors"
	:abstract "New systems have been implemented that make metal detectors far more sensitive"
	:presentation-index 6
	:speaker {Gary Abramowitz}
	:subevent-of {Workshop 1c}
)

(new-radar-presentation "Logistics of Pedestrian Flow"
	:abstract "In order to implement new pedestrian road signs and other concessions for the everyday walker this study took a look at how pedestrians move and how they respond to the road signs and other near-road indications. The paper addresses a view of what causes pedestrian bottle-necks and helps to answer the age-old question: is the walk now light long enough? The result is to propose longer walk now lights"
	:presentation-index 7
	:speaker {Vivek Kahn}
	:subevent-of {Workshop 1d}
)

(new-radar-presentation "The Importance of Space in Terminal Retrofitting"
	:abstract "New technology has added one new problem that some overlook: space. Certain advances to make the terminals of yesterday go into the future take up a lot of the room that had previously been used to hold passengers when waiting in the terminal or had been used for other commercial purposes such as shops. The paper emphasizes: Space must not be compromised and cites observations from the world''s most crowded terminals as being the places where problems arise most frequently and customers are generally dissatisfied with their experiences."
	:presentation-index 8
	:speaker {Yoo Fong}
	:subevent-of {Workshop 1d}
)

(new-radar-presentation "A Survey on the Impact of Advanced Maintenance"
	:abstract "Over 1000 drivers were surveyed on how they responded to advanced maintenance"
	:presentation-index 9
	:speaker {Rachel Greensburg}
	:subevent-of {Workshop 2a}
)

(new-radar-presentation "GIS in Roadway Maintenance"
	:abstract "The geographical information system allows for roadways to be observed and to see how the weather and the lay of the land affect roadways over time. The GIS system allows for maintenance on roadways to occur with the most knowledge of the area. When the maintenance occurs with sympathy to the geography of an area than the repairs last longer and are much more effective."
	:presentation-index 10
	:speaker {Steve Adams}
	:subevent-of {Workshop 2a}
)

(new-radar-presentation "Flexible Concrete"
	:abstract "The trouble with concrete is"
	:presentation-index 11
	:speaker {Alexander Gallatin}
	:subevent-of {Workshop 2b}
)

(new-radar-presentation "Leveraging Ferrous Content in Bridges"
	:abstract "Bridges"
	:presentation-index 12
	:speaker {Vivica Wagner}
	:subevent-of {Workshop 2b}
)

(new-radar-presentation "Surface Diagnosis Via Aerial Maps"
	:abstract "The conditions of road areas could be determined from above based on the lay of the land. However"
	:presentation-index 13
	:speaker {Jason O\'Reilly}
	:subevent-of {Workshop 2c}
)

(new-radar-presentation "Laser Saws for Asphalt Repair"
	:abstract "Lasers have been used for protection systems and for eye surgeries at a large rate in the past 10 years or so. Now lasers could be implemented in repairing the roads around. This study shows how lasers could be used in road repair because they are better for making fine incisions which then can be filled and strengthened so that the road will not suffer more damage. The lasers"
	:presentation-index 14
	:speaker {Shriya Shaw}
	:subevent-of {Workshop 2c}
)

(new-radar-presentation "Fuel Cell Powered Signal Lamps"
	:abstract "The paper addressed the problems that arise when signal lamps go down due to bulb problems or local electricity problems. The solution proposed"
	:presentation-index 15
	:speaker {Becky Green}
	:subevent-of {Workshop 2d}
)

(new-radar-presentation "Automated Fog Detection and Mitigation"
	:abstract "Navigation problems arise in the fog"
	:presentation-index 16
	:speaker {Maggie Foxenreiter}
	:subevent-of {Workshop 2d}
)

(new-radar-presentation "The New Mexico ACC Deployment"
	:abstract "Demonstrates the effects of a New Mexico test on cruise controls which senses the best responses to the surroundings. The new form of cruise control determines when it is best to slow down a bit"
	:presentation-index 17
	:speaker {Brianna Smith}
	:subevent-of {Demo A1}
)

(new-radar-presentation "Warning Using Seat Cushions"
	:abstract "The seat cushion system comes from San Francisco laboratories"
	:presentation-index 18
	:speaker {Sonal Malhotra}
	:subevent-of {Demo A1}
)

(new-radar-presentation "LIDAR-Based Side Collision Avoidance"
	:abstract "LIDAR is something like radar. A test of LIDAR in the field shows how the detection system works"
	:presentation-index 19
	:speaker {Tal Havartza}
	:subevent-of {Demo A1}
)

(new-radar-presentation "3D Sound Warnings"
	:abstract "Everyone is acquainted with the lights that turn on to check a check engine light. The program implemented and studied here was a combination of slight 3-D images and sound warnings when a collision was likely or taking place. The car can react to a collision before the driver may be able to"
	:presentation-index 20
	:speaker {Susan Mikhal}
	:subevent-of {Demo A1}
)

(new-radar-presentation "Drowsiness as a Function of Vehicle Type"
	:abstract "Drivers"
	:presentation-index 33
	:speaker {Timothy Margilois}
	:subevent-of {Poster C1}
)

(new-radar-presentation "Composite Materials for Green Trailer Construction"
	:abstract "Composite Materials for Green Trailer Construction"
	:presentation-index 34
	:speaker {Frank Ionola}
	:subevent-of {Poster C1}
)

(new-radar-presentation "Eco-Friendly Truck Tire Construction"
	:abstract "Eco-Friendly Truck Tire Construction"
	:presentation-index 35
	:speaker {Suzanne Marilios}
	:subevent-of {Poster C1}
)

(new-radar-presentation "Driver Monitoring Devices for CVO"
	:abstract "Driver Monitoring Devices for CVO"
	:presentation-index 36
	:speaker {Tina Fall}
	:subevent-of {Poster C1}
)

(new-radar-presentation "Roundabout Utilization by Articulated Trucks"
	:abstract "Roundabout Utilization by Articulated Trucks"
	:presentation-index 37
	:speaker {Ed Ivanovich}
	:subevent-of {Poster C1}
)

(new-radar-presentation "Regulatory Changes in CVO in South Africa"
	:abstract "Regulatory Changes in CVO in South Africa"
	:presentation-index 38
	:speaker {Westley Qualter}
	:subevent-of {Poster C1}
)

(new-radar-presentation "Armenian Experiences with Truck Emissions Improvements"
	:abstract "Armenian Experiences with Truck Emissions Improvements"
	:presentation-index 39
	:speaker {Ilise Nebuto}
	:subevent-of {Poster C1}
)

(new-radar-presentation "The Weekend Driver: The Effect of Part Time Drivers on CVO"
	:abstract "The Weekend Driver: The Effect of Part Time Drivers on CVO"
	:presentation-index 40
	:speaker {Dennis Johnson}
	:subevent-of {Poster C1}
)

(new-radar-presentation "Don''t Park Your Truck on Ice and Other Important CVO Lessons"
	:abstract "Don''t Park Your Truck on Ice and Other Important CVO Lessons"
	:presentation-index 41
	:speaker {Andy Sewally}
	:subevent-of {Poster C1}
)

(new-radar-presentation "The Impact of Truck Cab Noise on Hearing Loss"
	:abstract "The Impact of Truck Cab Noise on Hearing Loss"
	:presentation-index 42
	:speaker {Peter Havater}
	:subevent-of {Poster C1}
)

(new-radar-presentation "The Utility of Parabolic Mirrors for Forward Pedestrian Detection"
	:abstract "The Utility of Parabolic Mirrors for Forward Pedestrian Detection"
	:presentation-index 43
	:speaker {Yong Qian}
	:subevent-of {Poster C1}
)

(new-radar-presentation "Cost Effectiveness of Electronic Braking Systems for Dual Trailers"
	:abstract "Cost Effectiveness of Electronic Braking Systems for Dual Trailers"
	:presentation-index 44
	:speaker {Ola Neuen}
	:subevent-of {Poster C1}
)

(new-radar-presentation "Poor Performance of Ground Penetrating Radar for Pipe Surveys"
	:abstract "Poor Performance of Ground Penetrating Radar for Pipe Surveys"
	:presentation-index 45
	:speaker {Cathy Algowitz}
	:subevent-of {Poster M1}
)

(new-radar-presentation "Congestion Effects from Rear Seat DVD Players"
	:abstract "Congestion Effects from Rear Seat DVD Players"
	:presentation-index 46
	:speaker {Helmet Uter}
	:subevent-of {Poster M1}
)

(new-radar-presentation "Regulatory Forecasts for Passenger Entertainment Systems"
	:abstract "Regulatory Forecasts for Passenger Entertainment Systems"
	:presentation-index 47
	:speaker {Petra Svetski}
	:subevent-of {Poster M1}
)

(new-radar-presentation "Regional Transit Guidance on Handheld Devices"
	:abstract "Regional Transit Guidance on Handheld Devices"
	:presentation-index 48
	:speaker {Nina Idiz}
	:subevent-of {Poster M1}
)

(new-radar-presentation "Transit Buses as Congestion Probes"
	:abstract "Transit Buses as Congestion Probes"
	:presentation-index 49
	:speaker {Jana Padamanabahn}
	:subevent-of {Poster M1}
)

(new-radar-presentation "A Model of Double Parked Delivery Truck Induced Congestion"
	:abstract "A Model of Double Parked Delivery Truck Induced Congestion"
	:presentation-index 50
	:speaker {Mark Oriolos}
	:subevent-of {Poster M1}
)

(new-radar-presentation "Combiner Glass Windshields for Head-Up Display Imagery"
	:abstract "Combiner Glass Windshields for Head-Up Display Imagery"
	:presentation-index 51
	:speaker {Dana Johanson}
	:subevent-of {Poster M1}
)

(new-radar-presentation "Crashworthiness of Adaptive Equipment"
	:abstract "Crashworthiness of Adaptive Equipment"
	:presentation-index 52
	:speaker {Wanda Teven}
	:subevent-of {Poster M1}
)

(new-radar-presentation "Movie Rentals at the Pump"
	:abstract "Paper discusses the probability and possibility of an intricate sort of vending machine at gas stations. The goal is to create ways that the transportation industry can be a one stop shop. Unlike mega malls and grocery stores"
	:presentation-index 54
	:speaker {Larry Elry}
	:subevent-of {Session A1}
)

(new-radar-presentation "Speech Interfaces for In-Vehicle Internet"
	:abstract "How can the user use the internet in a safe and effective way during the drive? Many would say that they cannot but the discussion will challenge this view. With a speech interface linked to the car''s systems the car could deny access at unsafe times"
	:presentation-index 55
	:speaker {Deana Gona}
	:subevent-of {Session A1}
)

(new-radar-presentation "Using Bluetooth to Leverage a Driver''s Mobile Phone"
	:abstract "This paper addresses how the technology of Bluetooth can help the everyday cellphone using driver. Discusses how drivers might use a Bluetooth compatible phone to benefit them. If Bluetooth could be connected to both the phone and a device within the car"
	:presentation-index 56
	:speaker {Fiona Mansala}
	:subevent-of {Session A1}
)

(new-radar-presentation "Satellite Radio Adoption Rates by Motorists"
	:abstract "Studies and polls showing the amount of users who have acquired satellite radio in the past 2 years. Further studies on the demographics of users who have satellite radio versus those who do not. Also presents speculations on who is likely to want satellite radio now and how satellite radio affects the motorist versus traditional radio. Summaries of effects on moods towards the vehicle in general."
	:presentation-index 57
	:speaker {Ed Farvich}
	:subevent-of {Session A1}
)

(new-radar-presentation "Pilot Test of a Prototype Mileage Prediction System"
	:abstract "Shows the tests of a prototype system. The prototype is a variety of odometer that calculates mileage for a certain driver for a period of time based on previous times they have driven. Shows that if a driver drives an average of 30 miles per day the special odometer will calculate the predicted mileage at a rate of 30 miles per day. Goes into the technical aspects of how the system will link to the daily mileage database within the car."
	:presentation-index 58
	:speaker {Renee O\'Laughlin}
	:subevent-of {Session C1}
)

(new-radar-presentation "Acceptance Results of Driver-Initiated Monitoring Programs"
	:abstract "This paper details three processes of when the driver willingly monitors their progress as they drive. The tests discuss the comparison between the way driver''s monitor their own progress"
	:presentation-index 59
	:speaker {Tina Fall}
	:subevent-of {Session C1}
)

(new-radar-presentation "An In-Dash Computer for Tire Wear Management"
	:abstract "The purpose of the in-dash computer is to warn drivers early of a possible popped tire. This is not just convenient for warning against flats and preventing the problem"
	:presentation-index 60
	:speaker {Pete Fava}
	:subevent-of {Session C1}
)

(new-radar-presentation "The National Truck Stop Electronic Database"
	:abstract "In Iowa truckers have been experiencing a system that monitors progress and performance each time they stop at a truck stop. In the future"
	:presentation-index 61
	:speaker {Zachary Tonazu}
	:subevent-of {Session C1}
)

(new-radar-presentation "Sound Abatement Using Green Materials"
	:abstract "Oftentimes in small neighborhoods the largest problem in getting agreement for roadways is the problem of noise caused by nearby traffic. Previously"
	:presentation-index 62
	:speaker {Michael Reese}
	:subevent-of {Session P1}
)

(new-radar-presentation "Effects of Below-Grade Highways on Pedestrian Flow"
	:abstract "No one wants to walk around when there''s dirty exhaust in the air and the sound of booming semis on the wind. This study shows how far fewer people walk the sidewalks near and around bad highways"
	:presentation-index 63
	:speaker {Michael Winslow}
	:subevent-of {Session P1}
)

(new-radar-presentation "Congestion-Induced Health Effects in Neighboring Communities"
	:abstract "The classic example of Mexico City is beginning to emerge north of the border as well. In cities like LA and New York City the level of exhaust and trapped pollutants is growing each year. Patients of asthma"
	:presentation-index 64
	:speaker {Kelly Jovanovich}
	:subevent-of {Session P1}
)

(new-radar-presentation "The Impact of Exit Ramps on Commercial Development"
	:abstract "Commercial areas spring up on the highway just off the exit ramps. Sometimes"
	:presentation-index 65
	:speaker {Chian Lee}
	:subevent-of {Session P1}
)

(new-radar-presentation "Get on the Bus: Best Practices in Transit Planning"
	:abstract "Names the finest systems for mass transit among a list of 50 cities studied. The top three are used as models for what can be the most efficient mass transit systems. Some of the best practices are implementing an online system for getting a bus pass that means patrons don''t have to leave their home. Other ideas are setting up a website that logs bus progress so that patrons can check whether a bus is late or on time before they leave the house."
	:presentation-index 66
	:speaker {Shawanda North}
	:subevent-of {Session P2}
)

(new-radar-presentation "Implications of AHS on Arterial Congestion"
	:abstract "The automated highway system will allow drivers to have very few decisions while driving on the highway and will implement vast new technology to help control traffic. The trouble happens when arterial roads are then blocked by the massive inflow of traffic to an area where the automated system is not in place. Though the AHS has possible benefits to the highway system and controlling it in very busy areas"
	:presentation-index 67
	:speaker {Jack Petersen}
	:subevent-of {Session P2}
)

(new-radar-presentation "Feasibility of Automated Highways in Dedicated Truck Lanes"
	:abstract "Truck lanes are some of the most efficient forms of preventing congestion on highways because they prevent drivers from having to deal with slow moving trucks. Could the truck lanes benefit from AHS? The results would include a possible problem for truckers that have tight deadlines on the destinations they must reach"
	:presentation-index 68
	:speaker {Don Henry}
	:subevent-of {Session P2}
)

(new-radar-presentation "Penetration Rates of Hybrid Vehicles"
	:abstract "A consumer concern has been that hybrid vehicles are not as safe as traditional models. This study acknowledges the concerns: at what rates do hybrids encounter crashes that could be harmful to passengers inside? This study spans a three year period of which all crashes involving a hybrid were studied to see if the car is less safe than a traditional model and if passengers are more likely to suffer injuries in a crash involving a hybrid car."
	:presentation-index 69
	:speaker {Abigail Reeves}
	:subevent-of {Session P2}
)

(new-radar-presentation "The Shortest Distance Between Two Lines is Not a Hallway"
	:abstract "The system of roadways can result in a bogged down"
	:presentation-index 70
	:speaker {Calla Diller}
	:subevent-of {Session T1}
)

(new-radar-presentation "Passenger Flow Models for Transfer Stations"
	:abstract "The passengers of a vehicle can be monitored on major roadways to get an idea of total passengers on any road. The paper discusses how many passengers are on roadways at a given time"
	:presentation-index 71
	:speaker {Courtney Charles}
	:subevent-of {Session T1}
)

(new-radar-presentation "Preserving Sovereignty in Multi-Agency Stations"
	:abstract "There is no set way to determine how agencies will interact at train"
	:presentation-index 72
	:speaker {Katrina Aronza}
	:subevent-of {Session T1}
)

(new-radar-presentation "A Test Deployment of Bicycle Cars in Light Rail"
	:abstract "The test of bicycle cars was conducted last September at the monorail station in Savanna"
	:presentation-index 73
	:speaker {Laura Timdale}
	:subevent-of {Session T1}
)

(new-radar-presentation "Long-Term Ridership Data for Curritiba"
	:abstract "Analysis of riders on the busway for Curitiba Brazil. Curitiba"
	:presentation-index 74
	:speaker {Charles Derunn}
	:subevent-of {Session T2}
)

(new-radar-presentation "Quantification of Platform Boarding Efficiency"
	:abstract "Study presents data on boarding systems for train and bus systems. Boarding systems include turnstiles"
	:presentation-index 75
	:speaker {Michael Faye}
	:subevent-of {Session T2}
)

(new-radar-presentation "Successes and Failures of Signal Preemption"
	:abstract "The signal preemption system is a relatively new one to our streets. The idea is a unique one"
	:presentation-index 76
	:speaker {Bob Armstrong}
	:subevent-of {Session T2}
)

(new-radar-presentation "Flex Route Feeder Buses"
	:abstract "Urban and suburban sprawl means that one bus system in the city does not affect the greatest number of customers. The bus systems within this paper began a system of flex-route and feeder buses that allow the customers who live farther away from the center of the bus route. The flex route and feeder bus systems may increase how many customers use the bus route"
	:presentation-index 77
	:speaker {Richard Holley}
	:subevent-of {Session T2}
)

(new-radar-presentation "Single Camera Drowsiness Detection"
	:abstract "Drowsiness on the road is one of the biggest causes for accidents today. In order to catch the drowsiness of drivers"
	:presentation-index 78
	:speaker {Jun Lee}
	:subevent-of {Demo M1}
)

(new-radar-presentation "A Hybrid Model of Driver Inattention"
	:abstract "A new model has been created to demonstrate the driver''s lack of attention behind the wheel. The model clearly defines the varying reasons that distract a drivers attention and also analyzes what the driver is thinking at the time. Why was he interested in the distraction? What drew his attention? Is there any way he could get out of it? This model clarifies the reasons behind drivers'' distractions on the road."
	:presentation-index 79
	:speaker {Taylor Killian}
	:subevent-of {Demo M1}
)

(new-radar-presentation "Virtual Backseat Driving"
	:abstract "Virtual Backseat Driving will be the next best idea for the entire society. It will help out the adults and entertain the children. By including this invention in the car of every individual"
	:presentation-index 80
	:speaker {Scott Scheurman}
	:subevent-of {Demo M1}
)

(new-radar-presentation "Low-Cost Sensors for Appropriate Airbag Deployment"
	:abstract "A car accident is worsened by the one thing that is supposed to protect the passengers from the impact"
	:presentation-index 81
	:speaker {Laron Dale}
	:subevent-of {Demo M1}
)

(new-radar-presentation "Bus Arrival Time Prediction"
	:abstract "Although buses are scheduled to come at certain time periods"
	:presentation-index 82
	:speaker {Jarrod Michaels}
	:subevent-of {Demo T1}
)

(new-radar-presentation "ADA Compliant Stop Announcements"
	:abstract "Because of hostilities made towards disabled invidiuals"
	:presentation-index 83
	:speaker {Melissa Crosby}
	:subevent-of {Demo T1}
)

(new-radar-presentation "Assisted Wayfinding for Blind Passengers"
	:abstract "In order to help the blind"
	:presentation-index 84
	:speaker {Donald Allman}
	:subevent-of {Demo T1}
)

(new-radar-presentation "A Real-Time Telephone Information System"
	:abstract "Telephones are one of the communication applications that are advancing that fastest in the world. They combine all different types of applications that makes everyone''s lives much more comfortable away from the desk. The next goal for technicians is to install an information system into real-time telephones. By doing so, users can do more than communicate and save information in their little"
	:presentation-index 85
	:speaker {Julia Gonzalez}
	:subevent-of {Demo T1}
)

(new-radar-presentation "Advanced Transportation and Market Forces"
	:abstract "The paper focuses on the transportation of the future based on what we have seen in the transportation of today. What has happened to transportation in the past 5 years can be directly and indirectly linked to the forces of the consumer world. The paper will introduce an idea of what the transportation industry will look like based on today''s market. It will highlight the reasons why transportation today has come as far as it had"
	:presentation-index 86
	:speaker {Stuart Weitzmann}
	:subevent-of {Keynote}
)

(new-radar-presentation "An Analysis of Arterial Traffic Volume in Waterloo"
	:abstract "Ontario"
	:presentation-index 94
	:speaker {Juliet Frerking}
	:subevent-of {Poster P1}
)

(new-radar-presentation "A Mathematical Model for Estimating Adequate Parking Capacity"
	:abstract "Mathematicians have created a model that will solve all construction builders'' problems with parking lots. No matter how large the parking lot is built"
	:presentation-index 95
	:speaker {Thomas Collins}
	:subevent-of {Poster P1}
)

(new-radar-presentation "Community Response to Low Pressure Sodium Street Lamps"
	:abstract "The community has had a mixed response to low pressure sodium street lamps. They have seen that it saves the community more money by using them than the standard sodium street lamps. However"
	:presentation-index 96
	:speaker {Britt Daniel}
	:subevent-of {Poster P1}
)

(new-radar-presentation "Energy Efficiency of Good Color Quality Street Lighting Lamps"
	:abstract "The amount of energy put into good color quality street lamps is much higher than any ordinary lighting lamp. Because the color in each light is so bright"
	:presentation-index 97
	:speaker {Vicenzo Calabreze}
	:subevent-of {Poster P1}
)

(new-radar-presentation "Safety Assessment Techniques for Center Median Parkways"
	:abstract "Center median parkways have the highest risks for causing parkway accidents. Therefore"
	:presentation-index 98
	:speaker {Teo Ciccoli}
	:subevent-of {Poster P1}
)

(new-radar-presentation "Throughput Estimates for Automated Highway Systems"
	:abstract "For those of you who don''t know what throughput means"
	:presentation-index 99
	:speaker {Jack Petersen}
	:subevent-of {Poster P1}
)

(new-radar-presentation "Smog Reduction as a Result of AHS"
	:abstract "Since the beginning of Automated Highway Systems"
	:presentation-index 100
	:speaker {Lupe Bucca}
	:subevent-of {Poster P1}
)

(new-radar-presentation "Pittsburgh Ride Share Uptake Figures After Widespread Marketing"
	:abstract "Pittsburgh''s Ride transportations have been suffering much throughout the past years and have been struggling to increase its figures in many ways. Finally"
	:presentation-index 101
	:speaker {Jan Wilson}
	:subevent-of {Poster P1}
)

(new-radar-presentation "511 Usage Patterns in the District of Columbia"
	:abstract "In the District of Columbia"
	:presentation-index 102
	:speaker {Brenda Smallman}
	:subevent-of {Poster P1}
)

(new-radar-presentation "Flex Route Results for Helsinki Metro Transport"
	:abstract "Research has been conducted on the Helsinki Metro Transport to see how flexible the routes are for the drivers. In the past"
	:presentation-index 103
	:speaker {Richard Holley}
	:subevent-of {Poster P1}
)

(new-radar-presentation "String Stability for 20-Car Cooperative Cruise Control Platoons"
	:abstract "String stability"
	:presentation-index 104
	:speaker {Louisa Hollands}
	:subevent-of {Session A2}
)

(new-radar-presentation "ACC vs. CCC: Monte Carlo Simulations of String Stability"
	:abstract "Many wonder which is better- Adaptive or Cooperative Cruise Control? Well it depends. ACC utilizes a radar to see if an object is approaching too close to the vehicle while CCC uses communications to discover any objects in its path. As a result"
	:presentation-index 105
	:speaker {Jennifer Morris}
	:subevent-of {Session A2}
)

(new-radar-presentation "Modulated LED Tail Light Inter-Vehicle Communications"
	:abstract "Researchers and scientists have come to create LED tail lights that modulate for the use of inter-vehicle communications. This developement will help drivers communicate on the road through their tail lights. The LED lights will be proportional based on the distance between cars. So while cars are far apart from each other"
	:presentation-index 106
	:speaker {Angelica Antonias}
	:subevent-of {Session A2}
)

(new-radar-presentation "Wireless Requirements for Cooperative Cruise Control"
	:abstract "In order to have cruise control work towards the benefit of the driver"
	:presentation-index 107
	:speaker {Penelope Benold}
	:subevent-of {Session A2}
)

(new-radar-presentation "Implications of Recent NTSB Findings"
	:abstract "The National Transportation Safety Board have made some recent findings dealing with public transportations in main cities. The implications of these findings are that the NTSB might force new rules upon the conduct of bus drivers in the cities. They found some shocking information and experiences with bus drivers and their manners. The NTSB have realized that the conduct must change with the drivers and new rules may be created soon enough."
	:presentation-index 108
	:speaker {Stuart Milton}
	:subevent-of {Session A3}
)

(new-radar-presentation "The Braking Trap: How ACC Can Make Matters Worse"
	:abstract "Adaptive Cruise Control has proven to make things worse for drivers. Drivers have filed complaints about ACC because of how delayed the car breaks after ACC. Although the car is on its own and allows the driver to relax a bit"
	:presentation-index 109
	:speaker {Patricia Minor}
	:subevent-of {Session A3}
)

(new-radar-presentation "Stability Control for Four Wheel Steering"
	:abstract "Most cars that are on the road only have two wheel steering. The front two tires are the ones that guide the car in the driver''s desired direction. Researchers are looking into spreading the steering to all four wheels of the car. By doing so"
	:presentation-index 110
	:speaker {Staffan Bjorn}
	:subevent-of {Session A3}
)

(new-radar-presentation "Post-Crash Analysis of Stability Control Systems"
	:abstract "After much testing on the stability control systems"
	:presentation-index 111
	:speaker {Patrick Fitzsimmons}
	:subevent-of {Session A3}
)

(new-radar-presentation "The Impact of the New Hours of Service Rules"
	:abstract "Much of the community has complained about the change in service hours in public transportation. As a result"
	:presentation-index 112
	:speaker {Brian Car}
	:subevent-of {Session C2}
)

(new-radar-presentation "Trends in Highway Rest Areas"
	:abstract "Highway rest areas are the only places where drivers can pull over and rest without having to worry about where to park their vehicle or feeling scared on the road. With their toilets"
	:presentation-index 113
	:speaker {Lucas Zettle}
	:subevent-of {Session C2}
)

(new-radar-presentation "Low-Cost Drowsiness Detection"
	:abstract "Drowsiness is a hazard behind the wheel. Researchers have warned the community that it is almost worse than driving under the influence. Much of the community suffers from the lack of sleep and rest"
	:presentation-index 114
	:speaker {Jun Lee}
	:subevent-of {Session C2}
)

(new-radar-presentation "Less-Than-Load Drowsiness Crash Rates"
	:abstract "It has been proven that a large number of crashes are due to the driver''s drowsiness. The crash rates behind drowsiness is quite startling. In the cases for Less-Than-Load truckings"
	:presentation-index 115
	:speaker {Luke Moyer}
	:subevent-of {Session C2}
)

(new-radar-presentation "A Three State Collision Mitigation System"
	:abstract "The tri-state area has come together to create a system that would alleviate the number of collisions each state experiences. Since they each have several of the busiest highways running through their borders"
	:presentation-index 116
	:speaker {Michelle Wooldridge}
	:subevent-of {Session M1}
)

(new-radar-presentation "Field-Operational Test Results for the Auto-Inflate Tire System"
	:abstract "Flat tires have proven to be quite a nuisance to drivers because they are can be dangerous and preventing flat tires requires constant check ups on the vehicle. To solve this problem"
	:presentation-index 117
	:speaker {John Connor}
	:subevent-of {Session M1}
)

(new-radar-presentation "Non-Linear Models of Congestion in Mid-Size Metropolitan Areas"
	:abstract "The congestion in mid-size metropolitan areas have shown patterns that were not expected by researchers. They presented a non-linear model of the congestion patterns in these areas. The results were interesting and unexpected since they thought the congestion would have a linear pattern and show a relationship between the amount of congestion and the size of the areas."
	:presentation-index 118
	:speaker {Tom Thoreau}
	:subevent-of {Session M1}
)

(new-radar-presentation "Pedestrian Detection via Fused LADAR and Thermal Imagery"
	:abstract "Often times"
	:presentation-index 119
	:speaker {Motohiko Haguchi}
	:subevent-of {Session M1}
)

(new-radar-presentation "Effective Probe Vehicle Populations"
	:abstract "Probe vehicles are meant to detect any type of data in real-time. These vehicles have shown to be most effective in well-populated areas. With more cars and drivers"
	:presentation-index 120
	:speaker {Patti Minel}
	:subevent-of {Session O1}
)

(new-radar-presentation "Automated Camera-Based Incident Detection"
	:abstract "There are times when an incident occurs on the road and it cannot be recorded without a person or camera there. This leads to many illegal actions that go unpunished on the roads. In order to maintain regulations on the road"
	:presentation-index 121
	:speaker {Hope Ezzard}
	:subevent-of {Session O1}
)

(new-radar-presentation "Ground Loop Detector Models for Incident Event Identification"
	:abstract "In order to detect incidents and identify them as well"
	:presentation-index 122
	:speaker {Johannes Copper}
	:subevent-of {Session O1}
)

(new-radar-presentation "911 Call Patterns for Crashes in Los Angeles"
	:abstract "Los Angeles experiences one of the highest crash rates every year. Surprisingly"
	:presentation-index 123
	:speaker {Matushi Sakaguchi}
	:subevent-of {Session O1}
)

(new-radar-presentation "Collision Warning Ramifications of Behaviors Specific to Buses"
	:abstract "The public transportation that experiences the most hostility from both the passngers and the drivers is the bus system. Most complaints come from bus systems because of the hostile driving patterns of the drivers and the behaviors of the passengers. This caused the bus systems to have collision warnings that are specific for buses. With this"
	:presentation-index 124
	:speaker {Iris Johnson}
	:subevent-of {Session T3}
)

(new-radar-presentation "Analysis of Candidate CWS Display Location in Bus Cabs"
	:abstract "There has been much debate over where to place CWS displays in bus cabs. Some thought that it would be too bulky to place it in the front of the car where the driver sits"
	:presentation-index 125
	:speaker {Lauren Sherman}
	:subevent-of {Session T3}
)

(new-radar-presentation "Vehicles Behind Buses: A Rear-End Collision Warning System"
	:abstract "It is hard for a vehicle to see what is ahead when it is driving behind a large bus. The vehicle may become impatient from the bus''s slow speed and drive very closely behind the bus. With the bus''s frequent stops"
	:presentation-index 126
	:speaker {Lucinda Hollister}
	:subevent-of {Session T3}
)

(new-radar-presentation "Avoiding Collisions in Transit Maintenance Yards"
	:abstract "In transit maintenance yards"
	:presentation-index 127
	:speaker {Peter Ferguson}
	:subevent-of {Session T3}
)

(new-radar-presentation "Key Benefits of Automatic Vehicle Location to Schedule Planning"
	:abstract "There are many beneifts of having automatic vehicle location. It is extremely helpful when planning schedules because the source of transportation can automatically be located. With that"
	:presentation-index 128
	:speaker {James Sontag}
	:subevent-of {Session T4}
)

(new-radar-presentation "Dynamic Rider Route Guidance via AVL Data and Walking Maps"
	:abstract "A new type of guidance has been created for all drivers. It is called the Dynamic Rider Route Guidance system. This system can be called the best because of its precision and reliability. It utilizes Automatic Vehicle Location data and walking maps. With those tools under its belt"
	:presentation-index 129
	:speaker {Miho Sakato}
	:subevent-of {Session T4}
)

(new-radar-presentation "Next Bus Information on Mobile Phones"
	:abstract "Buses are rarely on time and barely follow the schedule given at bus stops. With inaccurate bus times"
	:presentation-index 130
	:speaker {Jarrod Michaels}
	:subevent-of {Session T4}
)

(new-radar-presentation "The Impact of Bus Arrival Time Predictions in Urban Areas"
	:abstract "Bus arrival times are very important in urban areas. In those areas"
	:presentation-index 131
	:speaker {Samuel Johns}
	:subevent-of {Session T4}
)

(new-radar-presentation "Find-A-Buddy Communication Systems"
	:abstract "Find-A-Buddy Communications Systems aims to help every individual who is need of companionship. There are millions of people who want to meet"
	:presentation-index 132
	:speaker {Andrew Akins}
	:subevent-of {Demo M2}
)

(new-radar-presentation "Movies On Demand Using Wireless Data Transfer"
	:abstract "People have been waiting for a way to get rid of the annoyance of going to the video store only to find out that the video they wanted isn''t there."
	:presentation-index 133
	:speaker {Russell Piper}
	:subevent-of {Demo M2}
)

(new-radar-presentation "Driving Games Meet the Digital Age"
	:abstract "Video games have advanced incredibly since the old days of Tetris and PacMan. Now, there are video games that seem almost life-like. The characters"
	:presentation-index 134
	:speaker {William Bernstein}
	:subevent-of {Demo M2}
)

(new-radar-presentation "Impact Safe Large Screen Displays"
	:abstract "When transporting objects"
	:presentation-index 135
	:speaker {Donald Hartfield}
	:subevent-of {Demo M2}
)

(new-radar-presentation "Critical Incident Analysis of Food-Related Distraction Periods"
	:abstract "Eating a Whopper Junior and fries on the road seems harmless, but what if an irresponsible driver cuts you off? You only have one free hand to steer the"
	:presentation-index 140
	:speaker {Jeanne Santos}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Ranging of Roadside Foliage"
	:abstract "Today, the amount of roadside foliage is increasing at an alarming rate. It has reach a point where the roads have become harmful because of the amount of"
	:presentation-index 141
	:speaker {David Rodgerson}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Radar Reflective Tagging of Road Signs"
	:abstract "Many advances have been made for driving at night time. Cars have developed better lighting for the road and more reflective tags have been marked on the"
	:presentation-index 142
	:speaker {John Constable}
	:subevent-of {Poster A1}
)

(new-radar-presentation "ACC Sensor Performance on Curved Roads"
	:abstract "Adaptive cruise control has been enjoyed by all drivers because of the additional comfort it gives drivers on the road. However"
	:presentation-index 143
	:speaker {Guillermo Credon}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Preemptive Safety Belt Pretensioning"
	:abstract "Safety belts are absolutely necessary for all drivers to use during their rides. Without them, any type of accident can be worsened without the help of"
	:presentation-index 144
	:speaker {Nigel Contorso}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Young Children and Distracted Driving"
	:abstract "There are many distractions on the road and children play a large role in that. Because children should often be supervised, parents are encouraged to"
	:presentation-index 145
	:speaker {Theodore Callman}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Eye Gaze Patterns When Driving in Snow"
	:abstract "The winter has proven to be one of the most dangerous seasons to drive on the road. After sessions of experiments, we have come to the conclusion that a"
	:presentation-index 146
	:speaker {Christine Johnston}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Wireless Communication with Roadway Fog Sensors"
	:abstract "Communication has been established in all different aspects of the technology world. However"
	:presentation-index 147
	:speaker {Emily Halwizer}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Lessons for Passenger Vehicles from the CVO Community"
	:abstract "The CVO community has created several lessons for the passengers to learn when found in vehicles. There are several safety precautions that are never taken"
	:presentation-index 148
	:speaker {Dennis Parrott}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Roadside Infrastructure for Intersection Collision Warning"
	:abstract "The foundation for roadsides are in dire need of improvement. Since they play such a large role in transportation, they should also have one in safety."
	:presentation-index 149
	:speaker {Bob Halstear}
	:subevent-of {Poster A1}
)

(new-radar-presentation "The Telematics Killer App - Still Searching"
	:abstract "Telematics has had an increasing demand throughout the years by all individuals. The combination of technology and business makes telematics quite appealing"
	:presentation-index 150
	:speaker {Daniel Hendridge}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Deer-Car Crash Prevention"
	:abstract "An increasing percentage of car accidents are caused by deer crossings. While driving, a deer may jump in front of the car and the driver does not have"
	:presentation-index 151
	:speaker {Andreas Schaeffer}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Misperception of Train Speed at Railroad Crossings"
	:abstract "Drivers are usually frustrated the most when waiting at railroad crossings. The wait is excrutiatingly long and for most of the time, nothing is crossing"
	:presentation-index 152
	:speaker {Gene Hawkings}
	:subevent-of {Poster A1}
)

(new-radar-presentation "Glare Characteristics of Variable Halogen Headlamps"
	:abstract "Vision is extremely important while operating vehicles. The driver must be able to see clearly or else he can miss important signs and such that help"
	:presentation-index 153
	:speaker {Troy Costales}
	:subevent-of {Poster A1}
)

(new-radar-presentation "An Integrated ACC and CWS Driver Interface"
	:abstract "When the driver utilizes his adaptive cruise control, he puts a large amount of trust into that system. He does so because he wants to relax his feet"
	:presentation-index 154
	:speaker {Phillip Hardunn}
	:subevent-of {Session A4}
)

(new-radar-presentation "Sliding Autonomy for Lane-Keeping Assistance"
	:abstract "When drivers are on the road"
	:presentation-index 155
	:speaker {Natalia Kinski}
	:subevent-of {Session A4}
)

(new-radar-presentation "Mixed Initiative Collision Avoidance"
	:abstract "Some road collisions are caused by mixed ideas of the other driver''s intentions. There have been moments where a driver believes the other would like to make a turn"
	:presentation-index 156
	:speaker {Raquel Diano}
	:subevent-of {Session A4}
)

(new-radar-presentation "Assessing Older Driver Competency with a Web-Based Test"
	:abstract "Older drivers need to be assessed on the road. With age comes health issues and it is not safe to continually renew their licenses without checking their"
	:presentation-index 157
	:speaker {John Drexel}
	:subevent-of {Session A4}
)

(new-radar-presentation "Heuristics for Variable License Renewal Exams"
	:abstract "Researchers are constantly trying to find different ways in order to change the license renewal exams in order to better fit the different types of people"
	:presentation-index 158
	:speaker {Debbie Cotton}
	:subevent-of {Session M2}
)

(new-radar-presentation "Modeling Pedestrian Traffic on the Charles Bridge"
	:abstract "Hundreds of pedestrians cross the Charles Bridge everyday. Because of the large number of people, it has one of the best pedestrian traffic in the country."
	:presentation-index 159
	:speaker {Elise Steiner}
	:subevent-of {Session M2}
)

(new-radar-presentation "Bus Mounted Bike Rack Utilization in the San Francisco Bay Area"
	:abstract "The Bay Area has one of the highest population of bikers. It helps the environment by not using gas and polluting the ozone with deadly fumes. However, bikers"
	:presentation-index 160
	:speaker {Hammond Hughes}
	:subevent-of {Session M2}
)

(new-radar-presentation "The Impact of Adaptive Cruise Control on Congestion"
	:abstract "When there is traffic congestion"
	:presentation-index 161
	:speaker {Gina Bertolucci}
	:subevent-of {Session M2}
)

(new-radar-presentation "Closed Course Evaluation of the Atlantic Northwest Trailer EBS"
	:abstract "The Atlantic Northwest has experienced an increasing number of goods transported by trucks. As a result, it has been thought best to create a course"
	:presentation-index 162
	:speaker {Mark Abokowicz}
	:subevent-of {Session C3}
)

(new-radar-presentation "Jackknife Crash Rates for Electronic Braking Systems"
	:abstract "The Electronic Braking Systems for vehicles has significantly lowered the crash rates over the past few years. The braking systems have improved very much"
	:presentation-index 163
	:speaker {Dan Murray}
	:subevent-of {Session C3}
)

(new-radar-presentation "EBS for Less-Than-Load Trailers"
	:abstract "The trailers that carried the heaviest load would have the best braking systems because of the weight of their loads. It was believed that they would experience"
	:presentation-index 164
	:speaker {Donald Reeves}
	:subevent-of {Session C3}
)

(new-radar-presentation "Articulated Hitch Sensors for Augmented EBS"
	:abstract "The new electronic braking system has satisfied many customers. However, it has reached the period where the high demand also asks for better improvements"
	:presentation-index 165
	:speaker {Henry Watson}
	:subevent-of {Session C3}
)

(new-radar-presentation "A Prototype Rollover Prevention System"
	:abstract "It has been suggested that a prevention system should be utilized for the trucking business. The truckers endure many dangers on the road and in the end"
	:presentation-index 166
	:speaker {Phillipe Defrano}
	:subevent-of {Session C4}
)

(new-radar-presentation "Electronic HazMat Transponders"
	:abstract "When there are hazardous materials in an area"
	:presentation-index 167
	:speaker {Judith Payne}
	:subevent-of {Session C4}
)

(new-radar-presentation "Intelligent Refrigeration Control in Tanker Trailers"
	:abstract "One of the most difficult items to transport are those that are sensitive to temperature. Those are usually liquids that need to be maintained at cold"
	:presentation-index 168
	:speaker {Judith Payne}
	:subevent-of {Session C4}
)

(new-radar-presentation "Dynamic Suspension for Liquid Load Balancing"
	:abstract "Trailers that transport liquid loads need to balance them before departure. This task is extremely difficult because the liquid cannot maintain a stiff"
	:presentation-index 169
	:speaker {Sean Martin}
	:subevent-of {Session C4}
)

(new-radar-presentation "AVL Systems for Homeland Security Sensitive Cargo"
	:abstract "Homeland Security Sensitive Cargo is one of the most important items to transport around the United States. Since the United States has experienced security"
	:presentation-index 170
	:speaker {Casper Miles}
	:subevent-of {Session O2}
)

(new-radar-presentation "Weigh-In-Motion via RFID Tags"
	:abstract "In order to find out where cargo is going"
	:presentation-index 171
	:speaker {Francine Stimmel}
	:subevent-of {Session O2}
)

(new-radar-presentation "Tire Tread Wear from High Resolution LIDAR Scans"
	:abstract "The light and detection reading scans have had a negative effect on tires. Although th LIDAR scans are used for mapping and other tests, the negative"
	:presentation-index 172
	:speaker {Terrence Porteus}
	:subevent-of {Session O2}
)

(new-radar-presentation "A Novel Method for Rapid Emissions Testing"
	:abstract "A new method has been created for rapid emissions testing. Researchers are trying to figure out how much emissions come from vehicles. As a result, they"
	:presentation-index 173
	:speaker {Dino Smith}
	:subevent-of {Session O2}
)

(new-radar-presentation "Why ITS Matters"
	:abstract "ITS is the next best thing for our society and its road to better transportation. What if you could not decide on where to turn? What is the"
	:presentation-index 174
	:speaker {Jonathon Robertson}
	:subevent-of {Plenary 3}
)

(new-radar-presentation "ARDRA Presidential Address"
	:abstract "The address overviews what the goals of ARDRA were when they set the conference up. Some of the conference goals were to increase the awareness of a variety of transportation-related causes and to integrate employees with similar interests. The address looks at how employees and attendees of the conference can use the conference to network ideas and help make ARDRA more successful in the future of transportation."
	:presentation-index 175
	:speaker {Andrea Manson}
	:subevent-of {Plenary 1}
)

(new-radar-presentation "Multi-Agency Fare Collection"
	:abstract "Toronto has implemented multi-agency fare collection"
	:presentation-index 176
	:speaker {Ben Steigerwald}
	:subevent-of {Tutorial 1a}
)

(new-radar-presentation "The ADA and Terminal Design"
	:abstract "The Americans with Disabilities Act provides that those with disabilities be considered in public places"
	:presentation-index 177
	:speaker {Martha Pialino}
	:subevent-of {Tutorial 2a}
)

(new-radar-presentation "Does Thruway Congestion Matter?"
	:abstract "This tutorial takes a closer look at congestion at an important area of roads: the thruways. These areas are not generally the focus of those trying to deal with traffic congestion problems"
	:presentation-index 178
	:speaker {Colleen Undranko}
	:subevent-of {Tutorial 3a}
)

(new-radar-presentation "Variable Toll Collection"
	:abstract "Why are tolls not always collected with the same speed"
	:presentation-index 179
	:speaker {Eric Musacelo}
	:subevent-of {Tutorial 4a}
)

(new-radar-presentation "Federal Advanced Transportation Programs"
	:abstract "The Federal System has taken into account the increased importance of advanced transportation. Because of the terrorism and other unfortunate events that have struck the United States"
	:presentation-index 180
	:speaker {Cara Ride}
	:subevent-of {Plenary 2}
)

