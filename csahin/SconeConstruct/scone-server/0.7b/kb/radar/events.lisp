;;; Events known to Radar in the test world.
;;; Automatically derived from the SQL files.

(new-radar-gathering "Workshop 1a"
		     :gathering-index 1
		     :title "Intermodal Passenger Screening"
		     :types '({workshop} {open gathering})
		     :person-in-charge {Sandra Nubanks}
		     :location {grass})

(new-radar-presentation "The Benefits of Seamless Transfers"
			:presentation-index 1
			:speaker {Sandra Nubanks}
			:subevent-of {Workshop 1a}
			:abstract "The Benefits of Seamless Transfers")

(new-radar-presentation "Bioterrorism and Large Scale Terminals"
			:presentation-index 2
			:speaker {William Alexander}
			:subevent-of {Workshop 1a}
			:abstract "Bioterrorism and Large Scale Terminals")

(new-radar-gathering "Workshop 1b"
		     :gathering-index 2
		     :title "Intermodal Passenger Screening"
		     :types '({workshop} {open gathering})
		     :person-in-charge {Sandra Nubanks}
		     :location {grass})

(new-radar-presentation "Spaceports: The Next Step"
			:presentation-index 3
			:speaker {Fabian Monoker}
			:subevent-of {Workshop 1b}
			:abstract "Spaceports: The Next Step")

(new-radar-presentation "Legal Issues in Multi-Agency Teminals"
			:presentation-index 4
			:speaker {Thomas Bouvier}
			:subevent-of {Workshop 1b}
			:abstract "Legal Issues in Multi-Agency Teminals")

(new-radar-gathering "Workshop 1c"
		     :gathering-index 3
		     :title "Intermodal Passenger Screening"
		     :types '({workshop} {open gathering})
		     :person-in-charge {Sandra Nubanks}
		     :location {grass})

(new-radar-presentation "Data Privacy Concerns"
			:presentation-index 5
			:speaker {Elizabeth McManis}
			:subevent-of {Workshop 1c}
			:abstract "Data Privacy Concerns")

(new-radar-presentation "Advances in Metal Detectors"
			:presentation-index 6
			:speaker {Gary Abramowitz}
			:subevent-of {Workshop 1c}
			:abstract "Advances in Metal Detectors")

(new-radar-gathering "Workshop 1d"
		     :gathering-index 4
		     :title "Intermodal Passenger Screening"
		     :types '({workshop} {open gathering})
		     :person-in-charge {Sandra Nubanks}
		     :location {grass})

(new-radar-presentation "Logistics of Pedestrian Flow"
			:presentation-index 7
			:speaker {Vivek Kahn}
			:subevent-of {Workshop 1d}
			:abstract "Logistics of Pedestrian Flow")

(new-radar-presentation "The Importance of Space in Terminal Retrofitting"
			:presentation-index 8
			:speaker {Yoo Fong}
			:subevent-of {Workshop 1d}
			:abstract "The Importance of Space in Terminal Retrofitting")

(new-radar-gathering "Workshop 2a"
		     :gathering-index 5
		     :title "Advances in Roadway Maintenance"
		     :types '({workshop} {open gathering})
		     :person-in-charge {Rachel Greensburg}
		     :location {grass})

(new-radar-presentation "A Survey on the Impact of Advanced Maintenance"
			:presentation-index 9
			:speaker {Rachel Greensburg}
			:subevent-of {Workshop 2a}
			:abstract "A Survey on the Impact of Advanced Maintenance")

(new-radar-presentation "GIS in Roadway Maintenance"
			:presentation-index 10
			:speaker {Steve Adams}
			:subevent-of {Workshop 2a}
			:abstract "GIS in Roadway Maintenance")

(new-radar-gathering "Workshop 2b"
		     :gathering-index 6
		     :title "Advances in Roadway Maintenance"
		     :types '({workshop} {open gathering})
		     :person-in-charge {Rachel Greensburg}
		     :location {grass})

(new-radar-presentation "Flexible Concrete"
			:presentation-index 11
			:speaker {Alexander Gallatin}
			:subevent-of {Workshop 2b}
			:abstract "Flexible Concrete")

(new-radar-presentation "Leveraging Ferrous Content in Bridges"
			:presentation-index 12
			:speaker {Vivica Wagner}
			:subevent-of {Workshop 2b}
			:abstract "Leveraging Ferrous Content in Bridges")

(new-radar-gathering "Workshop 2c"
		     :gathering-index 7
		     :title "Advances in Roadway Maintenance"
		     :types '({workshop} {open gathering})
		     :person-in-charge {Rachel Greensburg}
		     :location {grass})

(new-radar-presentation "Surface Diagnosis Via Aerial Maps"
			:presentation-index 13
			:speaker {Jason O'Reilly}
			:subevent-of {Workshop 2c}
			:abstract "Surface Diagnosis Via Aerial Maps")

(new-radar-presentation "Laser Saws for Asphalt Repair"
			:presentation-index 14
			:speaker {Shriya Shaw}
			:subevent-of {Workshop 2c}
			:abstract "Laser Saws for Asphalt Repair")

(new-radar-gathering "Workshop 2d"
		     :gathering-index 8
		     :title "Advances in Roadway Maintenance"
		     :types '({workshop} {open gathering})
		     :person-in-charge {Rachel Greensburg}
		     :location {grass})

(new-radar-presentation "Fuel Cell Powered Signal Lamps"
			:presentation-index 15
			:speaker {Becky Green}
			:subevent-of {Workshop 2d}
			:abstract "Fuel Cell Powered Signal Lamps")

(new-radar-presentation "Automated Fog Detection and Mitigation"
			:presentation-index 16
			:speaker {Maggie Foxenreiter}
			:subevent-of {Workshop 2d}
			:abstract "Automated Fog Detection and Mitigation")

(new-radar-gathering "Tutorial 1a"
		     :gathering-index 9
		     :title "Multi-Agency Fare Collection"
		     :types '({tutorial} {open gathering})
		     :person-in-charge {Ben Steigerwald}
		     :location {grass})

(new-radar-gathering "Tutorial 1b"
		     :gathering-index 10
		     :title "Multi-Agency Fare Collection"
		     :types '({tutorial} {open gathering})
		     :person-in-charge {Andrew Schwartz}
		     :location {grass})

(new-radar-gathering "Tutorial 2a"
		     :gathering-index 11
		     :title "The ADA and Terminal Design"
		     :types '({tutorial} {open gathering})
		     :person-in-charge {Martha Pialino}
		     :location {grass})

(new-radar-gathering "Tutorial 2b"
		     :gathering-index 12
		     :title "The ADA and Terminal Design"
		     :types '({tutorial} {open gathering})
		     :person-in-charge {Martha Pialino}
		     :location {grass})

(new-radar-gathering "Tutorial 3a"
		     :gathering-index 13
		     :title "Does Thruway Congestion Matter?"
		     :types '({tutorial} {open gathering})
		     :person-in-charge {Colleen Undranko}
		     :location {grass})

(new-radar-gathering "Tutorial 3b"
		     :gathering-index 14
		     :title "Does Thruway Congestion Matter?"
		     :types '({tutorial} {open gathering})
		     :person-in-charge {Colleen Undranko}
		     :location {grass})

(new-radar-gathering "Tutorial 4a"
		     :gathering-index 15
		     :title "Variable Toll Collection"
		     :types '({tutorial} {open gathering})
		     :person-in-charge {Eric Musacelo}
		     :location {grass})

(new-radar-gathering "Tutorial 4b"
		     :gathering-index 16
		     :title "Variable Toll Collection"
		     :types '({tutorial} {open gathering})
		     :person-in-charge {Arnold Wickner}
		     :location {grass})

(new-radar-gathering "Plenary 1"
		     :gathering-index 17
		     :title "ARDRA Presidential Address"
		     :types '({plenary} {open gathering})
		     :person-in-charge {Andrea Manson}
		     :location {grass})

(new-radar-gathering "Keynote"
		     :gathering-index 18
		     :title "Advanced Transportation and Market Forces"
		     :types '({keynote} {open gathering})
		     :person-in-charge {Stuart Weitzmann}
		     :location {grass})

(new-radar-gathering "Demo A1"
		     :gathering-index 19
		     :title "Collision Warning Systems"
		     :types '({demo} {open gathering})
		     :person-in-charge {Brianna Smith}
		     :location {grass})

(new-radar-presentation "The New Mexico ACC Deployment"
			:presentation-index 17
			:speaker {Brianna Smith}
			:subevent-of {Demo A1}
			:abstract "The New Mexico ACC Deployment")

(new-radar-presentation "Warning Using Seat Cushions"
			:presentation-index 18
			:speaker {Sonal Malhotra}
			:subevent-of {Demo A1}
			:abstract "Warning Using Seat Cushions")

(new-radar-presentation "LIDAR-Based Side Collision Avoidance"
			:presentation-index 19
			:speaker {Tal Havartza}
			:subevent-of {Demo A1}
			:abstract "LIDAR-Based Side Collision Avoidance")

(new-radar-presentation "3D Sound Warnings"
			:presentation-index 20
			:speaker {Susan Mikhal}
			:subevent-of {Demo A1}
			:abstract "3D Sound Warnings")

(new-radar-gathering "Panel1 A1"
		     :gathering-index 20
		     :title "Legal Aspects of Adaptive Cruise Control"
		     :types '({panel} {open gathering})
		     :person-in-charge {Ian Liluso}
		     :location {grass})

(new-radar-presentation "Panel1 A1.1"
			:presentation-index 21
			:speaker {Ian Liluso}
			:subevent-of {Panel1 A1})

(new-radar-presentation "Panel1 A1.2"
			:presentation-index 22
			:speaker {Ruben Salada}
			:subevent-of {Panel1 A1})

(new-radar-presentation "Panel1 A1.3"
			:presentation-index 23
			:speaker {Nina Riazo}
			:subevent-of {Panel1 A1})

(new-radar-presentation "Panel1 A1.4"
			:presentation-index 24
			:speaker {Gina Wilson}
			:subevent-of {Panel1 A1})

(new-radar-gathering "Panel C1"
		     :gathering-index 21
		     :title "Hours of Service and Fatigue"
		     :types '({panel} {open gathering})
		     :person-in-charge {Brian Car}
		     :location {grass})

(new-radar-presentation "Panel C1.1"
			:presentation-index 25
			:speaker {Brian Car}
			:subevent-of {Panel C1})

(new-radar-presentation "Panel C1.2"
			:presentation-index 26
			:speaker {Timothy Margilois}
			:subevent-of {Panel C1})

(new-radar-presentation "Panel C1.3"
			:presentation-index 27
			:speaker {Jun Lee}
			:subevent-of {Panel C1})

(new-radar-presentation "Panel C1.4"
			:presentation-index 28
			:speaker {April Towers}
			:subevent-of {Panel C1})

(new-radar-gathering "Panel T1"
		     :gathering-index 22
		     :title "Contactless Smart Cards"
		     :types '({panel} {open gathering})
		     :person-in-charge {Ashuk Wendorf}
		     :location {grass})

(new-radar-presentation "Panel T1.1"
			:presentation-index 29
			:speaker {Ashuk Wendorf}
			:subevent-of {Panel T1})

(new-radar-presentation "Panel T1.2"
			:presentation-index 30
			:speaker {Samantha Swick}
			:subevent-of {Panel T1})

(new-radar-presentation "Panel T1.3"
			:presentation-index 31
			:speaker {Susana Fariola}
			:subevent-of {Panel T1})

(new-radar-presentation "Panel T1.4"
			:presentation-index 32
			:speaker {Andrew Delafonte}
			:subevent-of {Panel T1})

(new-radar-gathering "Poster C1"
		     :gathering-index 23
		     :title "Commercial Vehicle Operations"
		     :types '({poster session} {open gathering})
		     :person-in-charge {Timothy Margilois}
		     :location {grass})

(new-radar-presentation "Drowsiness as a Function of Vehicle Type"
			:presentation-index 33
			:speaker {Timothy Margilois}
			:subevent-of {Poster C1}
			:abstract "Drowsiness as a Function of Vehicle Type")

(new-radar-presentation "Composite Materials for Green Trailer Construction"
			:presentation-index 34
			:speaker {Frank Ionola}
			:subevent-of {Poster C1}
			:abstract "Composite Materials for Green Trailer Construction")

(new-radar-presentation "Eco-Friendly Truck Tire Construction"
			:presentation-index 35
			:speaker {Suzanne Marilios}
			:subevent-of {Poster C1}
			:abstract "Eco-Friendly Truck Tire Construction")

(new-radar-presentation "Driver Monitoring Devices for CVO"
			:presentation-index 36
			:speaker {Tina Fall}
			:subevent-of {Poster C1}
			:abstract "Driver Monitoring Devices for CVO")

(new-radar-presentation "Roundabout Utilization by Articulated Trucks"
			:presentation-index 37
			:speaker {Ed Ivanovich}
			:subevent-of {Poster C1}
			:abstract "Roundabout Utilization by Articulated Trucks")

(new-radar-presentation "Regulatory Changes in CVO in South Africa"
			:presentation-index 38
			:speaker {Westley Qualter}
			:subevent-of {Poster C1}
			:abstract "Regulatory Changes in CVO in South Africa")

(new-radar-presentation "Armenian Experiences with Truck Emissions Improvements"
			:presentation-index 39
			:speaker {Ilise Nebuto}
			:subevent-of {Poster C1}
			:abstract "Armenian Experiences with Truck Emissions Improvements")

(new-radar-presentation "The Weekend Driver: The Effect of Part Time Drivers on CVO"
			:presentation-index 40
			:speaker {Dennis Johnson}
			:subevent-of {Poster C1}
			:abstract "The Weekend Driver: The Effect of Part Time Drivers on CVO")

(new-radar-presentation "Dont Park Your Truck on Ice and Other Important CVO Lessons"
			:presentation-index 41
			:speaker {Andy Sewally}
			:subevent-of {Poster C1}
			:abstract "Dont Park Your Truck on Ice and Other Important CVO Lessons")

(new-radar-presentation "The Impact of Truck Cab Noise on Hearing Loss"
			:presentation-index 42
			:speaker {Peter Havater}
			:subevent-of {Poster C1}
			:abstract "The Impact of Truck Cab Noise on Hearing Loss")

(new-radar-presentation "The Utility of Parabolic Mirrors for Forward Pedestrian Detection"
			:presentation-index 43
			:speaker {Yong Qian}
			:subevent-of {Poster C1}
			:abstract "The Utility of Parabolic Mirrors for Forward Pedestrian Detection")

(new-radar-presentation "Cost Effectiveness of Electronic Braking Systems for Dual Trailers"
			:presentation-index 44
			:speaker {Ola Neuen}
			:subevent-of {Poster C1}
			:abstract "Cost Effectiveness of Electronic Braking Systems for Dual Trailers")

(new-radar-gathering "Poster M1"
		     :gathering-index 24
		     :title "Mixed Posters"
		     :types '({poster session} {open gathering})
		     :person-in-charge {Cathy Algowitz}
		     :location {grass})

(new-radar-presentation "Poor Performance of Ground Penetrating Radar for Pipe Surveys"
			:presentation-index 45
			:speaker {Cathy Algowitz}
			:subevent-of {Poster M1}
			:abstract "Poor Performance of Ground Penetrating Radar for Pipe Surveys")

(new-radar-presentation "Congestion Effects from Rear Seat DVD Players"
			:presentation-index 46
			:speaker {Helmet Uter}
			:subevent-of {Poster M1}
			:abstract "Congestion Effects from Rear Seat DVD Players")

(new-radar-presentation "Regulatory Forecasts for Passenger Entertainment Systems"
			:presentation-index 47
			:speaker {Petra Svetski}
			:subevent-of {Poster M1}
			:abstract "Regulatory Forecasts for Passenger Entertainment Systems")

(new-radar-presentation "Regional Transit Guidance on Handheld Devices"
			:presentation-index 48
			:speaker {Nina Idiz}
			:subevent-of {Poster M1}
			:abstract "Regional Transit Guidance on Handheld Devices")

(new-radar-presentation "Transit Buses as Congestion Probes"
			:presentation-index 49
			:speaker {Jana Padamanabahn}
			:subevent-of {Poster M1}
			:abstract "Transit Buses as Congestion Probes")

(new-radar-presentation "A Model of Double Parked Delivery Truck Induced Congestion"
			:presentation-index 50
			:speaker {Mark Oriolos}
			:subevent-of {Poster M1}
			:abstract "A Model of Double Parked Delivery Truck Induced Congestion")

(new-radar-presentation "Combiner Glass Windshields for Head-Up Display Imagery"
			:presentation-index 51
			:speaker {Dana Johanson}
			:subevent-of {Poster M1}
			:abstract "Combiner Glass Windshields for Head-Up Display Imagery")

(new-radar-presentation "Crashworthiness of Adaptive Equipment"
			:presentation-index 52
			:speaker {Wanda Teven}
			:subevent-of {Poster M1}
			:abstract "Crashworthiness of Adaptive Equipment")

(new-radar-gathering "Session A1"
		     :gathering-index 25
		     :title "Multimedia Telematics"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Larry Elry}
		     :location {grass})

(new-radar-presentation "Movie Rentals at the Pump"
			:presentation-index 54
			:speaker {Larry Elry}
			:subevent-of {Session A1}
			:abstract "Movie Rentals at the Pump")

(new-radar-presentation "Speech Interfaces for In-Vehicle Internet"
			:presentation-index 55
			:speaker {Deana Gona}
			:subevent-of {Session A1}
			:abstract "Speech Interfaces for In-Vehicle Internet")

(new-radar-presentation "Using Bluetooth to Leverage a Drivers Mobile Phone"
			:presentation-index 56
			:speaker {Fiona Mansala}
			:subevent-of {Session A1}
			:abstract "Using Bluetooth to Leverage a Drivers Mobile Phone")

(new-radar-presentation "Satellite Radio Adoption Rates by Motorists"
			:presentation-index 57
			:speaker {Ed Farvich}
			:subevent-of {Session A1}
			:abstract "Satellite Radio Adoption Rates by Motorists")

(new-radar-gathering "Session C1"
		     :gathering-index 26
		     :title "In-Vehicle Information Systems"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Renee O'Laughlin}
		     :location {grass})

(new-radar-presentation "Pilot Test of a Prototype Mileage Prediction System"
			:presentation-index 58
			:speaker {Renee O'Laughlin}
			:subevent-of {Session C1}
			:abstract "Pilot Test of a Prototype Mileage Prediction System")

(new-radar-presentation "Acceptance Results of Driver-Initiated Monitoring Programs"
			:presentation-index 59
			:speaker {Tina Fall}
			:subevent-of {Session C1}
			:abstract "Acceptance Results of Driver-Initiated Monitoring Programs")

(new-radar-presentation "An In-Dash Computer for Tire Wear Management"
			:presentation-index 60
			:speaker {Pete Fava}
			:subevent-of {Session C1}
			:abstract "An In-Dash Computer for Tire Wear Management")

(new-radar-presentation "The National Truck Stop Electronic Database"
			:presentation-index 61
			:speaker {Zachary Tonazu}
			:subevent-of {Session C1}
			:abstract "The National Truck Stop Electronic Database")

(new-radar-gathering "Session P1"
		     :gathering-index 27
		     :title "Highway Design and Local Communities"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Michael Reese}
		     :location {grass})

(new-radar-presentation "Sound Abatement Using Green Materials"
			:presentation-index 62
			:speaker {Michael Reese}
			:subevent-of {Session P1}
			:abstract "Sound Abatement Using Green Materials")

(new-radar-presentation "Effects of Below-Grade Highways on Pedestrian Flow"
			:presentation-index 63
			:speaker {Michael Winslow}
			:subevent-of {Session P1}
			:abstract "Effects of Below-Grade Highways on Pedestrian Flow")

(new-radar-presentation "Congestion-Induced Health Effects in Neighboring Communities"
			:presentation-index 64
			:speaker {Kelly Jovanovich}
			:subevent-of {Session P1}
			:abstract "Congestion-Induced Health Effects in Neighboring Communities")

(new-radar-presentation "The Impact of Exit Ramps on Commercial Development"
			:presentation-index 65
			:speaker {Chian Lee}
			:subevent-of {Session P1}
			:abstract "The Impact of Exit Ramps on Commercial Development")

(new-radar-gathering "Session P2"
		     :gathering-index 28
		     :title "Successful Planning Strategies"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Shawanda North}
		     :location {grass})

(new-radar-presentation "Get on the Bus: Best Practices in Transit Planning"
			:presentation-index 66
			:speaker {Shawanda North}
			:subevent-of {Session P2}
			:abstract "Get on the Bus: Best Practices in Transit Planning")

(new-radar-presentation "Implications of AHS on Arterial Congestion"
			:presentation-index 67
			:speaker {Jack Petersen}
			:subevent-of {Session P2}
			:abstract "Implications of AHS on Arterial Congestion")

(new-radar-presentation "Feasibility of Automated Highways in Dedicated Truck Lanes"
			:presentation-index 68
			:speaker {Don Henry}
			:subevent-of {Session P2}
			:abstract "Feasibility of Automated Highways in Dedicated Truck Lanes")

(new-radar-presentation "Penetration Rates of Hybrid Vehicles"
			:presentation-index 69
			:speaker {Abigail Reeves}
			:subevent-of {Session P2}
			:abstract "Penetration Rates of Hybrid Vehicles")

(new-radar-gathering "Session T1"
		     :gathering-index 29
		     :title "Multimodal Station Design"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Calla Diller}
		     :location {grass})

(new-radar-presentation "The Shortest Distance Between Two Lines is Not a Hallway"
			:presentation-index 70
			:speaker {Calla Diller}
			:subevent-of {Session T1}
			:abstract "The Shortest Distance Between Two Lines is Not a Hallway")

(new-radar-presentation "Passenger Flow Models for Transfer Stations"
			:presentation-index 71
			:speaker {Courtney Charles}
			:subevent-of {Session T1}
			:abstract "Passenger Flow Models for Transfer Stations")

(new-radar-presentation "Preserving Sovereignty in Multi-Agency Stations"
			:presentation-index 72
			:speaker {Katrina Aronza}
			:subevent-of {Session T1}
			:abstract "Preserving Sovereignty in Multi-Agency Stations")

(new-radar-presentation "A Test Deployment of Bicycle Cars in Light Rail"
			:presentation-index 73
			:speaker {Laura Timdale}
			:subevent-of {Session T1}
			:abstract "A Test Deployment of Bicycle Cars in Light Rail")

(new-radar-gathering "Session T2"
		     :gathering-index 30
		     :title "Bus Rapid Transit"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Charles Derunn}
		     :location {grass})

(new-radar-presentation "Long-Term Ridership Data for Curritiba"
			:presentation-index 74
			:speaker {Charles Derunn}
			:subevent-of {Session T2}
			:abstract "Long-Term Ridership Data for Curritiba")

(new-radar-presentation "Quantification of Platform Boarding Efficiency"
			:presentation-index 75
			:speaker {Michael Faye}
			:subevent-of {Session T2}
			:abstract "Quantification of Platform Boarding Efficiency")

(new-radar-presentation "Successes and Failures of Signal Preemption"
			:presentation-index 76
			:speaker {Bob Armstrong}
			:subevent-of {Session T2}
			:abstract "Successes and Failures of Signal Preemption")

(new-radar-presentation "Flex Route Feeder Buses"
			:presentation-index 77
			:speaker {Richard Holley}
			:subevent-of {Session T2}
			:abstract "Flex Route Feeder Buses")

(new-radar-gathering "Plenary 2"
		     :gathering-index 31
		     :title "Federal Advanced Transportation Programs"
		     :types '({plenary} {open gathering})
		     :person-in-charge {Cara Ride}
		     :location {grass})

(new-radar-gathering "Demo M1"
		     :gathering-index 32
		     :title "Driver Monitoring Systems"
		     :types '({demo} {open gathering})
		     :person-in-charge {Jun Lee}
		     :location {grass})

(new-radar-presentation "Single Camera Drowsiness Detection"
			:presentation-index 78
			:speaker {Jun Lee}
			:subevent-of {Demo M1}
			:abstract "Single Camera Drowsiness Detection")

(new-radar-presentation "A Hybrid Model of Driver Inattention"
			:presentation-index 79
			:speaker {Taylor Killian}
			:subevent-of {Demo M1}
			:abstract "A Hybrid Model of Driver Inattention")

(new-radar-presentation "Virtual Backseat Driving"
			:presentation-index 80
			:speaker {Scott Scheurman}
			:subevent-of {Demo M1}
			:abstract "Virtual Backseat Driving")

(new-radar-presentation "Low-Cost Sensors for Appropriate Airbag Deployment"
			:presentation-index 81
			:speaker {Laron Dale}
			:subevent-of {Demo M1}
			:abstract "Low-Cost Sensors for Appropriate Airbag Deployment")

(new-radar-gathering "Demo T1"
		     :gathering-index 33
		     :title "Passenger Information Systems"
		     :types '({demo} {open gathering})
		     :person-in-charge {Jarrod Michaels}
		     :location {grass})

(new-radar-presentation "Bus Arrival Time Prediction"
			:presentation-index 82
			:speaker {Jarrod Michaels}
			:subevent-of {Demo T1}
			:abstract "Bus Arrival Time Prediction")

(new-radar-presentation "ADA Compliant Stop Announcements"
			:presentation-index 83
			:speaker {Melissa Crosby}
			:subevent-of {Demo T1}
			:abstract "ADA Compliant Stop Announcements")

(new-radar-presentation "Assisted Wayfinding for Blind Passengers"
			:presentation-index 84
			:speaker {Donald Allman}
			:subevent-of {Demo T1}
			:abstract "Assisted Wayfinding for Blind Passengers")

(new-radar-presentation "A Real-Time Telephone Information System"
			:presentation-index 85
			:speaker {Julia Gonzalez}
			:subevent-of {Demo T1}
			:abstract "A Real-Time Telephone Information System")

(new-radar-gathering "Panel A2"
		     :gathering-index 34
		     :title "Driver Distraction"
		     :types '({panel} {open gathering})
		     :person-in-charge {Natalia Kinski}
		     :location {grass})

(new-radar-presentation "Panel A2.1"
			:presentation-index 86
			:speaker {Natalia Kinski}
			:subevent-of {Panel A2})

(new-radar-presentation "Panel A2.2"
			:presentation-index 87
			:speaker {Theodore Callman}
			:subevent-of {Panel A2})

(new-radar-presentation "Panel A2.3"
			:presentation-index 88
			:speaker {Eli Dorunda}
			:subevent-of {Panel A2})

(new-radar-presentation "Panel A2.4"
			:presentation-index 89
			:speaker {Spence Pierro}
			:subevent-of {Panel A2})

(new-radar-gathering "Panel O1"
		     :gathering-index 35
		     :title "55 or 65?"
		     :types '({panel} {open gathering})
		     :person-in-charge {Cara Ride}
		     :location {grass})

(new-radar-presentation "Panel O1.1"
			:presentation-index 90
			:speaker {Cara Ride}
			:subevent-of {Panel O1})

(new-radar-presentation "Panel O1.2"
			:presentation-index 91
			:speaker {Brad Cruise}
			:subevent-of {Panel O1})

(new-radar-presentation "Panel O1.3"
			:presentation-index 92
			:speaker {Tom Thoreau}
			:subevent-of {Panel O1})

(new-radar-presentation "Panel O1.4"
			:presentation-index 93
			:speaker {Henry David}
			:subevent-of {Panel O1})

(new-radar-gathering "Poster P1"
		     :gathering-index 36
		     :title "Planning"
		     :types '({poster session} {open gathering})
		     :person-in-charge {Juliet Frerking}
		     :location {grass})

(new-radar-presentation "An Analysis of Arterial Traffic Volume in Waterloo"
			:presentation-index 94
			:speaker {Juliet Frerking}
			:subevent-of {Poster P1}
			:abstract "Ontario")

(new-radar-presentation "A Mathematical Model for Estimating Adequate Parking Capacity"
			:presentation-index 95
			:speaker {Thomas Collins}
			:subevent-of {Poster P1}
			:abstract "A Mathematical Model for Estimating Adequate Parking Capacity")

(new-radar-presentation "Community Response to Low Pressure Sodium Street Lamps"
			:presentation-index 96
			:speaker {Britt Daniel}
			:subevent-of {Poster P1}
			:abstract "Community Response to Low Pressure Sodium Street Lamps")

(new-radar-presentation "Energy Efficiency of Good Color Quality Street Lighting Lamps"
			:presentation-index 97
			:speaker {Vicenzo Calabreze}
			:subevent-of {Poster P1}
			:abstract "Energy Efficiency of Good Color Quality Street Lighting Lamps")

(new-radar-presentation "Safety Assessment Techniques for Center Median Parkways"
			:presentation-index 98
			:speaker {Teo Ciccoli}
			:subevent-of {Poster P1}
			:abstract "Safety Assessment Techniques for Center Median Parkways")

(new-radar-presentation "Throughput Estimates for Automated Highway Systems"
			:presentation-index 99
			:speaker {Jack Petersen}
			:subevent-of {Poster P1}
			:abstract "Throughput Estimates for Automated Highway Systems")

(new-radar-presentation "Smog Reduction as a Result of AHS"
			:presentation-index 100
			:speaker {Lupe Bucca}
			:subevent-of {Poster P1}
			:abstract "Smog Reduction as a Result of AHS")

(new-radar-presentation "Pittsburgh Ride Share Uptake Figures After Widespread Marketing"
			:presentation-index 101
			:speaker {Jan Wilson}
			:subevent-of {Poster P1}
			:abstract "Pittsburgh Ride Share Uptake Figures After Widespread Marketing")

(new-radar-presentation "511 Usage Patterns in the District of Columbia"
			:presentation-index 102
			:speaker {Brenda Smallman}
			:subevent-of {Poster P1}
			:abstract "511 Usage Patterns in the District of Columbia")

(new-radar-presentation "Flex Route Results for Helsinki Metro Transport"
			:presentation-index 103
			:speaker {Richard Holley}
			:subevent-of {Poster P1}
			:abstract "Flex Route Results for Helsinki Metro Transport")

(new-radar-gathering "Session A2"
		     :gathering-index 37
		     :title "Cooperative Cruise Control"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Louisa Hollands}
		     :location {grass})

(new-radar-presentation "String Stability for 20-Car Cooperative Cruise Control Platoons"
			:presentation-index 104
			:speaker {Louisa Hollands}
			:subevent-of {Session A2}
			:abstract "String Stability for 20-Car Cooperative Cruise Control Platoons")

(new-radar-presentation "ACC vs. CCC: Monte Carlo Simulations of String Stability"
			:presentation-index 105
			:speaker {Jennifer Morris}
			:subevent-of {Session A2}
			:abstract "ACC vs. CCC: Monte Carlo Simulations of String Stability")

(new-radar-presentation "Modulated LED Tail Light Inter-Vehicle Communications"
			:presentation-index 106
			:speaker {Angelica Antonias}
			:subevent-of {Session A2}
			:abstract "Modulated LED Tail Light Inter-Vehicle Communications")

(new-radar-presentation "Wireless Requirements for Cooperative Cruise Control"
			:presentation-index 107
			:speaker {Penelope Benold}
			:subevent-of {Session A2}
			:abstract "Wireless Requirements for Cooperative Cruise Control")

(new-radar-gathering "Session A3"
		     :gathering-index 38
		     :title "Safety Systems 1"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Stuart E. Milton}
		     :location {grass})

(new-radar-presentation "Implications of Recent NTSB Findings"
			:presentation-index 108
			:speaker {Stuart E. Milton}
			:subevent-of {Session A3}
			:abstract "Implications of Recent NTSB Findings")

(new-radar-presentation "The Braking Trap: How ACC Can Make Matters Worse"
			:presentation-index 109
			:speaker {Patricia Minor}
			:subevent-of {Session A3}
			:abstract "The Braking Trap: How ACC Can Make Matters Worse")

(new-radar-presentation "Stability Control for Four Wheel Steering"
			:presentation-index 110
			:speaker {Staffan Bjorn}
			:subevent-of {Session A3}
			:abstract "Stability Control for Four Wheel Steering")

(new-radar-presentation "Post-Crash Analysis of Stability Control Systems"
			:presentation-index 111
			:speaker {Patrick Fitzsimmons}
			:subevent-of {Session A3}
			:abstract "Post-Crash Analysis of Stability Control Systems")

(new-radar-gathering "Session C2"
		     :gathering-index 39
		     :title "Drowsy Driving"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Brian Car}
		     :location {grass})

(new-radar-presentation "The Impact of the New Hours of Service Rules"
			:presentation-index 112
			:speaker {Brian Car}
			:subevent-of {Session C2}
			:abstract "The Impact of the New Hours of Service Rules")

(new-radar-presentation "Trends in Highway Rest Areas"
			:presentation-index 113
			:speaker {Lucas Zettle}
			:subevent-of {Session C2}
			:abstract "Trends in Highway Rest Areas")

(new-radar-presentation "Low-Cost Drowsiness Detection"
			:presentation-index 114
			:speaker {Jun Lee}
			:subevent-of {Session C2}
			:abstract "Low-Cost Drowsiness Detection")

(new-radar-presentation "Less-Than-Load Drowsiness Crash Rates"
			:presentation-index 115
			:speaker {Luke Moyer}
			:subevent-of {Session C2}
			:abstract "Less-Than-Load Drowsiness Crash Rates")

(new-radar-gathering "Session M1"
		     :gathering-index 40
		     :title "Best Paper"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Michelle Wooldridge}
		     :location {grass})

(new-radar-presentation "A Three State Collision Mitigation System"
			:presentation-index 116
			:speaker {Michelle Wooldridge}
			:subevent-of {Session M1}
			:abstract "A Three State Collision Mitigation System")

(new-radar-presentation "Field-Operational Test Results for the Auto-Inflate Tire System"
			:presentation-index 117
			:speaker {John Connor}
			:subevent-of {Session M1}
			:abstract "Field-Operational Test Results for the Auto-Inflate Tire System")

(new-radar-presentation "Non-Linear Models of Congestion in Mid-Size Metropolitan Areas"
			:presentation-index 118
			:speaker {Tom Thoreau}
			:subevent-of {Session M1}
			:abstract "Non-Linear Models of Congestion in Mid-Size Metropolitan Areas")

(new-radar-presentation "Pedestrian Detection via Fused LADAR and Thermal Imagery"
			:presentation-index 119
			:speaker {Motohiko Haguchi}
			:subevent-of {Session M1}
			:abstract "Pedestrian Detection via Fused LADAR and Thermal Imagery")

(new-radar-gathering "Session O1"
		     :gathering-index 41
		     :title "Incident Detection"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Patti Minel}
		     :location {grass})

(new-radar-presentation "Effective Probe Vehicle Populations"
			:presentation-index 120
			:speaker {Patti Minel}
			:subevent-of {Session O1}
			:abstract "Effective Probe Vehicle Populations")

(new-radar-presentation "Automated Camera-Based Incident Detection"
			:presentation-index 121
			:speaker {Hope Ezzard}
			:subevent-of {Session O1}
			:abstract "Automated Camera-Based Incident Detection")

(new-radar-presentation "Ground Loop Detector Models for Incident Event Identification"
			:presentation-index 122
			:speaker {Johannes Copper}
			:subevent-of {Session O1}
			:abstract "Ground Loop Detector Models for Incident Event Identification")

(new-radar-presentation "911 Call Patterns for Crashes in Los Angeles"
			:presentation-index 123
			:speaker {Matushi Sakaguchi}
			:subevent-of {Session O1}
			:abstract "911 Call Patterns for Crashes in Los Angeles")

(new-radar-gathering "Session T3"
		     :gathering-index 42
		     :title "Collision Warning Systems for Buses"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Iris Johnson}
		     :location {grass})

(new-radar-presentation "Collision Warning Ramifications of Behaviors Specific to Buses"
			:presentation-index 124
			:speaker {Iris Johnson}
			:subevent-of {Session T3}
			:abstract "Collision Warning Ramifications of Behaviors Specific to Buses")

(new-radar-presentation "Analysis of Candidate CWS Display Location in Bus Cabs"
			:presentation-index 125
			:speaker {Lauren Sherman}
			:subevent-of {Session T3}
			:abstract "Analysis of Candidate CWS Display Location in Bus Cabs")

(new-radar-presentation "Vehicles Behind Buses: A Rear-End Collision Warning System"
			:presentation-index 126
			:speaker {Lucinda Hollister}
			:subevent-of {Session T3}
			:abstract "Vehicles Behind Buses: A Rear-End Collision Warning System")

(new-radar-presentation "Avoiding Collisions in Transit Maintenance Yards"
			:presentation-index 127
			:speaker {Peter Ferguson}
			:subevent-of {Session T3}
			:abstract "Avoiding Collisions in Transit Maintenance Yards")

(new-radar-gathering "Session T4"
		     :gathering-index 43
		     :title "Increasing Ridership via AVL"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {James P. Sontag}
		     :location {grass})

(new-radar-presentation "Key Benefits of Automatic Vehicle Location to Schedule Planning"
			:presentation-index 128
			:speaker {James P. Sontag}
			:subevent-of {Session T4}
			:abstract "Key Benefits of Automatic Vehicle Location to Schedule Planning")

(new-radar-presentation "Dynamic Rider Route Guidance via AVL Data and Walking Maps"
			:presentation-index 129
			:speaker {Miho Sakato}
			:subevent-of {Session T4}
			:abstract "Dynamic Rider Route Guidance via AVL Data and Walking Maps")

(new-radar-presentation "Next Bus Information on Mobile Phones"
			:presentation-index 130
			:speaker {Jarrod Michaels}
			:subevent-of {Session T4}
			:abstract "Next Bus Information on Mobile Phones")

(new-radar-presentation "The Impact of Bus Arrival Time Predictions in Urban Areas"
			:presentation-index 131
			:speaker {Samuel Johns}
			:subevent-of {Session T4}
			:abstract "The Impact of Bus Arrival Time Predictions in Urban Areas")

(new-radar-gathering "Plenary 3"
		     :gathering-index 44
		     :title "Why ITS Matters"
		     :types '({plenary} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Demo M2"
		     :gathering-index 45
		     :title "In-Vehicle Entertainment Systems"
		     :types '({demo} {open gathering})
		     :person-in-charge {Andrew Akins}
		     :location {grass})

(new-radar-presentation "Find-A-Buddy Communication Systems"
			:presentation-index 132
			:speaker {Andrew Akins}
			:subevent-of {Demo M2}
			:abstract "Find-A-Buddy Communication Systems")

(new-radar-presentation "Movies On Demand Using Wireless Data Transfer"
			:presentation-index 133
			:speaker {Russell Piper}
			:subevent-of {Demo M2}
			:abstract "Movies On Demand Using Wireless Data Transfer")

(new-radar-presentation "Driving Games Meet the Digital Age"
			:presentation-index 134
			:speaker {William B. Bernstein}
			:subevent-of {Demo M2}
			:abstract "Driving Games Meet the Digital Age")

(new-radar-presentation "Impact Safe Large Screen Displays"
			:presentation-index 135
			:speaker {Donald N. Hartfield}
			:subevent-of {Demo M2}
			:abstract "Impact Safe Large Screen Displays")

(new-radar-gathering "Panel O2"
		     :gathering-index 46
		     :title "High Occupancy Vehicle Lanes"
		     :types '({panel} {open gathering})
		     :person-in-charge {Sam Ketterson}
		     :location {grass})

(new-radar-presentation "Panel O2.1"
			:presentation-index 136
			:speaker {Sam Ketterson}
			:subevent-of {Panel O2})

(new-radar-presentation "Panel O2.2"
			:presentation-index 137
			:speaker {Kieth deCorta}
			:subevent-of {Panel O2})

(new-radar-presentation "Panel O2.3"
			:presentation-index 138
			:speaker {Harold Mientras}
			:subevent-of {Panel O2})

(new-radar-presentation "Panel O2.4"
			:presentation-index 139
			:speaker {Stanley Boznik}
			:subevent-of {Panel O2})

(new-radar-gathering "Poster A1"
		     :gathering-index 47
		     :title "Automobiles"
		     :types '({poster session} {open gathering})
		     :person-in-charge {Jeanne Santos}
		     :location {grass})

(new-radar-presentation "Critical Incident Analysis of Food-Related Distraction Periods"
			:presentation-index 140
			:speaker {Jeanne Santos}
			:subevent-of {Poster A1}
			:abstract "Critical Incident Analysis of Food-Related Distraction Periods")

(new-radar-presentation "Ranging of Roadside Foliage"
			:presentation-index 141
			:speaker {David Rodgerson}
			:subevent-of {Poster A1}
			:abstract "Ranging of Roadside Foliage")

(new-radar-presentation "Radar Reflective Tagging of Road Signs"
			:presentation-index 142
			:speaker {John Constable}
			:subevent-of {Poster A1}
			:abstract "Radar Reflective Tagging of Road Signs")

(new-radar-presentation "ACC Sensor Performance on Curved Roads"
			:presentation-index 143
			:speaker {Guillermo Credon}
			:subevent-of {Poster A1}
			:abstract "ACC Sensor Performance on Curved Roads")

(new-radar-presentation "Preemptive Safety Belt Pretensioning"
			:presentation-index 144
			:speaker {Nigel Contorso}
			:subevent-of {Poster A1}
			:abstract "Preemptive Safety Belt Pretensioning")

(new-radar-presentation "Young Children and Distracted Driving"
			:presentation-index 145
			:speaker {Theodore Callman}
			:subevent-of {Poster A1}
			:abstract "Young Children and Distracted Driving")

(new-radar-presentation "Eye Gaze Patterns When Driving in Snow"
			:presentation-index 146
			:speaker {Christine Johnston}
			:subevent-of {Poster A1}
			:abstract "Eye Gaze Patterns When Driving in Snow")

(new-radar-presentation "Wireless Communication with Roadway Fog Sensors"
			:presentation-index 147
			:speaker {Emily Halwizer}
			:subevent-of {Poster A1}
			:abstract "Wireless Communication with Roadway Fog Sensors")

(new-radar-presentation "Lessons for Passenger Vehicles from the CVO Community"
			:presentation-index 148
			:speaker {Dennis Parrott}
			:subevent-of {Poster A1}
			:abstract "Lessons for Passenger Vehicles from the CVO Community")

(new-radar-presentation "Roadside Infrastructure for Intersection Collision Warning"
			:presentation-index 149
			:speaker {Bob Halstear}
			:subevent-of {Poster A1}
			:abstract "Roadside Infrastructure for Intersection Collision Warning")

(new-radar-presentation "The Telematics Killer App - Still Searching"
			:presentation-index 150
			:speaker {Daniel Hendridge}
			:subevent-of {Poster A1}
			:abstract "The Telematics Killer App - Still Searching")

(new-radar-presentation "Deer-Car Crash Prevention"
			:presentation-index 151
			:speaker {Andreas Schaeffer}
			:subevent-of {Poster A1}
			:abstract "Deer-Car Crash Prevention")

(new-radar-presentation "Misperception of Train Speed at Railroad Crossings"
			:presentation-index 152
			:speaker {Gene Hawkings}
			:subevent-of {Poster A1}
			:abstract "Misperception of Train Speed at Railroad Crossings")

(new-radar-presentation "Glare Characteristics of Variable Halogen Headlamps"
			:presentation-index 153
			:speaker {Troy Costales}
			:subevent-of {Poster A1}
			:abstract "Glare Characteristics of Variable Halogen Headlamps")

(new-radar-gathering "Session A4"
		     :gathering-index 48
		     :title "Safety Systems 2"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Phillip Hardunn}
		     :location {grass})

(new-radar-presentation "An Integrated ACC and CWS Driver Interface"
			:presentation-index 154
			:speaker {Phillip Hardunn}
			:subevent-of {Session A4}
			:abstract "An Integrated ACC and CWS Driver Interface")

(new-radar-presentation "Sliding Autonomy for Lane-Keeping Assistance"
			:presentation-index 155
			:speaker {Natalia Kinski}
			:subevent-of {Session A4}
			:abstract "Sliding Autonomy for Lane-Keeping Assistance")

(new-radar-presentation "Mixed Initiative Collision Avoidance"
			:presentation-index 156
			:speaker {Raquel Diano}
			:subevent-of {Session A4}
			:abstract "Mixed Initiative Collision Avoidance")

(new-radar-presentation "Assessing Older Driver Competency with a Web-Based Test"
			:presentation-index 157
			:speaker {John Drexel}
			:subevent-of {Session A4}
			:abstract "Assessing Older Driver Competency with a Web-Based Test")

(new-radar-gathering "Session M2"
		     :gathering-index 49
		     :title "Potpourri"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Debbie Cotton}
		     :location {grass})

(new-radar-presentation "Heuristics for Variable License Renewal Exams"
			:presentation-index 158
			:speaker {Debbie Cotton}
			:subevent-of {Session M2}
			:abstract "Heuristics for Variable License Renewal Exams")

(new-radar-presentation "Modeling Pedestrian Traffic on the Charles Bridge"
			:presentation-index 159
			:speaker {Elise Steiner}
			:subevent-of {Session M2}
			:abstract "Modeling Pedestrian Traffic on the Charles Bridge")

(new-radar-presentation "Bus Mounted Bike Rack Utilization in the San Francisco Bay Area"
			:presentation-index 160
			:speaker {Hammond Hughes}
			:subevent-of {Session M2}
			:abstract "Bus Mounted Bike Rack Utilization in the San Francisco Bay Area")

(new-radar-presentation "The Impact of Adaptive Cruise Control on Congestion"
			:presentation-index 161
			:speaker {Gina Bertolucci}
			:subevent-of {Session M2}
			:abstract "The Impact of Adaptive Cruise Control on Congestion")

(new-radar-gathering "Session C3"
		     :gathering-index 50
		     :title "Electronic Braking Systems"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Mark Abokowicz}
		     :location {grass})

(new-radar-presentation "Closed Course Evaluation of the Atlantic Northwest Trailer EBS"
			:presentation-index 162
			:speaker {Mark Abokowicz}
			:subevent-of {Session C3}
			:abstract "Closed Course Evaluation of the Atlantic Northwest Trailer EBS")

(new-radar-presentation "Jackknife Crash Rates for Electronic Braking Systems"
			:presentation-index 163
			:speaker {Dan Murray}
			:subevent-of {Session C3}
			:abstract "Jackknife Crash Rates for Electronic Braking Systems")

(new-radar-presentation "EBS for Less-Than-Load Trailers"
			:presentation-index 164
			:speaker {Donald Reeves}
			:subevent-of {Session C3}
			:abstract "EBS for Less-Than-Load Trailers")

(new-radar-presentation "Articulated Hitch Sensors for Augmented EBS"
			:presentation-index 165
			:speaker {Henry M. Watson}
			:subevent-of {Session C3}
			:abstract "Articulated Hitch Sensors for Augmented EBS")

(new-radar-gathering "Session C4"
		     :gathering-index 51
		     :title "Tanker Trucks"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Phillipe Defrano}
		     :location {grass})

(new-radar-presentation "A Prototype Rollover Prevention System"
			:presentation-index 166
			:speaker {Phillipe Defrano}
			:subevent-of {Session C4}
			:abstract "A Prototype Rollover Prevention System")

(new-radar-presentation "Electronic HazMat Transponders"
			:presentation-index 167
			:speaker {Judith E. Payne}
			:subevent-of {Session C4}
			:abstract "Electronic HazMat Transponders")

(new-radar-presentation "Intelligent Refrigeration Control in Tanker Trailers"
			:presentation-index 168
			:speaker {Judith E. Payne}
			:subevent-of {Session C4}
			:abstract "Intelligent Refrigeration Control in Tanker Trailers")

(new-radar-presentation "Dynamic Suspension for Liquid Load Balancing"
			:presentation-index 169
			:speaker {Sean Martin}
			:subevent-of {Session C4}
			:abstract "Dynamic Suspension for Liquid Load Balancing")

(new-radar-gathering "Session O2"
		     :gathering-index 52
		     :title "Truck Inspection"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Casper Miles}
		     :location {grass})

(new-radar-presentation "AVL Systems for Homeland Security Sensitive Cargo"
			:presentation-index 170
			:speaker {Casper Miles}
			:subevent-of {Session O2}
			:abstract "AVL Systems for Homeland Security Sensitive Cargo")

(new-radar-presentation "Weigh-In-Motion via RFID Tags"
			:presentation-index 171
			:speaker {Francine Stimmel}
			:subevent-of {Session O2}
			:abstract "Weigh-In-Motion via RFID Tags")

(new-radar-presentation "Tire Tread Wear from High Resolution LIDAR Scans"
			:presentation-index 172
			:speaker {Terrence Porteus}
			:subevent-of {Session O2}
			:abstract "Tire Tread Wear from High Resolution LIDAR Scans")

(new-radar-presentation "A Novel Method for Rapid Emissions Testing"
			:presentation-index 173
			:speaker {Dino Smith}
			:subevent-of {Session O2}
			:abstract "A Novel Method for Rapid Emissions Testing")

(new-radar-gathering "Registration & Breakfast (Mon)"
		     :gathering-index 53
		     :title "Registration & Breakfast (Monday)"
		     :types '({administrative} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Registration & Breakfast (Tue)"
		     :gathering-index 54
		     :title "Registration & Breakfast (Tuesday)"
		     :types '({administrative} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Registration & Breakfast (Wed)"
		     :gathering-index 55
		     :title "Registration & Breakfast (Wednesday)"
		     :types '({administrative} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Registration & Breakfast (Thu)"
		     :gathering-index 56
		     :title "Registration & Breakfast (Thursday)"
		     :types '({administrative} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Conference Committee (Mon)"
		     :gathering-index 57
		     :title "Conference Committee (Monday)"
		     :types '({administrative} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Conference Committee (Tue)"
		     :gathering-index 58
		     :title "Conference Committee (Tuesday)"
		     :types '({administrative} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Conference Committee (Wed)"
		     :gathering-index 59
		     :title "Conference Committee (Wednesday)"
		     :types '({administrative} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Conference Committee (Thu)"
		     :gathering-index 60
		     :title "Conference Committee (Thursday)"
		     :types '({administrative} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Break (Mon AM)"
		     :gathering-index 61
		     :title "Break (Monday AM)"
		     :types '({conference break} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Break (Mon PM)"
		     :gathering-index 62
		     :title "Break (Monday PM)"
		     :types '({conference break} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Break (Tue AM)"
		     :gathering-index 63
		     :title "Break (Tuesday AM)"
		     :types '({conference break} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Break (Tue PM)"
		     :gathering-index 64
		     :title "Break (Tuesday PM)"
		     :types '({conference break} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Break (Wed AM)"
		     :gathering-index 65
		     :title "Break (Wednesday AM)"
		     :types '({conference break} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Break (Wed PM)"
		     :gathering-index 66
		     :title "Break (Wednesday PM)"
		     :types '({conference break} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Break (Thu AM)"
		     :gathering-index 67
		     :title "Break (Thursday AM)"
		     :types '({conference break} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Break (Thu PM)"
		     :gathering-index 68
		     :title "Break (Thursday PM)"
		     :types '({conference break} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Exhibits Setup"
		     :gathering-index 69
		     :title "Exhibits Setup"
		     :types '({exhibit} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Exhibits (Tue)"
		     :gathering-index 70
		     :title "Exhibits (Tuesday)"
		     :types '({conference break} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Exhibits (Wed)"
		     :gathering-index 71
		     :title "Exhibits (Wednesday)"
		     :types '({exhibit} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Exhibits (Thu)"
		     :gathering-index 72
		     :title "Exhibits (Thursday)"
		     :types '({exhibit} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Exhibit Teardown"
		     :gathering-index 73
		     :title "Exhibit Teardown"
		     :types '({exhibit} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Lunch & Keynote"
		     :gathering-index 74
		     :title "Lunch & Keynote"
		     :types '({conference lunch} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Exec Comm"
		     :gathering-index 75
		     :title "Exec Comm"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Special Interest 1"
		     :gathering-index 76
		     :title "Special Interest 1"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Special Interest 2"
		     :gathering-index 77
		     :title "Special Interest 2"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Special Interest 3"
		     :gathering-index 78
		     :title "Special Interest 3"
		     :types '({technical paper session} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Student Social"
		     :gathering-index 79
		     :title "Student Social"
		     :types '({party} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Exhibitor Social 1"
		     :gathering-index 80
		     :title "Exhibitor Social 1"
		     :types '({party} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Exhibitor Social 2"
		     :gathering-index 81
		     :title "Exhibitor Social 2"
		     :types '({party} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Exhibitor Social 3"
		     :gathering-index 82
		     :title "Exhibitor Social 3"
		     :types '({party} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})

(new-radar-gathering "Banquet"
		     :gathering-index 83
		     :title "Banquet"
		     :types '({party} {open gathering})
		     :person-in-charge {Jonathon Robertson}
		     :location {grass})
