;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Code for generating a Scone dictionary for Radar 1.0.
;;; Includes  (makedict) (register-person-phone-numbers) (register-person-email-addresses)
;;;
;;; AUTHOR: Alicia Tribble <atribble@cs.cmu.edu>
;;; ***************************************************************************

;;; Copyright (C) 2003-2005, Carnegie Mellon University.

;;; The Scone software is made available to the public under the CPL 1.0
;;; Open Source license.  A copy of this license is distributed with the
;;; software.  The license can also be found at URL
;;; <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone
;;; software, you agree to be bound by the terms and conditions set
;;; forth in the CPL 1.0 Open Source License.  If you do not agree to
;;; these terms and conditions, or if they are not legally applicable
;;; in the jurisdiction where such use takes place, then you may not
;;; use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by
;;; Scott E. Fahlman for IBM Coporation between June 2001 and May
;;; 2003.  IBM holds the copyright on NETL2 and has made that software
;;; available to the author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under contract numbers NBCHC030029 and
;;; NBCHC030030.  Any opinions, findings and conclusions or recommendations
;;; expressed in this material are those of the author(s) and do not
;;; necessarily reflect the views of DARPA or the Department of
;;; Interior-National Business Center (DOI-NBC).


;;; ***************************************************************************
;;; register-person-phone-numbers, register-person-email-addresses
;;; 
;;; Register all the person emails and phone numbers in the current dictionary.
;;;

(defun register-person-phone-numbers ()
  "For every proper-indv person in the current kb, 
   get their phone number and associate 
   these strings with the :english
   for that node.  

   Need to run this once before generating the dictionaries.
  "
  (with-temp-markers (m1)
    (downscan {person} m1)
    (do-marked (person m1)
      (if (proper-indv-node? person)
	  (let ((phone (get-the-x-of-y {primary phone number} person)))
	    (when phone
	      (english person (element-name phone))))))))

(defun register-person-email-addresses ()
  "For every proper-indv person in the current kb, 
   get their email address and associate 
   these strings with the :english
   for that node.

   Need to run this once before generating the dictionaries.
  "
  (with-temp-markers (m1)
    (downscan {person} m1)
    (do-marked (person m1)
      (if (proper-indv-node? person)
	  (let ((email (get-the-x-of-y {person email address} person)))
	    (when email
	      (setf email (element-name email))
	      (english person email)
	      ;; NEW: these typos should be fixed in the source SQL, so
	      ;; we don't need to compensate for them anymore.
	      ;; Add an additional fix for the .com / .org confusion
	      ;; (setf email (replace email ".org" :start1 (- (length email) 4)))
	      (english person email)))))))


;;; ***************************************************************************
;;; makedict
;;;
;;; Print out the current Scone dictionary in a format that can be read by RADAR.
;;;

;; Original function written by Othar
(defun othar-makedict()
  (let ((text nil) (tag-name nil) (dict nil))
    (dolist (hit (list-dictionary))
      (setf text (car hit))
      (setf tag-name (car (car (cadr hit))))
      (setf dict (append dict
                         (list
                          (format nil "~A        |        ~A        ~A"
                                  text
                                  "element-iname"
                                  tag-name)))))
    dict))


;; Tab character definition
(defconstant tabchar #\tab)


;; Modified by Alicia
;;
;; Generate a text file based on the Scone dictionary, for use
;; with the Radar classifier.
;;
;; If ANCESTORS is :immediate, print the parent of each node.
;; If ANCESTORS is :filtered, print ancestors subject to a filter for 
;; non-informative supertypes.
;; If ANCESTORS is t, print all ancestors of each node.
;; If ANCESTORS is nil, don't print any ancestors.  
;; 
;; If ALLOW-DUPLICATES is t, print all meanings for ambiguous terms.
;; If ALLOW-DUPLICATES is nil, print only the first meaning for ambiguous terms.
;; If ALLOW-DUPLICATES is :none, print no entry for ambiguous terms, 
;; only for the dictionary entries that are unambiguous.
;;
;; If PRINT-TO-FILE is non-nil, print the dictionary to a 
;; file specified by the value of PRINT-TO-FILE.
;;
;; Also produces "conference-speaker" and "conference-organizer" features
;; where appropriate.
;;
(defun makedict (&key (ancestors :immediate)
                      (allow-duplicates nil)
                      (print-to-file nil))
  (let ((text nil) (tag-name nil) (dict nil) (dict-entry nil) (parent nil) (to-remove nil))
    ;; For each entry in the dictionary

    (dolist (hit (list-dictionary))
      ;;(dolist (hit (remove-if-not #'(lambda (x) (equal (first x) "LENGTH")) (list-dictionary)))

      (setf text (car hit))
      ;; Find each Scone element associated with this text
      (dolist (pair (second hit))
        (setf tag-name (car pair))
        (setf dict-entry
              (format nil "~A~A|~A~A~A"
                      text tabchar
                      "element-iname" tabchar
                      tag-name))
        ;; ANCESTORS 
        (when ancestors 
          (cond ((eq ancestors ':immediate)
                 (setf parent (parent-wire (lookup-element tag-name)))
                 (when parent
                   (setf dict-entry
                         (format nil 
                                 "~A~Aparent-iname~A~A" 
                                 dict-entry tabchar tabchar
                                 (parent-wire (lookup-element tag-name))))))
                (t
                 (with-temp-markers (m1 m2)
		   (upscan tag-name m1)
		   (unmark tag-name m1)
		   (if (eq ancestors ':filtered)
		       (unmark-frequent-ancestors m1))
		   (do-marked (e m1)
		     (setf dict-entry (format nil
					      "~A~Aancestor-iname~A~A"
					      dict-entry tabchar tabchar
					      e)))))))
        ;; CONFERENCE-SPEAKER / CONFERENCE-ORGANIZER
        (when (is-x-a-y? tag-name {person})
          (when (is-x-a-y? tag-name {speaker})
            (setf dict-entry
                  (format nil 
                          "~A~Aconference-speaker~Atrue" 
                          dict-entry tabchar tabchar)))
          (when (is-x-a-y? tag-name {person in charge})
            (setf dict-entry
                  (format nil 
                          "~A~Aconference-organizer~Atrue" 
                          dict-entry tabchar tabchar))))

        ;; ALLOW-DUPLICATES
        (cond 
	  ((null allow-duplicates);; ALLOW-DUPLICATES is NIL
	   (pushnew dict-entry dict   
		    :test 'equal 
		    :key #'(lambda (entry) (entry-key entry))))

	  ((eq allow-duplicates ':none);;ALLOW-DUPLICATES is :none
	   ;; If the entry exists already, add it to to-remove.
	   (if (member text dict 
		       :test 'equal 
		       :key  #'(lambda (entry) (entry-key entry)))
	       (pushnew text to-remove)
	       ;; If it's new, add the entry to dict.
	       (push dict-entry dict)))
	  (t (push dict-entry dict)))));; ALLOW-DUPLICATES is 't
    ;; ALLOW-DUPLICATES :none
    (when (eq allow-duplicates ':none)
      ;;(format t "~%ALLOWING NO DUPLICATES~%")
      ;;(format t "to-remove: type ~A~%" (type-of to-remove))
      ;;(dolist (r to-remove) (format t "~A " r)) (format t "~%")
      (setf dict
            (delete-if  #'(lambda (e) (member (entry-key e) to-remove :test 'equal))
                        dict)))
    ;; PRINT-TO-FILE
    (when print-to-file
      (with-open-file 
	  (ofile print-to-file :direction :output)
	(format ofile ";;; Generated automatically by makedict ~%")
	(format ofile ";;; Parameter Settings: ancestors ~A, allow-duplicates ~A~%"
		ancestors allow-duplicates)
	(dolist (dict-entry (nreverse dict))
	  (format ofile "~A~%" dict-entry))))
    (nreverse dict)))


(defun unmark-frequent-ancestors (m)
  "Examine the nodes marked with M, and remove top-level
   nodes that are uninformative.  Specifically: 
    If {person} is marked, unmark all ancestors of {person}.
    If {building} is marked, unmark all ancestors of {building}.
    If {room} is marked, unmark all ancestors of {room}.
    If {event} is marked, unmark all ancestors of {event}.
  "
  (with-temp-markers (m1)
                     (when (marker-on? {person} m)
                       (upscan {person} m1)
                       (unmark {person} m1)
                       (do-marked (e m1) (if (marker-on? e m) (unmark e m))))
                     (when (marker-on? {room} m)
                       (upscan {person} m1)
                       (unmark {person} m1)
                       (do-marked (e m1) (if (marker-on? e m) (unmark e m))))
                     (when (marker-on? {building} m)
                       (upscan {person} m1)
                       (unmark {person} m1)
                       (do-marked (e m1) (if (marker-on? e m) (unmark e m))))
                     (when (marker-on? {event} m)
                       (upscan {person} m1)
                       (unmark {person} m1)
                       (do-marked (e m1) (if (marker-on? e m) (unmark e m))))))

(defun entry-key (dict-entry)
  "Retrieve the key from a dictionary entry already stored in output format"
  (string-trim '(#\tab) (subseq dict-entry 0 (search "|" dict-entry))))


(defun makedict-run-all-parameters ()
  "Run (makedict) with several paremeter variations.
   Used to generate files for experiments with the classifier."
  (register-person-phone-numbers)
  (register-person-email-addresses)
  (dolist (ancestors '(nil t :filtered :immediate))
    (dolist (allow-duplicates '(nil t :none))
      (let ((filename 
             (format nil "radar-scone.ancestors-~A.duplicates-~A.dict"
                     ancestors allow-duplicates)))
        (format t "Generating ~A~%" filename)
        (makedict :ancestors ancestors 
                  :allow-duplicates allow-duplicates
                  :print-to-file filename)))))
                              

