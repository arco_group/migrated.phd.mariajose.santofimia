;;; Rooms known to Radar in the test world.
;;; Automatically derived from the SQL files.

(new-indv {UC} {building})
(new-indv {Flagstaff} {building})
(new-indv {Stever} {building})


(new-radar-room "Connan"
		:room-index 1
		:building {UC})

(new-radar-room "Auditorium"
		:room-index 2
		:building {UC})

(new-radar-room "1987 Room"
		:room-index 3
		:building {UC})

(new-radar-room "Pake"
		:room-index 4
		:building {UC})

(new-radar-room "Dowd"
		:room-index 5
		:building {UC})

(new-radar-room "McKenna"
		:room-index 6
		:building {UC})

(new-radar-room "Peter"
		:room-index 7
		:building {UC})

(new-radar-room "Rangos 2 Sections"
		:room-index 8
		:building {UC})

(new-radar-room "Gym"
		:room-index 9
		:building {UC})

(new-radar-room "Conference A"
		:room-index 10
		:building {Flagstaff})

(new-radar-room "Conference B"
		:room-index 11
		:building {Flagstaff})

(new-radar-room "Conference C"
		:room-index 12
		:building {Flagstaff})

(new-radar-room "Conference D"
		:room-index 13
		:building {Flagstaff})

(new-radar-room "Conference E"
		:room-index 14
		:building {Flagstaff})

(new-radar-room "Ballroom/Urban"
		:room-index 15
		:building {Flagstaff})

(new-radar-room "Conference Foyer"
		:room-index 16
		:building {Flagstaff})

(new-radar-room "Frick"
		:room-index 17
		:building {Flagstaff})

(new-radar-room "Phipps"
		:room-index 18
		:building {Flagstaff})

(new-radar-room "Oliver"
		:room-index 19
		:building {Flagstaff})

(new-radar-room "Vandergrift"
		:room-index 20
		:building {Flagstaff})

(new-radar-room "Laughlin"
		:room-index 21
		:building {Flagstaff})

(new-radar-room "Heinz"
		:room-index 22
		:building {Flagstaff})

(new-radar-room "Parkview West"
		:room-index 23
		:building {Flagstaff})

(new-radar-room "Parkview West Foyer"
		:room-index 24
		:building {Flagstaff})

(new-radar-room "Parkview East"
		:room-index 25
		:building {Flagstaff})

(new-radar-room "Carnegie I"
		:room-index 26
		:building {Flagstaff})

(new-radar-room "Carnegie II"
		:room-index 27
		:building {Flagstaff})

(new-radar-room "William Penn Ballroom"
		:room-index 28
		:building {Flagstaff})

(new-radar-room "Three Rivers"
		:room-index 29
		:building {Flagstaff})

(new-radar-room "Sternwheeler"
		:room-index 30
		:building {Flagstaff})

(new-radar-room "Riverboat"
		:room-index 31
		:building {Flagstaff})

(new-radar-room "Oakmont"
		:room-index 32
		:building {Flagstaff})

(new-radar-room "Fox Chapel"
		:room-index 33
		:building {Flagstaff})

(new-radar-room "Churchill"
		:room-index 34
		:building {Flagstaff})

(new-radar-room "Mt. Lebanon"
		:room-index 35
		:building {Flagstaff})

(new-radar-room "Sewickley"
		:room-index 36
		:building {Flagstaff})

(new-radar-room "Shadyside"
		:room-index 37
		:building {Flagstaff})

(new-radar-room "Paneled/Oval"
		:room-index 38
		:building {Flagstaff})

(new-radar-room "Grant Suite A"
		:room-index 39
		:building {Flagstaff})

(new-radar-room "Grant Suite B"
		:room-index 40
		:building {Flagstaff})

(new-radar-room "Grant Suite C"
		:room-index 41
		:building {Flagstaff})

(new-radar-room "Grant Suite Bar"
		:room-index 42
		:building {Flagstaff})

(new-radar-room "Grant Suite Foyer"
		:room-index 43
		:building {Flagstaff})

(new-radar-room "Grand Ballroom"
		:room-index 44
		:building {Flagstaff})

(new-radar-room "Ballroom w/Balcony"
		:room-index 45
		:building {Flagstaff})

(new-radar-room "Urban"
		:room-index 46
		:building {Flagstaff})

(new-radar-room "Monongahela"
		:room-index 47
		:building {Flagstaff})

(new-radar-room "Allegheny"
		:room-index 48
		:building {Flagstaff})

(new-radar-room "Parlor D"
		:room-index 49
		:building {Flagstaff})

(new-radar-room "Parlor G"
		:room-index 50
		:building {Flagstaff})

(new-radar-room "Parlor E/F"
		:room-index 51
		:building {Flagstaff})

(new-radar-room "Sky"
		:room-index 52
		:building {Flagstaff})

(new-radar-room "Stever 1250"
		:room-index 53
		:building {Stever})

(new-radar-room "Stever 1260"
		:room-index 54
		:building {Stever})

(new-radar-room "Stever 1265"
		:room-index 55
		:building {Stever})

(new-radar-room "Stever 1270"
		:room-index 56
		:building {Stever})

(new-radar-room "Stever 1340"
		:room-index 57
		:building {Stever})

(new-radar-room "Stever 1345"
		:room-index 58
		:building {Stever})

(new-radar-room "Stever 1350"
		:room-index 59
		:building {Stever})

(new-radar-room "Stever 1420"
		:room-index 60
		:building {Stever})

(new-radar-room "Stever 1435"
		:room-index 61
		:building {Stever})

(new-radar-room "Stever 1440"
		:room-index 62
		:building {Stever})

(new-radar-room "Stever 2255"
		:room-index 63
		:building {Stever})

(new-radar-room "Stever 2260"
		:room-index 64
		:building {Stever})

(new-radar-room "Stever 2355"
		:room-index 65
		:building {Stever})

(new-radar-room "Stever 2420"
		:room-index 66
		:building {Stever})

(new-radar-room "Stever 2455"
		:room-index 67
		:building {Stever})

(new-radar-room "Stever 2450 A"
		:room-index 68
		:building {Stever})

(new-radar-room "Stever 2450 B"
		:room-index 69
		:building {Stever})

(new-radar-room "Stever 2450 C"
		:room-index 70
		:building {Stever})

(new-radar-room "Costa Auditorium"
		:room-index 71
		:building {Stever})

(new-radar-room "Dowd Auditorium"
		:room-index 72
		:building {Stever})

(new-radar-room "Landis Auditorium"
		:room-index 73
		:building {Stever})

(new-radar-room "Reception Foyer"
		:room-index 74
		:building {Stever})

(new-radar-room "Reception I"
		:room-index 75
		:building {Stever})

(new-radar-room "Reception II"
		:room-index 76
		:building {Stever})

(new-radar-room "Reception III"
		:room-index 77
		:building {Stever})

(new-radar-room "Reception IV"
		:room-index 78
		:building {Stever})

(new-radar-room "grass"
		:room-index 79)

