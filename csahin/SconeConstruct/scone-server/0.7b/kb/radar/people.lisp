;;; People known to Radar in the test world.
;;; Automatically derived from the SQL files.

(new-radar-person "Jonathon Robertson"
		  :person-index 1
		  :first-name "Jonathon"
		  :last-name "Robertson"
		  :email-address "jrobertson@ardra.com"
		  :primary-phone-number "412 281 1346"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Francine Allen"
		  :person-index 2
		  :first-name "Francine"
		  :last-name "Allen"
		  :email-address "fra@ardra.com"
		  :primary-phone-number "412 281 1348"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Tom Whittenburg"
		  :person-index 3
		  :first-name "Tom"
		  :last-name "Whittenburg"
		  :email-address "tlwhiten@ardra.com"
		  :primary-phone-number "412 281 1350"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Jan Wilson"
		  :person-index 4
		  :first-name "Jan"
		  :last-name "Wilson"
		  :email-address "janice@ardra.com"
		  :primary-phone-number "412 281 1352"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Michael Winslow"
		  :person-index 5
		  :first-name "Michael"
		  :last-name "Winslow"
		  :email-address "mrwins@ardra.com"
		  :primary-phone-number "412 281 1354"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Rob Anderson"
		  :person-index 6
		  :first-name "Rob"
		  :last-name "Anderson"
		  :email-address "roa@ardra.com"
		  :primary-phone-number "412 281 1356"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Spence Pierro"
		  :person-index 7
		  :first-name "Spence"
		  :last-name "Pierro"
		  :email-address "spierro@ardra.com"
		  :primary-phone-number "412 281 1358"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Natalie Cohee"
		  :person-index 8
		  :first-name "Natalie"
		  :last-name "Cohee"
		  :email-address "ncohen@ardra.com"
		  :primary-phone-number "412 281 1360"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Julie Coil"
		  :person-index 9
		  :first-name "Julie"
		  :last-name "Coil"
		  :email-address "jcoyle@ardra.com"
		  :primary-phone-number "412 281 1362"
		  :radar-weight 50)

(new-radar-person "Meredith Lorenz"
		  :person-index 10
		  :first-name "Meredith"
		  :last-name "Lorenz"
		  :email-address "lorenze@ardra.com"
		  :primary-phone-number "412 281 1364"
		  :radar-weight 50)

(new-radar-person "Carl Spencer"
		  :person-index 11
		  :first-name "Carl"
		  :last-name "Spencer"
		  :email-address "cas@ardra.com"
		  :primary-phone-number "412 281 1366"
		  :radar-weight 50)

(new-radar-person "Art Furr"
		  :person-index 12
		  :first-name "Art"
		  :last-name "Furr"
		  :email-address "afurr@ardra.com"
		  :primary-phone-number "412 281 1368"
		  :radar-weight 50)

(new-radar-person "Blake Randal"
		  :person-index 13
		  :first-name "Blake"
		  :last-name "Randal"
		  :email-address "bor@ardra.com"
		  :primary-phone-number "412 281 1370"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Sam Katterson"
		  :person-index 14
		  :first-name "Sam"
		  :last-name "Katterson"
		  :email-address "skatters@ardra.com"
		  :primary-phone-number "412 281 1372"
		  :radar-weight 25)

(new-radar-person "Brad Cruise"
		  :person-index 15
		  :first-name "Brad"
		  :last-name "Cruise"
		  :email-address "bcruise@ardra.com"
		  :primary-phone-number "412 281 1374"
		  :radar-weight 50)

(new-radar-person "Andrea L. Manson"
		  :person-index 16
		  :first-name "Andrea"
		  :last-name "Manson"
		  :email-address "amanson@ardra.com"
		  :primary-phone-number "412 281 1376"
		  :radar-weight 25)

(new-radar-person "Steph Waggenman"
		  :person-index 17
		  :first-name "Steph"
		  :last-name "Waggenman"
		  :email-address "swaggenme@ardra.com"
		  :primary-phone-number "412 281 1378"
		  :radar-weight 25)

(new-radar-person "Elaine R. Simpson"
		  :person-index 18
		  :first-name "Elaine"
		  :last-name "Simpson"
		  :email-address "ems@ardra.com"
		  :primary-phone-number "412 281 1380"
		  :radar-weight 25)

(new-radar-person "Elizabeth McCall"
		  :person-index 19
		  :first-name "Elizabeth"
		  :last-name "McCall"
		  :email-address "lam@ardra.com"
		  :primary-phone-number "412 281 1382"
		  :radar-weight 25)

(new-radar-person "Agatha W. Daniels"
		  :person-index 20
		  :first-name "Agatha"
		  :last-name "Daniels"
		  :email-address "awest@ardra.com"
		  :primary-phone-number "412 281 1384"
		  :radar-weight 25)

(new-radar-person "Virgina N. Baker"
		  :person-index 21
		  :first-name "Virgina"
		  :last-name "Baker"
		  :email-address "vlb@ardra.com"
		  :primary-phone-number "412 281 1386"
		  :radar-weight 25)

(new-radar-person "Sophie Cooper"
		  :person-index 22
		  :first-name "Sophie"
		  :last-name "Cooper"
		  :email-address "stc@ardra.com"
		  :primary-phone-number "412 281 1388"
		  :radar-weight 25)

(new-radar-person "Isabelle Russel"
		  :person-index 23
		  :first-name "Isabelle"
		  :last-name "Russel"
		  :email-address "imr@ardra.com"
		  :primary-phone-number "412 281 1390"
		  :radar-weight 25)

(new-radar-person "Miles W. Ritter"
		  :person-index 24
		  :first-name "Miles"
		  :last-name "Ritter"
		  :email-address "mmr@ardra.com"
		  :primary-phone-number "412 281 1392"
		  :radar-weight 25)

(new-radar-person "Ryan McPeak"
		  :person-index 25
		  :first-name "Ryan"
		  :last-name "McPeak"
		  :email-address "rmcpeak@ardra.com"
		  :primary-phone-number "412 281 1394"
		  :radar-weight 25)

(new-radar-person "Hollie Fereday"
		  :person-index 26
		  :first-name "Hollie"
		  :last-name "Fereday"
		  :email-address "hfereday@ardra.com"
		  :primary-phone-number "412 281 1396"
		  :radar-weight 25)

(new-radar-person "Jack Fishbein"
		  :person-index 27
		  :first-name "Jack"
		  :last-name "Fishbein"
		  :email-address "jfishbein@ardra.com"
		  :primary-phone-number "412 281 1398"
		  :radar-weight 25)

(new-radar-person "Walter Evenson"
		  :person-index 28
		  :first-name "Walter"
		  :last-name "Evenson"
		  :email-address "wevenso@ardra.com"
		  :primary-phone-number "412 281 1400"
		  :radar-weight 25)

(new-radar-person "Gerald Buckner"
		  :person-index 29
		  :first-name "Gerald"
		  :last-name "Buckner"
		  :email-address "jbuckner@ardra.com"
		  :primary-phone-number "412 281 1402"
		  :radar-weight 25)

(new-radar-person "Marie Isner"
		  :person-index 30
		  :first-name "Marie"
		  :last-name "Isner"
		  :email-address "mei@ardra.com"
		  :primary-phone-number "412 281 1404"
		  :radar-weight 25)

(new-radar-person "Mary Anne Eisner"
		  :person-index 31
		  :first-name "Mary"
		  :last-name "Eisner"
		  :email-address "meisner@ardra.com"
		  :primary-phone-number "412 281 1406"
		  :radar-weight 25)

(new-radar-person "Louis Kabalero"
		  :person-index 32
		  :first-name "Louis"
		  :last-name "Kabalero"
		  :email-address "lkabalero@ardra.com"
		  :primary-phone-number "412 281 1408"
		  :radar-weight 25)

(new-radar-person "Vincent McMurray"
		  :person-index 33
		  :first-name "Vincent"
		  :last-name "McMurray"
		  :email-address "vmcmurray@ardra.com"
		  :primary-phone-number "412 281 1410"
		  :radar-weight 25)

(new-radar-person "Valerie Kidd"
		  :person-index 34
		  :first-name "Valerie"
		  :last-name "Kidd"
		  :email-address "vkidd@ardra.com"
		  :primary-phone-number "412 281 1412"
		  :radar-weight 25)

(new-radar-person "Beatrice Rodriguez"
		  :person-index 35
		  :first-name "Beatrice"
		  :last-name "Rodriguez"
		  :email-address "brodriguez@ardra.com"
		  :primary-phone-number "412 281 1414"
		  :radar-weight 25)

(new-radar-person "Sigfried Calloway"
		  :person-index 36
		  :first-name "Sigfried"
		  :last-name "Calloway"
		  :email-address "scalloway@ardra.com"
		  :primary-phone-number "412 281 1416"
		  :radar-weight 25)

(new-radar-person "Sandra Nubanks"
		  :person-index 37
		  :first-name "Sandra"
		  :last-name "Nubanks"
		  :email-address "snubanks@ardra.com"
		  :primary-phone-number "412 281 4562"
		  :radar-weight 25)

(new-radar-person "William Alexander"
		  :person-index 38
		  :first-name "William"
		  :last-name "Alexander"
		  :email-address "walex@ardra.com"
		  :primary-phone-number "412 281 4564"
		  :radar-weight 25)

(new-radar-person "Fabian Monoker"
		  :person-index 39
		  :first-name "Fabian"
		  :last-name "Monoker"
		  :email-address "fmono@ardra.com"
		  :primary-phone-number "412 281 4566"
		  :radar-weight 25)

(new-radar-person "Thomas Bouvier"
		  :person-index 40
		  :first-name "Thomas"
		  :last-name "Bouvier"
		  :email-address "thomas@ardra.com"
		  :primary-phone-number "412 281 4568"
		  :radar-weight 25)

(new-radar-person "Elizabeth McManis"
		  :person-index 41
		  :first-name "Elizabeth"
		  :last-name "McManis"
		  :email-address "emcma@ardra.com"
		  :primary-phone-number "412 281 4670"
		  :radar-weight 25)

(new-radar-person "Gary Abramowitz"
		  :person-index 42
		  :first-name "Gary"
		  :last-name "Abramowitz"
		  :email-address "gabram@ardra.com"
		  :primary-phone-number "412 281 4672"
		  :radar-weight 25)

(new-radar-person "Vivek Kahn"
		  :person-index 43
		  :first-name "Vivek"
		  :last-name "Kahn"
		  :email-address "vivekk@ardra.com"
		  :primary-phone-number "412 281 4674"
		  :radar-weight 25)

(new-radar-person "Yoo Fong"
		  :person-index 44
		  :first-name "Yoo"
		  :last-name "Fong"
		  :email-address "yoof@ardra.com"
		  :primary-phone-number "412 281 4676"
		  :radar-weight 25)

(new-radar-person "Rachel Greensburg"
		  :person-index 45
		  :first-name "Rachel"
		  :last-name "Greensburg"
		  :email-address "regreen@ardra.com"
		  :primary-phone-number "412 281 4678"
		  :radar-weight 25)

(new-radar-person "Steve Adams"
		  :person-index 46
		  :first-name "Steve"
		  :last-name "Adams"
		  :email-address "sadams@ardra.com"
		  :primary-phone-number "412 281 4680"
		  :radar-weight 25)

(new-radar-person "Alexander Gallatin"
		  :person-index 47
		  :first-name "Alexander"
		  :last-name "Gallatin"
		  :email-address "alexg@ardra.com"
		  :primary-phone-number "412 281 4682"
		  :radar-weight 25)

(new-radar-person "Vivica Wagner"
		  :person-index 48
		  :first-name "Vivica"
		  :last-name "Wagner"
		  :email-address "vwagner@ardra.com"
		  :primary-phone-number "412 281 4684"
		  :radar-weight 25)

(new-radar-person "Jason O'Reilly"
		  :person-index 49
		  :first-name "Jason"
		  :last-name "O'Reilly"
		  :email-address "jasono@ardra.com"
		  :primary-phone-number "412 281 4686"
		  :radar-weight 25)

(new-radar-person "Shriya Shaw"
		  :person-index 50
		  :first-name "Shriya"
		  :last-name "Shaw"
		  :email-address "sshaw@ardra.com"
		  :primary-phone-number "412 281 4688"
		  :radar-weight 25)

(new-radar-person "Becky Green"
		  :person-index 51
		  :first-name "Becky"
		  :last-name "Green"
		  :email-address "rgreen@ardra.com"
		  :primary-phone-number "412 281 4690"
		  :radar-weight 25)

(new-radar-person "Maggie Foxenreiter"
		  :person-index 52
		  :first-name "Maggie"
		  :last-name "Foxenreiter"
		  :email-address "mfox@ardra.com"
		  :primary-phone-number "412 281 4692"
		  :radar-weight 25)

(new-radar-person "Ben Steigerwald"
		  :person-index 53
		  :first-name "Ben"
		  :last-name "Steigerwald"
		  :email-address "bsteiger@ardra.com"
		  :primary-phone-number "412 281 4694"
		  :radar-weight 25)

(new-radar-person "Andrew Schwartz"
		  :person-index 54
		  :first-name "Andrew"
		  :last-name "Schwartz"
		  :email-address "aschwartz@ardra.com"
		  :primary-phone-number "412 281 4696"
		  :radar-weight 25)

(new-radar-person "Martha Pialino"
		  :person-index 55
		  :first-name "Martha"
		  :last-name "Pialino"
		  :email-address "mpialino@ardra.com"
		  :primary-phone-number "412 281 4698"
		  :radar-weight 25)

(new-radar-person "Colleen Undranko"
		  :person-index 56
		  :first-name "Colleen"
		  :last-name "Undranko"
		  :email-address "cundra@ardra.com"
		  :primary-phone-number "412 281 4700"
		  :radar-weight 25)

(new-radar-person "Eric Musacelo"
		  :person-index 57
		  :first-name "Eric"
		  :last-name "Musacelo"
		  :email-address "emusa@ardra.com"
		  :primary-phone-number "412 281 4702"
		  :radar-weight 25)

(new-radar-person "Arnold Wickner"
		  :person-index 58
		  :first-name "Arnold"
		  :last-name "Wickner"
		  :email-address "awickner@ardra.com"
		  :primary-phone-number "412 281 4704"
		  :radar-weight 25)

(new-radar-person "Brianna Smith"
		  :person-index 59
		  :first-name "Brianna"
		  :last-name "Smith"
		  :email-address "bsmith@ardra.com"
		  :primary-phone-number "412 281 4706"
		  :radar-weight 25)

(new-radar-person "Sonal Malhotra"
		  :person-index 60
		  :first-name "Sonal"
		  :last-name "Malhotra"
		  :email-address "smalh@ardra.com"
		  :primary-phone-number "412 281 4708"
		  :radar-weight 25)

(new-radar-person "Tal Havartza"
		  :person-index 61
		  :first-name "Tal"
		  :last-name "Havartza"
		  :email-address "thava@ardra.com"
		  :primary-phone-number "412 281 4710"
		  :radar-weight 25)

(new-radar-person "Susan Mikhal"
		  :person-index 62
		  :first-name "Susan"
		  :last-name "Mikhal"
		  :email-address "susanm@ardra.com"
		  :primary-phone-number "412 281 4712"
		  :radar-weight 25)

(new-radar-person "Ian Liluso"
		  :person-index 63
		  :first-name "Ian"
		  :last-name "Liluso"
		  :email-address "ianlilu@ardra.com"
		  :primary-phone-number "412 281 4714"
		  :radar-weight 25)

(new-radar-person "Ruben Salada"
		  :person-index 64
		  :first-name "Ruben"
		  :last-name "Salada"
		  :email-address "rsala@ardra.com"
		  :primary-phone-number "412 281 4716"
		  :radar-weight 25)

(new-radar-person "Nina Riazo"
		  :person-index 65
		  :first-name "Nina"
		  :last-name "Riazo"
		  :email-address "ninar@ardra.com"
		  :primary-phone-number "412 281 4718"
		  :radar-weight 25)

(new-radar-person "Gina Wilson"
		  :person-index 66
		  :first-name "Gina"
		  :last-name "Wilson"
		  :email-address "ginaw@ardra.com"
		  :primary-phone-number "412 281 4720"
		  :radar-weight 25)

(new-radar-person "Brian Car"
		  :person-index 67
		  :first-name "Brian"
		  :last-name "Car"
		  :email-address "bcar@ardra.com"
		  :primary-phone-number "412 281 4722"
		  :radar-weight 25)

(new-radar-person "Timothy Margilois"
		  :person-index 68
		  :first-name "Timothy"
		  :last-name "Margilois"
		  :email-address "tmargil@ardra.com"
		  :primary-phone-number "412 281 4724"
		  :radar-weight 25)

(new-radar-person "Jun Lee"
		  :person-index 69
		  :first-name "Jun"
		  :last-name "Lee"
		  :email-address "jlee@ardra.com"
		  :primary-phone-number "412 281 4726"
		  :radar-weight 25)

(new-radar-person "April Towers"
		  :person-index 70
		  :first-name "April"
		  :last-name "Towers"
		  :email-address "atowers@ardra.com"
		  :primary-phone-number "412 281 4728"
		  :radar-weight 25)

(new-radar-person "Ashuk Wendorf"
		  :person-index 71
		  :first-name "Ashuk"
		  :last-name "Wendorf"
		  :email-address "awendorf@ardra.com"
		  :primary-phone-number "412 281 4730"
		  :radar-weight 25)

(new-radar-person "Samantha Swick"
		  :person-index 72
		  :first-name "Samantha"
		  :last-name "Swick"
		  :email-address "sswick@ardra.com"
		  :primary-phone-number "412 281 6512"
		  :radar-weight 25)

(new-radar-person "Susana Fariola"
		  :person-index 73
		  :first-name "Susana"
		  :last-name "Fariola"
		  :email-address "sfariola@ardra.com"
		  :primary-phone-number "412 281 6514"
		  :radar-weight 25)

(new-radar-person "Andrew Delafonte"
		  :person-index 74
		  :first-name "Andrew"
		  :last-name "Delafonte"
		  :email-address "adelafon@ardra.com"
		  :primary-phone-number "412 281 6516"
		  :radar-weight 25)

(new-radar-person "Frank Ionola"
		  :person-index 75
		  :first-name "Frank"
		  :last-name "Ionola"
		  :email-address "fionola@ardra.com"
		  :primary-phone-number "412 281 6518"
		  :radar-weight 25)

(new-radar-person "Suzanne Marilios"
		  :person-index 76
		  :first-name "Suzanne"
		  :last-name "Marilios"
		  :email-address "smarilios@ardra.com"
		  :primary-phone-number "412 281 6520"
		  :radar-weight 25)

(new-radar-person "Tina Fall"
		  :person-index 77
		  :first-name "Tina"
		  :last-name "Fall"
		  :email-address "tgf@ardra.com"
		  :primary-phone-number "412 281 6522"
		  :radar-weight 25)

(new-radar-person "Ed Ivanovich"
		  :person-index 78
		  :first-name "Ed"
		  :last-name "Ivanovich"
		  :email-address "eivanovich@ardra.com"
		  :primary-phone-number "412 281 6524"
		  :radar-weight 25)

(new-radar-person "Westley Qualter"
		  :person-index 79
		  :first-name "Westley"
		  :last-name "Qualter"
		  :email-address "qualter@ardra.com"
		  :primary-phone-number "412 281 6526"
		  :radar-weight 25)

(new-radar-person "Ilise Nebuto"
		  :person-index 80
		  :first-name "Ilise"
		  :last-name "Nebuto"
		  :email-address "ilisen@ardra.com"
		  :primary-phone-number "412 281 6558"
		  :radar-weight 25)

(new-radar-person "Dennis Johnson"
		  :person-index 81
		  :first-name "Dennis"
		  :last-name "Johnson"
		  :email-address "dlj@ardra.com"
		  :primary-phone-number "412 281 6560"
		  :radar-weight 25)

(new-radar-person "Andy Sewally"
		  :person-index 82
		  :first-name "Andy"
		  :last-name "Sewally"
		  :email-address "asewally@ardra.com"
		  :primary-phone-number "412 281 6562"
		  :radar-weight 25)

(new-radar-person "Peter Havater"
		  :person-index 83
		  :first-name "Peter"
		  :last-name "Havater"
		  :email-address "phavater@ardra.com"
		  :primary-phone-number "412 281 6564"
		  :radar-weight 25)

(new-radar-person "Yong Qian"
		  :person-index 84
		  :first-name "Yong"
		  :last-name "Qian"
		  :email-address "yquian@ardra.com"
		  :primary-phone-number "412 281 6566"
		  :radar-weight 25)

(new-radar-person "Ola Neuen"
		  :person-index 85
		  :first-name "Ola"
		  :last-name "Neuen"
		  :email-address "neuen@ardra.com"
		  :primary-phone-number "412 281 6568"
		  :radar-weight 25)

(new-radar-person "Cathy Algowitz"
		  :person-index 86
		  :first-name "Cathy"
		  :last-name "Algowitz"
		  :email-address "calgowitz@ardra.com"
		  :primary-phone-number "412 281 6570"
		  :radar-weight 25)

(new-radar-person "Helmet Uter"
		  :person-index 87
		  :first-name "Helmet"
		  :last-name "Uter"
		  :email-address "helmet@ardra.com"
		  :primary-phone-number "412 281 6572"
		  :radar-weight 25)

(new-radar-person "Petra Svetski"
		  :person-index 88
		  :first-name "Petra"
		  :last-name "Svetski"
		  :email-address "svetski@ardra.com"
		  :primary-phone-number "412 281 6574"
		  :radar-weight 25)

(new-radar-person "Nina Idiz"
		  :person-index 89
		  :first-name "Nina"
		  :last-name "Idiz"
		  :email-address "nidiz@ardra.com"
		  :primary-phone-number "412 281 6576"
		  :radar-weight 25)

(new-radar-person "Jana Padamanabahn"
		  :person-index 90
		  :first-name "Jana"
		  :last-name "Padamanabahn"
		  :email-address "padamana@ardra.com"
		  :primary-phone-number "412 281 6578"
		  :radar-weight 25)

(new-radar-person "Mark Oriolos"
		  :person-index 91
		  :first-name "Mark"
		  :last-name "Oriolos"
		  :email-address "moriolos@ardra.com"
		  :primary-phone-number "412 281 6580"
		  :radar-weight 25)

(new-radar-person "Dana Johanson"
		  :person-index 92
		  :first-name "Dana"
		  :last-name "Johanson"
		  :email-address "dwj@ardra.com"
		  :primary-phone-number "412 281 6582"
		  :radar-weight 25)

(new-radar-person "Wanda Teven"
		  :person-index 93
		  :first-name "Wanda"
		  :last-name "Teven"
		  :email-address "teven@ardra.com"
		  :primary-phone-number "412 281 6584"
		  :radar-weight 25)

(new-radar-person "Larry Elry"
		  :person-index 94
		  :first-name "Larry"
		  :last-name "Elry"
		  :email-address "lelry@ardra.com"
		  :primary-phone-number "412 281 6586"
		  :radar-weight 25)

(new-radar-person "Deana Gona"
		  :person-index 95
		  :first-name "Deana"
		  :last-name "Gona"
		  :email-address "dgona@ardra.com"
		  :primary-phone-number "412 281 6588"
		  :radar-weight 25)

(new-radar-person "Fiona Mansala"
		  :person-index 96
		  :first-name "Fiona"
		  :last-name "Mansala"
		  :email-address "fmans@ardra.com"
		  :primary-phone-number "412 281 6590"
		  :radar-weight 25)

(new-radar-person "Ed Farvich"
		  :person-index 97
		  :first-name "Ed"
		  :last-name "Farvich"
		  :email-address "emf@ardra.com"
		  :primary-phone-number "412 281 6592"
		  :radar-weight 25)

(new-radar-person "Renee O'Laughlin"
		  :person-index 98
		  :first-name "Renee"
		  :last-name "O'Laughlin"
		  :email-address "olaughlin@ardra.com"
		  :primary-phone-number "412 281 6594"
		  :radar-weight 25)

(new-radar-person "Pete Fava"
		  :person-index 99
		  :first-name "Pete"
		  :last-name "Fava"
		  :email-address "pfava@ardra.com"
		  :primary-phone-number "412 281 6596"
		  :radar-weight 25)

(new-radar-person "Zachary Tonazu"
		  :person-index 100
		  :first-name "Zachary"
		  :last-name "Tonazu"
		  :email-address "tonazu@ardra.com"
		  :primary-phone-number "412 281 6598"
		  :radar-weight 25)

(new-radar-person "Michael Reese"
		  :person-index 101
		  :first-name "Michael"
		  :last-name "Reese"
		  :email-address "mreese@ardra.com"
		  :primary-phone-number "412 281 6600"
		  :radar-weight 25)

(new-radar-person "Tina Sutter"
		  :person-index 102
		  :first-name "Tina"
		  :last-name "Sutter"
		  :email-address "tlsutter@ardra.com"
		  :primary-phone-number "412 281 6602"
		  :radar-weight 25)

(new-radar-person "Kelly Jovanovich"
		  :person-index 103
		  :first-name "Kelly"
		  :last-name "Jovanovich"
		  :email-address "jovanovich@ardra.com"
		  :primary-phone-number "412 281 6604"
		  :radar-weight 25)

(new-radar-person "Chian Lee"
		  :person-index 104
		  :first-name "Chian"
		  :last-name "Lee"
		  :email-address "chian@ardra.com"
		  :primary-phone-number "412 281 6606"
		  :radar-weight 25)

(new-radar-person "Shawanda North"
		  :person-index 105
		  :first-name "Shawanda"
		  :last-name "North"
		  :email-address "shawanda@ardra.com"
		  :primary-phone-number "412 281 6608"
		  :radar-weight 25)

(new-radar-person "Jack Petersen"
		  :person-index 106
		  :first-name "Jack"
		  :last-name "Petersen"
		  :email-address "jpeterse@ardra.com"
		  :primary-phone-number "412 281 6610"
		  :radar-weight 25)

(new-radar-person "Don Henry"
		  :person-index 107
		  :first-name "Don"
		  :last-name "Henry"
		  :email-address "dhenry@ardra.com"
		  :primary-phone-number "412 281 4802 "
		  :radar-weight 25)

(new-radar-person "Abigail Reeves"
		  :person-index 108
		  :first-name "Abigail"
		  :last-name "Reeves"
		  :email-address "abireeves@ardra.com"
		  :primary-phone-number "412 281 4804"
		  :radar-weight 25)

(new-radar-person "Calla Diller"
		  :person-index 109
		  :first-name "Calla"
		  :last-name "Diller"
		  :email-address "calla@ardra.com"
		  :primary-phone-number "412 281 4806"
		  :radar-weight 25)

(new-radar-person "Courtney Charles"
		  :person-index 110
		  :first-name "Courtney"
		  :last-name "Charles"
		  :email-address "courtneyd@ardra.com"
		  :primary-phone-number "412 281 4808"
		  :radar-weight 25)

(new-radar-person "Katrina Aronza"
		  :person-index 111
		  :first-name "Katrina"
		  :last-name "Aronza"
		  :email-address "katrinaa@ardra.com"
		  :primary-phone-number "412 281 4810"
		  :radar-weight 25)

(new-radar-person "Laura Timdale"
		  :person-index 112
		  :first-name "Laura"
		  :last-name "Timdale"
		  :email-address "laurat2@ardra.com"
		  :primary-phone-number "412 281 4812"
		  :radar-weight 25)

(new-radar-person "Charles Derunn"
		  :person-index 113
		  :first-name "Charles"
		  :last-name "Derunn"
		  :email-address "cderunn@ardra.com"
		  :primary-phone-number "412 281 4814"
		  :radar-weight 25)

(new-radar-person "Michael Faye"
		  :person-index 114
		  :first-name "Michael"
		  :last-name "Faye"
		  :email-address "mfaye@ardra.com"
		  :primary-phone-number "412 281 4816"
		  :radar-weight 25)

(new-radar-person "Bob Armstrong"
		  :person-index 115
		  :first-name "Bob"
		  :last-name "Armstrong"
		  :email-address "robertarm@ardra.com"
		  :primary-phone-number "412 281 4818"
		  :radar-weight 25)

(new-radar-person "Richard Holley"
		  :person-index 116
		  :first-name "Richard"
		  :last-name "Holley"
		  :email-address "rholley@ardra.com"
		  :primary-phone-number "412 281 4820"
		  :radar-weight 25)

(new-radar-person "Hollis Carrow"
		  :person-index 117
		  :first-name "Hollis"
		  :last-name "Carrow"
		  :email-address "hollis@ardra.com"
		  :primary-phone-number "412 281 4822"
		  :radar-weight 25)

(new-radar-person "Becky Anupira"
		  :person-index 118
		  :first-name "Becky"
		  :last-name "Anupira"
		  :email-address "bta@ardra.com"
		  :primary-phone-number "412 281 4824"
		  :radar-weight 25)

(new-radar-person "Vincent Almoore"
		  :person-index 119
		  :first-name "Vincent"
		  :last-name "Almoore"
		  :email-address "vga4@ardra.com"
		  :primary-phone-number "412 281 4826"
		  :radar-weight 25)

(new-radar-person "Taylor Killian"
		  :person-index 120
		  :first-name "Taylor"
		  :last-name "Killian"
		  :email-address "killian@ardra.com"
		  :primary-phone-number "412 281 4828"
		  :radar-weight 25)

(new-radar-person "Scott Scheurman"
		  :person-index 121
		  :first-name "Scott"
		  :last-name "Scheurman"
		  :email-address "scheurman@ardra.com"
		  :primary-phone-number "412 281 4830"
		  :radar-weight 25)

(new-radar-person "Laron Dale"
		  :person-index 122
		  :first-name "Laron"
		  :last-name "Dale"
		  :email-address "led@ardra.com"
		  :primary-phone-number "412 281 4832"
		  :radar-weight 25)

(new-radar-person "Jarrod Michaels"
		  :person-index 123
		  :first-name "Jarrod"
		  :last-name "Michaels"
		  :email-address "jarrod@ardra.com"
		  :primary-phone-number "412 281 4834"
		  :radar-weight 25)

(new-radar-person "Melissa Crosby"
		  :person-index 124
		  :first-name "Melissa"
		  :last-name "Crosby"
		  :email-address "mcrosby@ardra.com"
		  :primary-phone-number "412 281 4836"
		  :radar-weight 25)

(new-radar-person "Donald Allman"
		  :person-index 125
		  :first-name "Donald"
		  :last-name "Allman"
		  :email-address "dallman@ardra.com"
		  :primary-phone-number "412 281 4838"
		  :radar-weight 25)

(new-radar-person "Julia Gonzalez"
		  :person-index 126
		  :first-name "Julia"
		  :last-name "Gonzalez"
		  :email-address "jlgonzalez@ardra.com"
		  :primary-phone-number "412 281 4840"
		  :radar-weight 25)

(new-radar-person "Natalia Kinski"
		  :person-index 127
		  :first-name "Natalia"
		  :last-name "Kinski"
		  :email-address "nekinski@ardra.com"
		  :primary-phone-number "412 281 4842"
		  :radar-weight 25)

(new-radar-person "Theodore Callman"
		  :person-index 128
		  :first-name "Theodore"
		  :last-name "Callman"
		  :email-address "tcallman@ardra.com"
		  :primary-phone-number "412 281 4844"
		  :radar-weight 25)

(new-radar-person "Eli Dorunda"
		  :person-index 129
		  :first-name "Eli"
		  :last-name "Dorunda"
		  :email-address "elid@ardra.com"
		  :primary-phone-number "412 281 4846"
		  :radar-weight 25)

(new-radar-person "Federico Muniz"
		  :person-index 130
		  :first-name "Federico"
		  :last-name "Muniz"
		  :email-address "federico@ardra.com"
		  :primary-phone-number "412 281 4848"
		  :radar-weight 25)

(new-radar-person "Cara Ride"
		  :person-index 131
		  :first-name "Cara"
		  :last-name "Ride"
		  :email-address "cmride@ardra.com"
		  :primary-phone-number "412 281 4850"
		  :radar-weight 25)

(new-radar-person "Bradley Noriega"
		  :person-index 132
		  :first-name "Bradley"
		  :last-name "Noriega"
		  :email-address "bam@ardra.com"
		  :primary-phone-number "412 281 4852"
		  :radar-weight 25)

(new-radar-person "Tom Thoreau"
		  :person-index 133
		  :first-name "Tom"
		  :last-name "Thoreau"
		  :email-address "thoreau@ardra.com"
		  :primary-phone-number "412 281 4854"
		  :radar-weight 25)

(new-radar-person "Henry David"
		  :person-index 134
		  :first-name "Henry"
		  :last-name "David"
		  :email-address "hbdavid@ardra.com"
		  :primary-phone-number "412 281 4856"
		  :radar-weight 25)

(new-radar-person "Juliet Frerking"
		  :person-index 135
		  :first-name "Juliet"
		  :last-name "Frerking"
		  :email-address "frerking@ardra.com"
		  :primary-phone-number "412 281 4858"
		  :radar-weight 25)

(new-radar-person "Thomas Collins"
		  :person-index 136
		  :first-name "Thomas"
		  :last-name "Collins"
		  :email-address "tcollins@ardra.com"
		  :primary-phone-number "412 281 4860"
		  :radar-weight 25)

(new-radar-person "Britt Daniel"
		  :person-index 137
		  :first-name "Britt"
		  :last-name "Daniel"
		  :email-address "brittd@ardra.com"
		  :primary-phone-number "412 281 4862"
		  :radar-weight 25)

(new-radar-person "Vicenzo Calabreze"
		  :person-index 138
		  :first-name "Vicenzo"
		  :last-name "Calabreze"
		  :email-address "vicenzo@ardra.com"
		  :primary-phone-number "412 281 4864"
		  :radar-weight 25)

(new-radar-person "Teo Ciccoli"
		  :person-index 139
		  :first-name "Teo"
		  :last-name "Ciccoli"
		  :email-address "teo@ardra.com"
		  :primary-phone-number "412 281 4866"
		  :radar-weight 25)

(new-radar-person "Lupe Bucca"
		  :person-index 140
		  :first-name "Lupe"
		  :last-name "Bucca"
		  :email-address "lbucca@ardra.com"
		  :primary-phone-number "412 281 4868"
		  :radar-weight 25)

(new-radar-person "Brenda Smallman"
		  :person-index 141
		  :first-name "Brenda"
		  :last-name "Smallman"
		  :email-address "bsmallman@ardra.com"
		  :primary-phone-number "412 281 4870"
		  :radar-weight 25)

(new-radar-person "Louisa Hollands"
		  :person-index 142
		  :first-name "Louisa"
		  :last-name "Hollands"
		  :email-address "lhollands@ardra.com"
		  :primary-phone-number "412 281 4872"
		  :radar-weight 25)

(new-radar-person "Jennifer Morris"
		  :person-index 143
		  :first-name "Jennifer"
		  :last-name "Morris"
		  :email-address "jlmorris@ardra.com"
		  :primary-phone-number "412 281 4874"
		  :radar-weight 25)

(new-radar-person "Angelica Antonias"
		  :person-index 144
		  :first-name "Angelica"
		  :last-name "Antonias"
		  :email-address "ata@ardra.com"
		  :primary-phone-number "412 281 4876"
		  :radar-weight 25)

(new-radar-person "Penelope Benold"
		  :person-index 145
		  :first-name "Penelope"
		  :last-name "Benold"
		  :email-address "penelope@ardra.com"
		  :primary-phone-number "412 281 4878"
		  :radar-weight 25)

(new-radar-person "Stuart E. Milton"
		  :person-index 146
		  :first-name "Stuart"
		  :last-name "Milton"
		  :email-address "semilton@ardra.com"
		  :primary-phone-number "412 281 4880"
		  :radar-weight 25)

(new-radar-person "Patricia Minor"
		  :person-index 147
		  :first-name "Patricia"
		  :last-name "Minor"
		  :email-address "pminor@ardra.com"
		  :primary-phone-number "412 281 4882"
		  :radar-weight 25)

(new-radar-person "Staffan Bjorn"
		  :person-index 148
		  :first-name "Staffan"
		  :last-name "Bjorn"
		  :email-address "staffan@ardra.com"
		  :primary-phone-number "412 281 4884"
		  :radar-weight 25)

(new-radar-person "Patrick Fitzsimmons"
		  :person-index 149
		  :first-name "Patrick"
		  :last-name "Fitzsimmons"
		  :email-address "fitzsimm@ardra.com"
		  :primary-phone-number "412 281 4886"
		  :radar-weight 25)

(new-radar-person "Jeffrey Lester"
		  :person-index 150
		  :first-name "Jeffrey"
		  :last-name "Lester"
		  :email-address "jlester@ardra.com"
		  :primary-phone-number "412 281 4888"
		  :radar-weight 25)

(new-radar-person "Lucas Zettle"
		  :person-index 151
		  :first-name "Lucas"
		  :last-name "Zettle"
		  :email-address "zettle@ardra.com"
		  :primary-phone-number "412 281 4890"
		  :radar-weight 25)

(new-radar-person "Lisa Sweeney"
		  :person-index 152
		  :first-name "Lisa"
		  :last-name "Sweeney"
		  :email-address "lsweeney@ardra.com"
		  :primary-phone-number "412 281 4892"
		  :radar-weight 25)

(new-radar-person "Luke Moyer"
		  :person-index 153
		  :first-name "Luke"
		  :last-name "Moyer"
		  :email-address "lmoyer@ardra.com"
		  :primary-phone-number "412 281 4894"
		  :radar-weight 25)

(new-radar-person "Michelle Wooldridge"
		  :person-index 154
		  :first-name "Michelle"
		  :last-name "Wooldridge"
		  :email-address "mew@ardra.com"
		  :primary-phone-number "412 281 4896"
		  :radar-weight 25)

(new-radar-person "John Connor"
		  :person-index 155
		  :first-name "John"
		  :last-name "Connor"
		  :email-address "jlc@ardra.com"
		  :primary-phone-number "412 281 4898"
		  :radar-weight 25)

(new-radar-person "Mitchell Bradbury"
		  :person-index 156
		  :first-name "Mitchell"
		  :last-name "Bradbury"
		  :email-address "mitchellb@ardra.com"
		  :primary-phone-number "412 281 4990"
		  :radar-weight 25)

(new-radar-person "Motohiko Haguchi"
		  :person-index 157
		  :first-name "Motohiko"
		  :last-name "Haguchi"
		  :email-address "motohiko@ardra.com"
		  :primary-phone-number "412 281 4992"
		  :radar-weight 25)

(new-radar-person "Patti Minel"
		  :person-index 158
		  :first-name "Patti"
		  :last-name "Minel"
		  :email-address "pminel@ardra.com"
		  :primary-phone-number "412 281 4994"
		  :radar-weight 25)

(new-radar-person "Hope Ezzard"
		  :person-index 159
		  :first-name "Hope"
		  :last-name "Ezzard"
		  :email-address "ezzard@ardra.com"
		  :primary-phone-number "412 281 4996"
		  :radar-weight 25)

(new-radar-person "Johannes Copper"
		  :person-index 160
		  :first-name "Johannes"
		  :last-name "Copper"
		  :email-address "jpcopper@ardra.com"
		  :primary-phone-number "412 281 4998"
		  :radar-weight 25)

(new-radar-person "Matushi Sakaguchi"
		  :person-index 161
		  :first-name "Matushi"
		  :last-name "Sakaguchi"
		  :email-address "sakaguchi@ardra.com"
		  :primary-phone-number "412 281 5000"
		  :radar-weight 25)

(new-radar-person "Iris Johnson"
		  :person-index 162
		  :first-name "Iris"
		  :last-name "Johnson"
		  :email-address "ijohnson@ardra.com"
		  :primary-phone-number "412 281 5002"
		  :radar-weight 25)

(new-radar-person "Lauren Sherman"
		  :person-index 163
		  :first-name "Lauren"
		  :last-name "Sherman"
		  :email-address "lsherman@ardra.com"
		  :primary-phone-number "412 281 5004"
		  :radar-weight 25)

(new-radar-person "Lucinda Hollister"
		  :person-index 164
		  :first-name "Lucinda"
		  :last-name "Hollister"
		  :email-address "lucinda@ardra.com"
		  :primary-phone-number "412 281 5006"
		  :radar-weight 25)

(new-radar-person "Peter Ferguson"
		  :person-index 165
		  :first-name "Peter"
		  :last-name "Ferguson"
		  :email-address "pferguson@ardra.com"
		  :primary-phone-number "412 281 5008"
		  :radar-weight 25)

(new-radar-person "James P. Sontag"
		  :person-index 166
		  :first-name "James"
		  :last-name "Sontag"
		  :email-address "jpsontag@ardra.com"
		  :primary-phone-number "412 281 5010"
		  :radar-weight 25)

(new-radar-person "Miho Sakato"
		  :person-index 167
		  :first-name "Miho"
		  :last-name "Sakato"
		  :email-address "mihos@ardra.com"
		  :primary-phone-number "412 281 5012"
		  :radar-weight 25)

(new-radar-person "Iban Zong"
		  :person-index 168
		  :first-name "Iban"
		  :last-name "Zong"
		  :email-address "iban@ardra.com"
		  :primary-phone-number "412 281 5014"
		  :radar-weight 25)

(new-radar-person "Samuel Johns"
		  :person-index 169
		  :first-name "Samuel"
		  :last-name "Johns"
		  :email-address "sjohns@ardra.com"
		  :primary-phone-number "412 281 5016"
		  :radar-weight 25)

(new-radar-person "Laura B. Schnieder"
		  :person-index 170
		  :first-name "Laura"
		  :last-name "Schnieder"
		  :email-address "lbschnieder@ardra.com"
		  :primary-phone-number "412 281 5018"
		  :radar-weight 25)

(new-radar-person "Sunala Rodriguez"
		  :person-index 171
		  :first-name "Sunala"
		  :last-name "Rodriguez"
		  :email-address "sunala@ardra.com"
		  :primary-phone-number "412 281 5020"
		  :radar-weight 25)

(new-radar-person "Frank Luin"
		  :person-index 172
		  :first-name "Frank"
		  :last-name "Luin"
		  :email-address "frluin@ardra.com"
		  :primary-phone-number "412 281 5022"
		  :radar-weight 25)

(new-radar-person "David B. Kingsley"
		  :person-index 173
		  :first-name "David"
		  :last-name "Kingsley"
		  :email-address "dbkingsley@ardra.com"
		  :primary-phone-number "412 281 5024"
		  :radar-weight 25)

(new-radar-person "Andrew Akins"
		  :person-index 174
		  :first-name "Andrew"
		  :last-name "Akins"
		  :email-address "aakins@ardra.com"
		  :primary-phone-number "412 281 5026"
		  :radar-weight 25)

(new-radar-person "Russell Piper"
		  :person-index 175
		  :first-name "Russell"
		  :last-name "Piper"
		  :email-address "rpiper@ardra.com"
		  :primary-phone-number "412 281 5028"
		  :radar-weight 25)

(new-radar-person "William B. Bernstein"
		  :person-index 176
		  :first-name "William"
		  :last-name "Bernstein"
		  :email-address "wbernste@ardra.com"
		  :primary-phone-number "412 281 5030"
		  :radar-weight 25)

(new-radar-person "Donald N. Hartfield"
		  :person-index 177
		  :first-name "Donald"
		  :last-name "Hartfield"
		  :email-address "dnhart@ardra.com"
		  :primary-phone-number "412 281 5032"
		  :radar-weight 25)

(new-radar-person "Norman Beatty"
		  :person-index 178
		  :first-name "Norman"
		  :last-name "Beatty"
		  :email-address "nbeatty@ardra.com"
		  :primary-phone-number "412 281 5034"
		  :radar-weight 25)

(new-radar-person "Kieth deCorta"
		  :person-index 179
		  :first-name "Kieth"
		  :last-name "deCorta"
		  :email-address "decorta@ardra.com"
		  :primary-phone-number "412 281 5036"
		  :radar-weight 25)

(new-radar-person "Harold Mientras"
		  :person-index 180
		  :first-name "Harold"
		  :last-name "Mientras"
		  :email-address "hmientras@ardra.com"
		  :primary-phone-number "412 281 5038"
		  :radar-weight 25)

(new-radar-person "Stanley Boznik"
		  :person-index 181
		  :first-name "Stanley"
		  :last-name "Boznik"
		  :email-address "boznik@ardra.com"
		  :primary-phone-number "412 281 5040"
		  :radar-weight 25)

(new-radar-person "Jeanne Santos"
		  :person-index 182
		  :first-name "Jeanne"
		  :last-name "Santos"
		  :email-address "jeannes@ardra.com"
		  :primary-phone-number "412 281 5042"
		  :radar-weight 25)

(new-radar-person "David Rodgerson"
		  :person-index 183
		  :first-name "David"
		  :last-name "Rodgerson"
		  :email-address "dlr@ardra.com"
		  :primary-phone-number "412 281 5044"
		  :radar-weight 25)

(new-radar-person "John Constable"
		  :person-index 184
		  :first-name "John"
		  :last-name "Constable"
		  :email-address "jconstable@ardra.com"
		  :primary-phone-number "412 281 5046"
		  :radar-weight 25)

(new-radar-person "Guillermo Credon"
		  :person-index 185
		  :first-name "Guillermo"
		  :last-name "Credon"
		  :email-address "guillermo@ardra.com"
		  :primary-phone-number "412 281 5048"
		  :radar-weight 25)

(new-radar-person "Nigel Contorso"
		  :person-index 186
		  :first-name "Nigel"
		  :last-name "Contorso"
		  :email-address "nigelc@ardra.com"
		  :primary-phone-number "412 281 5050"
		  :radar-weight 25)

(new-radar-person "Jenny Santiago"
		  :person-index 187
		  :first-name "Jenny"
		  :last-name "Santiago"
		  :email-address "jls2@ardra.com"
		  :primary-phone-number "412 281 5052"
		  :radar-weight 25)

(new-radar-person "Christine Johnston"
		  :person-index 188
		  :first-name "Christine"
		  :last-name "Johnston"
		  :email-address "cjohnston@ardra.com"
		  :primary-phone-number "412 281 5054"
		  :radar-weight 25)

(new-radar-person "Emily Halwizer"
		  :person-index 189
		  :first-name "Emily"
		  :last-name "Halwizer"
		  :email-address "halwizer@ardra.com"
		  :primary-phone-number "412 281 5056"
		  :radar-weight 25)

(new-radar-person "Dennis Parrott"
		  :person-index 190
		  :first-name "Dennis"
		  :last-name "Parrott"
		  :email-address "dparrott@ardra.com"
		  :primary-phone-number "412 281 5058"
		  :radar-weight 25)

(new-radar-person "Bob Halstear"
		  :person-index 191
		  :first-name "Bob"
		  :last-name "Halstear"
		  :email-address "halstear@ardra.com"
		  :primary-phone-number "412 281 5060"
		  :radar-weight 25)

(new-radar-person "Daniel Hendridge"
		  :person-index 192
		  :first-name "Daniel"
		  :last-name "Hendridge"
		  :email-address "dhendridge@ardra.com"
		  :primary-phone-number "412 281 5062"
		  :radar-weight 25)

(new-radar-person "Andreas Schaeffer"
		  :person-index 193
		  :first-name "Andreas"
		  :last-name "Schaeffer"
		  :email-address "andreass@ardra.com"
		  :primary-phone-number "412 281 5064"
		  :radar-weight 25)

(new-radar-person "Gene Hawkings"
		  :person-index 194
		  :first-name "Gene"
		  :last-name "Hawkings"
		  :email-address "hawkings@ardra.com"
		  :primary-phone-number "412 281 5066"
		  :radar-weight 25)

(new-radar-person "Troy Costales"
		  :person-index 195
		  :first-name "Troy"
		  :last-name "Costales"
		  :email-address "troy@ardra.com"
		  :primary-phone-number "412 281 5068"
		  :radar-weight 25)

(new-radar-person "Phillip Hardunn"
		  :person-index 196
		  :first-name "Phillip"
		  :last-name "Hardunn"
		  :email-address "pmhardunn@ardra.com"
		  :primary-phone-number "412 281 5070"
		  :radar-weight 25)

(new-radar-person "Fred Nampton"
		  :person-index 197
		  :first-name "Fred"
		  :last-name "Nampton"
		  :email-address "fnampton@ardra.com"
		  :primary-phone-number "412 281 5072"
		  :radar-weight 25)

(new-radar-person "Raquel Diano"
		  :person-index 198
		  :first-name "Raquel"
		  :last-name "Diano"
		  :email-address "raquel@ardra.com"
		  :primary-phone-number "412 281 5074"
		  :radar-weight 25)

(new-radar-person "John Drexel"
		  :person-index 199
		  :first-name "John"
		  :last-name "Drexel"
		  :email-address "jdrex@ardra.com"
		  :primary-phone-number "412 281 5076"
		  :radar-weight 25)

(new-radar-person "Debbie Cotton"
		  :person-index 200
		  :first-name "Debbie"
		  :last-name "Cotton"
		  :email-address "dfcotton@ardra.com"
		  :primary-phone-number "412 281 5078"
		  :radar-weight 25)

(new-radar-person "Elise Steiner"
		  :person-index 201
		  :first-name "Elise"
		  :last-name "Steiner"
		  :email-address "elises@ardra.com"
		  :primary-phone-number "412 281 5080"
		  :radar-weight 25)

(new-radar-person "Hammond Hughes"
		  :person-index 202
		  :first-name "Hammond"
		  :last-name "Hughes"
		  :email-address "hammond@ardra.com"
		  :primary-phone-number "412 281 5082"
		  :radar-weight 25)

(new-radar-person "Gina Bertolucci"
		  :person-index 203
		  :first-name "Gina"
		  :last-name "Bertolucci"
		  :email-address "bertolucci@ardra.com"
		  :primary-phone-number "412 281 5084"
		  :radar-weight 25)

(new-radar-person "Mark Abokowicz"
		  :person-index 204
		  :first-name "Mark"
		  :last-name "Abokowicz"
		  :email-address "abokowicz@ardra.com"
		  :primary-phone-number "412 281 5086"
		  :radar-weight 25)

(new-radar-person "Dan Murray"
		  :person-index 205
		  :first-name "Dan"
		  :last-name "Murray"
		  :email-address "dgmurray@ardra.com"
		  :primary-phone-number "412 281 5088"
		  :radar-weight 25)

(new-radar-person "Donald Reeves"
		  :person-index 206
		  :first-name "Donald"
		  :last-name "Reeves"
		  :email-address "donaldr@ardra.com"
		  :primary-phone-number "412 281 5090"
		  :radar-weight 25)

(new-radar-person "Henry M. Watson"
		  :person-index 207
		  :first-name "Henry"
		  :last-name "Watson"
		  :email-address "hmw@ardra.com"
		  :primary-phone-number "412 281 5092"
		  :radar-weight 25)

(new-radar-person "Phillipe Defrano"
		  :person-index 208
		  :first-name "Phillipe"
		  :last-name "Defrano"
		  :email-address "philliped@ardra.com"
		  :primary-phone-number "412 281 5094"
		  :radar-weight 25)

(new-radar-person "Judith E. Payne"
		  :person-index 209
		  :first-name "Judith"
		  :last-name "Payne"
		  :email-address "jepayne@arda.com"
		  :primary-phone-number "412 281 5096"
		  :radar-weight 25)

(new-radar-person "Sean Martin"
		  :person-index 210
		  :first-name "Sean"
		  :last-name "Martin"
		  :email-address "smartin@ardra.com"
		  :primary-phone-number "412 281 2563"
		  :radar-weight 25)

(new-radar-person "Howard Richards"
		  :person-index 211
		  :first-name "Howard"
		  :last-name "Richards"
		  :email-address "howard@ardra.com"
		  :primary-phone-number "412 281 2565"
		  :radar-weight 25)

(new-radar-person "Casper Miles"
		  :person-index 212
		  :first-name "Casper"
		  :last-name "Miles"
		  :email-address "cmiles@ardra.com"
		  :primary-phone-number "412 281 2567"
		  :radar-weight 25)

(new-radar-person "Francine Stimmel"
		  :person-index 213
		  :first-name "Francine"
		  :last-name "Stimmel"
		  :email-address "frstimmel@ardra.com"
		  :primary-phone-number "412 281 2569"
		  :radar-weight 25)

(new-radar-person "Terrence Porteus"
		  :person-index 214
		  :first-name "Terrence"
		  :last-name "Porteus"
		  :email-address "tporteus@ardra.com"
		  :primary-phone-number "412 281 2571"
		  :radar-weight 25)

(new-radar-person "Dino Smith"
		  :person-index 215
		  :first-name "Dino"
		  :last-name "Smith"
		  :email-address "dinos@ardra.com"
		  :primary-phone-number "412 281 2573"
		  :radar-weight 25)

(new-radar-person "Christina Santiago"
		  :person-index 216
		  :first-name "Christina"
		  :last-name "Santiago"
		  :email-address "csantiago@ardra.com"
		  :primary-phone-number "412 281 2575"
		  :radar-weight 25)

(new-radar-person "Charline Spring"
		  :person-index 217
		  :first-name "Charline"
		  :last-name "Spring"
		  :email-address "cspring@ardra.com"
		  :primary-phone-number "412 281 2577"
		  :radar-weight 25)

(new-radar-person "Jordan Taylor"
		  :person-index 218
		  :first-name "Jordan"
		  :last-name "Taylor"
		  :email-address "jordant@ardra.com"
		  :primary-phone-number "412 281 2579"
		  :radar-weight 25)

(new-radar-person "David Thompson"
		  :person-index 219
		  :first-name "David"
		  :last-name "Thompson"
		  :email-address "dthomps@ardra.com"
		  :primary-phone-number "412 281 2581"
		  :radar-weight 25)

(new-radar-person "Deborah Young"
		  :person-index 220
		  :first-name "Deborah"
		  :last-name "Young"
		  :email-address "dyoung@ardra.com"
		  :primary-phone-number "412 281 2583"
		  :radar-weight 25)

(new-radar-person "Aaron Stein"
		  :person-index 221
		  :first-name "Aaron"
		  :last-name "Stein"
		  :email-address "asteing@ardra.com"
		  :primary-phone-number "412 281 2585"
		  :radar-weight 25)

(new-radar-person "Jonathon Miller"
		  :person-index 222
		  :first-name "Jonathon"
		  :last-name "Miller"
		  :email-address "jmiller@ardra.com"
		  :primary-phone-number "412 281 2587"
		  :radar-weight 25)

(new-radar-person "Eli Montrose"
		  :person-index 223
		  :first-name "Eli"
		  :last-name "Montrose"
		  :email-address "elim@ardra.com"
		  :primary-phone-number "412 281 2589"
		  :radar-weight 25)

(new-radar-person "Patti Stevens"
		  :person-index 224
		  :first-name "Patti"
		  :last-name "Stevens"
		  :email-address "pattis@ardra.com"
		  :primary-phone-number "412 281 2591"
		  :radar-weight 25)

(new-radar-person "Margaret Hilldale"
		  :person-index 225
		  :first-name "Margaret"
		  :last-name "Hilldale"
		  :email-address "mhilldale@ardra.com"
		  :primary-phone-number "412 281 2593"
		  :radar-weight 25)

(new-radar-person "George Peterson"
		  :person-index 226
		  :first-name "George"
		  :last-name "Peterson"
		  :email-address "gpeterso@ardra.com"
		  :primary-phone-number "412 281 2595"
		  :radar-weight 25)

(new-radar-person "Estelle Giles"
		  :person-index 227
		  :first-name "Estelle"
		  :last-name "Giles"
		  :email-address "estelle@ardra.com"
		  :primary-phone-number "412 281 2597"
		  :radar-weight 25)

(new-radar-person "Lillie Furman"
		  :person-index 228
		  :first-name "Lillie"
		  :last-name "Furman"
		  :email-address "lfurma@ardra.com"
		  :primary-phone-number "412 281 2599"
		  :radar-weight 25)

(new-radar-person "Arthur Peters"
		  :person-index 229
		  :first-name "Arthur"
		  :last-name "Peters"
		  :email-address "apeters@ardra.com"
		  :primary-phone-number "412 281 2601"
		  :radar-weight 25)

(new-radar-person "Violo Watkins"
		  :person-index 230
		  :first-name "Violo"
		  :last-name "Watkins"
		  :email-address "watkins@ardra.com"
		  :primary-phone-number "412 281 2603"
		  :radar-weight 25)

(new-radar-person "Mitchell Blakeslee"
		  :person-index 231
		  :first-name "Mitchell"
		  :last-name "Blakeslee"
		  :email-address "blakeslee@ardra.com"
		  :primary-phone-number "412 281 2605"
		  :radar-weight 25)

(new-radar-person "Lisa Albough"
		  :person-index 232
		  :first-name "Lisa"
		  :last-name "Albough"
		  :email-address "lalbough@ardra.com"
		  :primary-phone-number "412 281 2607"
		  :radar-weight 25)

(new-radar-person "Jeffrey Winslow"
		  :person-index 233
		  :first-name "Jeffrey"
		  :last-name "Winslow"
		  :email-address "jwinslow@ardra.com"
		  :primary-phone-number "412 281 2609"
		  :radar-weight 25)

(new-radar-person "Charlotte Carlson"
		  :person-index 234
		  :first-name "Charlotte"
		  :last-name "Carlson"
		  :email-address "ccarlson@ardra.com"
		  :primary-phone-number "412 281 2611"
		  :radar-weight 25)

(new-radar-person "Margie Bradshaw"
		  :person-index 235
		  :first-name "Margie"
		  :last-name "Bradshaw"
		  :email-address "mbradsha@ardra.com"
		  :primary-phone-number "412 281 2613"
		  :radar-weight 25)

(new-radar-person "Frank Tribeza"
		  :person-index 236
		  :first-name "Frank"
		  :last-name "Tribeza"
		  :email-address "ftribeza@ardra.com"
		  :primary-phone-number "412 281 2615"
		  :radar-weight 25)

(new-radar-person "Mathias Croft"
		  :person-index 237
		  :first-name "Mathias"
		  :last-name "Croft"
		  :email-address "mcroft@ardra.com"
		  :primary-phone-number "412 281 2617"
		  :radar-weight 25)

(new-radar-person "Jerome Walters"
		  :person-index 238
		  :first-name "Jerome"
		  :last-name "Walters"
		  :email-address "jwalters@ardra.com"
		  :primary-phone-number "412 281 2619"
		  :radar-weight 25)

(new-radar-person "Patrick Stanwix"
		  :person-index 239
		  :first-name "Patrick"
		  :last-name "Stanwix"
		  :email-address "pstanwix@ardra.com"
		  :primary-phone-number "412 281 2621"
		  :radar-weight 25)

(new-radar-person "Tricia McMurray"
		  :person-index 240
		  :first-name "Tricia"
		  :last-name "McMurray"
		  :email-address "mcmurray@ardra.com"
		  :primary-phone-number "412 281 2623"
		  :radar-weight 25)

(new-radar-person "Elizabeth Stanton"
		  :person-index 241
		  :first-name "Elizabeth"
		  :last-name "Stanton"
		  :email-address "estanton@ardra.com"
		  :primary-phone-number "412 281 2625"
		  :radar-weight 25)

(new-radar-person "Garret Mitchell"
		  :person-index 242
		  :first-name "Garret"
		  :last-name "Mitchell"
		  :email-address "gmitchell@ardra.com"
		  :primary-phone-number "412 281 2627"
		  :radar-weight 25)

(new-radar-person "Nina Murray"
		  :person-index 243
		  :first-name "Nina"
		  :last-name "Murray"
		  :email-address "nina@ardra.com"
		  :primary-phone-number "412 281 2629"
		  :radar-weight 25)

(new-radar-person "Nolan Wagner"
		  :person-index 244
		  :first-name "Nolan"
		  :last-name "Wagner"
		  :email-address "nwagner@ardra.com"
		  :primary-phone-number "412 281 2631"
		  :radar-weight 25)

(new-radar-person "Gina Cargiullo"
		  :person-index 245
		  :first-name "Gina"
		  :last-name "Cargiullo"
		  :email-address "cargiullo@ardra.com"
		  :primary-phone-number "412 281 2633"
		  :radar-weight 25)

(new-radar-person "Dorren Waits"
		  :person-index 246
		  :first-name "Dorren"
		  :last-name "Waits"
		  :email-address "dwaits@ardra.com"
		  :primary-phone-number "412 281 3132"
		  :radar-weight 25)

(new-radar-person "Adam Constanza"
		  :person-index 247
		  :first-name "Adam"
		  :last-name "Constanza"
		  :email-address "constanza@ardra.com"
		  :primary-phone-number "412 281 3133"
		  :radar-weight 25)

(new-radar-person "Shin Yi"
		  :person-index 248
		  :first-name "Shin"
		  :last-name "Yi"
		  :email-address "shin@ardra.com"
		  :primary-phone-number "412 281 3134"
		  :radar-weight 25)

(new-radar-person "Trevor Masse"
		  :person-index 249
		  :first-name "Trevor"
		  :last-name "Masse"
		  :email-address "tmasse@ardra.com"
		  :primary-phone-number "412 281 3135"
		  :radar-weight 25)

(new-radar-person "Leonard Church"
		  :person-index 250
		  :first-name "Leonard"
		  :last-name "Church"
		  :email-address "lchurch@ardra.com"
		  :primary-phone-number "412 281 3136"
		  :radar-weight 25)

(new-radar-person "Tommy Hollis"
		  :person-index 251
		  :first-name "Tommy"
		  :last-name "Hollis"
		  :email-address "thollis@ardra.com"
		  :primary-phone-number "412 281 3137"
		  :radar-weight 25)

(new-radar-person "Karen Duchamp"
		  :person-index 252
		  :first-name "Karen"
		  :last-name "Duchamp"
		  :email-address "duchamp@ardra.com"
		  :primary-phone-number "412 281 3138"
		  :radar-weight 25)

(new-radar-person "Kris Vaughan"
		  :person-index 253
		  :first-name "Kris"
		  :last-name "Vaughan"
		  :email-address "klv@ardra.com"
		  :primary-phone-number "412 281 3139"
		  :radar-weight 25)

(new-radar-person "Jimmy Contoro"
		  :person-index 254
		  :first-name "Jimmy"
		  :last-name "Contoro"
		  :email-address "jcontor@ardra.com"
		  :primary-phone-number "412 281 3140"
		  :radar-weight 25)

(new-radar-person "China Elliot"
		  :person-index 255
		  :first-name "China"
		  :last-name "Elliot"
		  :email-address "chinae@ardra.com"
		  :primary-phone-number "412 281 3141"
		  :radar-weight 25)

(new-radar-person "Eva Masters"
		  :person-index 256
		  :first-name "Eva"
		  :last-name "Masters"
		  :email-address "emasters@ardra.com"
		  :primary-phone-number "412 281 3142"
		  :radar-weight 25)

(new-radar-person "Brad Mann"
		  :person-index 257
		  :first-name "Brad"
		  :last-name "Mann"
		  :email-address "bmann@ardra.com"
		  :primary-phone-number "412 281 3143"
		  :radar-weight 25)

(new-radar-person "Victoria Johnson"
		  :person-index 258
		  :first-name "Victoria"
		  :last-name "Johnson"
		  :email-address "vej@ardra.com"
		  :primary-phone-number "412 281 3144"
		  :radar-weight 25)

(new-radar-person "Stephanie Gabel"
		  :person-index 259
		  :first-name "Stephanie"
		  :last-name "Gabel"
		  :email-address "sgabel@ardra.com"
		  :primary-phone-number "412 281 3145"
		  :radar-weight 25)

(new-radar-person "Angelo Stienhardt"
		  :person-index 260
		  :first-name "Angelo"
		  :last-name "Stienhardt"
		  :email-address "steinhardt@ardra.com"
		  :primary-phone-number "412 281 3146"
		  :radar-weight 25)

(new-radar-person "David McCullar"
		  :person-index 261
		  :first-name "David"
		  :last-name "McCullar"
		  :email-address "mccullar@ardra.com"
		  :primary-phone-number "412 281 3147"
		  :radar-weight 25)

(new-radar-person "Seth Kubicek"
		  :person-index 262
		  :first-name "Seth"
		  :last-name "Kubicek"
		  :email-address "kubicek@ardra.com"
		  :primary-phone-number "412 281 3148"
		  :radar-weight 25)

(new-radar-person "Steven Bose"
		  :person-index 263
		  :first-name "Steven"
		  :last-name "Bose"
		  :email-address "slbose@ardra.com"
		  :primary-phone-number "412 281 3149"
		  :radar-weight 25)

(new-radar-person "Jason Hogan"
		  :person-index 264
		  :first-name "Jason"
		  :last-name "Hogan"
		  :email-address "jmhogan@ardra.com"
		  :primary-phone-number "412 281 3150"
		  :radar-weight 25)

(new-radar-person "Jason Rios"
		  :person-index 265
		  :first-name "Jason"
		  :last-name "Rios"
		  :email-address "jlwrios@ardra.com"
		  :primary-phone-number "412 281 3151"
		  :radar-weight 25)

(new-radar-person "Lynn Barclay"
		  :person-index 266
		  :first-name "Lynn"
		  :last-name "Barclay"
		  :email-address "lbarc@ardra.com"
		  :primary-phone-number "412 281 3152"
		  :radar-weight 25)

(new-radar-person "Katie Graham"
		  :person-index 267
		  :first-name "Katie"
		  :last-name "Graham"
		  :email-address "kgraham@ardra.com"
		  :primary-phone-number "412 281 3153"
		  :radar-weight 25)

(new-radar-person "Sarah Chesney"
		  :person-index 268
		  :first-name "Sarah"
		  :last-name "Chesney"
		  :email-address "schesney@ardra.com"
		  :primary-phone-number "412 281 3154"
		  :radar-weight 25)

(new-radar-person "Jane Liebrock"
		  :person-index 269
		  :first-name "Jane"
		  :last-name "Liebrock"
		  :email-address "jjl@ardra.com"
		  :primary-phone-number "412 281 3155"
		  :radar-weight 25)

(new-radar-person "Boomer Butler"
		  :person-index 270
		  :first-name "Boomer"
		  :last-name "Butler"
		  :email-address "boom@ardra.com"
		  :primary-phone-number "412 281 3156"
		  :radar-weight 25)

(new-radar-person "Austin Jaffe"
		  :person-index 271
		  :first-name "Austin"
		  :last-name "Jaffe"
		  :email-address "austinj@ardra.com"
		  :primary-phone-number "412 281 3157"
		  :radar-weight 25)

(new-radar-person "Joel Nilene"
		  :person-index 272
		  :first-name "Joel"
		  :last-name "Nilene"
		  :email-address "jonile@ardra.com"
		  :primary-phone-number "412 281 3158"
		  :radar-weight 25)

(new-radar-person "Miriam Dunn"
		  :person-index 273
		  :first-name "Miriam"
		  :last-name "Dunn"
		  :email-address "mdunn@ardra.com"
		  :primary-phone-number "412 281 3159"
		  :radar-weight 25)

(new-radar-person "Austin Parton"
		  :person-index 274
		  :first-name "Austin"
		  :last-name "Parton"
		  :email-address "aparton@ardra.com"
		  :primary-phone-number "412 281 3160"
		  :radar-weight 25)

(new-radar-person "Ashley Guinne"
		  :person-index 275
		  :first-name "Ashley"
		  :last-name "Guinne"
		  :email-address "agguinne@ardra.com"
		  :primary-phone-number "412 281 3161"
		  :radar-weight 25)

(new-radar-person "Taylor Childress"
		  :person-index 276
		  :first-name "Taylor"
		  :last-name "Childress"
		  :email-address "tchildre@ardra.com"
		  :primary-phone-number "412 281 3162"
		  :radar-weight 25)

(new-radar-person "Leah Davies"
		  :person-index 277
		  :first-name "Leah"
		  :last-name "Davies"
		  :email-address "ldavies@ardra.com"
		  :primary-phone-number "412 281 3163"
		  :radar-weight 25)

(new-radar-person "Trevor Rodgriguez"
		  :person-index 278
		  :first-name "Trevor"
		  :last-name "Rodgriguez"
		  :email-address "trevor@ardra.com"
		  :primary-phone-number "412 281 3164"
		  :radar-weight 25)

(new-radar-person "Dan Gold"
		  :person-index 279
		  :first-name "Dan"
		  :last-name "Gold"
		  :email-address "dgold@ardra.com"
		  :primary-phone-number "412 281 3165"
		  :radar-weight 25)

(new-radar-person "Ellen Green"
		  :person-index 280
		  :first-name "Ellen"
		  :last-name "Green"
		  :email-address "ewg@ardra.com"
		  :primary-phone-number "412 281 3166"
		  :radar-weight 25)

(new-radar-person "Lorraine Baker"
		  :person-index 281
		  :first-name "Lorraine"
		  :last-name "Baker"
		  :email-address "lbaker@ardra.com"
		  :primary-phone-number "412 281 3167"
		  :radar-weight 25)

(new-radar-person "Sam Ketterson"
		  :person-index 282
		  :first-name "Sam"
		  :last-name "Ketterson"
		  :radar-weight 25)

(new-radar-person "Stuart Weitzmann"
		  :person-index 283
		  :first-name "Stuart"
		  :last-name "Weitzmann"
		  :vip t
		  :radar-weight 100)

(new-radar-person "Andrea Manson"
		  :person-index 284
		  :first-name "Andrea"
		  :last-name "Manson"
		  :vip t
		  :radar-weight 100)
