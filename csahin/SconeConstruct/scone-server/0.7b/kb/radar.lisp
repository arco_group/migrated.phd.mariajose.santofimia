;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; 
;;; This file is a script that loads the radar stuff.
;;;
;;; Author: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2003, Carnegie Mellon University.

;;; The Scone software and documentation are made available to the
;;; public under the CPL 1.0 Open Source license.  A copy of this
;;; license is distributed with the software.  The license can also be
;;; found at URL <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone
;;; software, you agree to be bound by the terms and conditions set
;;; forth in the CPL 1.0 Open Source License.  If you do not agree to
;;; these terms and conditions, or if they are not legally applicable
;;; in the jurisdiction where such use takes place, then you may not
;;; use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by
;;; Scott E. Fahlman for IBM Coporation between June 2001 and May
;;; 2003.  IBM holds the copyright on NETL2 and has made that software
;;; available to the author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under Contract No. NBCHC030029 and
;;; NBCHD030010.  Any opinions, findings and conclusions or recommendations
;;; expressed in this material are those of the author(s) and do not
;;; necessarily reflect the views of DARPA or the Department of
;;; Interior-National Business Center (DOI-NBC).
;;; ***************************************************************************
;;;
;;; Script to load a buncnh of core-knowledge files with LOAD-KB.
(load-kb "core-kb")
(load-kb "radar/radar")
(load-kb "radar/radar-aux")
(load-kb "radar/split-sequence")
(load-kb "radar/get-scone-annotations")
(load-kb "radar/makedict")
;; UPDATED Jul 1 2005 : now the people, events, and locations are in a single file
(load-kb "radar/people-locations-events")
;; UPDATED Jul 15 2005 : register all the phones and emails as :english names at load-time.
(register-person-email-addresses)
(register-person-phone-numbers)
;; UPDATED Oct 14: post-wargaming fixes to the dictionary.
;; remove false hits and add two elements
(load-kb "radar/dictionary-fixes")


;;; Start the show.
(clear-all-markers)
(in-context {common:general})
(commentary "~&Radar knowledge loaded and ready.~%")

