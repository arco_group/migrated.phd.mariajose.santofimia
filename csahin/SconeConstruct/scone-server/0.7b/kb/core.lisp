;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; 
;;; Core knowledge.  This is basically just a script that loads many
;;; files full of general knowledge.  these are things useful across
;;; almost all applications.
;;;
;;; Author: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2003-2006, Carnegie Mellon University.

;;; The Scone software and documentation are made available to the
;;; public under the CPL 1.0 Open Source license.  A copy of this
;;; license is distributed with the software.  The license can also be
;;; found at URL <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone
;;; software, you agree to be bound by the terms and conditions set
;;; forth in the CPL 1.0 Open Source License.  If you do not agree to
;;; these terms and conditions, or if they are not legally applicable
;;; in the jurisdiction where such use takes place, then you may not
;;; use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by
;;; Scott E. Fahlman for IBM Coporation between June 2001 and May
;;; 2003.  IBM holds the copyright on NETL2 and has made that software
;;; available to the author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under Contract No. NBCHD030010.  Any
;;; opinions, findings and conclusions or recommendations expressed in this
;;; material are those of the author(s) and do not necessarily reflect the
;;; views of DARPA or the Department of Interior-National Business Center
;;; (DOI-NBC).
;;; ***************************************************************************
;;;
;;; Script to load a buncnh of core-knowledge files with LOAD-KB.
(load-kb "core-kb")

;;; Start the show.
(in-context {common:general})
(commentary "~&Core knowledge loaded and ready.~%")


