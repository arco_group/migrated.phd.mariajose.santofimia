(defun mark-context-contents (context m &key (append nil))
  (declare (fixnum m))
  (setq context (lookup-element-test context))
  (check-legal-marker-pair m)

  (unless append
    (clear-marker-pair m))

  (let* ((queue (list context)))

    (dolist (c queue)
      (dolist (elem (incoming-context-wires c))
	(mark elem m)
	(append queue (list elem))))))

(defun list-context-contents (context)
  (with-temp-markers (m)
    (mark-context-contents context m)
    (list-marked m)))
    
(defun show-context-contents (context)
  (with-temp-markers (m)
    (mark-context-contents context m)
    (show-marked m)))


(defun mark-before (event m)
  
  (let* ((event (lookup-element event))
	 (before (get-the-x-role-of-y *before-context* event))
	 (parents (list-supertypes event)))
    
    (mark-context-contents before m)
    
    (dolist (p parents)
      (when (is-x-a-y? p *event*)
	(format t "Going in to ~A...~&" p)
	(setq before (get-the-x-role-of-y *before-context* p))
	(mark-context-contents before m :append t))
    (marker-count m))))

(defun list-before (event)
  (with-temp-markers (m1)
    (mark-before event m1)
    (list-marked m1)))

(defun show-before (event)
  (with-temp-markers (m1)
    (mark-before event m1)
    (show-marked m1)))


(defun is-x-a-y-of-z? (x y z)
  (with-temp-markers (m)
	(mark-all-x-of-y y z m)
	(marker-on? x m)))

(defun x-is-not-the-y-of-z (x y z)
  "Create a negated map link between X and Y of Z"
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (setq z (lookup-element-test z))
  (new-map x y z :negate t))

(defun the-x-of-y-is-not-z (x y z)
  "Create a negated map link between Z and X of Y"
  (x-is-not-a-y-of-z z x y))

(defun x-is-not-a-y-of-z (x y z)
  "Y of Z should be a type-role.  If there is an explicit is-a from
   X to Y of Z, let the current context cancel it. Otherwise, just
   create a not-is-a."
  (setq x (lookup-element x))
  (setq y (lookup-element-test y))
  (setq z (lookup-element-test z))

  (let* ((role-node (get-the-x-role-of-y y z)))
    ;; check for an is-a
    
    (new-is-not-a x role-node)))

(defun the-x-of-y-is-not-a-z (x y z)
  "Find or create the node for the X of Y. Then, add a new IS-NOT-A link from
   this node to Z"
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (setq z (lookup-element-test z))
  (let ((role-node (get-the-x-role-of-y x y)))
    (new-is-not-a role-node z)))
