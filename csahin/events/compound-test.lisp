(new-event-type {eventA}
 '({event})
 :roles
 ((:indv {roleX of A} {thing})
  (:indv {roleY of A} {thing})))

(new-event-type {eventB}
 '({event})
 :roles
 ((:indv {roleX of B} {thing})
  (:indv {roleZ of B} {thing})))

(new-event-type {eventC}
 '({event})
 :roles
 ((:indv {roleY of C} {thing})
  (:indv {roleZ of C} {thing})))


(new-event-type {eventABC} 
 '({event}))

(new-compound {eventABC}
 '(:seq
   ({eventA} (:bind 1 {roleX of A}) (:bind 2 {roleY of A}))
   ({eventB} (:bind 1 {roleX of B}) (:bind 3 {roleZ of B}))
   ({eventC} (:bind 2 {roleY of C}) (:bind 3 {roleZ of C}))))

