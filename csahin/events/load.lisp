;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; 
;;; Load the Scone engine, bootstrap files, and events/actions files.
;;;
;;; Author: Cinar Sahin
;;; ***************************************************************************

;; Load the final engine
(load "/home/mjsantof/tesis/CMU/Scone/csahin/events/engine")
(load "/home/mjsantof/tesis/CMU/Scone/csahin/events/helper")

;; Load general knowledge
(load-kb "/home/mjsantof/tesis/CMU/Scone/csahin/events/kb/bootstrap.lisp")
(load-kb "/home/mjsantof/tesis/CMU/Scone/csahin/events/kb/core-kb.lisp")
(commentary "~&Core knowledge loaded...~%")

(defvar *context-node* (lookup-element {context}))
(defvar *general-context* (lookup-element {general}))

;; Load general events knowledge
(load-kb "/home/mjsantof/tesis/CMU/Scone/csahin/events/events.lisp")

;; Load general event functions
(load "/home/mjsantof/tesis/CMU/Scone/csahin/events/new-event")
(load "/home/mjsantof/tesis/CMU/Scone/csahin/events/expansion")

;; Load movement knowledge base
(load-kb "/home/mjsantof/tesis/CMU/Scone/csahin/events/kb/core-extension.lisp")
(load-kb "/home/mjsantof/tesis/CMU/Scone/csahin/events/kb/move.lisp")
(load-kb "/home/mjsantof/tesis/CMU/Scone/csahin/events/kb/food.lisp")

(commentary "~&Events knowledge loaded...~%")

(in-context {common:general})
(in-namespace "events" :include "common")
(commentary "~&All knowledge loaded and read. Enjoy!~%")
