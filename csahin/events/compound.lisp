(defun new-compound (event form)
  "Makes EVENT a new compound event depending on the form it takes."
  
  ;; check event
  (cond ((null event)
	 (setq event (new-type event *event*)))
	
	((not (is-x-a-y? event *event*))
	 (commentary "~A not an event." event)
	 (return-from new-compound nil))
	
	((is-x-a-y? event *compound-event*)
	 (commentary "~A already a compound event." event)
	 (return-from new-compound nil)))
  
  ;; check expansion args
  (unless (check-compound-form form)
    (return-from new-compound nil))

  ;; now execute the proper internal function
  (ecase (car form)
    
    (:seq (new-sequential-internal event form)))
  event)


(defun check-compound-form (form)
  "Recursively checks the compound form. A legal form starts with a compound
   type keyword (:seq, etc..) and continues with events or other compound forms"

  (unless (listp form)
    (commentary "Compound form is not a list.")
    (return-from check-compound-form nil))

  (let* ((compound-type (car form))
	 (compound-rest (cdr form)))

    (ecase compound-type
      (:seq
       ;; check if all arguments are events or valid compound forms
       (unless (> (length compound-rest) 1)
	 (commentary "Trivial sequential compound.")
	 (return-from check-compound-form nil))

       (dolist (arg compound-rest)
	 (cond
	   ;; if it's a list and starts with an iname/element
	   ((and (listp arg)
		 (or (typep (car arg) 'element) 
		     (typep (car arg) 'element-iname)))
	    
	    (unless (is-x-a-y? (car arg) *event*)
	      (commentary "~A is not an event." (car arg))))

	   ;; if it's a list and doesn't start with an iname/element it must be a compound form

	   ((listp arg)
	    (unless (check-compound-form arg)
	      (return-from check-compound-form nil)))
		
	   ;; otherwise it must be an event
	   (t
	    (unless (is-x-a-y? arg *event*)
	      (commentary "~A is not an event." arg)))))
       t))))

(defun unify-bind-list (key role-list)
  "Given a list of elements creates EQ links between them in a linear manner. KEY is not used."

  (format t "~&Unify called: ~A~%" role-list)

  ;; to avoid the warning
  (eq key key)

  (unless (listp role-list)
    (commentary "Bind list is not a list!")
    (return-from unify-bind-list nil))

  (when (= 1 (length role-list))
    (commentary "Bind list is trivial!")
    (return-from unify-bind-list nil))

  (let ((role1 (lookup-element (car role-list))))
    
    (dolist (role2 (cdr role-list))
      (setq role2 (lookup-element role2))

      (new-eq role1 role2)
      (setq role1 role2))))


(defun new-sequential-internal (event expansion-args)
  "Makes EVENT a sequential event. EXPANSION-ARGS is a list starting
   with :seq, containing the sequentials subevents of EVENT. No argument checking
   is done"
  
  (new-is-a event *sequential-event*)

  (let ((prev nil)
	(curr (cadr expansion-args)))

    (cond
      ;; if curr is an atom just
      ((atom curr)
       (setq curr (lookup-element curr)))
      
      ;; if the first is an iname or an element it look for bindings
      ((or (typep (car curr) 'element)
	   (typep (car curr) 'element-iname))
       (setq curr (lookup-element (car curr))))

      ;; otherwise it must be another compound form
      (t
       (setq curr (new-compound nil curr))))
    (create-the-x-role-of-y *first-ordered-event* event
			    :parent curr
			    :iname (gen-iname (internal-name curr)))			    
    (setq prev (find-the-x-role-of-y *first-ordered-event* event))

    (dolist (arg (cddr expansion-args))
      (cond
	;; if curr is an atom just
	((atom arg)
	 (setq arg (lookup-element arg)))
	
	;; if the first is an iname or an element it look for bindings
	((or (typep (car arg) 'element)
	     (typep (car arg) 'element-iname))
	 (setq arg (lookup-element (car arg))))
	
	;; otherwise it must be another compound form
	
	(t
	 (setq arg (new-compound nil arg))))
      
      (setq arg (new-indv nil arg :proper nil))
      (x-is-a-y-of-z arg *subevent* event)
      ;; create the temporal statement
      (new-statement prev *happens-before* arg)
      ;; weave the contexts
      (new-eq (get-the-x-role-of-y *after-context*  prev)
	      (get-the-x-role-of-y *before-context* arg))
      (setq prev arg)))

  ;; the subevents are weaved now do the role bindings
  (let ((subevents (list-sequential-subevents event))
	(bindings (make-hash-table)))
    
    ;; go throught the arguments and the actual role-fillers at the same time
    (do* ((subevent-list subevents (cdr subevent-list))
	  (subevent (car subevent-list) (car subevent-list))
	  (arg-list (cdr expansion-args) (cdr arg-list))
	  (arg (car arg-list) (car arg-list)))

	 ((null subevent-list) event)
      
      (when
	  ;; if the argument is a list and the first is an iname or an element, look for bindings
	  (and (listp arg) (or (typep (car arg) 'element)
			       (typep (car arg) 'element-iname)))
	
	(dolist (bind-form (cdr arg))
	  ;; make sure :bind is there
	  (unless (eq (car bind-form) ':bind)
	    (commentary "Ill-formed binding: ~A~% Ignoring all bindings." bind-form)
	    (return-from new-sequential-internal event))
	  
	  (let* ((key (second bind-form))
		 (role (get-the-x-role-of-y (third bind-form) subevent))
		 (binds (gethash key bindings)))
	    
	    (if (null binds)
		(setf (gethash key bindings) (list role))
		(setf (gethash key bindings) (cons role binds)))))))
    
    ;; now all the binding lists are constructed go through them and bind them
    (maphash 'unify-bind-list bindings)))




(defun list-sequential-subevents (event)
  "Given a sequential event, finds the subevents and puts them in a list"
  
  (setq event (lookup-element-test event))
  
  (unless (is-x-a-y? event *sequential-event*)
    (commentary "~A is not a sequential event." event)
    (return-from list-sequential-subevents nil))
  
  
  (with-temp-markers (m1)
    
    (do* ((curr-step (find-the-x-role-of-y *first-ordered-event* event))
	  (sequence-list (list curr-step) (if curr-step
					      (cons curr-step sequence-list)
					      sequence-list)))
	 
	 ((null curr-step)
	  (nreverse sequence-list))
      
      ;; determine the possible next steps
      (mark-rel curr-step *happens-before* m1)
      
      
      (cond
	;; if there is nothing marked, the chains has ended
	((= 0 (marker-count m1))
	 (setq curr-step nil))
	
	;; there should be exactly one marked element
	((> (marker-count m1) 1)
	 (commentary "~A has multiple ordered events after." curr-step)
	 (return-from list-sequential-subevents nil))
	
	(t
	 (setq curr-step (car (list-marked m1))))))))


(defun play-sequential-event (event)
  "Given a sequential event, goes through the steps
   and prints out the activated elements."

  (setq event (lookup-element-test event))

  (unless (is-x-a-y? event *sequential-event*)
    (commentary "~A is not a sequential event." event)
    (return-from play-sequential-event nil))

  (let* ((subevents (list-sequential-subevents event))
	 (context nil)
	 (contents-one nil)
	 (contents nil)
	 (curr-step nil))

    (with-temp-markers (m1)
      
      (dolist (step subevents)
	(format t "~%~%Step ~A:~%" step)
	(setq curr-step step)
	(setq contents-one nil)
	(setq contents nil)

	;; get the before context
	(setq context (get-the-x-role-of-y *before-context* step))
	;; propagate to all eq'ed contexts
	(eq-scan context m1 :basic nil)

	;; the generic context is VC so go to the owner events, go upto the parent
	;; and print the context of the parent
	(do-marked (c m1)
	  ;;(format t "~%Printing Context: ~A -- Owner: ~A~%" c (caddr (definition c)))
	  (setq contents-one (list-context-contents c))
	  (when (not (null contents-one))
	    (push contents-one contents)))

	(format t "~A" contents))

      ;; now print the final after context
      (format t "~%Step ~A:~&" (parent-wire curr-step))
      (setq context (get-the-x-role-of-y *after-context* curr-step))
      (setq contents-one nil)
      (setq contents nil)
      (eq-scan context m1 :basic nil)
      
      (do-marked (c m1)
	;;(format t "~%Printing Context: ~A -- Owner: ~A~%~%" c (caddr (definition c)))
	(setq contents-one (list-context-contents c))
	(when (not (null contents-one))
	  (push contents-one contents)))
      
      (format t "~A~%" contents))))
      
      


(defun expand-event (event &key
			   (depth 0)
			   (all nil))
  "Returns the subevents of the given event."

  (setq event (lookup-element-test event))

  ;; We can only expand an event if it's expandable
  (unless (is-x-a-y? event *compound-event*)
    (commentary "~A is not a {compound event}." event)
    (return-from expand-event (list event)))
  
  (let* ((expansion nil)
	 (deeper-expansion nil))

      ;; we will return different structures depending on what type of event this is
      (cond ((is-x-a-y? event *sequential-event*)
	     (setq expansion (list-sequential-subevents event))
	     (cons ':SEQ expansion)))
      
      (when (or all (> depth 0))
	
	(when (> depth 0)
	  (setq depth (1- depth)))
	
	(dolist (step expansion)
	  (cond ((and (or (typep step 'element-iname)
			  (typep step 'element))
		      (is-x-a-y? step *compound-event*))
		 
		 (push (expand-event step
				     :depth depth
				     :all all) deeper-expansion))
		
		(t
		 (push step deeper-expansion))))
	(setq expansion (nreverse deeper-expansion)))
      
      expansion))





