;; General Event Knowledge Base
;; -------------------------------------------------
;; 
;; Contents
;; 
;; A. Event
;;    1. Sequential Events
;;    2. ALL Events
;;    3. OR Events
;;    4. Optional Events
;;    5. Concurrent Events
;;    6. Iterative Events
;;    7. Conditional Events
;;
;; B. Event DEFVARs
;;
;;
;; -------------------------------------------------

(in-context {general})
(in-namespace "events" :include "common")

;; I. Event

(new-type {event} {thing}
 :english '("happening"))

;; The four crucial contexts of events:
;;
;; surrounding context: this serves as a basis for the rest of the
;; contexts. When we want to define general things about an event
;; we would use the surrounding context.
;;
;; before, during and after contexts: these all start as clones of
;; the surrounding context, but have differences that tell us what
;; is true before, during or after the event.

(new-indv-role {surrounding context} {event} {context})
(new-indv-role {before context} {event} {surrounding context})
(new-indv-role {during context} {event} {surrounding context})
(new-indv-role {after context} {event} {surrounding context})

;; Anything that was involved in the event belongs to this type-role.
(new-type-role {object involved} {event} {thing})

;; Usually events have a location.
(new-indv-role {location} {event} {place}
 :english '(:no-iname "location" "venue" "place")
 :may-have t)

;; things cause events
(new-relation {causes} :a-inst-of {thing}
		       :b-inst-of {event})

;; A. Subevents

(new-type {expandable event} {event})

;;; All expandable events have sub-events
(new-type-role {subevent} {expandable event} {event})

(new-complete-split-subtypes {expandable event}
 '({sequential event}
   {all event}
   {or event}
   {iterative event}
   {concurrent event}
   {optional event}
   {conditional event}))

;; 1. Sequantial Events

;; sequential event's subevent have two relations defined for them
;; they can be viewed as a chain of events weaved with these relations
(get-the-x-role-of-y {subevent} {sequential event} :iname {ordered event})

;; both these relations belong to a sequential event when used as statements.
;; These sequential events may use the same event twice, to keep the ordering
;; right we need indeces for each relation. That is the c-value
(new-relation {happens before} :a-inst-of {ordered event}
			       :b-inst-of {ordered event}
			       :c-inst-of {integer}
			       :english '(:relation
					  :inverse-relation "happens after")
			       :transitive t)

;; we might have cycles, so keep a head of the sequence so that we know where to start
(new-indv-role {first ordered event} {sequential event} {ordered event})

;; 2. ALL Events
;; 3. OR Events
;; 4. Optional Events
;; 5. Concurrent Events
;; Being a subevent of these is sufficient knowledge to reason about them.


;; 6. Iterative Events

;; there are two types, these are complete and split, for now
(new-complete-split-subtypes {iterative event}
 '({do n times event} {do until event}))

;;do n times events have a number of iterations
(new-indv-role {number of iterations} {do n times event} {integer})

;;do until events have a termination clause function
(new-indv-role {termination clause} {do until event} {function})


;; 7. Conditional Events

;; Conditional events have predicates.
(new-indv-role {predicate} {conditional event} {function})


;; B. Event DEFVARs

(defvar *event* (lookup-element {event}))
(defvar *surrounding-context* (lookup-element {surrounding context}))
(defvar *before-context* (lookup-element {before context}))
(defvar *during-context* (lookup-element {during context}))
(defvar *after-context* (lookup-element {after context}))
(defvar *object-involved* (lookup-element {object involved}))
(defvar *subevent* (lookup-element {subevent}))

(defvar *expandable-event* (lookup-element {expandable event}))

(defvar *sequential-event* (lookup-element {sequential event}))
(defvar *ordered-event* (lookup-element {ordered event}))
(defvar *first-ordered-event* (lookup-element {first ordered event}))
(defvar *happens-before* (lookup-element {happens before}))

(defvar *or-event* (lookup-element {or event}))
(defvar *all-event* (lookup-element {all event}))

(defvar *cond-event* (lookup-element {conditional event}))
(defvar *predicate* (lookup-element {predicate}))

(defvar *iterative-event* (lookup-element {iterative event}))
(defvar *do-until-event* (lookup-element {do until event}))
(defvar *do-n-times-event* (lookup-element {do n times event}))
(defvar *n-of-iterations* (lookup-element {number of iterations}))
(defvar *termination-clause* (lookup-element {termination clause}))

(defvar *optional-event* (lookup-element {optional event}))
(defvar *concurrent-event* (lookup-element {concurrent event}))


