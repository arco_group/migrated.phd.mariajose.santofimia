;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; 
;;; Core knowledge for the Scone Knowledge Representation System.
;;;
;;; Author: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2003-2005, Carnegie Mellon University.

;;; The Scone software and documentation are made available to the
;;; public under the CPL 1.0 Open Source license.  A copy of this
;;; license is distributed with the software.  The license can also be
;;; found at URL <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone
;;; software, you agree to be bound by the terms and conditions set
;;; forth in the CPL 1.0 Open Source License.  If you do not agree to
;;; these terms and conditions, or if they are not legally applicable
;;; in the jurisdiction where such use takes place, then you may not
;;; use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by
;;; Scott E. Fahlman for IBM Coporation between June 2001 and May
;;; 2003.  IBM holds the copyright on NETL2 and has made that software
;;; available to the author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under contract number NBCHD030010.
;;; Any opinions, findings and conclusions or recommendations expressed in
;;; this material are those of the author(s) and do not necessarily reflect
;;; the views of DARPA or the Department of Interior-National Business
;;; Center (DOI-NBC).
;;; ***************************************************************************
;;;
(in-namespace "common")
(in-context {universal})

;;; ------------------------------------------------------------------------
;;; MATHEMATICAL STUFF: SETS, ETC.

;;; Define a sequence.  Could be finite, unbounded, or cyclic.
(new-type {sequence} {set})

;; Each sequence member has an index.
(new-indv-role {sequence member index}
	       (get-the-x-role-of-y {member} {sequence}
				    :iname {sequence member})
	       {integer})

(new-indv-role {successor}
	       {sequence member}
	       {sequence member}
	       :english '(:role :inverse-role "predecessor"))

;;; A finite sequence has a first and last member.
(new-type {finite sequence} {sequence})

(new-indv-role {finite sequence length} {finite sequence}
	       {integer}
	       :english '(:role "length"))

;; A sequence has a first and last member.
(new-indv-role {first member} {finite sequence}
	       (get-the-x-role-of-y
		{member}
		{finite sequence}
		:iname {finite sequence member})
	       :english '(:role "first element"))

(new-indv-role {last member} {finite sequence}
	 {finite sequence member}
	 :english '(:role "last element"))

#| %%% These don't work for now.

;; The last member of a finite set has no successor.
(x-has-no-y {last member} {successor})

;; The first member of a finite set has no predecessor.
(x-has-no-y (first member} {predecessor})
|#

(new-type {cyclic sequence} {sequence})

(new-indv-role {cyclic sequence period} {cyclic sequence}
	       (get-the-x-role-of-y {member} {cyclic sequence}
					    :iname {cyclic sequence member})
	       :english '(:role "period" "cycle length" "length"))

;;;; -------------------------------------------------------------------------
;;;; PHYSICAL OBJECTS AND MATERIALS

(in-context {general})

(new-complete-split-subtypes
 {tangible}
 '(({naturally occurring} :adj "natural")
   ({man-made} :adj "artificial")))

(new-type {physical object} {tangible}
	  :english '("object"))

(new-intersection-type {natural object}
		       '({physical object} {naturally occurring})
		       :english '("naturally occurring object"))

(new-intersection-type {man-made object}
		       '({physical object} {man-made})
		       :english '("artificial object"))

(new-complete-split-subtypes
 {physical object}
 '(({animate} :adj "active" "living" "alive")
   ({inanimate} :adj "inert" "dead")))

;;; Stuff and Materials

;;; We use {stuff} to include pseudo-subtances like time and information.
(new-type {stuff} {thing})

;;; We use material for physical substances like concrete and water.
(new-type {material} {stuff}
	  :english '("substance" "physical substance"))

(new-is-a {material} {tangible})

(new-intersection-type {natural material}
		       '({material} {naturally occurring})
		       :english '("naturally occurring material"
				  "natural substance"
				  "Naturally occurring substance"))

(new-intersection-type {man-made material}
		       '({material} {man-made})
		       :english '("artificial material"
				  "man-made substance"
				  "artificial substance"))

(new-split-subtypes
 {material}
 '(({solid} :adj-noun)
   ({liquid} :adj-noun)
   ({gas} :adj-noun)))

;;;; -------------------------------------------------------------------------
;;;; Units, Measures and Quantities

;;; %%% Most of the units and type-checking are in.  Automatic unit
;;; conversions are not yet in.

;;; The set of all qualities measured by units.
(new-type {measurable quality} {stuff})

(new-is-a {measurable quality} {intangible})

;;; This split divides all the qualities from one another.
(new-split nil :iname {measurable quality split})
	   
;;; The set of all units.
(new-type {unit} {intangible})

;;; Every unit measures some quality.
(new-indv-role {unit measurable quality} {unit}
	       {measurable quality}
	       :english '(:role "quality"))

;;; This split divides all the unit types from one another.
(new-split nil :iname {unit type split})

;;; We use MEASURE for a value that has a magnitude (a number) and a unit,
;;; such as {16.0 ton} or {5280 foot}.
(new-type {measure} {intangible})

;;; This split divides all the unit types from one another.
(new-split nil :iname {measure type split})

(new-indv-role {measure magnitude} {measure} {number}
	       :english '(:role "magnitude"))

(new-indv-role {measure unit} {measure} {unit}
	       :english '(:role "unit"))

(new-indv-role {measure measurable quality} {measure} {measurable quality}
	       :english '(:role "quality"))

;;; Function to define a new type or family of unit and the associated
;;; methods.

(defun new-unit-type (string unit-list)
  (let ((quality
	 (new-indv (make-element-iname :string string)
		   {measurable quality}))
	(unit-type
	 (new-type (make-element-iname :string (format nil "~A unit" string))
		   {unit}))
	(measure-type
	 (new-type (make-element-iname :string (format nil "~A measure" string))
		   {measure})))
    (add-to-split {measurable quality split} quality)
    (add-to-split {unit type split} unit-type)
    (add-to-split {measure type split} measure-type)
    (the-x-of-y-is-z {unit measurable quality} unit-type quality)
    (the-x-of-y-is-z {measure measurable quality} measure-type quality)
    (the-x-of-y-is-a-z {measure unit} measure-type unit-type)
    (new-members unit-type unit-list)))

(new-unit-type "length"
  '(({meter} "m")
    ({millimeter} "mm")
    ({centimeter} "cm")
    ({kilometer} "Km")
    ({inch} "in")
    ({foot} "ft")
    ({yard})
    ({mile} "mi")
    ({light-year} "ly")))

(new-unit-type "area"
  '(({square meter} "sq m")
    ({square kilometer})
    ({hectare} "ha")
    ({square inch} "sq in")
    ({square foot} "sq ft")
    ({square mile} "sq mi")
    ({acre})))

(new-unit-type "volume"
  '(({cubic meter})
    ({cubic centimeter} "cc")
    ({cubic inch})
    ({cubic yard})
    ({cubic mile})
    ({cubic kilometer})
    ({milliliter} "mL")
    ({liter} "L")
    ({pint} "pt")
    ({quart} "qt")
    ({gallon} "gal")
    ({acre foot})))

(new-unit-type "mass"
  '(({kilogram} "Kg")
    ({gram} "g")
    ({milligram} "mg")
    ({microgram} "ug")
    ({pound} "lb")
    ({ounce} "oz")
    ({ton})
    ({stone})))

(new-unit-type "time"
  '(({second} "sec" "s")
    ({minute} "min" "m")
    ({hour} "hr" "h")
    ({day} "d")
    ({week} "wk")
    ({month})
    ({year} "yr" "y")
    ({decade})
    ({century})
    ({millennium})
    ({millisecond} "msec" "ms")
    ({microsecond} "usec" "us")
    ({nanosecond} "nsec" "ns")))

(new-unit-type "temperature"
  '(({degree kelvin} "K")
    ({degree celsius} "C")
    ({degree farenheit} "F")))

(new-unit-type "currency"
  '(({dollar} "$")
    ({cent})
    ({pound sterling})
    ({yen})
    ({franc})
    ({mark})))

(new-unit-type "information"
  '(({byte})
    ({bit})
    ({kilobyte} "Kbyte" "Kb")
    ({megabyte} "Mbyte" "Mb")
    ({gigabyte} "Gbyte" "Gb")
    ({terabyte} "Tbyte" "Tb")
    ({petabyte})))

(new-unit-type "frequency"
  '(({hertz} "Hz" "cycles per second" "cycles")
    ({kilohertz} "KHz" "kilocycles")
    ({megahertz} "MHz")
    ({gigahertz} "GHz")))

(new-unit-type "power"
  '(({watt} "W")
    ({kilowatt} "KW")
    ({megawatt} "MW")
    ({gigawatt})
    ({milliwatt})
    ({microwatt})
    ({nanowatt})))


;;; %%% We want to figure out the appropriate parent -- {time measure} or
;;; whatever -- automatically from the unit.

(defun new-measure (magnitude unit)
  "A convenience function to create a new measure, given a MAGNITUDE and
   a UNIT.  Optionally, supply some parent such as AREA-MEASURE."
  (setq unit (lookup-element-test unit))
  (setq magnitude (lookup-element-test magnitude))
  (let* ((name (make-element-iname
		:string
		(format Nil "Measure ~A ~A"
			(internal-name magnitude)
			(internal-name unit))))
	 (e (lookup-element-pred name :noun))
	 (quality (get-the-x-of-y {unit measurable quality} unit))
	 (measure-type
	  (x-is-the-y-of-what? quality {measure measurable quality})))
    (unless measure-type
      (error "Unknown measure type for ~S." unit))
    (unless (can-x-be-the-y-of-z? unit {common:measure unit} measure-type)
      (error "~S is cannot be the unit for ~S." unit measure-type))
    (or e
	(progn
	  (setq e (new-indv name measure-type))
	  (the-x-of-y-is-z {common:measure magnitude} e magnitude)
	  (the-x-of-y-is-z {common:measure unit} e unit)
	  e))))

;;; We use QUANTITY for a particular blob of some subtance.  It has a
;;; STUFF and a MEASURE, such as "sixteen tons of coal" or "ten
;;; minutes (of time)".

(new-type {quantity} {intangible})

(new-indv-role {quantity measure} {quantity}
	       {measure}
	       :english '(:role "measure"))

(new-indv-role {quantity stuff} {quantity}
	       {stuff}
	       :english '(:role "stuff"))

;;;; -------------------------------------------------------------------------
;;;; TIME MODEL

;;; Based loosely on the "KANI Time Ontology" by Richard Fikes and Selene
;;; Makarios, Stanford University Knowledge Systems Lab Tech Report KSL-04-05.
;;; URL: http://www.ksl.stanford.edu/KSL_Abstracts/KSL-04-05.html

;;; We need to refer both to points in time and to intervals of time.
(new-type {time point} {intangible}
	  :english '("point in time" "instant" "time"))

(new-type {time interval} {intangible}
	  :english '("time" "period"))

;;; A time interval is defined by two time points.
(new-indv-role {time interval start} {time interval}
	       {time point}
	       :english '(:role "starting time" "start time" "start"))

(new-indv-role {time interval finish} {time interval}
	       {time point}
	       :english '(:role "finish time" "finish"
				"ending time" "end time" "end"))

;;; A duration is an amount of time.
(new-type {duration} {time measure}
	  :english '("length of time" "amount of time"))

(new-indv-role {magnitude in seconds} {time measure} {number})

;;; A time unit is a duration.
(new-is-a {time unit} {duration})

;;; A time interval has a duration.
(new-indv-role {time interval duration} {time interval}
	       {duration}
	       :english '(:role "duration" "length"))

;;; Convenience function to create a time interval given two time-points.
(defun new-time-interval (tp1 tp2 &key (iname nil))
  "Create a new time interval with the specified start and end
   time-points and optional iname."
  (let ((ti (new-indv iname {time interval})))
    (x-is-the-y-of-z tp1 {time interval start} ti)
    (x-is-the-y-of-z tp2 {time interval end} ti)))

;;; Divide time-units into clock and calendar units.
(new-split-subtypes {time unit}
		    '({clock unit} {calendar unit}))

(new-is-a {hour} {clock unit})
(new-is-a {minute} {clock unit})
(new-is-a {second} {clock unit})

(new-is-a {year} {calendar unit})
(new-is-a {month} {calendar unit})
(new-is-a {week} {calendar unit})
(new-is-a {day} {calendar unit})

(the-x-of-y-is-z {magnitude in seconds} {nanosecond} {1/1000000000})
(the-x-of-y-is-z {magnitude in seconds} {microsecond} {1/1000000})
(the-x-of-y-is-z {magnitude in seconds} {millisecond} {1/1000})
(the-x-of-y-is-z {magnitude in seconds} {second} {1})
(the-x-of-y-is-z {magnitude in seconds} {minute} {60})
(the-x-of-y-is-z {magnitude in seconds} {hour} {3600})
(the-x-of-y-is-z {magnitude in seconds} {day} {86400})
(the-x-of-y-is-z {magnitude in seconds} {week} {604800})
;;; The rest are average magnitudes.
(the-x-of-y-is-z  {magnitude in seconds} {month} {2629744D0})
(the-x-of-y-is-z  {magnitude in seconds} {year} {31556926D0})
(the-x-of-y-is-z  {magnitude in seconds} {decade} {31556926D1})
(the-x-of-y-is-z  {magnitude in seconds} {century} {31556926D2})
(the-x-of-y-is-z  {magnitude in seconds} {millennium} {31556926D3})

;;; A time standard is a system for representation for time relative to a
;;; certain origin.
(new-type {time standard} {intangible})

;;; A time-zone is a time standard.
(new-type {time zone} {time standard})

(new-indv-role {time zone offset} {time zone} {number})

(defun define-time-zones (tz-list)
  "Takes a list of lists.  Each sublist defines a time zone.  It has four
   elements: a name which is expended into 'X Standard Time' and 'X
   Daylight Time', a numeric offset from GMT, an alternative English name
   for the primary time zone, and an alternative English name for the
   Daylight time zone."
  (dolist (x tz-list)
    (let* ((tz (new-indv
		(make-element-iname
		 :string (format nil "~A Standard Time" (nth 0 x)))
		{time zone}
		:english (list (nth 2 x))))	       
	   (dtz (new-indv
		 (make-element-iname
		  :string (format nil "~A Daylight Time" (nth 0 x)))
		 {time zone}
		 :english (list (nth 3 x)))))
      (x-is-the-y-of-z (new-number (nth 1 x)) {time zone offset} tz)
      (x-is-the-y-of-z (new-number (+ (nth 1 x) 1)) {time zone offset} dtz))))

;;; Just define a few time-zones for now.
(define-time-zones
    '(("Greenwich" 0 "GMT" "BST")
      ("Atlantic" -4 "AST" "ADT")
      ("Eastern" -5 "EST" "EDT")
      ("Central" -6 "CST" "CDT")
      ("Mountain" -7 "MST" "MDT")
      ("Pacific" -8 "PST" "PDT")
      ("Alaska" -9 "AKST" "AKDT")
      ("Hawaii-Aleutian" -10 "HST" "HDT")))

(english {Greenwich Standard Time} "Greenwich Mean Time" "UTC")
(english {Greenwich Daylight Time} "British Summer Time")


;;; A calendar interval is an interval that coincides with some division of
;;; the calendar.  This is relative to a time-standard. 

(new-type {calendar interval} {time interval})

(new-indv-role {calendar interval time standard}
	       {calendar interval}
	       {time standard})

(new-split-subtypes {calendar interval}
		    '({calendar year}
		      {calendar month}
		      {calendar day}
		      {calendar week}
		      {calender century}
		      {calender millennium}))

(defun define-cycle (sequence member-parent list)
  "Define a new cyclic SEQUENCE.  Takes a list of lists.  Each sublist
   defines a subtype under MEMBER-PARENT.  Each sublist has a name for the
   member element, followed by any number of alternative names.  In
   addition to defining these subtypes and adding the properties, we
   connect them in a loop via the {has-successor} relation."
  (setq member-parent (lookup-element-test member-parent))
  (let* ((seq (new-indv sequence {cyclic sequence}))
	 (elist nil)
	 (i 1))
    (dolist (x list)
      (let* ((e (new-type
		 (make-element-iname
		  :string (car x))
		 member-parent
		 :english (cddr x))))
	(x-is-a-y-of-z e {member} seq)
; %%%	(x-is-the-y-of-z (new-integer i) {sequence member index} e)
	(push e elist)
	(incf i)))
    ;; Elements are now on elist in reverse order.  Make the successor
    ;; relations.
;   (do ((el elist (cdr el)))
;	((null (cdr el)))
;     (the-x-of-y-is-a-z {successor} (cadr el) (car el)))
;   ;; Make the last one.
;   (the-x-of-y-is-a-z {successor} (car elist) (car (last elist)))
    seq))

(define-cycle {days in week}
	      {calendar day}
  '(("Sunday" "Sun" "SU")
    ("Monday" "Mon" "MO" "M")
    ("Tuesday" "Tue" "TU" "T")
    ("Wednesday" "Wed" "We" "W")
    ("Thursday" "Thu" "Th" "R")
    ("Friday" "Fri" "Fr" "F")
    ("Saturday" "Sat" "Sa")))

(define-cycle {months in year}
	      {calendar month}
  '(("January" "Jan")
    ("February" "Feb")
    ("March" "Mar")
    ("April" "Apr")
    ("May")
    ("June" "Jun")
    ("July" "Jul")
    ("August" "Aug")
    ("September" "Sep" "Sept")
    ("October" "Oct")
    ("November" "Nov")
    ("December" "Dec")))

(new-indv-role {days in month} {calendar month}
	       {number})

(the-x-of-y-is-z {days in month} {January}    {31})
(the-x-of-y-is-z {days in month} {February}   {30.4375})
(the-x-of-y-is-z {days in month} {March}      {31})
(the-x-of-y-is-z {days in month} {April}      {30})
(the-x-of-y-is-z {days in month} {May}        {31})
(the-x-of-y-is-z {days in month} {June}       {30})
(the-x-of-y-is-z {days in month} {July}       {31})
(the-x-of-y-is-z {days in month} {August}     {31})
(the-x-of-y-is-z {days in month} {September}  {30})
(the-x-of-y-is-z {days in month} {October}    {31})
(the-x-of-y-is-z {days in month} {November}   {30})
(the-x-of-y-is-z {days in month} {December}   {31})

(defun leap-year? (year)
  "Year is an integer.  If this is a leap year in the Gregorian calendar,
   return T.  If it is not, return NIL."
  (cond ((= 0 (mod year 400)) t)
	((= 0 (mod year 100)) nil)
	((= 0 (mod year 4)) t)
	(t nil)))

(defun compute-year-length (year)
  "YEAR should be an integer node or NIL.  Compute the length of the year
   in seconds.  For now, pretend that the Gregorian calendar goes back
   forever, though it wasn't adopted until the 1700's."
   (if (integer-node? year)
       (if (leap-year? (internal-name year))
	   (* 366 86400)
	   (* 365 86400))
       (* 365.2422 86400)))

(defun compute-month-length (month year)
  "Month should be an element naming a month.  Return its length in seconds.
   For February, we have to look at the year or return an average value."
  (* 86400
     (cond ((null month) 30.4375)
	   ((and (eq month (lookup-element {february})) year)
	    (if (leap-year? (internal-name year)) 29 28))
	   ((find-the-x-role-of-y month {days in month}))
	   (t 30.4375))))

(defun duration-magnitude-in-seconds (duration &key which-month which-year)
  "Given a DURATION or a time-unit, return the number of seconds in it.
   This will be an integer or ratio if possible, or a double-float if
   necessary.  For months and years, whose duration is variable, you can
   provide a specific month and year as keywords.  Else, you will get an
   average value.  MONTH should be an element such as {January} and YEAR should
   be an integer node such as {2005}."
  (setq duration (lookup-element-test duration))
  (when which-month (setq which-month (lookup-element-test which-month)))
  (when which-year (setq which-year (lookup-element-test which-year)))
  (let ((unit nil)
	(number nil)
	(factor nil))
    ;; Extract and check the unit and magnitude of the duration argument.
    (cond ((is-x-a-y? duration {time unit})
	   (setq unit duration)
	   (setq number 1))
	  ((is-x-a-y? duration {time measure})
	   (setq unit (get-the-x-role-of-y {measure unit} duration))
	   (setq number (get-the-x-role-of-y {measure magnitude} duration)))
	  (t (error "~S wrong type for duration." duration)))
    (unless (and (proper-indv-node? unit)
		 (is-x-a-y? unit {time unit}))
      (error "~S bad unit in duration." unit))
    (if (number-node? number)
	(setq number (internal-name number))
	;; Should never get to this error.
	(error "~S bad magnitude in duration." number))
    ;; Find the conversion factor for this unit.  Some are tricky.
    (setq factor
	  (cond ((find-the-x-role-of-y {magnitude in seconds} unit))
		((eq unit (lookup-element {year}))
		 (compute-year-length which-year))
		((eq unit (lookup-element {month}))
		 (compute-month-length which-month which-year))))
    ;; If factor is NIL here, we don't know the answer.  Return NIL.
    (if factor
	(* factor number)
	nil)))

;;; Time point roles and comparison.

;;; %%% Define absolute-time, era, year, month, day, hour, minute, second.

;;; %%% Define time-point-compare => :later-than, :earlier-than, :same-time-as, nil.



#|
(defun cotemporal (ti1 ti2)
  "Predicate.  Do time intervals T1 and T2 have the same start and end points?"
  %%%)


;;; Old.

;;; YEAR OF TIME is normally A.D.  Negative is B.C.  Because there's no year
;;; 0, it's not quite correct to just subtract.

(new-indv-role {year of time} {time}
	       {integer}
	       :english '(:role "year"))

;;; This is an integer 1-12.
(new-indv-role {month of time} {time}
	       {integer}
	       :english '(:role "month"))

;;; This is an integer 1-31.
(new-indv-role {day of time} {time}
	       {integer}
	       :english '(:role "day"))

;;; This is an integer 0-23.
(new-indv-role {hour of time} {time}
	       {integer}
	       :english '(:role "hour"))

;;; This is an integer 0-59.
(new-indv-role {minute of time} {time}
	       {integer}
	       :english '(:role "minute"))

;;; This is a number, >= 0, < 60.
(new-indv-role {second of time} {time}
	       {number}
	       :english '(:role "second"))

(defun make-time (year month day hour minute second)
  (let ((time-node
         (new-indv
          (format nil "~S/~S/~S ~S:~S:~S"
                  month day year hour minute second)
          {common:time})))
    (the-x-of-y-is-z {common:year of time} time-node year)
    (the-x-of-y-is-z {common:month of time} time-node month)
    (the-x-of-y-is-z {common:day of time} time-node day)
    (the-x-of-y-is-z {common:hour of time} time-node hour)
    (the-x-of-y-is-z {common:minute of time} time-node minute)
    (the-x-of-y-is-z {common:second of time} time-node second)
    time-node))

;;; Time periods

;;; These are like areas, whereas {time} is more like a point.

(new-type {time interval}
	  {stuff})

(new-type {length of time} {quantity})

(x-is-the-y-of-z  {time period} {stuff of quantity} {length of time})

(the-x-of-y-is-a-z {measure of quantity} {length of time} {time measure})



|#


;;;; -------------------------------------------------------------------------
;;;; SPACE MODEL

;;; "Place" is used in a few ways: geographical area, point in 3-space, etc. 

(new-type {place} {thing} 
	  :english '("place" "location" "position"))

(new-type {physical medium} {thing})

(new-split-subtypes {physical medium}
 '({land} {water} {air} {space}))


;;; A geographical-area is a 2-D region on earth.
;;; We'll worry about other planets later.

(new-type {geographical area} {place}
	  :english '("area" "location" "geographical area"))

;;; Define some relations over geographical areas.

(new-relation {area near}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
              :symmetric t
	      :english '(:relation "near"))

(new-relation {area adjacent to}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
              :symmetric t
	      :english '(:relation "adjacent to"))

(new-relation {area contains}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
	      :english '(:relation "contains"))

(new-relation {same area as}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
              :symmetric t)

(new-relation {greater area than}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
	      :english '(:relation "larger than"
				   "greater area than"))

(new-relation {area more populous than}
	      :a-inst-of {geographical area}
	      :b-inst-of {geographical area}
	      :english '(:relation "larger than"
				   "more populous than"))

;; EDITED FOR BLOCKS
(new-relation {located at}
	      :a-inst-of {physical object}
	      :b-inst-of {place})

;;;; -------------------------------------------------------------------------
;;;; GENERAL PHYSICS KNOWLEDGE

;;; Every physical object has a mass.
(new-indv-role {object mass} {physical object}
	       {mass measure}
	       :english '(:role "mass" "weight"))

;;; Every physical object also has some dimensions.
(new-indv-role {object height} {physical object}
	       {length measure}
	       :english '(:role "height"))

(new-indv-role {object length} {physical object}
	       {length measure}
	       :english '(:role "length"))

(new-indv-role {object width} {physical object}
	       {length measure}
	       :english '(:role "width" "breadth"))

(new-indv-role {object volume} {physical object}
	       {volume measure}
	       :english '(:role "volume"))

(new-indv-role {object area} {physical object}
	       {area measure}
	       :english '(:role "area" "footprint"))

;;;; -------------------------------------------------------------------------
;;;; SIMPLE BIOLOGY

(new-complete-split-subtypes
 {animate}
 '({animal} {plant}))

(new-complete-split-subtypes
 {animate}
 '(({mature} :adj "adult" "full-grown")
   ({immature} :adj)))

;;; Sex and Gender

(new-complete-split-subtypes
 {animate}
 '(({sexually reproducing} :adj)
   ({asexually reproducing} :adj)))

(new-complete-split-subtypes
 {sexually reproducing}
 '(({male} :adj-noun)
   ({female} :adj-noun)))

(new-type-role {parent} {sexually reproducing}
               {sexually reproducing}
	       :n 2)

(new-indv-role {mother} {sexually reproducing}
               {parent})

(new-is-a {mother} {female})

(new-indv-role {father} {sexually reproducing}
               {parent})

(new-is-a {father} {male})

(new-type-role {offspring} {sexually reproducing}
               {sexually reproducing}
	       :may-have t)

(new-type-role {son} {sexually reproducing}
               {offspring}
	       :may-have t)

(new-is-a {son} {male})

(new-type-role {daughter} {sexually reproducing}
               {offspring}
	       :may-have t)

(new-is-a {daughter} {female})


;;; Habitat

(new-complete-split-subtypes
 {animal}
 '(({flying} :adj)
   ({non-flying} :adj )))

(new-complete-split-subtypes
 {animal}
 '(({water-dwelling} :adj "aquatic")
   ({land-dwelling} :adj)
   ({amphibious} :adj)))

(new-complete-split-subtypes
 {animal}
 '(({air-breathing} :adj)
   ({water-breathing} :adj)))

(new-is-a {land-dwelling} {air-breathing})
(new-is-a {water-dwelling} {water-breathing})


;;; Taxonomy.

(new-complete-split-subtypes {animal}
  '({vertebrate} {invertebrate}))

(new-is-a {vertebrate} {sexually reproducing})

(new-complete-split-subtypes {vertebrate}
  '({fish} {amphibian} {reptile} {bird} {mammal}))

(new-is-a {fish} {water-dwelling})
(new-is-a {amphibian} {amphibious})
(new-is-a {reptile} {land-dwelling})
(new-is-a {bird} {land-dwelling})
(new-is-a {mammal} {land-dwelling})

(new-type {water-dwelling mammal} {mammal})
(new-is-not-a {water-dwelling mammal} {land-dwelling})
(new-is-a {water-dwelling mammal} {water-dwelling})
(new-is-not-a {water-dwelling mammal} {water-breathing})
(new-is-a {water-dwelling mammal} {air-breathing})

(new-is-a {fish} {non-flying})
(new-is-a {amphibian} {non-flying})
(new-is-a {reptile} {non-flying})
(new-is-a {bird} {flying})
(new-is-a {mammal} {non-flying})

;;; Define a few animal types, just as an example.
(new-split-subtypes {mammal}
  '({elephant} {lion} {tiger} {bat} {monkey} {person} {pig} {horse}
    {cow} {dog} {cat} {whale}))

(new-is-not-a {bat} {non-flying})
(new-is-a {bat} {flying})

(new-is-a {whale} {water-dwelling mammal})

(new-split-subtypes {bird}
  '({canary} {eagle} {crow} {penguin}))

(new-is-not-a {penguin} {flying})
(new-is-a {penguin} {non-flying})

(new-split-subtypes {fish}
  '({shark} {tuna} {perch} {goldfish} {flying-fish}))

(new-is-not-a {flying-fish} {non-flying})
(new-is-a {flying-fish} {flying})

;;; Diseases

(new-type {disease} {intangible})

(new-complete-split-subtypes
 {disease}
 '(({infectious} :adj)
   ({non-infectious} :adj)))

(new-indv-role {pathogen} {infectious}
	       {animate})

;;; Define a few diseases.
(new-members {infectious}
  '({influenza} {smallpox} {anthrax}))

(new-members {non-infectious}
  '({scurvy} {cystic fibrosis} {melanoma}))

(new-complete-split-subtypes
 {animate}
 '(({sick} :adj "ill" "diseased")
   ({healthy} :adj)))

(new-indv-role {affliction} {sick} {disease}
	       :english '(:role "malady"))

;;;; ----------------------------------------------------------------------
;;;; ASPECTS OF PERSON

(new-intersection-type {boy}
		       '({person} {male} {immature}))

(new-intersection-type {girl}
		       '({person} {female} {immature}))

(new-intersection-type {man}
		       '({person} {male} {mature}))

(new-intersection-type {woman}
		       '({person} {male} {mature}))

(new-indv-role {person full name} {person} {string}
	       :english '(:role "name" "full name"))

(new-indv-role {person title} {person}
	       {string}
	       :may-have t
	       :english '(:role "title" "honorific title"))

;;;; ----------------------------------------------------------------------
;;;; ORGANIZATIONS

;;; An organization is a set of people.
(new-type {organization} {set})

(the-x-of-y-is-a-z {member} {organization} {person})

;;; A "legal entity" is an organization or a person able to act as a
;;; person under the law.
(new-union-type {legal entity}
		'({organization} {person})
		:english '("legal person"))

;;; A few kinds of organization.
(new-split-subtypes {organization}
  '({school} {company} {government} {club} {military unit}))

;;; A few common kinds of school.
(new-split-subtypes {school}
		    '({university}
		      {community college}
		      {high school}
		      {middle school}
		      {grade school}
		      {kindergarten}
		      {pre-school}))

;;; A few universities.
(new-members
 {university}
 '(({Carnegie Mellon} "CMU" "Carnegie Mellon University")
   ({MIT} "Massachusetts Institute of Technology")
   ({Stanford} "Stanford University")
   ({Berkeley} "University of California at Berkeley" "Cal")
   ({Harvard} "Harvard University")
   ({Yale} "Yale University")
   ({Princeton} "Princeton University")
   ({Oxford} "Oxford University")
   ({Cambridge} "Cambridge University" "University of Cambridge")))
