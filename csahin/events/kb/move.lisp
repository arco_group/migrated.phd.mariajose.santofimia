;; Movement Knowledge Base - Requires gen-events.lisp
;; --------------------------------------------------

(in-context {general})
(in-namespace "events" :include "common")

(new-event-type {move}
 '({event})
 :roles
 ((:type {moving object} {physical object})
  (:indv {origin} {place} :english ("beginning" "start"))
  (:indv {destination} {place} :english ("end" "finish")))
 
 :throughout
 ((new-not-eq {origin} {destination}))
 
 :before
 ((new-statement {moving object} {located at} {origin}))
 
 :after
 ((new-not-statement {moving object} {located at} {origin})
  (new-statement {moving object} {located at} {destination})))

(new-event-type {move an object}
 '({move} {action})
 :roles
 ((:rename {agent} {mover})))

(new-event-type {travel}
 '({move} {action})
 :roles
 ((:rename {agent} {traveler}))
 :throughout
 ((x-is-a-y-of-z {traveler} {moving object} {travel})))

(new-event-type {travel on land}
 '({travel})
 :throughout
 ((the-x-of-y-is-a-z {origin} {travel on land} {land})
  (the-x-of-y-is-a-z {destination} {travel on land} {land})))

(new-event-type {travel in water}
 '({travel})
 :throughout
 ((the-x-of-y-is-a-z {origin} {travel in water} {water})
  (the-x-of-y-is-a-z {destination} {travel in water} {water})))

(new-event-type {travel in air}
 '({travel})
 :throughout
 ((the-x-of-y-is-a-z {origin} {travel in air} {air})
  (the-x-of-y-is-a-z {destination} {travel in air} {air})))

(new-event-type {travel in space}
 '({travel})
 :throughout
 ((the-x-of-y-is-a-z {origin} {travel in space} {space})
  (the-x-of-y-is-a-z {destination} {travel in space} {space})))

(new-event-type {travel by carrier}
 '({travel})
 :throughout
 ((new-type-role {travel carrier} {travel by carrier} {carrier})
  (x-is-a-y-of-z {travel carrier} {moving object} {travel by carrier})))

(new-event-type {travel by vehicle}
 '({travel by carrier})
 :roles
 ((:rename {travel carrier} {travel vehicle}))
 :throughout
 ((new-is-a {travel vehicle} {vehicle})))

(new-event-type {travel by animal}
 '({travel by carrier})
 :roles
 ((:rename {travel carrier} {travel animal}))
 :throughout
 ((new-is-a {travel animal} {mountable animal})))

(new-event-type {travel as passenger}
 '({travel by carrier})
 :throughout
 ((the-x-of-y-is-a-z {agent} {travel as passenger} {passenger})))

(new-event-type {travel as driver}
 '({travel by carrier})
 :english '("travel as pilot")
 :throughout
 ((the-x-of-y-is-a-z {agent} {travel as driver} {driver})))

(new-event-type {walk}
 '({travel on land})
 :roles
 ((:rename {agent} {walker}))
 :throughout
 ((new-is-a {walker} {legged animate})))

(new-event-type {run}
 '({travel on land})
 :roles
 ((:rename {agent} {runner}))
 :throughout
 ((new-is-a {runner} {legged animate})))

(new-event-type {travel by car}
 '({travel by vehicle} {travel on land})
 :throughout
 ((the-x-of-y-is-a-z {travel carrier} {travel by car} {car})))

(new-event-type {travel by bus}
 '({travel by vehicle} {travel on land})
 :throughout
 ((the-x-of-y-is-a-z {travel carrier} {travel by bus} {bus})))

(new-event-type {travel by taxi cab}
 '({travel by vehicle} {travel on land})
 :english '("travel by taxi" "travel by cab")
 :throughout
 ((the-x-of-y-is-a-z {travel carrier} {travel by taxi cab} {taxi cab})))

(new-event-type {travel by bike}
 '({travel by vehicle} {travel on land})
 :english '("bike" "ride a bike")
 :throughout
 ((the-x-of-y-is-a-z {travel carrier} {travel by bike} {bike})))

(new-event-type {travel by motorbike}
 '({travel by vehicle} {travel on land})
 :english '("bike" "ride a bike")
 :throughout
 ((the-x-of-y-is-a-z {travel carrier} {travel by motorbike} {motorbike})))

(new-event-type {travel by boat}
 '({travel by vehicle} {travel in water})
 :throughout
 ((the-x-of-y-is-a-z {travel carrier} {travel by boat} {boat})))

(new-event-type {travel by plane}
 '({travel by vehicle} {travel in air})
 :english '("fly")
 :throughout 
 ((the-x-of-y-is-a-z {travel carrier} {travel by plane} {airplane})))

(new-event-type {travel by plane as passenger}
 '({travel by plane} {travel as passenger})
 :english '("fly"))

(new-event-type {fly}
 '({travel in air})
 :roles
 ((:rename {agent} {flier}))
 :throughout
 ((new-is-a {flier} {flying animate})))

(new-event-type {swim}
 '({travel in water})
 :roles
 ((:rename {agent} {swimmer}))
 :throughout
 ((new-is-a {swimmer} {swimming animate})))
