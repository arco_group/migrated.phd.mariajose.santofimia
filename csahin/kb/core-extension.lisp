(new-type {potential agent} {thing})

(new-is-a {animate} {potential agent})
(new-is-a {organization} {potential agent})


(new-type {legged animate} {animate})
(new-type {winged animate} {animate})
(new-type {flying animate} {animate})
(new-type {swimming animate} {animate})

(new-type {mountable animal} {animal})
(new-is-a {elephant} {mountable animal})
(new-is-a {person} {mountable animal})
(new-is-a {horse} {mountable animal})

(new-type {vehicle} {man-made})
(new-type {engine powered vehicle} {vehicle})
(new-type-role {engine} {engine powered vehicle} {man-made})
(new-type {man powered vehicle} {vehicle})

(new-union-type {carrier}
 '({vehicle} {mountable animal})
 :parent {physical object})

(new-type {computer system} {potential agent})

(new-union-type {potential driver}
 '({animate} {computer system}) :parent {potential agent})

(new-indv-role {driver} {vehicle} {potential driver})
(new-type-role {passenger} {vehicle} {person} :may-have t)
(new-type-role {cargo} {vehicle} {physical object} :may-have t)

(new-type {air vehicle} {vehicle})
(new-type {land vehicle} {vehicle})
(new-type {water vehicle} {vehicle})
(new-type {space vehicle} {vehicle})
(new-intersection-type {amphibious vehicle}
 '({water vehicle} {land vehicle}))

(get-the-x-role-of-y {driver} {air vehicle} :iname {pilot})
(get-the-x-role-of-y {driver} {water vehicle} :iname {captain})
(get-the-x-role-of-y {driver} {space vehicle} :iname {commander})

;; Land vehicles
(new-split-subtypes {land vehicle}
 '({car} {truck} {bus} {bike} {motorbike}))
(new-type {taxi cab} {car})


;; Water Vehicles
(new-type {boat} {water vehicle})

(new-split-subtypes {boat}
 '({man powered boat} {wind powered boat} {engine powered boat}))

(new-is-a {man powered boat} {man powered vehicle})
(new-is-a {wind powered boat} {man powered vehicle})

(new-split-subtypes {man powered boat}
 '({rowing boat} {canoe} {kayak}))

(new-type {sail boat} {wind powered boat})


;; Air Vehicles
(new-split-subtypes {air vehicle}
 '({airplane} {helicopter} {glider}))



