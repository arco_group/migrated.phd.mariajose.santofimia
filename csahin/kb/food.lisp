(new-complete-split-subtypes {physical object}
 '(({edible} :adj)
   ({inedible} :adj)))

(new-split-subtypes {edible}
 '({food} {beverage}))

(new-split-subtypes {beverage}
 '({alcoholic beverage} {non-alcoholic beverage}))

(new-split-subtypes {alcoholic beverage}
 '({beer} {wine} {votka} {gin} {whiskey}))

(new-split-subtypes {non-alcoholic beverage}
 '({milk} {coca cola} {pepsi cola} {sprite} {seven up} {ginger ale})
 :iname {non-alcoholic split})

(add-to-split {non-alcoholic split} {water})
(new-is-a {water} {non-alcoholic beverage})

(new-split-subtypes {milk}
 '({whole milk} {skim milk}))

(new-split-subtypes {food}
 '({vegetable} {meat} {spice} {herb} {dairy} {oil} {grain}))

(new-split-subtypes {food}
 '({raw} {cooked}))

(new-split-subtypes {food}
 '({sliced} {diced} {whole} {chopped} {pureed} {minced} {grated} {ground}))

(new-split-subtypes {food}
 '({fruit} {non-fruit}))

(new-split-subtypes {grain}
 '({rice} {wheat} {rye} {oats} {barley} {wild rice}))

(new-split-subtypes {dairy}
 '({butter} {cheese} {yoghurt} {cream}))

(new-split-subtypes {butter}
 '({salted butter} {unsalted butter}))

(new-type {whipped cream} {cream})

(new-is-a {meat} {non-fruit})
(new-is-a {oil} {non-fruit})
(new-is-a {spice} {non-fruit})
(new-is-a {herb} {non-fruit})

(new-split-subtypes {vegetable}
 '({tomato} {mushroom} {pepper} {onion}
   {olive} {aubergine} {broccoli} {cucumber}
   {avacado} {green beans} {peas} {corn} {zucchini} {carrot}
   {potato} {cabbage} {chickpeas} {artichoke} {squash} {asparagus}
   {garlic} {leak} {celery} {pumpkin} {spinach}))

(new-is-a {tomato} {fruit})
(new-is-a {avacado} {fruit})
(new-is-a {cucumber} {fruit})
(new-is-a {aubergine} {fruit})
(new-is-a {olive} {fruit})
(new-is-a {pepper} {fruit})
(new-is-a {pumpkin} {fruit})

(new-split-subtypes {fruit}
 '({berry} {nut} {apple} {pear} {banana} {grape} {fig} {rosehip} {cherry}
   {star fruit} {watermelon} {melon} {mango} {passion fruit} {guava}
   {grapefruit} {orange} {mandarin} {peach} {apricot} {plum} {kiwi}
   {date} {pineapple} {pomegrenate} {lemon} {lime} {papaya} {prune}))

(new-split-subtypes {berry}
 '({strawberry} {blackberry} {blueberry} {raspberry} {cranberry} {currant} {elderberry} {gooseberry} {mulberry}))

(new-split-subtypes {meat}
 '({beef meat} {lamb meat} {pork meat} {poultry} {seafood}))

(new-split-subtypes {poultry}
 '({chicken} {duck} {turkey} {ostrich}))

(new-split-subtypes {seafood}
 '({fish meat} {scallop} {shrimp} {calamari} {caviar} {octopus} {clam}))

(new-split-subtypes {oil}
 '({vegetable oil} {animal fat}))

(new-split-subtypes {vegetable oil}
 '({canola oil} {coconut oil} {corn oil} {cottonseed oil}
   {olive oil} {margarine} {soybean oil} {sunflower oil}))

(new-type {lard} {animal fat})

(new-split-subtypes {spice}
 '({salt} {black pepper} {crushed red peppers} {curry} {cinnamon}))

(new-split-subtypes {herb}
 '({rosemary} {oregano} {parsley} {thyme} {basil}
   {cilantro} {chives} {dill} {mint} {sage}))

(new-split-subtypes {nut}
 '({hazelnut} {peanut} {pecan} {almond} {walnut} {pistachio} {pine nut} {chestnut} {cashew} {macademia}))

;; add the cheese
(load-kb "kb/cheese.lisp")

;;; Here we start inedible objects

(new-type {utensil} {inedible})
(new-type {container} {inedible})

(new-type-role {contents} {container} {physical object})
(new-indv-role {inside} {container} {place})
(new-statement {contents} {located at} {inside})

(new-complete-split-subtypes {place}
 '({empty} {full}))

(new-split-subtypes {container}
 '({appliance} {cookware}))

(new-split-subtypes {cookware}
 '({pan} {pot} {bowl} {plate} {glass} {measuring cup} {pasta drain}))

(new-split-subtypes {appliance}
 '({oven} {stovetop} {microwave} {blender} {toaster} {fridge} {freezer}))

(new-complete-split-subtypes {appliance}
 '({on appliance} {off appliance}))

(new-split-subtypes {utensil}
 '({knife} {cutting board} {fork} {spoon} {spatula} {turner} {ladle} {peeler} {can opener} {grater} {whisk} {handle mit}))
