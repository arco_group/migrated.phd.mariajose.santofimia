;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Scone Knowledge Representation System
;;; Bootstrap KB definitions.
;;;
;;; Author: Scott E. Fahlman
;;; ***************************************************************************
;;; Copyright (C) 2003-2005, Carnegie Mellon University.

;;; The Scone software and documentation are made available to the
;;; public under the CPL 1.0 Open Source license.  A copy of this
;;; license is distributed with the software.  The license can also be
;;; found at URL <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone
;;; software, you agree to be bound by the terms and conditions set
;;; forth in the CPL 1.0 Open Source License.  If you do not agree to
;;; these terms and conditions, or if they are not legally applicable
;;; in the jurisdiction where such use takes place, then you may not
;;; use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by
;;; Scott E. Fahlman for IBM Coporation between June 2001 and May
;;; 2003.  IBM holds the copyright on NETL2 and has made that software
;;; available to the author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under contract numbers NBCHC030029 and
;;; NBCHD030010.  Any opinions, findings and conclusions or recommendations
;;; expressed in this material are those of the author(s) and do not
;;; necessarily reflect the views of DARPA or the Department of
;;; Interior-National Business Center (DOI-NBC).
;;; ***************************************************************************

;;; Load this before any other KB files.

;;; This file contains the initial bootstrap structures for the Scone
;;; Knowledge Base.  This includes concepts such as {thing} and {set} that
;;; are essential to the proper working of the Scone engine.

;;; Some of the elements contained in this file are stored in global
;;; variables such as *thing* and *set* so that engine functions can easily
;;; refer to them.  These variables are declared in the engine file.

;;; This file necessarily contains some forward references.
(setq *defer-unknown-connections* t)

;;; Suppress error checkign for this file.
(setq *no-checking* t)

(in-namespace "common")

;;; Define the {has} relation.

;;; We have to put this first, since it is used even for {thing}.
;;; The dangling wires get fixed later.

(setq *has*
      (new-relation {has}
		    :a nil
		    :b nil
		    :c nil
		    :parent {relation}
		    :english '(:inverse-relation "exists for" "exists in")))

;;; This is the top node of the type hierarchy.  Everything is a {thing}.
;;; Note that the parent of {thing} is {thing} -- top of the tree.
(setq *thing*
      (new-type {thing} {thing}
		:context {universal}))

(new-split-subtypes {thing}
		    '(({tangible} :adj)
		      ({intangible} :adj))
		    :context {universal})

;;; Create the CONTEXT type.
(setq *context*
      (new-type {context} {intangible}
		:context {universal}))

;;; The universal context is always on. Statements in this context are
;;; always active, and entities in this context always exist.
(setq *universal*
      (new-indv {universal} {context}
		:context {universal}
		:english :adj-noun))

;;; This is the default context, representing actual reality.
(new-indv {general} {universal}
	  :context {universal}
	  :english :adj-noun)

;;; Parent node for relations.
(setq *relation*
      (new-type {relation} {intangible} :context {universal}))


;;; Numbers types.

(setq *number* (new-type {number} {intangible}
			 :context {universal}))

(new-complete-split-subtypes {number}
  '({exact number} {inexact number})
  :context {universal})

(setq *integer*
      (new-type {integer} {exact number}
		:context {universal}
		:english '("whole number" "counting number")))

(setq *ratio*
      (new-type {ratio} {exact number}
		:context {universal}
		:english '("fraction")))

(new-split '({integer} {ratio})
	   :context {universal})

(setq *float*
      (new-type {floating-point number} {inexact number}
		:context {universal}
		:english '("float"
			   :adj-noun
			   "real")))

;;; Strings.
(setq *string*
      (new-type {string} {intangible}
		:context {universal}))

;;; Functions.
(setq *function*
      (new-type {function} {intangible}
		:context {universal}))

;;; Lisp Defstructs.
(setq *struct*
      (new-type {struct} {intangible}
		:context {universal}))

;;; Parent nodes for built-in link types.
(new-type {link} {relation})

(setq *is-a-link*
      (new-type {is-a link} {link}))

(setq *is-not-a-link*
      (new-type {is-not-a link} {link}))

(setq *eq-link*
      (new-type {eq link} {link}))

(setq *not-eq-link*
      (new-type {not-eq link} {link}))

(setq *map-link*
      (new-type {map link} {link}))

(setq *not-map-link*
      (new-type {not-map link} {link}))

(setq *cancel-link*
      (new-type {cancel link} {link}))

;;; Fix some loose ends on {has}.
(connect-wire :a *has* (new-indv {A-role of has} *thing*
				 :context {universal}
				 :proper nil
				 :english :no-iname))

(connect-wire :b *has* (new-indv {B-role of has} *thing*
				 :context {universal}
				 :proper nil
				 :english :no-iname))

(connect-wire :c *has* (new-indv {cardinality-role of has} *integer*
				 :context {universal}
				 :proper nil
				 :english :no-iname))

;;; Define sets.
(setq *set*
      (new-type {set} {intangible}
		:context {universal}
		:english '("group"
			   "collection"
			   "bunch")))

;;; Useful contants.
(setq *zero* {0})
(setq *one* {1})
(setq *two* {2})

(process-deferred-connections)

;;; Set the context to {general}.
(in-context {general})

;;; Now we can add some role nodes.

;;; Almost any THING can have a set of parts.  The interpetation of PART
;;; may vary a bit between classes like TYPEWRITER, TEXAS, and THURSDAY,
;;; but all of these fall under this PART role.
(new-type-role {part} {thing} {thing}
	       :may-have t)

;;; Every set has a set of MEMBERs.
(new-type-role {member} {set} {thing})

;;; Every set has a cardinality, which is an integer.
(setq *cardinality*
      (new-indv-role {cardinality} {set} {integer}
		     :english '("number of members")))

;;; Define empty and non-empty sets.
(new-complete-split-subtypes
 {set}
 '({empty set}
   {non-empty set})
 :context {universal})

(setq *empty-set* (lookup-element {empty set}))

(setq *non-empty-set* (lookup-element {non-empty set}))

;;; Note the last element created by this file.
(setq *last-bootstrap-element* *last-element*)
