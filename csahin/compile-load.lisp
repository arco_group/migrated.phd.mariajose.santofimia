(compile-file "engine.lisp")
(load "engine")

;; Load general knowledge
(load-kb "kb/bootstrap.lisp")
(load-kb "kb/core-kb.lisp")
(commentary "~&Core knowledge loaded...~%")

(defvar *context-node* (lookup-element {context}))
(defvar *general-context* (lookup-element {general}))

(load-kb "events.lisp")

(compile-file "helper.lisp")
(load "helper")

;; Load general event functions
(compile-file "new-event.lisp")
(load "new-event")
(compile-file "compound.lisp")
(load "compound")

;; Load movement knowledge base
(load-kb "kb/core-extension.lisp")
(load-kb "kb/move.lisp")
(load-kb "kb/food.lisp")

(commentary "~&Events knowledge loaded...~%")

(in-context {common:general})
(in-namespace "events" :include "common")
(commentary "~&All knowledge loaded and read. Enjoy!~%")


