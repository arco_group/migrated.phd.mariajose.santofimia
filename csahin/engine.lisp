;;; -*- Mode:Lisp -*-
;;; ***************************************************************************
;;; Engine for the Scone Knowledge Representation System
;;;
;;; AUTHOR: Scott E. Fahlman <sef@cs.cmu.edu>
;;; ***************************************************************************

;;; Copyright (C) 2003-2006, Carnegie Mellon University.

;;; The Scone software is made available to the public under the CPL 1.0
;;; Open Source license.  A copy of this license is distributed with the
;;; software.  The license can also be found at URL
;;; <http://www.opensource.org/licenses/cpl.php>.

;;; By using, modifying, reproducing, or distibuting the Scone software,
;;; you agree to be bound by the terms and conditions set forth in the CPL
;;; 1.0 Open Source License.  If you do not agree to these terms and
;;; conditions, or if they are not legally applicable in the jurisdiction
;;; where such use takes place, then you may not use the Scone software.

;;; Scone incoporates some parts of the NETL2 system, developed by Scott E.
;;; Fahlman for IBM Coporation between June 2001 and May 2003.  IBM holds
;;; the copyright on NETL2 and has made that software available to the
;;; author under the CPL 1.0 Open Source license.

;;; Development of Scone has been supported in part by the Defense Advanced
;;; Research Projects Agency (DARPA) under contract number NBCHD030010.
;;; Any opinions, findings and conclusions or recommendations expressed in
;;; this material are those of the author(s) and do not necessarily reflect
;;; the views of DARPA or the Department of Interior-National Business
;;; Center (DOI-NBC).

;;; ***************************************************************************
;;; GENERAL STYLE NOTE: We want the runtime marker-scanning operations to
;;; be as fast as possible and to be non-consing.  So that code is
;;; optimized for performance first and for clarity second.  Code that
;;; builds or modifies the KB, or that does higher-level setup for marker
;;; ops, can be slower, so I have tried to optimize that for clarity and
;;; ease of maintenance..
;;; ***************************************************************************
;;; TABLE OF CONTENTS:

;;; Global Variables, Constants, and Macros
;;; Definition of the Scone Element Structure
;;; Low-Level Marker Operations
;;; Definition of the SCONE Element Types
;;; Marker Scans
;;; Queries and Predicates
;;; Mark, List, and Show Functions
;;; Roles, Relations, and Statements
;;; Cardinality Functions
;;; Internal Names and Namespaces
;;; English names
;;; Loading KB Files
;;; KB Checkpointing and Persistence
;;; Removing Elements from the KB
;;; Server Machinery


;;; ***************************************************************************
;;;     HERE BEGINS THE ACTUAL CODE
;;; ***************************************************************************

;;; ***************************************************************************
;;; Global Variables, Constants, and Macros
;;; ***************************************************************************

;;; This section contains global variables, constants, and macros used by
;;; the Scone engine.

;;; ========================================================================
;;; High-level control variables.

;;; Default pathname for various load-store operations.
;;; NOTE: Customize this for your own site.

(defvar *default-kb-pathname*
  (pathname "/home/mjsantof/tesis/CMU/Scone/csahin/events/anonymous.lisp")
  "Default location for text-format KB files.")

(defvar *reading-kb-file* nil
  "If reading a KB file, this wil be set.  Certain interactive dialogues
   are suppressed, etc.")

(defvar *loaded-files* nil
  "A list of files loaded in the current KB.  The head of the list is
   the file loaded most recently.")

(defvar *last-loaded-elements* nil
  "Every time we load a file, we push the value of the last loaded element
   onto this list.  The first element of the list is therefore the last
   element we loaded.  Any elements created after this in the element chain
   must be newly created.  The structure of this list parallels that of
   *LOADED-FILES*: for each file name in one list, we have its last element
   in the other list.")

(defvar *last-bootstrap-element* nil
  "Last element created by the BOOTSTRAP.LISP file.")

(defvar *verbose-loading* nil
  "If set, loading files will produce printout showing progress.  Notmally
   set by the :verbose keyword arg in various load functions.")

(defvar *defer-unknown-connections* nil
  "Normally when we see a reference to an unknown element, an error is
   signalled.  If this variable is set, save the known connection on the
   *deferred-connections* list and try to fix it later.  This allows for
   forward or circular references when these are necessary.")

(defvar *deferred-connections* nil
  "When loading a KB file, we may find references to elements not yet
   loaded or defined.  When we do, push a triplet onto this list and try to
   create the connection later.")

(defvar *no-checking* nil
  "If T, suppress most checking when creating KB elements.  Use this only
   when loading a file that you've loaded successfully before.")

(defvar *show-stream* *standard-output*
  "Stream to which we print the output of SHOW functions.  If NIL, don't
   print these at all.")

(defvar *commentary-stream* *standard-output*
  "Stream to which we print commentary about Scone's actions.  For example,
   if Scone makes a deduction and adds something to the KB as a result of 
   that, we note that here.  If NIL, don't print this stuff at all.")

(defun commentary (&rest args)
  "Just like FORMAT but with no STREAM arg.  Printing goes to the
   *COMMENTARY-STREAM*, if any."
  (when *commentary-stream*
    (apply #'format (cons *commentary-stream* args))))

(defvar *kb-logging-stream* nil
  "If non-nil, every KB alteration is reflected by an entry to this stream,
   so that the resulting file can be read back in with LOAD-KB.")

(defun kb-log (&rest args)
  "Syntax is just like FORMAT with no stream arg.  If *KB-LOGGING-STREAM*
   is non-null, execute the format call, writing the resulting string out
   to the KB logging stream."
  (when *kb-logging-stream*
    (apply #'format *kb-logging-stream* args)
    (force-output *kb-logging-stream*)))

;;; ========================================================================
;;; Element-Related Variables

(defvar *n-elements* 0
  "The number of elements currently in use in the KB.")
(declaim (type fixnum *n-elements*))

;;; There is a chain of pointers running through all the elements in the
;;; KB.  This starts at *FIRST-ELEMENT*.  Each element has a NEXT-ELEMENT
;;; slot pointing to the next element in the chain.  We keep hold of
;;; *LAST-ELEMENT* so that we can extend the chain at the end.

(defvar *first-element* nil
  "First element in the chain of all elements in the KB.")

(defvar *last-element* nil
  "Last element in the chain of all elements in the KB.")

;;; ========================================================================
;;; Essential Network Elements

;;; Variables to hold a few special network elements referred to by the
;;; engine code.  These are created in the BOOTSTRAP file and the elements,
;;; once created, are saved here.

;;; Nodes of general importance.

(defvar *thing* nil)
(defvar *context*)
(defvar *universal* nil)
(defvar *set* nil)
(defvar *empty-set* nil)
(defvar *non-empty-set* nil)
(defvar *cardinality* nil)
(defvar *zero* nil)
(defvar *one* nil)
(defvar *two* nil)
(defvar *has*)  

;;; Parent nodes for various built-in Scone element types.

(defvar *number* nil)
(defvar *integer* nil)
(defvar *ratio* nil)
(defvar *float* nil)
(defvar *string* nil)
(defvar *function* nil)
(defvar *struct* nil)
(defvar *relation* nil)
(defvar *is-a-link* nil) 
(defvar *is-not-a-link* nil)
(defvar *eq-link* nil)
(defvar *not-eq-link* nil)
(defvar *map-link* nil)
(defvar *not-map-link* nil)
(defvar *cancel-link* nil)
(defvar *split* nil)
(defvar *complete-split* nil)
(defvar *statement-link* nil)
(defvar *not-statement-link* nil)

;;; ========================================================================
;;; Variables and Macros Related to Marker Bits

;;; These macros are hacked for maximum spped in CMU Common Lisp.  Type
;;; inference is weaker than it should be, so these have lots of
;;; declarations to ensure that array access and other ops will be coded
;;; inline.

;;; The Scone system supports a fixed number of markers, designated by an
;;; integers greater than or equal to zero and less than N-MARKERS.

;;; Sense how many bits we have to work with in a fixnum, and use that as
;;; the number of marker bits.  THis varies from Lisp to Lisp, and of
;;; course will be very large in a 64-bit Common Lisp.

;;; NOTE: Allegro Common Lisp fails when we omit the EVAL-WHEN below.  I
;;; think that's a bug, but we'll just include this for maximum
;;; portability.

(eval-when (:execute :load-toplevel :compile-toplevel)
  (defconstant n-markers
    (logcount most-positive-fixnum)
    "The maximum number of marker bits allowed in the Scone engine.")
  (declaim (type fixnum n-markers)))

(defmacro legal-marker? (m)
  `(typep ,m '(integer 0 (,n-markers))))

(defconstant all-markers-mask (- (ash 1 n-markers) 1)
  "A bit mask with ones in every legal marker position.")
(declaim (type fixnum all-markers-mask))

(defmacro marker-bit (m)
  "Get the marker bit associated with marker M."
  `(the (unsigned-byte ,n-markers) (ash 1 (the (mod ,n-markers) ,m))))

(defmacro check-legal-marker (m)
  "In interactive routines, check if M is a legal marker.  If not, signal
   an error."
  `(unless (legal-marker? ,m)
     (error "~S is not a legal marker." ,m)))

;;; ---------------------------------------------------------------------
;;; Marker Pairs

;;; Markers are allocated in pairs.  For each marker we allocate, there is
;;; a corresponding cancel-marker.  So we divide the space of markers in
;;; half, rounding down.  Indices in the lower half designate markers (or
;;; marker pairs) available to the user, while indices in the upper half
;;; designate the corresponding cancel-markers.

(eval-when (:execute :load-toplevel :compile-toplevel)
  (defconstant n-marker-pairs (floor n-markers 2)
    "The maximum number of marker pairs.")
  (declaim (type fixnum n-markers)))

(defmacro cancel-marker (m)
  "Get the cancel-marker corresponding to marker M."
  `(the fixnum (+ (the fixnum ,m)
		  (the fixnum n-marker-pairs))))

(defmacro legal-marker-pair? (m)
  `(typep ,m '(integer 0 (,n-marker-pairs))))

(defmacro check-legal-marker-pair (m)
  "In interactive routines, check if M is a legal user marker.  If not,
   signal an error."
  `(unless (legal-marker-pair? ,m)
     (error "~S is not a legal user marker." ,m)))

;;; ---------------------------------------------------------------------
;;; Marker Predicates

(defmacro fast-marker-on? (e m)
  "Check whether element E has marker M turned on."
  `(/= 0 (logand (bits ,e) (marker-bit ,m))))

(defmacro fast-marker-off? (e m)
  "Check whether element E has marker M turned off."
  `(= 0 (logand (bits ,e) (marker-bit ,m))))

(defmacro all-bits-on? (bits mask)
  "Predicate tests whether all the bits in MASK are on in fixnum BITS."
  `(= 0 (the fixnum (logand (lognot (the fixnum ,bits))
			    (the fixnum ,mask)))))

(defmacro any-bits-on? (bits mask)
  "Predicate tests whether any of the bits in MASK are on in fixnum BITS."
  `(/= 0 (the fixnum (logand (the fixnum ,bits)
			     (the fixnum ,mask)))))

(defmacro all-bits-off? (bits mask)
  "Predicate tests whether all the bits in MASK are off in fixnum BITS."
  `(= 0 (the fixnum (logand (the fixnum ,bits)
			    (the fixnum ,mask)))))

;;; ---------------------------------------------------------------------
;;; Marker Allocation

;;; Note: Don't just grab a random marker and try to use it.  Call
;;; GET-MARKER and use whatever marker it gives you.  Keep it forever or
;;; use FREE-MARKER to return it when you're done.  If you use markers
;;; without telling the allocation machinery, you'll confuse things and may
;;; get subtle bugs.

;;; An array with a boolean value for each general marker pair.  T means
;;; the marker is available for allocation.  NIL means the marker is in
;;; use.

(defvar *available-markers*
  (make-array (list n-marker-pairs) :initial-element t))

(eval-when (:execute :load-toplevel :compile-toplevel)
  (proclaim `(type (simple-array t (,n-marker-pairs))
		   *available-markers*)))

(defvar *n-available-markers* n-marker-pairs
  "The number of markers currently available for allocation.")
(declaim (type fixnum *n-available-markers*))

(defvar *locked-markers*
  (make-array (list n-marker-pairs) :initial-element nil)
  "A vector with an entry for each marker.  If the entry is T, this
   marker is locked and cannot be released by FREE-MARKER.")

(eval-when (:execute :load-toplevel :compile-toplevel)
  (proclaim `(type (simple-array t (,n-marker-pairs))
		   *locked-markers*)))

(defun lock-marker (m)
  "A locked marker will not be released by FREE-MARKER."
  (setf (aref *locked-markers* m) t))

(defun unlock-marker (m)
  "Release the lock on marker M."
  (setf (aref *locked-markers* m) nil))

(defun locked-marker? (m)
  "Predicate to determine if marker M is locked."
  (svref *locked-markers* m))

;;; An array with a notation for each marker pair, indicating what the
;;; marker is currently being used for.

(defvar *marker-usage*
  (make-array (list n-marker-pairs) :initial-element nil))

(eval-when (:execute :load-toplevel :compile-toplevel)
  (proclaim `(type (simple-array t (,n-marker-pairs))
		   *marker-usage*)))

(defun set-marker-usage (m u)
  "Set the usage note for marker M to expression U."
  (setf (aref *marker-usage* m) u))

(defun available-marker? (m)
  "Predicate to check wether marker M is currently available."
  (and (legal-marker-pair? m)
       (aref *available-markers* m)))

(defun get-marker (&optional (purpose :unknown))
  "Allocate an available marker, returning the index.  If a PURPOSE is
   supplied, record it.  If no available markers remain, return NIL."
  (when (> *n-available-markers* 0)
    ;; Scan all the markers to find an available one.
    (dotimes (i n-marker-pairs)
      (when (available-marker? i)
	;; Marker I is the winner.
	(setf (svref *available-markers* i) nil)
	(decf *n-available-markers*)
	(set-marker-usage i purpose)
	(return-from get-marker i)))))

(defun free-marker (m)
  "Return Marker M to the pool of available markers and clear the marker.
   If the marker is already available or is locked, do nothing."
  (check-legal-marker-pair m)
  (clear-marker-pair m)
  (unless (or (available-marker? m) (locked-marker? m))
    (setf (aref *available-markers* m) t)
    (incf *n-available-markers*)
    (set-marker-usage m nil)))

(defun free-markers ()
  "Return all markers to the free pool and clear them."
  (dotimes (i n-marker-pairs)
    (free-marker i)))

(defmacro with-temp-markers ((&rest marker-vars) &body forms)
  "MARKER-VARS is a list of variables that are locally bound, each getting
   the index of a freshly allocated marker.  We then execute the the BODY
   forms and return whatever value or values the last form returns.  But
   before leaving this form, we de-allocate and clear the markers."
  (let ((bind-list nil)
	(free-list))
    ;; Prepare a let-list and a list of free-marker forms.
    (dolist (mv marker-vars)
      (push `(,mv (get-marker)) bind-list)
      (push `(free-marker ,mv) free-list))
    ;; Allocate the markers in the expected order.
    (setq bind-list (nreverse bind-list))
    ;; Now construct the macro-expansion.  Bind each variable to a
    ;; freshly-allocated marker, execute the body forms, and free the
    ;; markers on the way out, even if the user throws.
    `(let ,bind-list
       (unwind-protect
	   (progn ,@forms)
	 (progn ,@free-list)))))

(defmacro with-temp-marker ((marker-var) &body forms)
  "Singular versions of WITH-TEMP-MARKERS."
  `(with-temp-markers (,marker-var) ,@forms))

;;; If running under the Hemlock editor, indent WITH-TEMP-MARKERS and
;;; WITH-TEMP-MARKER like DO-LIST.

#+hemlock (hemlock::defindent "with-temp-markers" 1)
#+hemlock (hemlock::defindent "with-temp-marker" 1)

(defmacro not-enough-markers (n comment)
  "If we have fewer than N markers available, print the COMMENT and return
   T.  Else return NIL."
  `(when (< (the fixnum *n-available-markers*) ,n)
     (commentary ,comment)
     t))

;;; ---------------------------------------------------------------------
;;; Context variable and context marker.

;;; The variable *CONTEXT* holds the currently-active context node.  The
;;; CONTEXT-MARKER is placed on this node and upscanned whenever we change
;;; *CONTEXT*.

;;; NOTE: In earlier versions of Scone, a specific CONTEXT-MARKER was
;;; reserved, with the choice of marker hard-wired in the code.  Now we use
;;; GET-MARKER to allocated a context marker if IN-CONTEXT is called and no
;;; CONTEXT-MARKER is in use.

(defvar *context* nil
  "The element representing the current context.")

;;; CONTEXT-MARKER marks the current context and all its superiors.

(defvar *context-marker* -1
  "Used to mark the current *CONTEXT* node and its superiors.  If negative,
   no marker is currently assigned.")
(declaim (type fixnum *context-marker*))

(defvar *context-mask* 0
  "Bit mask for CONTEXT-MARKER.")
(declaim (type fixnum *context-mask*))

(defvar *context-cancel-marker* -1
  "Cancellation marker corresponding to CONTEXT-MARKER.")
(declaim (type fixnum *context-cancel-marker*))

(defvar *context-cancel-mask* 0
  "Bit mask for CONTEXT-CANCEL-MARKER.")
(declaim (type fixnum *context-cancel-mask*))

(defun in-context (e)
  "E is an element representing a context.  Activate it as the current
   context.  If there is currently no *context-marker*, assign one."
  (when (minusp *context-marker*)
    (setq *context-marker* (get-marker :context-marker))
    (commentary "Assigning ~D as context marker." *context-marker*)
    (setq *context-mask* (marker-bit *context-marker*))
    (setq *context-cancel-marker* (cancel-marker *context-marker*))
    (setq *context-cancel-mask* (marker-bit *context-cancel-marker*))
    (lock-marker *context-marker*))
  ;; Now activate context E.
  (setq e (lookup-element-test e))
  (clear-all-markers)
  (upscan e *context-marker* :ignore-context t)
  (setq *context* e)
  e)

;;; ---------------------------------------------------------------------
;;; Marker Implementation

;;; For each marker, there is a doubly-linked list running through all the
;;; elements.

;;; NOTE: This is a tradeoff of space for time.  Many scans want to do some
;;; operation for "all elements with marker N".  We want to avoid scanning
;;; ALL elements, so we need a list for each marker.  But we also want to
;;; avoid allocating list cells that we will have to GC later.  This means
;;; that every element has space pre-allocated for all these chains.  Store
;;; the start and end of the chain here, along with the count of marked
;;; elements.

(defvar *first-marked-element*
  (make-array n-markers :initial-element nil)
  "A vector with an entry for each marker.  This is the first element in
   the chain of elements having that mark.")

(eval-when (:execute :load-toplevel :compile-toplevel)
  (proclaim `(type (simple-vector ,n-markers) *first-marked-element*)))

(defvar *last-marked-element*
  (make-array n-markers :initial-element nil)
  "A vector with an entry for each marker.  This is the last element in the
   chain of elements having that mark.")

(eval-when (:execute :load-toplevel :compile-toplevel)
  (proclaim `(type (simple-vector ,n-markers) *last-marked-element*)))

(defvar *n-marked-elements*
  (make-array (list n-markers) :element-type 'fixnum :initial-element 0))

(eval-when (:execute :load-toplevel :compile-toplevel)
  (proclaim `(type (simple-array fixnum (,n-markers)) *n-marked-elements*)))

(defmacro fast-marker-count (m)
  "Return the number of elements marked with marker M."
  `(aref (the (simple-array fixnum (,n-markers)) 
	      *n-marked-elements*)
	 ,m))

(defmacro inc-marker-count (m)
  "Increment the number of elements marked with marker M."
  `(incf (the fixnum (aref (the (simple-array fixnum (,n-markers)) 
				*n-marked-elements*)
			   ,m))))

(defmacro dec-marker-count (m)
  "Decrement the number of elements marked with marker M."
  `(decf (the fixnum (aref (the (simple-array fixnum (,n-markers)) 
				*n-marked-elements*)
			   ,m))))

(defmacro zero-marker-count (m)
  "Set to zero the number of elements marked with marker M."
  `(setf (aref (the (simple-array fixnum (,n-markers)) 
		    *n-marked-elements*)
	       ,m)
	 0))

(defmacro do-marked ((var
		      m
		      &optional
		      after)
		     &body body)
  "This macro iterates over the set of elements with marker M.  Each
   element in turn is bound to VAR and then the body is executed, returning
   NIL.  If AFTER is present and non-nil, it should evaluate into an
   element marked with M.  Only iterate over elements after this one in the
   chain of M-marked elements.  NOTE: It is OK to mark new elements with M
   in the body of this loop, but don't unmark any elements."
  `(do ((,var
	 ,(if after
	      `(if ,after
		   (svref (next-marked-element ,after) ,m)
		   (svref *first-marked-element* ,m))
	      `(svref *first-marked-element* ,m))
	 (svref (next-marked-element ,var) ,m)))
       ((null ,var))
     ,@body))

;;; If running under the Hemlock editor, indent DO-MARKED like DOLIST.
#+hemlock (hemlock::defindent "do-marked" 1)

;;; ========================================================================
;;; Definitions Related to Flag Bits

;;; A flag is like a permanent marker-bit on an element.  Each flag-bit is
;;; assigned to some system-defined purpose -- these are not meant to be
;;; user-defined.

;;; There is a word full of flag-bits in each element, but we do not
;;; maintain a linked list of elements with a given flag.  This is a
;;; difference between markers and flags.

;;; One important use of flag bits is to record the type of each element in
;;; a way that can quickly be tested by boolean-mask operations.

;;; IMPLEMENTATION NOTE: For maximum speed in marker-scans we would store
;;; the flag-bits and the marker-bits in a single-word integer (or Common
;;; Lisp "fixnum").  This would allow us to check the type of an element
;;; and the presence of certain markers in a single boolean test.  This
;;; might be possible in CMU CL, which has true 32-bit fixnums, but some
;;; commercial Common Lisp systems have as few as 24-bit fixnums, reserving
;;; the rest of the word for headers.  So for now, we will use one fixnum
;;; for markers and a separate fixnum for flag bits.  Revisit this decision
;;; if we ever port Scone to Common Lisp on a 64-bit machine with longer
;;; fixnums.

(defmacro new-flags (start &rest names)
  "Allocate a number of flags in order, starting with START."
  (let ((list nil))
    (do ((n start (1+ n))
	 (l names (cdr l)))
	((null l)
	 (cons 'progn (nreverse list)))
      (push `(defconstant ,(car l) ,(ash 1 n)) list))))

;;; Simple flag macros and functions.

(defmacro fast-flag-on? (e f)
  "Check whether element E has flag F turned on."
  `(/= 0 (logand (flags ,e) ,f)))

(defmacro fast-flag-off? (e f)
  "Check whether element E has flag F turned off."
  `(= 0 (logand (flags ,e) ,f)))

;;; Define flags for the basic element types and modifiers.

(new-flags
 0
 
 ;; Basic families.
 node-flag	        ; An indv or type node.
 link-flag		; Any kind of link.
 split-flag		; A split-element, maybe complete.
 relation-flag		; A relation-element.
 
 ;; Subtypes and modifiers of a NODE.
 indv-flag		; Any kind of individual node.
 type-flag		; A type-node.
 
 primitive-flag		; An indv representing a CL object.
 proper-flag		; A proper individual, not generic.
 defined-flag		; This node has a definition.
 
 ;; Subtypes and modfiers of a LINK or RELATION.
 is-a-flag		; An IS-A link.
 eq-flag		; An EQ link.
 map-flag               ; A MAP-link, connecting a role-player to a role.
 cancel-flag		; A CANCEL link.
 statement-flag		; A link instantiating a user-defined relation.
 
 not-flag		; Set on a link to indicate negation.
 true-flag		; Set on a link tht is not negated.
 up-flag		; Any link that markers cross A to B in an upscan.
 h-flag			; A relation or statement.
 
 transitive-flag	; A transitive relation.
 symmetric-flag		; A smmetric relation.
 
 ;; Subtypes and modifiers of a SPLIT.
 complete-split-flag)	; A complete split.


;;; The UP-MASK designates any link that propagates markers A-to-B in an UPSCAN.
(defconstant up-mask (logior link-flag true-flag up-flag))

;;; The CANCEL-UP-MASK designates any link that propagates cancel markers
;;; A-to-B in an UPSCAN.
(defconstant cancel-up-mask (logior link-flag not-flag up-flag))

;;; The H-MASK designates a STATEMENT or RELATION -- basically, any link
;;; type that propagates markers from the A, B, or C node to the
;;; corresponding node of the parent -- an H-shaped inheritance pattern.
(defconstant h-mask h-flag)

;;; ========================================================================
;;; Internal Name Variables

(defvar *namespace* nil
  "The structure representing the current default namespace.")

(defvar *namespaces* (make-hash-table :test #'equal)
  "A hash table containing an entry for each namespace currently loaded in
   the KB.  Associates a string (the name of the namespace) with a
   namespace structure.")

(defvar *number-hashtable*
  (make-hash-table :test #'eql)
  "A hashtable associating numbers (integer, ratio, or float) with existing
   elements.")

(defvar *string-hashtable*
  (make-hash-table :test #'equal)
  "A hashtable associating strings with existing string-nodes.  Lookup is
   case-sensitive")

(defvar *function-hashtable*
  (make-hash-table :test #'eq)
  "A hashtable associating Common Lisp functions with existing
   function-nodes.  This works only for named functions, as in #'foo.  It
   does not work for lambda expressions, even if they are equal.")

(defvar *struct-hashtable*
  (make-hash-table :test #'eq)
  "A hashtable associating Common Lisp desfstruct objects with existing
   primitive elements.")

(defvar *iname-counter* 0
  "Counter used by GEN-INAME to create unique internal names for elements.
   Increment this counter each time it is used.")

(defvar *iname-prefix* 0
  "Increment this to start a whole new series of unique internal inames.")
  

;;; ========================================================================
;;; English Name Variables and Declarations

(defvar *english-dictionary*
  (make-hash-table :test #'equal)
  "Hash table associating English words and phrases (strings in Lisp) with
   Scone elements.  A given string may be associated with many elements, so
   the hash-table value is a list if it exists at all.  Each element in the
   list is a pair (element . syntax-tag).")

(defvar *legal-syntax-tags*
  '(:noun :adj :adj-noun :role :inverse-role :relation :inverse-relation
	  :action)
  "Each external name has an associated syntax-tag.  The tags currently
   defined are stored in this constant.")


;;; ***************************************************************************
;;; Definition of the SCONE Element Structure
;;; ***************************************************************************

;;; An element is the basic unit of knowledge in the Scone system.  An
;;; element is either a NODE, representing some individual, type, or
;;; entity, a LINK, representing the statement of a relation between two or
;;; more entities.  But a link has a built-in "handle node" representing
;;; the statement itself, and a node may have some "wires" that represent
;;; certain special relations with other nodes.  So every element has both
;;; a set of outgoing wires (PARENT, CONTEXT, A, B, and C) plus connection
;;; points for any number of incoming wires.

;;; NOTE: Speed in the inner loops is very important.  The natural thing,
;;; would be to implement ELEMENT as a CLOS class and all of the various
;;; element types as sub-classes.  In principle, the CLOS machinery in
;;; Common Lisp should be able to implement that very efficiently.
;;; However, CLOS implementations vary a lot and are inefficient in some
;;; Common Lisp implementations.  So for now we simply use use the same
;;; structure (a Common Lisp DEFSTRUCT) for every element, and use the flag
;;; bits to distinguish the element's type.

;;; NOTE: We could reorganize this to use separate subtypes of element for
;;; nodes and links, saving some space for the simpler node elements.  But
;;; the savings is small and this might actually slow down some of the
;;; scans.  Sometime I should run an experiment to explore the time/space
;;; tradeoff here, but a single element type will do for now.

;;; If defstruct itself is compiled at speed 3 we get spurious efficiency notes.
(declaim (optimize (speed 1) (space 1) (safety 1)))

(defstruct (element
	    (:constructor internal-make-element)
	    (:conc-name nil)
	    (:print-function  print-element))
  ;; Each element has an integer whose bits indicate the current markers on
  ;; that element, and another integer holding the flag bits.
  (bits 0 :type fixnum)
  (flags 0 :type fixnum)
  ;; A vector with an entry for each marker. The marker's entry in this
  ;; vector points to the next marked element in the chain.
  (next-marked-element (make-array n-markers :initial-element nil))
  ;; A vector with an entry for each marker.  This points to previous
  ;; marked element in the chain.
  (prev-marked-element (make-array n-markers :initial-element nil))
  ;; Outging connections (known as "wires").
  (parent-wire nil)
  (context-wire nil)
  (a-wire nil)
  (b-wire nil)
  (c-wire nil)
  (split-wires nil)
  ;; Markers can move across wires in either direction, so we need to keep
  ;; back-pointers.  These are the wires of each type that are attached to
  ;; this element.
  (incoming-parent-wires nil)
  (incoming-context-wires nil)
  (incoming-a-wires nil)
  (incoming-b-wires nil)
  (incoming-c-wires nil)
  (incoming-split-wires nil)
  ;; There is a chain of pointers running through all the elements so that
  ;; you can scan them all.
  (next-element nil)
  ;; If this element is a defined type, the definition lives here.  It is a
  ;; list, all of whose elements must be true. Elements may be
  ;; type-membership, a relation, or a funciton of one variable.
  (definition nil)
  ;; When you add a new member of the class represented by this element,
  ;; run the functions on this list, or consider membership in the defined
  ;; types on the list.
  (if-added nil)
  ;; The INTERNAL-NAME and NAMESPACE together form an unambiguous,
  ;; permanent, and globally unique identifier for a SCONE element.
  (internal-name nil)
  (namespace nil)
  ;; A property list for various propeties and annotations that some
  ;; elements have, but not all, and that are not critical to system
  ;; performance.
  (properties nil))


;;; This is the element-iname structure -- the thing represented by a name
;;; withing curly braces.  It is described in the "INTERNAL NAMES AND
;;; NAMESPACES" section, but we put the definition here for proper
;;; compilation.

(defstruct (element-iname
	    (:print-function print-element-iname))
  ;; A string that is the name of the element.
  (string))


;;; The operations beyond this point must be as fast as possible.
(declaim (optimize (speed 3) (space 0) (safety 0)))

(defun make-element (flags iname parent context a b c 
			   english english-default hashtable definition
			   properties)
  "Common code creates and returns an element, after doing all the
   necessary bookkeeping.  This is called by the element-creator for each
   specific Scone element-type.  A unique INAME must be supplied.  If
   HASHTABLE is supplied, register the name in this hashtable instead of
   the default namespace.  The ENGLISH and ENGLISH-DEFAULT arguments
   control english names for the new element."
  ;; Prevent duplicate or ill-formed inames.  Skip this check for primitive
  ;; elements with their own hashtable.
  (unless hashtable
    (if (typep iname 'element-iname)
	(when (lookup-element-pred iname)
	  (error "The iname ~S is already in use." iname))
	(error "~S must be an element-iname structure." iname)))
  ;; Create and set up the element.
  (let ((e (internal-make-element :flags flags)))
    ;; Set and register the iname.
    (cond (hashtable
	   ;; If :HASHTABLE was supplied, this type of element has its own
	   ;; hastable, not a true namespace.
	   (setf (gethash iname hashtable) e)
	   (setf (internal-name e) iname))
	  (t
	   ;; Register in specified namespace or default to current one.
	   (register-internal-name iname e)))
    ;; Tie new element into the chain of all elements.
    (if *last-element*
	(setf (next-element *last-element*) e))
    (setq *last-element* e)      
    (if (null *first-element*)
	(setq *first-element* e))
    ;; Tick the element counter.
    (incf (the fixnum *n-elements*))
    ;; Process all the outoging wire connections.
    (when parent (connect-wire :parent e parent t t))
    (when context (connect-wire :context e context t t))
    (when a (connect-wire :a e a t t))
    (when b (connect-wire :b e b t t))
    (when c (connect-wire :c e c t t))
    ;; Process English names, if any.
    (unless (listp english)
      (setq english (list english)))
    (cond ((or (null english-default)
	       (eq english-default :no-iname))
	   ;; No default, just pass the :ENGLISH arg, if any, to ENGLISH.
	   (when english
	     (english-internal e english)))
	  ((and english (member (car english) *legal-syntax-tags* :test #'eq))
	   (english-internal e (cons (car english)
				     (cons :iname
					   (cdr english)))))
	  (t (english-internal e (cons english-default
				       (cons :iname english)))))
    ;; If there is a definition, record it and make sure the defined-flag
    ;; is set.
    (when definition
      (set-definition e definition t))
    ;;; If there is a :PLIST argument, that becomes the property list.
    (when properties
      (set-properties e properties t))
    ;;; Log the creation of a new element.
    (kb-log "(make-element ~S ~S ~S ~S ~S ~S ~S '~S '~S '~S '~S '~S)~%"
	    flags iname parent context a b c english english-default
	    hashtable definition properties)
    e))

(defun set-flag (e flag &optional (already-logged nil))
  "Set the specified FLAG (a bit mask) in element E.  Log this KB change
   unless it was done already."
  (declare (fixnum flag))
  (prog1
      (setf (flags e)
	    (logior (flags e) flag))
    (unless already-logged
      (kb-log "(set-flag ~S '~S)~%" e flag))))

(defun clear-flag (e flag &optional (already-logged nil))
  "Clear the specified FLAG (a bit mask) in element E.  Log this KB change
   unless it was done already."
  (declare (fixnum flag))
  (prog1
      (setf (flags e)
	    (logand (flags e) (lognot flag)))
    (unless already-logged
      (kb-log "(clear-flag ~S '~S)~%" e flag))))

(defun set-definition (e definition &optional (already-logged nil))
  "Add DEFINITION to element E, setting the defined-flag of E.  Log the KB
   change unless this has already been done."
  (setq e (lookup-element-test e))
  (setf (definition e) definition)
  (set-flag e defined-flag t)
  (unless already-logged
    (kb-log "(set-definition ~S '~S)~%" e definition)))

(defun set-properties (e plist &optional (already-logged nil))
  "Set the property list of element E to PLIST.  Log the KB change unless
   this has already been done."
  (setq e (lookup-element-test e))
  (setf (properties e) plist)
  (unless already-logged
    (kb-log "(set-properties ~S '~S)~%" e plist)))

(defmacro do-elements ((var &optional (after nil)) &body body)
  "This macro iterates over the set of all elements.  Each element in turn
   is bound to VAR and then the body is executed.  Otherwise return NIL.
   If AFTER is supplied, it is an element.  Execute the body only for
   elements created after AFTER."
  `(do ((,var
	 ,(if after
	      `(if ,after
		   (next-element ,after)
		   *first-element*)
	      `*first-element*)
	 (next-element ,var)))
       ((null ,var))
     ,@body))

;;; If running under the Hemlock editor, indent DO-ELEMENTS like DOLIST.
#+hemlock (hemlock::defindent "do-elements" 1)

(defun find-next-element (e)
  "Given an element, get the next one in the chain."
  (setq e (lookup-element-test e))
  (next-element e))

(defun find-previous-element (e)
  "Given an element, get the previous element in the chain.  NOTE: this is
   inefficient, since we don't keep back-pointers in this chain.  Used in
   infrequent tasks such as removing an element from the KB."
  (setq e (lookup-element-test e))
  (do ((x *first-element* y)
       (y nil))
      ((null x) nil)
    (setq y (next-element x))
    (when (eq y e) (return x))))


;;; ------------------------------------------------------------------------
;;; Element Properties

;;; NOTE: Use a property rather than a permanently allocated element slot
;;; if the property is not present in all elements and if the property is
;;; not used in operations that must run at maximum speed.

(defmacro get-element-property (e property)
  "Get the specified PROPERTY of element E, or NIL if this property is
   not present."
  `(getf (properties ,e) ,property))

(defun set-element-property (e property &optional (value t)
			       (already-logged nil))
  "Set the specified PROPERTY of element E to the designated VALUE,
   which defaults to T.  Log this KB change unless it was done already."
  (prog1
      (setf (getf (properties e) property) value)
    (unless already-logged
      (kb-log "(set-element-property ~S '~S '~S)~%" e property value))))

(defun clear-element-property (e property &optional (already-logged nil))
  "Remove the specified PROPERTY of element E.  If E doesn't ahve this property,
   do nothing.  Log this KB change unless it was done already."
  (prog1
      (remf (properties e) property)
    (unless already-logged
      (kb-log "(clear-element-property ~S '~S)~%" e property))))

(defun push-element-property (e property value &optional (already-logged nil))
  "The PROPERTY of E should be a list.  Push VALUE onto this list.  Create
   the property if it doesn't already exist.  Log this KB change
   unless it was done already."
  (prog1 
      (push value (getf (properties e) property))
    (unless already-logged
      (kb-log "(push-element-property ~S '~S '~S)~%" e property value))))


;;; ========================================================================
;;; Accessing, Connecting, and Disconnecting Wires

(defun find-wire (wire e)
  "Find the element attached to the specified wire, one of :A, :B, :C,
   :PARENT, :CONTEXT, or :SPLIT.  For :SPLIT wires, return a list."
  (setq e (lookup-element-test e))
  (ecase wire
    (:a (a-wire e))
    (:b (b-wire e))
    (:c (c-wire e))
    (:parent (parent-wire e))
    (:context (context-wire e))
    (:split (split-wires e))))

(defun find-incoming-wires (wire e)
  "Find the list of incoming elements via the specified wire, one of :A,
   :B, :C, :PARENT, :CONTEXT, or :SPLIT."
  (setq e (lookup-element-test e))
  (ecase wire
    (:a (incoming-a-wires e))
    (:b (incoming-b-wires e))
    (:c (incoming-c-wires e))
    (:parent (incoming-parent-wires e))
    (:context (incoming-context-wires e))))

;;; Use normal compilation policy, where safety counts as much as speed.
(declaim (optimize (speed 1) (space 1) (safety 1)))

(defun connect-wire (wire from to &optional (may-defer nil)
			  (already-logged nil))
  "Connect the named WIRE (:A, :PARENT, etc.) of element FROM to element
   TO.  If FROM is already connected somewhere, disconnect it first.  Log
   the KB change if it is not already logged."
  (setq from (lookup-element-test from))
  (setq to (lookup-element to))
  (cond ((typep to 'element)
	 (when (and (find-wire wire from)
		    (not (eq wire :split)))
	   (disconnect-wire wire from t))
	 (ecase wire
	   (:a (setf (a-wire from) to)
	       (push from (incoming-a-wires to)))
	   (:b (setf (b-wire from) to)
	       (push from (incoming-b-wires to)))
	   (:c (setf (c-wire from) to)
	       (push from (incoming-c-wires to)))
	   (:parent (setf (parent-wire from) to)
		    (push from (incoming-parent-wires to)))
	   (:context (setf (context-wire from) to)
		     (push from (incoming-context-wires to)))
	   (:split (push to (split-wires from))
		   (push from (incoming-split-wires to))))
	 ;; Log the new connection.
	 (unless already-logged
	   (kb-log "(connect-wire ~S ~S ~S ~S)~%" wire from to may-defer)))
	((and may-defer (typep to '(or element-iname string)))
	 (defer-connection wire from to))
	(t (error "~S is not an element." to))))

(defun disconnect-wire (wire from &optional (already-logged nil))
  "Disconnect the specified WIRE (:A, :PARENT, etc.)  of element FROM from
   wherever it is currently connected to.  If it is not connected, do
   nothing.  Log the KR change if it is not already logged."
  (setq from (lookup-element-test from))
  (let ((to (find-wire wire from)))
    (when to
      (ecase wire
	(:a (setf (a-wire from) nil)
	    (setf (incoming-a-wires to)
		  (delete from (incoming-a-wires to))))
	(:b (setf (b-wire from) nil)
	    (setf (incoming-b-wires to)
		  (delete from (incoming-b-wires to))))
	(:c (setf (c-wire from) nil)
	    (setf (incoming-c-wires to)
		  (delete from (incoming-c-wires to))))
	(:parent (setf (parent-wire from) nil)
		 (setf (incoming-parent-wires to)
		       (delete from (incoming-parent-wires to))))
	(:context (setf (context-wire from) nil)
		  (setf (incoming-context-wires to)
			(delete from (incoming-context-wires to)))))
      ;; Log the new connection.
      (unless already-logged
	(kb-log "(disconnect-wire ~S ~S)~%" wire from)))))

;;; We need a separate function for disconnecting one split wire, since we
;;; need to specify the TO argument.

(defun disconnect-split-wire (from to &optional (already-logged nil))
  "Disconnect the SPLIT-wire between element FROM and element TO.  If they
   are not connected, do nothing.  Log the KR change if it is not already
   logged."
  (setq from (lookup-element-test from))
  (setq to (lookup-element-test to))
  (setf (split-wires from)
	(delete to (split-wires from)))
  (setf (incoming-split-wires to)
	(delete from (incoming-split-wires to)))
  ;; Log the new connection.
  (unless already-logged
    (kb-log "(disconnect-split-wire ~S ~S)~%" from to)))

(defun disconnect-split-wires (from &optional (already-logged nil))
  "Disconnect all the SPLIT-wires between element FROM and other elements."
  (setq from (lookup-element-test from))
  (dolist (to (split-wires from))
    (disconnect-split-wire from to already-logged)))

(defun convert-parent-wire-to-link (e &key (context *context*))
  "The parent-wire of element E is disconnected and replaced by an
   equivalent is-a link, which can then be cancelled or modified in some
   situations.  If the user does not supply a :context for the is-a link,
   use the current *context*."
  (setq e (lookup-element-test e))
  (let* ((old-parent (parent-wire e)))
    (when old-parent
      (disconnect-wire :parent e)
      (new-is-a e old-parent :context context))))


;;; ***************************************************************************
;;; Low-Level Marker Operations
;;; ***************************************************************************

;;; These operations must be as fast as possible.
(declaim (optimize (speed 3) (space 0) (safety 0)))

(declaim (inline fast-mark))

(defun fast-mark (e m)
  (declare (fixnum m))
  "Mark element E with marker M.  Do not check arguments."
  (let ((bit (marker-bit m)))
    (declare (fixnum bit))
    ;; If element E is already marked with marker M, do nothing.
    (when (= 0 (logand bit (bits e)))
      (if (null (svref *first-marked-element* m))
	  ;; This is the first element to get marker M, so record E as the
	  ;; first marked element.
	  (setf (svref *first-marked-element* m) e)
	  ;; There arlready are other elements with M, so add E to the chain.
	  (setf (svref (next-marked-element (svref *last-marked-element* m)) m) e))
      ;; Adjust forward and back pointers in E.
      (setf (svref (next-marked-element e) m) nil)
      (setf (svref (prev-marked-element e) m)
	    (svref *last-marked-element* m))
      ;; E is now the last marked element.
      (setf (svref *last-marked-element* m) e)
      ;; Turn on the marker bit in E.
      (setf (bits e)
	    (logior (bits e) bit))
      ;; Update the count of elements marked with M.
      (inc-marker-count m))))

(declaim (inline fast-unmark))

(defun fast-unmark (e m)
  (declare (fixnum m))
  "Remove marker M from element E.  Do no check arguments."
  (let ((bit (marker-bit m)))
    (declare (fixnum bit))
    ;; If element E is not marked with marker M, do nothing.
    (unless (= 0 (logand bit (bits e)))
      ;; Remember the elements before and after E in marker M's chain.
      (let ((prev (svref (prev-marked-element e) m))
            (next (svref (next-marked-element e) m)))
        ;; Clear the chain entries from E.
        (setf (svref (prev-marked-element e) m) nil)
        (setf (svref (next-marked-element e) m) nil)
        (if (null prev)
            ;; Element E was the first element in M's chain.
            ;; Make E's successor first.
            (setf (svref *first-marked-element* m) next)
          ;; Else, update next-pointer in previous element.
          (setf (svref (next-marked-element prev) m) next))
        (if (null next)
            ;; Element E was the last element in M's chain.
            ;; Make E's predecessor the last.
            (setf (svref *last-marked-element* m) prev)
          ;; Else, update the prev-pointer in the next element.
          (setf (svref (prev-marked-element next) m) prev))
        ;; Turn off the marker bit in E.
        (setf (bits e)
              (logand (bits e) (lognot bit)))
        ;; Update the count of elements marked with M.
        (dec-marker-count m)))))

(declaim (inline faster-mark))

(defun faster-mark (e m)
  (declare (fixnum m))
  "Just like FAST-MARK, but we use this when we're sure that E does not already
   hold marker M and that there is at least one element already marked with M.
   This is a common situation and saves a few cycles."
  ;; Add E to the end of the chain.
  (setf (svref (next-marked-element (svref *last-marked-element* m)) m) e)
  ;; Adjust forward and back pointers in E.
  (setf (svref (next-marked-element e) m) nil)
  (setf (svref (prev-marked-element e) m)
        (svref *last-marked-element* m))
  ;; E is now the last marked element.
  (setf (svref *last-marked-element* m) e)
  ;; Turn on the marker bit in E.
  (setf (bits e)
        (logior (bits e) (marker-bit m)))
  ;; Update the count of elements marked with M.
  (inc-marker-count m))

(declaim (inline faster-unmark))

(defun faster-unmark (e m)
  (declare (fixnum m))
  "This is like FAST-UNMARK, but it is used in situations where you are
   sure that element E has mark M.  Saves a few cycles."
  (let ((bit (marker-bit m)))
    (declare (fixnum bit))
    ;; Remember the elements before and after E in marker M's chain.
    (let ((prev (svref (prev-marked-element e) m))
          (next (svref (next-marked-element e) m)))
      ;; Clear the chain entries from E.
      (setf (svref (prev-marked-element e) m) nil)
      (setf (svref (next-marked-element e) m) nil)
      (if (null prev)
          ;; Element E was the first element in M's chain.
          ;; Make E's successor first.
          (setf (svref *first-marked-element* m) next)
        ;; Else, update next-pointer in previous element.
        (setf (svref (next-marked-element prev) m) next))
      (if (null next)
          ;; Element E was the last element in M's chain.
          ;; Make E's predecessor the last.
          (setf (svref *last-marked-element* m) prev)
        ;; Else, update the prev-pointer in the next element.
        (setf (svref (prev-marked-element next) m) prev))
      ;; Turn off the marker bit in E.
      (setf (bits e)
            (logand (bits e) (lognot bit)))
      ;; Update the count of elements marked with M.
      (dec-marker-count m))))

(declaim (inline convert-marker))

(defun convert-marker (m1 m2)
  "For every element marked with M1, mark it with M2 (if it was not marked
   with M2 already) and clear M1 from E.  This could be done with
   MARK-BOOLEAN, but this specialized version is faster."
  (declare (fixnum m1 m2))
  (let* ((bit1 (marker-bit m1))
	 (bit2 (marker-bit m2))
	 (mask1 (lognot bit1))
	 (bits 0)
	 (last-m2 (svref *last-marked-element* m2))
	 (next nil))
    (declare (fixnum bit1 bit2 mask1 bits))
    ;; For each element E in the chain for marker M1...
    (do ((e (svref *first-marked-element* m1) next))
	((null e))
      (setq bits (bits e))
      ;; Mark E with M2, unless it is already marked with M2.
      (when (= 0 (logand bit2 bits))
	(if last-m2
	    ;; There are already elements marked with M2, add E to the
	    ;; chain.
	    (setf (svref (next-marked-element last-m2) m2) e)
	    ;; This is the first element marked with M2.
	    (setf (svref *first-marked-element* m2) e))
	;; Adjust the back-pointer in E.
	(setf (svref (prev-marked-element e) m2) last-m2)
	;; Turn on the M2 marker bit in E.
	(setq bits (logior bits bit2))
	;; Update the count of elements marked with M.
	(inc-marker-count m2)
	;; E is now the last element in the m2 chain.
	(setq last-m2 e))
      ;; Remove the M1 links from E, but remember the next element.
      (setq next (svref (next-marked-element e) m1))
      (setf (svref (next-marked-element e) m1) nil)
      (setf (svref (prev-marked-element e) m1) nil)
      ;; Update the marker bits of E.
      (setf (bits e) (logand bits mask1)))
    ;; No elements are now marked with M1.
    (zero-marker-count m1)
    (setf (svref *first-marked-element* m1) nil)
    (setf (svref *last-marked-element* m1) nil)
    ;; Record the last element marked with M2.
    (setf (svref *last-marked-element* m2) last-m2)
    nil))

(defun clear-marker (m)
  "Clear marker M from all elements."
  (declare (fixnum m))
  (check-legal-marker m)
  (let ((mask (lognot (marker-bit m))))
    (declare (fixnum mask))
    (do ((e (svref *first-marked-element* m)))
	((null e) nil)
      ;; For each element E in the chain for marker M...
      (let ((next (svref (next-marked-element e) m)))
	;; Unlink from the chain.
	(setf (svref (next-marked-element e) m) nil)
	(setf (svref (prev-marked-element e) m) nil)
	;; Clear the marker bit.
	(setf (bits e) (logand (bits e) mask))
	(setf e next))))
  ;; Zero the count and set first and last elements to NIL.
  (zero-marker-count m)
  (setf (svref *first-marked-element* m) nil)
  (setf (svref *last-marked-element* m) nil))

(defun clear-marker-pair (m)
  "Clear marker M and the associated cancel marker, but do not free it."
  (declare (fixnum m))
  (check-legal-marker-pair m)
  (clear-marker m)
  (clear-marker (cancel-marker m)))

(defun clear-all-markers ()
  "Clear and free all markers from all elements in the KB."
  (do ((i 0 (1+ i)))
      ((>= i n-markers))
    (declare (fixnum i))
    (clear-marker i)
    (free-markers)))

(defun make-mask (markers)
  "Takes a list of markers.
   Returns the corresponding mask (a fixnum)."
  (let ((mask 0))
    (declare (fixnum mask))
    (dolist (m markers)
      (declare (fixnum m))
      (check-legal-marker m)
      (setq mask (logior mask (marker-bit m))))
    mask))

(defun make-cancel-mask (markers)
  "Takes a list of markers.  Returns the mask corresponding to 
   the cacnel marker for each marker in the list."
  (let ((mask 0))
    (declare (fixnum mask))
    (dolist (m markers)
      (declare (fixnum m))
      (check-legal-marker m)
      (setq mask (logior mask (marker-bit (cancel-marker m)))))
    mask))

;;; ---------------------------------------------------------------------------
;;; User-Level Marker Operations

;;; The following marker operations may be called directly by users, so do
;;; more argument checking.

(defun mark (e m)
  "User-level MARK function."
  (check-legal-marker m)
  (setq e (lookup-element-test e))
  (fast-mark e m))

(defun unmark (e m)
  "User-level UNMARK function."
  (check-legal-marker m)
  (setq e (lookup-element-test e))
  (fast-unmark e m))

(defun marker-count (m)
  (check-legal-marker m)
  (fast-marker-count m))

(defun marker-on? (e m)
  "User-level function to determine whether element E has marker M
   turned on."
  (check-legal-marker m)
  (setq e (lookup-element-test e))
  (fast-marker-on? e m))

(defun marker-off? (e m)
  "User-level function to determine whether element E has marker M
   turned off."
  (check-legal-marker m)
  (setq e (lookup-element-test e))
  (fast-marker-off? e m))


;;; ***************************************************************************
;;; Definition of the SCONE Element Types
;;; ***************************************************************************

;;; Use normal compilation policy, where safety counts as much as speed.
(declaim (optimize (speed 1) (space 1) (safety 1)))

;;; Some predicates for basic types.  These are meant to be fast, so
;;; they assume you've already got an element-object as the argument.

(defmacro node? (e)
  `(fast-flag-on? ,e node-flag))

(defmacro link? (e)
  `(fast-flag-on? ,e link-flag))

(defmacro h-link? (e)
  "An H-link is a relation or statement."
  `(fast-flag-on? ,e h-flag))

(defmacro defined? (e)
  "Predicate to detect if the defined-flag is on."
  `(fast-flag-on? ,e defined-flag))

;;; ========================================================================
;;; INDV NODE

;;; An INDV-NODE represents an individual entity.

(defconstant indv-mask (logior node-flag indv-flag))
(defmacro indv-node? (e)
  `(all-bits-on? (flags ,e) indv-mask))

;;; A PROPER-INDV-NODE represents a specific first-class entity such as
;;; "Clyde" or "Afghanistan".

(defconstant proper-indv-mask (logior indv-mask proper-flag))

(defmacro proper-indv-node? (e)
  `(all-bits-on? (flags ,e) proper-indv-mask))

;;; A GENERIC-INDV-NODE is an INDV-NODE that does not have the PROPER-FLAG
;;; on.  A generic node is usually defined.

(defmacro generic-indv-node? (e)
  `(let ((f (flags ,e)))
     (and (all-bits-on? f indv-mask)
	  (all-bits-off? f proper-flag))))

;;; An individual or type node may have a definition: "the man who shot
;;; Kennedy" or "three-legged blue elephants".  For these, we put some form
;;; into the element's DEFINITION slot and we set the DEFINED-FLAG.

(defconstant defined-indv-mask (logior indv-mask defined-flag))

(defmacro defined-indv-node? (e)
  `(all-bits-on? (flags ,e) defined-indv-mask))

(defun new-indv (iname parent
		       &key
		       (context *context*)
		       (proper t)
		       definition
		       english
		       english-default)
  "Make and return a new INDV-NODE with the specified internal name and
   PARENT.  If :PROPER is supplied and NIL, this is a generic node,
   otherwise assume it is a proper individual.  If :DEFINITION is non-null,
   set the defined flag and stick the value in the element's definition
   field.  Also add a {HAS} statement from the :CONTEXT to the new node.
   If the caller specifies no :ENGLISH argument, assume :INAME."
  (setq parent (lookup-element parent))
  ;; If there's no iname supplied, make one up based on the parent.
  (unless iname
    (push :no-iname english)
    (setq iname
	  (if (typep parent 'element)
	      (gen-iname (internal-name parent))
	      (gen-iname "Indv"))))
  ;; A defined class is never proper.
  (let* ((e (make-element (if (and proper (not definition))
			      proper-indv-mask
			      indv-mask)
			  iname parent context nil nil nil
			  english (or english-default :noun) nil
			  definition nil)))
    (when context (new-statement context *has* e :c *one*))
    e))


;;; PRIMTIVE NODE

;;; A PRIMITIVE-NODE is a sub-type of INDV-NODE that represents a Lisp
;;; object of some kind: number, string, function, etc.  The lisp object
;;; itself is stored as the element's internal name.

(defconstant primitive-mask (logior node-flag indv-flag
				 primitive-flag proper-flag))

(defmacro primitive-node? (e)
  `(all-bits-on? (flags ,e) primitive-mask))

(defun new-primitive-node (value parent hashtable)
  "Common code to make a primitive node of some type."
  (or (gethash value hashtable)
      (make-element primitive-mask
		    value parent *universal* nil nil nil nil nil
		    hashtable nil nil)))

(defmacro number-node? (e)
  `(and (primitive-node? ,e)
	(typep (internal-name ,e) 'number)))

(defmacro integer-node? (e)
  `(and (primitive-node? ,e)
	(typep (internal-name ,e) 'integer)))

(defmacro ratio-node? (e)
  `(and (primitive-node? ,e)
	(typep (internal-name ,e) 'ratio)))

(defmacro float-node? (e)
  `(and (primitive-node? ,e)
	(typep (internal-name ,e) 'float)))

(defun new-number (value &key (parent nil))
  "Represents any type of number present in Common Lisp: integer, float,
   ratio, etc.  If a number node with the specified VALUE already exists,
   return it.  Else, create and return the new function node.  Optionally
   provide a :PARENT element.  If no parent is provided, an appropriate one
   will be chosen."
  (check-type value number)
  (unless parent
    (setq parent 
	  (typecase value
	    (integer *integer*)
	    (float *float*)
	    (ratio *ratio*)
	    (t *number*))))
  (new-primitive-node value parent *number-hashtable*))

(defun new-integer (value &key (parent *integer*))
  "VALUE must be a Common Lisp integer.  Create and return a Scone element 
   representing that ratio."
  (check-type value integer)
  (new-primitive-node value parent *number-hashtable*))

(defun new-ratio (value &key (parent *ratio*))
  "VALUE must be a Common Lisp ratio.  Create and return a Scone element 
   representing that ratio."
  (check-type value ratio)
  (new-primitive-node value parent *number-hashtable*))

(defun new-float (value &key (parent *float*))
  "VALUE must be a Common Lisp floating-point number.  Create and return a
   Scone element representing that number."
  (check-type value float)
  (new-primitive-node value parent *number-hashtable*))

(defmacro string-node? (e)
  `(and (primitive-node? ,e)
	(typep (internal-name ,e) 'string)))

(defun new-string (value
		     &key
		     (parent *string*))
  "Represents a string in Common Lisp.  If a string node with the specified
   VALUE already exists, return it.  Else, create and return the new number
   node.  Optionally provide :PARENT element."
  (check-type value string)
  (new-primitive-node value parent *string-hashtable*))

(defmacro function-node? (e)
  `(and (primitive-node? ,e)
	(typep (internal-name ,e) 'function)))

(defun new-function (value
		     &key
		     (parent *function*))
  "Represents a function in Common Lisp.  If a function node with the
   specified VALUE already exists, return it.  Else, create and return the
   new function node.  Optionally provide :PARENT element."
  (check-type value function)
  (new-primitive-node value parent *function-hashtable*))

(defmacro fn-call (fn &rest r)
  `(let ((fn1 ,fn))
     (if (function-node? fn1)
	 (funcall (internal-name fn1) ,@r)
	 (funcall fn1 ,@r))))

(defmacro struct-node? (e)
  `(and (primitive-node? ,e)
	(typep (internal-name ,e) 'structure-object)))

(defun new-struct (value
		     &key
		     (parent *struct*))
  "Represents a DEFSTRUCT object in Common Lisp.  If a struct node with the
   specified VALUE already exists, return it.  Else, create and return the
   new struct node.  Optionally provide :PARENT element."
  (check-type value structure-object)
  (new-primitive-node value parent *struct-hashtable*))

;;; ========================================================================
;;; TYPE NODE

;;; A TYPE-NODE represents the typical member of some class, such as
;;; ELEPHANT.  Each type node is in principle paired with a set-node
;;; representing the set of these elements.  However, we only create the
;;; set-node when we have something to say about it.  Until then, it is
;;; only virtually present.

;;; The type node and its set node point to one another via the :SET-NODE
;;; and :TYPE-NODE properties of the element, since these conncetions are
;;; relatively rare and are not followed during time-critical inner loops.

(defconstant type-mask (logior node-flag type-flag))

(defmacro type-node? (e)
  `(all-bits-on? (flags ,e) type-mask))

(defun new-type (iname parent
		       &key
		       (context *context*)
		       definition
		       n
		       english
		       english-default)
  "Make and return a new TYPE-NODE with the specified internal name.  If
   :DEFINITION is non-null, set the defined flag and stick the value in the
   element's definition field.  Also connect the parent-wire to the
   specified PARENT and create a {HAS} statement from the :CONTEXT to the
   new type-node.  If the :N argument is supplied, this says that there are
   N members of this class in the specified context.  If the caller
   specifies no :ENGLISH argument, assume :INAME."
  (setq parent (lookup-element parent))
  ;; If there's no iname supplied, make one up based on the parent.
  (unless iname
    (push :no-iname english)
    (setq iname
	  (if (typep parent 'element)
	      (gen-iname (internal-name parent))
	      (gen-iname "Type"))))
  (let* ((e (make-element type-mask iname parent context nil nil nil
			  english (or english-default :noun) nil
			  definition nil)))
    (when context (new-statement context *has* e :c n))
    e))


;;; Meta-Node machinery

;;; &&& I'm not happy with this.  Re-think it.

;;; A type node may have have an associated meta-node, which represents the
;;; type (or set) itself, rather than the typical member of the set.

(defun find-meta-node (e)
  "Return the meta-node of element E, if the meta-node already exists.
   Else, return NIL."
  (setq e (lookup-element-test e))
  (unless (type-node? e) (error "~S is not a type-node." e))
  (get-element-property e :meta-node))

(defun get-meta-node (e &optional (parent *set*))
  "Return the meta-node associated with element E, creating it if
   necessary."
  (setq e (lookup-element-test e))
  (unless (type-node? e) (error "~S is not a type-node." e))
  (let ((mn (get-element-property e :meta-node)))
    (or mn
	(let* ((iname
		(gen-iname (format t "META ~A"
				   (internal-name e)))))
	  (setq mn (new-indv iname parent))
	  (set-element-property e :meta-node mn)
	  (set-element-property mn :meta-node-of e)
	  mn))))

(defun find-inverse-meta-node (mn)
  "Given a node MN, find the type-node for which this is the meta-node.
   If MN is not a meta-node, return NIL."
  (setq mn (lookup-element-test mn))
  (get-element-property mn :meta-node))

(defun remove-meta-node (e)
  "Given an element E, sever the connection to its meta-node if it has one.
   Else, do nothing and return NIL."
  (let ((mn (find-meta-node e)))
    (when mn
      (clear-element-property e :meta-node)
      (clear-element-property mn :meta-node-of)
      mn)))

;;; ========================================================================
;;; IS-A LINK

;;; IS-A-LINK Indicates that A "is a" B.
;;; A can be any node.  B generally will be a type node.

(defconstant is-a-mask
  (logior link-flag true-flag is-a-flag up-flag))

(defconstant is-not-a-mask
  (logior link-flag not-flag is-a-flag up-flag))

(defmacro is-a-link? (e)
  `(all-bits-on? (flags ,e) is-a-mask))

(defun new-is-a (a b
		   &key
		   negate
		   iname
		   (parent *is-a-link*)
		   (context *context*)
		   english)
  "Make and return a new IS-A-LINK with the specified elements on the A and
   B wires.  Optionally provide :PARENT and :CONTEXT."
  (setq a (lookup-element a))
  (setq b (lookup-element b))
  (unless (or *no-checking* negate (can-x-be-a-y? a b))
    (error "~S cannot be a ~S." a b))
  ;; Try to create a human-readable iname.
  (unless iname
    (setq iname
	  (if (and (typep a 'element)
		   (typep b 'element))
	      (gen-iname (format nil "~A is-a ~A"
				 (internal-name a)
				 (internal-name b)))
	      (gen-iname "Is-A"))))
  (make-element (if negate is-not-a-mask is-a-mask)
		iname parent context a b nil english nil nil nil nil))

;;; An IS-NOT-A-LINK Indicates an exception: A is NOT a B, and any
;;; assertion that it is a B should be questioned.

(defmacro is-not-a-link? (e)
  `(all-bits-on? (flags ,e) is-not-a-mask))

(defun new-is-not-a (a b
		       &key
		       iname
		       (parent *is-not-a-link*)
		       (context *context*)
		       english)
  "Make and return a new IS-NOT-A-LINK with the specified elements on the A
   and B wires.  Optionally provide :PARENT and :CONTEXT.  This is
   equivalent to NEW-IS-A with the :NEGATE flag."
  (setq a (lookup-element a))
  (setq b (lookup-element b))
  ;; Try to create a human-readable iname.
  (unless iname
    (setq iname
	  (if (and (typep a 'element)
		   (typep b 'element))
	      (gen-iname (format nil "~A is-not-a ~A"
				 (internal-name a)
				 (internal-name b)))
	      (gen-iname "Is-Not-A"))))
  (new-is-a a b
	    :negate t
	    :iname iname
	    :parent parent
	    :context context
	    :english english))

;;; ========================================================================
;;; EQ LINK

;;; EQ-LINK Indicates that elements A and B represent the same entity.

(defconstant eq-mask
  (logior link-flag true-flag eq-flag up-flag))

(defconstant not-eq-mask
  (logior link-flag not-flag eq-flag up-flag))

(defmacro eq-link? (e)
  `(all-bits-on? (flags ,e) eq-mask))

(defun new-eq (a b
		 &key
		 negate
		 iname
		 (parent *eq-link*)
		 (context *context*)
		 english)
  "Make and return a new EQ-LINK with the specified elements on the A and B
   wires.  Optionally provide :PARENT and :CONTEXT."
  (setq a (lookup-element a))
  (setq b (lookup-element b))
  (unless (or *no-checking* negate (can-x-be-a-y? a b))
    (error "~S cannot be eq to ~S." a b))
  ;; Try to create a human-readable iname.
  (unless iname
    (setq iname
	  (if (and (typep a 'element)
		   (typep b 'element))
	      (gen-iname (format nil "~A eq ~A"
				 (internal-name a)
				 (internal-name b)))
	      (gen-iname "Eq"))))
  (make-element (if negate not-eq-mask eq-mask)
		iname parent context a b nil english nil nil nil nil))

;;; NOT-EQ-LINK Indicates that elements A and B do NOT represent the same
;;; entity.

(defmacro not-eq-link? (e)
  `(all-bits-on? (flags ,e) not-eq-mask))

(defun new-not-eq (a b
		     &key
		     iname
		     (parent *not-eq-link*)
		     (context *context*)
		     english)
  "Make and return a new NOT-EQ-LINK with the specified elements on the A
   and B wires.  Optionally provide :CONTEXT."
  (setq a (lookup-element a))
  (setq b (lookup-element b))
  ;; Try to create a human-readable iname.
  (unless iname
    (setq iname
	  (if (and (typep a 'element)
		   (typep b 'element))
	      (gen-iname (format nil "~A not eq ~A"
				 (internal-name a)
				 (internal-name b)))
	      (gen-iname "Not-Eq"))))
  (new-eq a b
	  :negate t 
	  :iname iname
	  :parent parent
	  :context context
	  :english english))

;;; ========================================================================
;;; MAP LINK

;;; The MAP-LINK states that the element on its A-wire plays the role of
;;; the element on its B-wire in the description on its C-wire.  The
;;; context wire controls activation of this statement, as is the case for
;;; other links.

(defconstant map-mask 
    (logior link-flag not-flag map-flag))

(defconstant not-map-mask 
    (logior link-flag true-flag map-flag))

(defmacro map-link? (e)
  `(all-bits-on? (flags ,e) map-mask))

;;; &&& Add some type checking here.

(defun new-map (player
		role
		owner
		&key
		negate
		iname
		(parent *map-link*)
		(context *context*)
		english)
  "Make and return a new MAP-LINK stating that A plays role B in
   description C.  The role and owner must already exist -- no deferrals
   allowed here.  Optionally provide a :PARENT, a :CONTEXT and an :INAME."
  (setq player (lookup-element-test player))
  (setq role (lookup-element-test role))
  (setq owner (lookup-element-test owner))
  ;; If no iname supplied, try to make one up.
  (unless iname
    (setq iname (gen-iname
		 (format nil "~A is the ~A of ~A"
			 (internal-name player)
			 (internal-name role)
			 (internal-name owner)))))
  ;; Now create and return the map-node.
  (make-element (if negate not-map-mask map-mask)
		iname parent context player role owner
		english nil nil nil nil))

;;; A not-map link indicates that A is definitely not the B of C.

(defmacro not-map-link? (e)
  `(all-bits-on? (flags ,e) not-map-mask))

(defun new-not-map (player
		    role
		    owner
		    &key
		    iname
		    (parent *not-map-link*)
		    (context *context*)
		    english)
  "Make and return a new NOT-MAP-LINK with the specified ROLE, PLAYER, and
   OWNER.  Optionally provide :PARENT, :CONTEXT, and :INAME.  This is
   equivalent to NEW-MAP with the :NEGATE flag."
  (setq player (lookup-element-test player))
  (setq role (lookup-element-test role))
  (setq owner (lookup-element-test owner))
  ;; Try to create a human-readable iname.
  (unless iname
    (setq iname (gen-iname
		 (format nil "~A is not the ~A of ~A"
			 (internal-name player)
			 (internal-name role)
			 (internal-name owner)))))
  (new-map player role owner
	   :negate t
	   :iname iname
	   :parent parent
	   :context context
	   :english english))


;;; ========================================================================
;;; CANCEL LINK

;;; A CANCEL-LINK says that in description A, statement B (which would
;;; otherwise be true) is inoperative.  Do not use this to alter the
;;; membership of A in some class, for example by cancelling an IS-A or EQ
;;; link.  Use an IS-NOT-A link for that.

(defconstant cancel-mask (logior link-flag not-flag cancel-flag up-flag))

(defmacro cancel-link? (e)
  `(all-bits-on? (flags ,e) cancel-mask))

(defun new-cancel (a b
		     &key
		     iname
		     (parent *cancel-link*)
		     (context *context*)
		     english)
  "Make and return a new CANCEL-LINK with the specified elements on the A
   and B wires.  Optionally provide :CONTEXT element."
  (setq a (lookup-element a))
  (setq b (lookup-element b))
  ;; Try to create a human-readable iname.
  (unless iname
    (setq iname
	  (if (and (typep a 'element)
		   (typep b 'element))
	      (gen-iname (format nil "~A cancels ~A"
				 (internal-name a)
				 (internal-name b)))
	      (gen-iname "Cancel"))))
  (make-element cancel-mask
		iname parent context a b nil english nil nil nil nil))

;;; NOTE: Symmetry would dictate having a NOT-CANCEL link, but I can't
;;; think of a good use for that, so I've left it out for now.

;;; ========================================================================
;;; SPLIT

;;; A SPLIT is like a link or statement, with a context and a handle node.
;;; But instead of A and B wires it has any number of split-wires.  The
;;; meaning is that the elements (usually type nodes) connected by this
;;; split are disjoint and non-overlapping.

;;; So, for example, "bird", "reptile", and "mammal" form a split.  The
;;; engine automatically and efficiently detects any attempt to violate a
;;; split.  If you create an individual that belongs to more than one of
;;; these classes, the engine will detect this and complain, unless that
;;; individual cancels the split.

(defconstant split-mask split-flag)

(defmacro split? (e)
  `(all-bits-on? (flags ,e) split-mask))

(defun new-split (members
		  &key
		  iname
		  (parent *split*)
		  (context *context*)
		  english)
  "Make and return a new SPLIT-NODE with the specified members.
   Optionally provide :INAME and :CONTEXT."
  ;; Try to make a meaningful iname, mentioning the first two members
  ;; in the split.
  (unless iname
    (let* ((m1 (car members))
	   (m2 (cadr members))
	   (s1 nil)
	   (s2 nil))
      (cond ((typep m1 'element)
	     (setq s1 (internal-name m1)))
	    ((typep m1 'element-iname)
	     (setq s1 (element-iname-string m1))))
      (cond ((null m1))
	    ((typep m2 'element)
	     (setq s2 (internal-name m2)))
	    ((typep m2 'element-iname)
	     (setq s2 (element-iname-string m2))))
      (setq iname
	    (if m2
		(gen-iname (format nil "split ~A ~A" s1 s2))
		(gen-iname "Split")))))
  (let ((s (make-element
	    split-mask
	    iname parent context
	    nil nil nil english nil nil nil nil)))
    (dolist (m members)
      (setq m (lookup-element-test m))
      (connect-wire :split s m))
    s))

(defun add-to-split (split new-item)
  "Add NEW-ITEM to the set of items declared disjoint by SPLIT."
  (setq split (lookup-element-test split))
  (unless (split? split)
    (error "~S is not a SPLIT." split))
  (setq new-item (lookup-element-test new-item))
  (connect-wire :split split new-item)
  split)


;;; ========================================================================
;;; COMPLETE-SPLIT

;;; A COMPLETE-SPLIT is a subtype of split.  We note the superclass of the
;;; split as a property in the COMPLETE-SPLIT element.  The COMPLETE-SPLIT
;;; completely partitiions this type.

(defconstant complete-split-mask (logior split-flag complete-split-flag))

(defmacro complete-split? (e)
  `(all-bits-on? (flags ,e) complete-split-mask))

(defun new-complete-split (supertype members
				     &key
				     iname
				     (parent *complete-split*)
				     (context *context*)
				     english)
  "Make and return a new COMPLETE-SPLIT-NODE with the specified members,
   all under the specified SUPERTYPE.  Optionally provide :INAME and
   :CONTEXT."
  (setq supertype (lookup-element supertype))
  ;; Try to make a meaningful iname, mentioning the first two members
  ;; in the split.
  (unless iname
    (let* ((m1 (car members))
	   (m2 (cadr members))
	   (s1 nil)
	   (s2 nil))
      (cond ((typep m1 'element)
	     (setq s1 (internal-name m1)))
	    ((typep m1 'element-iname)
	     (setq s1 (element-iname-string m1))))
      (cond ((null m1))
	    ((typep m2 'element)
	     (setq s2 (internal-name m2)))
	    ((typep m2 'element-iname)
	     (setq s2 (element-iname-string m2))))
      (setq iname
	    (if m2
		(gen-iname (format nil "csplit ~A ~A" s1 s2))
		(gen-iname "C-Split")))))
  (let ((s (make-element
	    complete-split-mask
	    iname parent context
	    nil nil nil english nil nil nil nil)))
    (set-element-property s :split-supertype supertype t)
    (push-element-property supertype :complete-split-subtypes s t)
    (dolist (m members)
      (setq m (lookup-element-test m))
      (connect-wire :split s m))
    s))

;;; ========================================================================
;;; RELATION

;;; A RELATION element creates a new relation that we can instantiate with
;;; a STATEMENT link.

;;; The RELATION element is similar in structure to a link, since it has A,
;;; B, and C wires, but in function it is more like a type-node.  That is,
;;; the RELATION element itself does not make any statement -- it just
;;; serves to define what its instances (statements) mean.  A RELATION may
;;; be a sub-type (specialization) of other relations.

;;; At the end of the A, B, and C wires are defined generic nodes that
;;; serve as prototypes whose properties are inherited by the A, B, and C
;;; elements of instances (statements) under this relation.  The A node
;;; corresponds (more or less) to the domain of traditional binary
;;; relations, while B corresponds to the range.  The optional C node is a
;;; third element so that we can represent trinary relations such as "the
;;; distance from A to B is C".

(defconstant relation-mask (logior relation-flag h-flag))

(defmacro relation? (e)
  `(all-bits-on? (flags ,e) relation-mask))

(defun new-relation (iname &key
			   a
			   a-inst-of
			   a-type-of
			   b
			   b-inst-of
			   b-type-of
			   c
			   c-inst-of
			   c-type-of
			   (parent *relation*)
			   symmetric
			   transitive
			   english)
  "Define a new RELATION element that can be instantiated by a STATEMENT
   link.  The INAME is the name of the relation from A to B.  This relation
   may have another relation as a parent, or a type-node.  Normally the
   caller supplies elements as :A, :B, and :C arguments.  However, for the
   common case where you want to create a new generic node with a specified
   parent, you can instead supply :A-INST-OF or :A-TYPE-OF, and similarly
   for B and C.  Optionally provide a :PARENT element for the relation
   itself.  Optionally set the :SYMMETRIC and :TRANSITIVE properties of the
   relation.  These refer to the relation from A to B."
  (unless (and iname
	       (typep iname 'element-iname)
	       (null (lookup-element-predicate iname)))
    (error "Must supply a unique iname."))
  (let ((iname-string (element-iname-string iname))
	(flags relation-mask))
    ;; Set up additional flags.
    (when symmetric
      (setq flags (logior flags symmetric-flag)))
    (when transitive
      (setq flags (logior flags transitive-flag)))
    ;; Create the A, B, and C nodes if necessary
    (cond (a)
	  (a-inst-of 
	   (setq a (new-indv
		    (gen-iname
		     (format nil "A-role of ~A" iname-string))
		    a-inst-of
		    :proper nil :english :no-iname)))
	  (a-type-of
	   (setq a (new-type
		    (gen-iname
		     (format nil "A-role of ~A" iname-string))
		    a-type-of :english :no-iname))))
    (cond (b)
	  (b-inst-of 
	   (setq b (new-indv
		    (gen-iname
		     (format nil "B-role of ~A" iname-string))
		    b-inst-of
		    :proper nil :english :no-iname)))
	  (b-type-of
	   (setq b (new-type
		    (gen-iname
		     (format nil "B-role of ~A" iname-string))
		    b-type-of :english :no-iname))))
    (cond (c)
	  (c-inst-of 
	   (setq c (new-indv
		    (gen-iname
		     (format nil "C-role of ~A" iname-string))
		    c-inst-of
		    :proper nil :english :no-iname)))
	  (c-type-of
	   (setq c (new-type
		    (gen-iname
		     (format nil "C-role of ~A" iname-string))
		    c-type-of :english :no-iname))))
    ;; Now create the relation-element itself.
    (make-element flags
		  iname parent *universal* a b c
		  english :relation nil nil nil)))

(defmacro symmetric? (e) 
  `(fast-flag-on? ,e symmetric-flag))

(defmacro transitive? (e) 
  `(fast-flag-on? ,e transitive-flag))


;;; ========================================================================
;;; STATEMENT

;;; A STATEMENT represents an instance of a relation defined by the
;;; relation that is its parent.

(defconstant statement-mask
  (logior link-flag true-flag statement-flag h-flag))

(defconstant not-statement-mask
  (logior link-flag not-flag statement-flag))

(defmacro statement? (e)
  `(all-bits-on? (flags ,e) statement-mask))

(defun new-statement (a rel b
			&key
			c
			(context *context*)
			negate
			iname
			english)
  "State that relation REL holds between A and B in CONTEXT.  REL must be a
   relation or statement node.  Optionally connect a C node.  If :NEGATE is
   present, this is the negation of the statement that would otherwise be
   created.  If :A, :B, or :C is :CREATE, create a new node representing
   that role."
  ;; The relation must already be present.  No deferrals.
  (setq rel (lookup-element-test rel))
  (unless ( or (relation? rel) (statement? rel))
    (error "~S not an existing relation or statement." rel))
  (let* ((ax (lookup-element-predicate a))
	 (bx (lookup-element-predicate b))
	 (cx (lookup-element-predicate c))
	 (rel-a (a-wire rel))
	 (rel-b (b-wire rel))
	 (rel-c (c-wire rel)))
    ;; Check the types of the A, B, and C arguments if they are present.  Do not
    ;; perform the check if this statement is negated.
    (unless *no-checking*
      (when (and ax
		 rel-a
		 (not negate)
		 (not (can-x-be-a-y? ax rel-a)))
	(error "~S cannot be the A-element of a ~S relation."
	       ax rel))
      (when (and bx
		 rel-b
		 (not negate)
		 (not (can-x-be-a-y? bx rel-b)))
	(error "~S cannot be the B-element of a ~S relation."
	       bx rel))
      (when (and cx
		 rel-c
		 (not negate)
		 (not (can-x-be-a-y? cx rel-c)))
	(error "~S cannot be the C-element of a ~S relation."
	       cx rel)))
    ;; Try to create a human-readable iname
    (unless iname
      (setq iname
	    (cond 
	     ;; If we are missing either AX or BX, just mention REL in the
	     ;; iname.
	     ((not (and (typep ax 'element)
			(typep bx 'element)))
	      (gen-iname (internal-name rel)))
	     ;; If we have AX, BX, and CX, mention them all.
	     ((typep cx 'element)
	      (gen-iname
	       (format nil "~A ~A ~A ~A"
		       (internal-name ax)
		       (internal-name rel)
		       (internal-name bx)
		       (internal-name cx))))
	     ;; Just name the thing AX REL BX.
	     (t (gen-iname
		 (format nil "~A ~A ~A"
			 (internal-name ax)
			 (internal-name rel)
			 (internal-name bx)))))))
    ;; If A, B, or C is :CREATE, create a generic node.	 
    (when (eq a :create)
      (if (type-node? rel-a)
	  (setq ax (new-type
		    (gen-iname
		     (format nil "A-role of ~A" iname))
		    *thing* :english :no-iname))
	  (setq ax (new-indv
		    (gen-iname
		     (format nil "A-role of ~A" iname))
		    *thing* :proper nil :english :no-iname))))
    (when (eq b :create)
      (if (type-node? rel-b)
	  (setq bx (new-type
		    (gen-iname
		     (format nil "B-role of ~A" iname))
		    *thing* :english :no-iname))
	  (setq bx (new-indv
		    (gen-iname
		     (format nil "B-role of ~A" iname))
		    *thing* :proper nil :english :no-iname))))
    (when (eq c :create)
      (if (type-node? rel-c)
	  (setq cx (new-type
		    (gen-iname
		     (format nil "C-role of ~A" iname))
		    *thing* :english :no-iname))
	  (setq cx (new-indv
		    (gen-iname
		     (format nil "C-role of ~A" iname))
		    *thing* :proper nil :english :no-iname))))
    (let ((flags (if negate not-statement-mask statement-mask)))
      (when (symmetric? rel)
	(setq flags (logior flags symmetric-flag)))
      (when (transitive? rel)
	(setq flags (logior flags transitive-flag)))
      (make-element flags
		    iname rel context ax bx cx
		    english nil nil nil nil))))


;;; A NOT-STATEMENT states that something is *not* true.

(defmacro not-statement? (e)
  `(all-bits-on? (flags ,e) not-statement-mask))

(defun new-not-statement (a rel b
			    &key
			    c
			    (context *context*)
			    iname
			    english)
  "Make and return a new NOT-STATEMENT with the specified elements on the A
   and B wires.  Optionally provide :CONTEXT."
  ;; Try to create a human-readable iname.
  (unless iname
    (setq a (lookup-element a))
    (setq b (lookup-element b))
    (when c (setq c (lookup-element c)))
    (setq rel (lookup-element-test rel))
    (setq iname
	  (cond 
	   ;; If we are missing either A or B, just mention REL in the
	   ;; iname.
	   ((not (and (typep a 'element)
		      (typep b 'element)))
	    (gen-iname 	
	     (format nil "not ~A"
		     (internal-name rel))))
	   ;; If we have A, B, and C, mention them all.
	   ((typep c 'element)
	    (gen-iname
	     (format nil "Not ~A ~A ~A ~A"
		     (internal-name a)
		     (internal-name rel)
		     (internal-name b)
		     (internal-name c))))
	   ;; Just name the thing Not A REL B.
	   (t (gen-iname
	       (format nil "Not ~A ~A ~A"
		       (internal-name a)
		       (internal-name rel)
		       (internal-name b)))))))
  (new-statement a rel b :c c :context context
		 :negate t :iname iname :english english))


;;; ========================================================================
;;; The PRINT-ELEMENT function must be defined after all the element-type
;;; predicates have been defined.

(defun print-element (e stream depth)
  "Print element E in a human-readable form."
  (declare (ignore depth))
  (if (string-node? e)
      ;; Special-case print formula for string nodes.
      (format stream "{~S}" (internal-name e))
      (format stream "{~A}" (full-iname e))))

(defun gen-iname (string)
  "Generate an iname.  STRING is used as the first part of the name,
   followed by a counter to ensure uniqueness.  If STRING has a counter
   embedded in it, remove that first."
  (prog1
      (make-element-iname
       :string
       (format nil "~A (~D-~D)"
	       (strip-counters string)
	       *iname-prefix*
	       *iname-counter*))
      (incf *iname-counter*)))

(defun strip-counters (string)
  "Given a STRING, strip out any counters that GEN-INAME may have added.
   These are integers, surrounded by parentheses, preceded by a space."
  (do ((p 1 (+ p 1))
       (q nil))
      ((= p (length string)) string)
    (and (char= (elt string p) #\( )
	 (char= (elt string (- p 1)) #\space)
	 (setq q (position #\) string :start p))
	 (> (- q p) 1)
	 (every #'digit-or-dash (subseq string (+ p 1) q))
	 (return
	  (strip-counters
	   (concatenate 'string
			(subseq string 0 (- p 1))
			(subseq string (+ q 1))))))))

(defun digit-or-dash (x)
  (or (digit-char-p x)
      (char= x #\-)))

;;; ========================================================================
;;; Adding roles.

;;; Adding a role to a description is normally done using one of these
;;; functions.

(defun new-indv-role (iname owner parent
			    &key
			    (context *context*)
			    may-have
			    english)
  "States that OWNER type-node has an individual role with the specified
   INAME and PARENT.  Normally we also create an EXISTS-FOR statement
   indicating that every member of the specified type has one instance of
   this role.  However, if :MAY-HAVE is T, it indicates that the role
   exists for some instances of OWNER and not others, so we do not create
   this statement."
  (let ((role-node
	 (new-indv iname
		   parent
		   :context owner
		   :proper nil
		   :english english
		   :english-default :role)))
    (unless may-have
      (new-statement owner *has* role-node
		     :context context
		     :c *one*))
    role-node))

(defun new-type-role (iname owner parent
			    &key
			    (context *context*)
			    n
			    may-have
			    english)
  "States that OWNER type-node has a type-role with the specified INAME and
   PARENT.  Normally we also create an EXISTS-FOR statement indicating that
   every member of the specified type has at least one instance of this
   role.  However, if :MAY-HAVE is T, it indicates that the role exists for
   some instances of OWNER and not others, so we do not create this
   statement.  If :N is specified, that is a node representing the number
   of these things that a typical OWNER has."
  (let ((role-node
	 (new-type iname
		   parent
		   :context owner
		   :english english
		   :english-default :role)))
    (unless may-have
      (new-statement owner *has* role-node
		     :context context
		     :c n))
    role-node))

;;; ========================================================================
;;; Adding split subtypes and instances.

(defun new-split-subtypes (parent members
				  &key
				  (iname nil)
				  (context *context*)
				  english)
  "MEMBERS is a list.  Each member is either an ELEMENT-INAME or a list
   whose first element is an ELEMENT-INAME.  For each member create a new
   type-node under PARENT.  These new elements are all part of a new split.
   After processing all members, return the new split.  The :CONTEXT arg
   controls the context of the new type-nodes and the split.  The optional
   :INAME and :ENGLISH arguments give the name of the split element.  If a
   member of the MEMBERS list is a list, its first element is the iname of
   the new typeype node and the rest is passed as the :ENGLISH argument for
   the new type."
  (let ((new-elements nil))
    (dolist (m members)
      ;; Create a type-node for each item in MEMBERS.
      (push
       (new-type (if (consp m) (car m) m)
		 parent
		 :context context
		 :english (if (consp m)
			      (cdr m)
			      :iname))
       new-elements))
    ;; Create and return the split.
    (new-split (nreverse new-elements)
	       :iname iname
	       :context context
	       :english english)))

(defun new-complete-split-subtypes (parent members
					   &key
					   (iname nil)
					   (context *context*)
					   english)
  "Like NEW-SPLIT-SUBTYPES, but creates a complete split of the elements
   under PARENT."
  (let ((new-elements nil))
    (dolist (m members)
      ;; Create a type-node for each item in MEMBERS.
      (push
       (new-type (if (consp m) (car m) m)
		 parent
		 :context context
		 :english (if (consp m)
			      (cdr m)
			      :iname))
       new-elements))
    ;; Create and return the split.
    (new-complete-split parent
			(nreverse new-elements)
			:iname iname
			:context context
			:english english)))

(defun new-members (parent members
			   &key
			   (iname nil)
			   (context *context*)
			   english)
  "MEMBERS is a list.  Each member is either an ELEMENT-INAME or a list
   whose first element is an ELEMENT-INAME.  For each member create a new
   proper indv node under PARENT.  These new elements are all part of a new
   split.  After processing all members, return the new split.  The
   :CONTEXT arg controls the context of the new type-nodes and the split.
   The optional :INAME and :ENGLISH arguments give the name of the split
   element.  If a member of the MEMBERS list is a list, its first element
   is the iname of the new typeype node and the rest is passed as the
   :ENGLISH argument for the new type.  Note: It is normally assumed that
   proper individuals are distinct, so the split is somewhat redundant, but
   here we create it anyway just to be safe."
  (let ((new-elements nil))
    (dolist (m members)
      ;; Create a type-node for each item in MEMBERS.
      (push
       (new-indv (if (consp m) (car m) m)
		 parent
		 :context context
		 :english (if (consp m)
			      (cdr m)
			      :iname))
       new-elements))
    ;; Create and return the split.
    (new-split (nreverse new-elements)
	       :iname iname
	       :context context
	       :english english)))

(defun new-complete-members (parent members
				    &key
				    (iname nil)
				    (context *context*)
				    english)
  "Like NEW-MEMBERS, but we create a complete split, indicating that we
   believe this set of members to be complete."
  (let ((new-elements nil))
    (dolist (m members)
      ;; Create a type-node for each item in MEMBERS.
      (push
       (new-indv (if (consp m) (car m) m)
		 parent
		 :context context
		 :english (if (consp m)
			      (cdr m)
			      :iname))
       new-elements))
    ;; Create and return the split.
    (new-complete-split parent
			(nreverse new-elements)
			:iname iname
			:context context
			:english english)))

;;; ========================================================================
;;; Defined Classes

;;; &&& Fix this.

;;; Note: This function is just a place-holder for now.  It creates a class
;;; and saves away a definition, but the machinery does not yet exist to
;;; detect subtypes and instances that fit the definition.

(defun new-intersection-type (iname parents
				    &key
				    (context *context*) 
				    english)
  "PARENTS is a list of type-nodes.  Creates and returns a new type with
   name INAME that represent, by definition, the intersection of the parent
   types."
  (when (< (length parents) 2)
    (error "Parent list for ~S must have two or more elements." iname))
  (setq parents (mapcar #'lookup-element-test parents))
  (let ((node (new-type iname (car parents)
			:definition (cons :intersection parents)
			:context context
			:english english)))
    (dolist (p (cdr parents))
      (new-is-a node p))
    node))

(defun new-union-type (iname  subtypes
			      &key
			      (parent *thing*)
			      (context *context*)
			      english)
  "SUBTYPES is a list of type-nodes or individuals.  Creates and returns a
   new type that is, by definition, the union of these subtypes.  The
   must-be-one-of-these constraint is implemented using a complete-split.
   The user may optionally supply a :PARENT for this type."
  (when (< (length subtypes) 2)
    (error "Subtype list for ~S must have two or more elements." iname))
  (setq subtypes (mapcar #'lookup-element-test subtypes))
  (let* ((node (new-type iname parent
			 :definition (cons :union subtypes)
			 :context context
			 :english english)))
    (new-complete-split node subtypes)
    node))


;;; ***************************************************************************
;;; Marker Scans
;;; ***************************************************************************

;;; These operations must be as fast as possible.
(declaim (optimize (speed 3) (space 0) (safety 0)))

;;; ========================================================================
;;; Macros for internal use.

(defmacro active-element? (e)
  "Internal macro to determine whether element E is active in the current
   context.  IGNORE-CONTEXT must be lexically visible.  If IGNORE-CONTEXT
   is T, the element is always active."
  `(or ignore-context
    (let ((e-context (context-wire ,e)))
      (and e-context
	   (all-bits-on? (bits e-context) *context-mask*)))))

(defmacro unblocked-element? (e)
  "Internal macro to determine whether element E is unblocked.  BLOCKING-MASK must
   be set up and lexically visible."
  `(all-bits-off? (bits ,e) blocking-mask))

(defmacro active-unblocked-element? (e)
  "Internal macro to determine if an element is both active and unblocked."
  `(and (active-element? ,e) (unblocked-element? ,e)))

(defmacro active-map-link? (e)
  "Internal macro to determine whether element E is a map link that is
   active in the current context."
  `(and (map-link? ,e) (active-element? ,e)))

(defmacro markable-element? (e)
  "Internal macro to determine whether element E is not blocked and does
   not already have marker M.  BLOCKING-M-MASK must be set up and lexically
   visible."
  `(all-bits-off? (bits ,e) blocking-m-mask))


;;; ========================================================================
;;; IS-A Hierarchy Scans

(defmacro h-upscan (wire)
  "Internal macro to upscan from an A, B, or C node of the H-element Z (a
   relation or statement) to the corresponding node of the H-element's
   parent. We have already verified that Z is an H-link.  WIRE is one of
   A-WIRE, B-WIRE, or C-WIRE."
  `(let* ((h-parent (parent-wire z))
	  (target nil))
     (when (and h-parent
		(h-link? h-parent)
		(active-element? h-parent))
       (setq target (,wire h-parent))
       (when (and target (markable-element? target))
	 (faster-mark target m)))))

(defmacro h-downscan (wire)
  "Internal macro to downscan from an A, B, or C node of the H-element Z (a
   relation or statement) to the corresponding nodes of the H-element's
   children. We have already verified that Z is an h-link.  WIRE in one of
   A-WIRE, B-WIRE, or C-WIRE."
  `(dolist (h-child (incoming-parent-wires z))
     (when (and (h-link? h-child)
		(active-element? h-child))
       (let ((target (,wire h-child)))
	 (when (and target (markable-element? target))
	   (faster-mark target m))))))

(defmacro scan-loop (&body body)
  "Macro to supply the code that is common among the various scan routines.
   Assumes that the following variables are bound in the caller: M,
   FOCUS-M, AFTER, and ONE-STEP.  This macro interates over M-marked
   elements, binding each to E and executing BODY for each."
  `(progn
     (if focus-m (check-legal-marker-pair focus-m))
     ;; Set up some variables and bit masks for use during the scan.
     (let* (;; The last element marked with M before we begin scanning.
	    (last-m)
	    ;; The bit mask for marker M.
	    (m-mask (marker-bit m))
	    ;; The cancel marker for M.
	    (m-cancel (cancel-marker m))
	    ;; The bit mask for M-CANCEL.
	    (m-cancel-mask (marker-bit m-cancel))
	    ;; Masks for the gate marker.  May be 0.
	    (focus-mask (if focus-m (marker-bit focus-m) 0))
	    (gate-cancel-mask 
	     (if focus-m (marker-bit (cancel-marker focus-m)) 0))
	    ;; If any of these bits are set, the node or link cannot
	    ;; be marked.
	    (blocking-mask
	     (logior *context-cancel-mask*
		     m-cancel-mask
		     gate-cancel-mask))
	    ;; Like BLOCKING-MASK, but also has marker M's bit set.
	    (blocking-m-mask
	     (logior blocking-mask m-mask)))
       (declare (fixnum m-mask m-cancel m-cancel-mask
			focus-mask gate-cancel-mask
			blocking-mask blocking-m-mask))
       ;; Note the last element marked with M before we begin.
       (setq last-m  (svref *last-marked-element* m))
       ;; Scan all the M-marked elements, looking for opportunities to
       ;; propagate M in the desired direction.  Note that the chain of
       ;; elements marked with M may grow ahead of us, but we'll reach the
       ;; end eventually because there are a finite number of elements and
       ;; we never mark an element that is already marked.
       (do-marked (e m after)
	 ,@body
	 ;; If :ONE-STEP and ELEMENT was the last element marked with M
	 ;; going in, quit now.
	 (when (and one-step (eq e last-m))
	   (return (fast-marker-count m)))))
     (fast-marker-count m))) 

(defun upscan-internal (m focus-m after one-step ignore-context)
  "Internal function to perform a single upscan, not activating
   descriptions.  This is a non-keyword function, sacrificing some clarity
   for speed."
  (declare (fixnum m))
  (scan-loop
   ;; Check incoming A wires for various link types.
   (dolist (z (incoming-a-wires e))
     (let ((z-flags (flags z)))
       (declare (fixnum z-flags))
       ;; Check that the link is active and unblocked.
       (when (active-unblocked-element? z)
	 (cond
	  ;; Is this link a negation or CANCEL link?
	  ((all-bits-on? z-flags cancel-up-mask)
	   ;; Yes, mark the target node as cancelled.
	   (let ((target (b-wire z)))
	     (when target
	       (fast-mark target m-cancel))))
	  ;; Is this an IS-A or EQ link?
	  ((all-bits-on? z-flags up-mask)
	   (let ((target (b-wire z)))
	     ;; Is the target unblocked and not yet marked?
	     (when (and target
			(markable-element? target))
	       ;; Yes, mark the target with marker M.
	       (faster-mark target m))))
	  ;; Is this a MAP link with the gate marker on its C-wire?
	  ((and focus-m
		(map-link? z)
		(all-bits-on? (bits (c-wire z)) focus-mask))
	   ;; Yes, mark the element on the B-wire.
	   (let ((target (b-wire z)))
	     (when (and target (markable-element? target))
	       (faster-mark target m))))
	  ;; Is Z an H-element?
	  ((all-bits-on? z-flags h-mask)
	   (h-upscan a-wire))))))
   ;; Check and mark the parent element of E.
   (let ((target (parent-wire e)))
     (when (markable-element? target)
       (faster-mark target m)))
   ;; Check incoming B wires for EQ, MAP, and H-links.
   (dolist (z (incoming-b-wires e))
     (let ((z-flags (flags z)))
       (declare (fixnum z-flags))
       ;; Check that the link is active, unblocked.
       (when (active-unblocked-element? z)
	 (cond
	  ;; Is this an EQ link?
	  ((all-bits-on? z-flags eq-mask)
	   (let ((target (a-wire z)))
	     ;; Is the target unblocked and not yet marked?
	     (when (and target (markable-element? target))
	       ;; Yes, mark the target with M.
	       (faster-mark target m))))
	  ;; Is this a MAP link with the gate marker on its C-wire?
	  ((and focus-m
		(map-link? z)
		(all-bits-on? (bits (c-wire z)) focus-mask))
	   ;; Yes, mark the element on the A-wire.
	   (let ((target (a-wire z)))
	     (when (and target (markable-element? target))
	       (faster-mark target m))))
	  ;; Is Z an H-element?
	  ((all-bits-on? z-flags h-mask)
	   (h-upscan b-wire))))))
   ;; Check incoming C wires for H-links.
   (dolist (z (incoming-c-wires e))
     (let ((z-flags (flags z)))
       (declare (fixnum z-flags))
       ;; Is Z active, unblocked, and an H-link?
       (when (and (all-bits-on? z-flags h-mask)
		  (active-unblocked-element? z))
	 (h-upscan c-wire))))))

(defun downscan-internal (m focus-m after one-step ignore-context)
  "Internal function to perform a single upscan, not activating
   descriptions.  This is a non-keyword function, sacrificing some clarity
   for speed."
  (declare (fixnum m))
  (scan-loop
   ;; Check incoming B wires for various link types.
   (dolist (z (incoming-b-wires e))
     (let ((z-flags (flags z)))
       (declare (fixnum z-flags))
       ;; Check that the link is active and unblocked.
       (when (active-unblocked-element? z)
	 (cond
	  ;; Is this link a negation or CANCEL link?
	  ((all-bits-on? z-flags cancel-up-mask)
	   ;; Yes, mark the target node as cancelled.
	   (let ((target (a-wire z)))
	     (when target
	       (fast-mark target m-cancel))))
	  ;; Is this an IS-A, EQ, or MAP link?
	  ((all-bits-on? z-flags up-mask)
	   (let ((target (a-wire z)))
	     ;; Is the target unblocked and not yet marked?
	     (when (and target
			(markable-element? target))
	       ;; Yes, mark the target with marker M.
	       (faster-mark target m))))
	  ;; Is this a MAP link with the gate marker on its C-wire?
	  ((and focus-m
		(map-link? z)
		(all-bits-on? (bits (c-wire z)) focus-mask))
	   ;; Yes, mark the element on the A-wire.
	   (let ((target (a-wire z)))
	     (when (and target (markable-element? target))
	       (faster-mark target m))))
	  ;; Is Z an H-element?
	  ((all-bits-on? z-flags h-mask)
	   (h-downscan b-wire))))))
   ;; Check and mark the elements whose parent-wires connect to E.
   ;; Cross parent wires of map-nodes in downward direction only.
   (dolist (target (incoming-parent-wires e))
     (when (markable-element? target)
       (faster-mark target m)))
   ;; Check incoming A wires for EQ, MAP and H-links.
   (dolist (z (incoming-a-wires e))
     (let ((z-flags (flags z)))
       (declare (fixnum z-flags))
       ;; Check that the link is active, unblocked.
       (when (active-unblocked-element? z)
	 (cond
	  ;; Is this an EQ-link?
	  ((all-bits-on? z-flags eq-mask)
	   (let ((target (b-wire z)))
	     ;; Is the target unblocked and not yet marked?
	     (when (and target (markable-element? target))
	       ;; Yes, mark the target with M.
	       (faster-mark target m))))
	  ;; Is Z an H-element?
	  ((all-bits-on? z-flags h-mask)
	   (h-downscan a-wire))
	  ;; Is this a MAP link with the gate marker on its C-wire?
	  ((and focus-m
		(map-link? z)
		(all-bits-on? (bits (c-wire z)) focus-mask))
	   ;; Yes, marker the element on the B-wire.
	   (let ((target (b-wire z)))
	     (when (and target (markable-element? target))
	       (faster-mark target m))))))))
   ;; Check incoming C wires for H-links.
   (dolist (z (incoming-c-wires e))
     (let ((z-flags (flags z)))
       (declare (fixnum z-flags))
       ;; Is Z active, unblocked, and an H-link?
       (when (and (all-bits-on? z-flags h-mask)
		  (active-unblocked-element? z))
	 (h-downscan c-wire))))))

(defun eq-scan-internal (m focus-m after one-step ignore-context)
  "Internal function to perform a single eq-scan, not activating
   descriptions.  This is a non-keyword function, sacrificing some clarity
   for speed."
  (declare (fixnum m))
  (scan-loop
   ;; Check incoming A wires for EQ, MAP and H-links.
   (dolist (z (incoming-a-wires e))
     (let ((z-flags (flags z)))
       (declare (fixnum z-flags))
       ;; Check that the link is active, unblocked.
       (when (active-unblocked-element? z)
	 (cond
	  ;; Is this an EQ-link?
	  ((all-bits-on? z-flags eq-mask)
	   (let ((target (b-wire z)))
	     ;; Is the target unblocked and not yet marked?
	     (when (and target (markable-element? target))
	       ;; Yes, mark the target with M.
	       (faster-mark target m))))
	  ;; Is this a MAP link with the gate marker on its C-wire?
	  ((and focus-m
		(map-link? z)
		(all-bits-on? (bits (c-wire z)) focus-mask))
	   ;; Yes, marker the element on the B-wire.
	   (let ((target (b-wire z)))
	     (when (and target (markable-element? target))
	       (faster-mark target m))))))))
   ;; Check incoming B wires for active EQ or MAP links.
   (dolist (z (incoming-b-wires e))
     (let ((z-flags (flags z)))
       (declare (fixnum z-flags))
       ;; Check that the link is active, unblocked.
       (when (active-unblocked-element? z)
	 (cond
	  ;; Is this a MAP link with the gate marker on its C-wire?
	  ((and focus-m
		(map-link? z)
		(all-bits-on? (bits (c-wire z)) focus-mask))
	   ;; Yes, mark the element on the A-wire.
	   (let ((target (a-wire z)))
	     (when (and target (markable-element? target))
	       (faster-mark target m))))
	  ;; Is this an EQ link?
	  ((all-bits-on? z-flags eq-mask)
	   (let ((target (a-wire z)))
	     ;; Is the target unblocked and not yet marked?
	     (when (and target (markable-element? target))
	       ;; Yes, mark the target with M.
	       (faster-mark target m))))))))))

(defun lowermost? (e m &optional ignore-context)
  "Assume element E is marked with marker M.  Determine whether there
   are any other M-marked elements among E's immediate children."
  (dolist (child (incoming-parent-wires e))
    (when (fast-marker-on? child m)
      (return-from lowermost? nil)))
  (dolist (link (incoming-b-wires e))
    (let ((a-element nil))
      (and (is-a-link? link)
	   (active-element? link)
	   (setq a-element (a-wire link))
	   (fast-marker-on? a-element m)
	   (not (eq a-element e))
	   (return-from lowermost? nil))))
  t)

(defmacro x-scan (internal-scan)
  "Internal macro to provide the elements common to UPSCAN, DOWNSCAN, and
   EQ-SCAN.  Expects to be in a lexical context where the following
   variables are bound: M, AUGMENT, ONE-STEP, BASIC, IGNORE-CONTEXT
   and RECURSIVE-MASK."
  `(progn
    (check-legal-marker-pair m)
    (unless augment
      (clear-marker-pair m))
    (cond
      ;; If there's a start element, mark it.
      (start-element
       (setq start-element (lookup-element-test start-element))
       (mark start-element m))
      ;; This case is OK.
      ((and augment (> (fast-marker-count m) 0)))
      ;; Not OK.  Complain to the user.
      (t (error "If you don't supply a start-element, you must ~
		 set :AUGMENT and mark at least one element with M.")))
    ;; First call the INTERNAL-SCAN to do a simple scan with M.
    (,internal-scan m nil nil one-step ignore-context)
    ;; Now maybe activate containing descriptions individually and
    ;; continue the scan.
    (unless (or basic one-step)
      ;; Examine all elements E marked with M.
      (do-marked (e m)
	;; Look for map-links attached above E.
	(dolist (link (incoming-a-wires e))
	  (when (active-map-link? link)
	    ;; Found one.  Focus on the description.
	     (let ((description (c-wire link)))
	       ;; Don't upscan a description we're already upscanning.
	       (when (and description
			  (not (any-bits-on? (bits description)
					     recursive-mask)))
		 ;; Found a description to check.  See if we have
		 ;; enough markers.
		 (unless
		     (not-enough-markers
		      2
		      "Too many nested descriptions to do a full scan. ~
		       Continuing...~%")
		   ;; Allocate the markers we will need.
		   ;; We use M1 to mark and upscan each description we
		   ;; want to examine.  We use M2 to continue the
		   ;; original scan in this description.
		   (with-temp-markers (m1 m2)
		     ;; Do a full upscan with M1.  Might recurse.
		     (upscan description m1
			     :ignore-context ignore-context
			     :recursive-mask (logior (marker-bit m)
						     recursive-mask))
		     ;; Now whatever-scan M2 from E within this focus.
		     (mark e m2)
		     (,internal-scan m2 m1 nil nil ignore-context)
		     ;; Convert M2 to M and clear M1.
		     ;;Note: This may add more M-marked elements that we will
		     ;;check as the outer loop continues.
		     (convert-marker m2 m)
		     (clear-marker m1)))))))))
      (fast-marker-count m)))

(defun upscan (start-element m
			     &key
			     (augment nil)
			     (one-step nil)
			     (basic nil)
			     (ignore-context nil)
			     (recursive-mask 0))
  "M is a user-marker.  Mark START-ELEMENT with marker M and do a full
   upscan, propagating M upward to supertypes of START-ELEMENT.

   If :AUGMENT is non-T, add new M markers, but don't clear any
   existing ones.  If START-ELEMENT is NIL but :AUGMENT is T, just
   propagate existing M markers.  (It is an error if no M-markers exist.)
   
   If :ONE-STEP is T, propagate across only one level of links or
   wires, then propagate no further.
   
   If :BASIC or :ONE-STEP is T, just do a simple scan.  Don't activate
   descriptions one-by-one.
   
   If :IGNORE-CONTEXT is T, propagate across links regardless of whether
   the link is in an active context.

   If :RECURSIVE-MASK is non-zero, we are in a recursive scan.  This is
   a bit mask showing which markers are already involved in the scan.  To
   avoid infinite recursion, we don't recursively scan any description node
   that has one of these bits on it.

   Returns the number of elements marked with M after the scan."
  (declare (fixnum m recursive-mask))
  (x-scan upscan-internal))

(defun downscan (start-element m
			       &key
			       (augment nil)
			       (one-step nil)
			       (basic nil)
			       (ignore-context nil)
			       (recursive-mask 0))
  "M is a user-marker.  Mark START-ELEMENT with marker M and do a full
   downscan, propagating M downward to subtypes of START-ELEMENT.
   
   If :AUGMENT is T, add new M markers, but don't clear any
   existing ones.  If START-ELEMENT is NIL but :AUGMENT is T, just
   propagate existing M markers.  (It is an error if no M-markers exist.)
   
   If :ONE-STEP is T, propagate across only one level of links or
   wires, then propagate no further.
   
   If :BASIC or :ONE-STEP is T, just do a simple scan.  Don't activate
   descriptions one-by-one.
   
   If :IGNORE-CONTEXT is T, propagate across links regardless of whether
   the link is in an active context.
   
   If :RECURSIVE-MASK is non-zero, we are in a recursive scan.  This is
   a bit mask showing which markers are already involved in the scan.  To
   avoid infinite recursion, we don't recursively scan any description node
   that has one of these bits on it.

   Returns the number of elements marked with M after the scan."
  (declare (fixnum m recursive-mask))
  (x-scan downscan-internal))

(defun eq-scan (start-element m
			      &key
			      (augment nil)
			      (one-step nil)
			      (basic nil)
			      (ignore-context nil)
			      (recursive-mask 0))
  "M is a user-marker.  Mark START-ELEMENT with marker M, then propagate M
   to equivalent nodes, not upward or downward.

   If :AUGMENT is T, add new M markers, but don't clear any
   existing ones.  If START-ELEMENT is NIL but :AUGMENT T, just
   propagate existing M markers.  (It is an error if no M-markers exist.)
   
   If :ONE-STEP is T, propagate across only one level of links or
   wires, then propagate no further.
   
   If :BASIC or :ONE-STEP is T, just do a simple scan.  Don't activate
   descriptions one-by-one.
   
   If :IGNORE-CONTEXT is T, propagate across links regardless of whether
   the link is in an active context.
   
   If :RECURSIVE-MASK is non-zero, we are in a recursive scan.  This is
   a bit mask showing which markers are already involved in the scan.  To
   avoid infinite recursion, we don't recursively scan any description node
   that has one of these bits on it.
   
   Returns the number of elements marked with M after the scan."
  (declare (fixnum m recursive-mask))
  (x-scan eq-scan-internal))

;;; Specialized downscan for use in marking the owners of a role.

(defmacro h-downscan1 (wire)
  "Similar to H-DOWNSCAN macro, but restricts propagation of M2 to
   M1-marked nodes."
  `(dolist (h-child (incoming-parent-wires z))
     (when (and (h-link? h-child)
		(active-element? h-child))
       (let ((target (,wire h-child)))
	 (when (and target
		    (fast-marker-on? target m1)
		    (markable-element? target))
	   (faster-mark target m2))))))

(defun role-owner-downscan (m1 m2 m3 ignore-context)
  "Similar to INTERNAL-DOWNSCAN.  Downscans M2 from X, marking only nodes
   that already have M1 on them.  We cross all active map-links in the
   downward (B-to-A) direction, marking the owner with M3 as we cross."
  (declare (fixnum m1 m2 m3))
  ;; Set up some variables and bit masks for use during the scan.
  (let* (;; The bit mask for marker M.
	 (m2-mask (marker-bit m2))
	 ;; The cancel marker for M.
	 (m2-cancel (cancel-marker m2))
	 ;; The bit mask for M-CANCEL.
	 (m2-cancel-mask (marker-bit m2-cancel))
	 ;; If any of these bits are set, the node or link cannot
	 ;; be marked.
	 (blocking-mask
	  (logior *context-cancel-mask* m2-cancel-mask))
	 ;; Like BLOCKING-MASK, but also has marker M's bit set.
	 (blocking-m-mask
	  (logior blocking-mask m2-mask)))
    (declare (fixnum m2-mask m2-cancel m2-cancel-mask
		     blocking-mask blocking-m-mask))
    ;; Scan all the M2-marked elements, looking for opportunities to
    ;; propagate M2 downward.  Note that the chain of
    ;; elements marked with M2 may grow ahead of us, but we'll reach the
    ;; end eventually because there are a finite number of elements and
    ;; we never mark an element that is already marked.
    (do-marked (e m2)
      ;; Check incoming B wires for various link types.
      (dolist (z (incoming-b-wires e))
	(let ((z-flags (flags z)))
	  (declare (fixnum z-flags))
	  ;; Check that the link is active and unblocked.
	  (when (active-unblocked-element? z)
	    (cond
	      ;; Is this link a negation or CANCEL link?
	      ((all-bits-on? z-flags cancel-up-mask)
	       ;; Yes, mark the target node as cancelled.
	       (let ((target (a-wire z)))
		 (when target
		   (fast-mark target m2-cancel))))
	      ;; Is this an IS-A or EQ link?
	      ((all-bits-on? z-flags up-mask)
	       (let ((target (a-wire z)))
		 ;; Is the target unblocked, marked with M1, and not
		 ;; yet marked with M2?
		 (when (and target
			    (fast-marker-on? target m1)
			    (markable-element? target))
		   ;; Yes, mark the target with marker M2.
		   (faster-mark target m2))))
	      ;; Is this an MAP link?
	      ((map-link? z)
	       ;; Yes, mark the element on the A-wire.
	       (let ((target (a-wire z))
		     (owner (c-wire z)))
		 (when (and target
			    owner
			    (fast-marker-on? target m1)
			    (markable-element? target))
		   (faster-mark target m2)
		   (fast-mark owner m3))))
	      ;; Is Z an H-element?
	      ((all-bits-on? z-flags h-mask)
	       (h-downscan1 b-wire))))))
      ;; Check and mark the elements whose parent-wires connect to E.
      ;; Cross parent wires of map-nodes in downward direction only.
      (dolist (target (incoming-parent-wires e))
	(when (and (markable-element? target))
	  (faster-mark target m2)))
      ;; Check incoming A wires for EQ and H-links.
      (dolist (z (incoming-a-wires e))
	(let ((z-flags (flags z)))
	  (declare (fixnum z-flags))
	  ;; Check that the link is active, unblocked.
	  (when (active-unblocked-element? z)
	    (cond
	      ;; Is this an EQ-link?
	      ((all-bits-on? z-flags eq-mask)
	       (let ((target (b-wire z)))
		 ;; Is the target unblocked and not yet marked?
		 (when (and target (markable-element? target))
		   ;; Yes, mark the target with M.
		   (faster-mark target m2))))
	      ;; Is Z an H-element?
	      ((all-bits-on? z-flags h-mask)
	       (h-downscan1 a-wire))))))
      ;; Check incoming C wires for H-links.
      (dolist (z (incoming-c-wires e))
	(let ((z-flags (flags z)))
	  (declare (fixnum z-flags))
	  ;; Is Z active, unblocked, and an H-link?
	  (when (and (all-bits-on? z-flags h-mask)
		     (active-unblocked-element? z))
	    (h-downscan1 c-wire)))))
    (fast-marker-count m2)))


;;; ========================================================================
;;; Boolean operations over the whole KB.

(defun mark-boolean (m must-be-set must-be-clear
		       &optional
		       (flags-set 0)
		       (flags-clear 0))
  "M is a marker.  MUST-BE-SET and MUST-BE-CLEAR are lists of markers
   indicated by name or by number.  Scan all elements in the KB.  If an
   element has all the MUST-BE-SET bits on and all the MUST-BE-CLEAR bits
   off, set marker M on that element.  If supplied the FLAGS-SET and
   FLAGS-CLEAR are integer masks indicating which flag bits in the element
   must be on or off."
  (declare (fixnum m flags-set flags-clear))
  (check-legal-marker m)
  (let ((set-mask (make-mask must-be-set))
	(clear-mask (make-mask must-be-clear)))
    (declare (fixnum set-mask clear-mask))
    (if (not (= 0 set-mask))
	;; We know that elements of interest have at least one mark set.
	;; Save time by scanning along the shortest marker chain.
	(let ((shortest 0)
	      (length (1+ (the fixnum *n-elements*))))
	  (declare (fixnum shortest length))
	  ;; Find the shortest marker chain to scan.
	  (dotimes (mm n-markers)
	    (declare (fixnum mm))
	    (if (not (= 0 (logand set-mask (marker-bit mm))))
		(let ((n-marked (fast-marker-count mm)))
		  (declare (fixnum n-marked))
		  ;; If any of the "must be set" markers is not on any
		  ;; elements, we can go home early.
		  (cond ((= 0 n-marked)
			 (return-from mark-boolean nil))
			((< n-marked length)
			 (setq shortest mm)
			 (setq length n-marked))))))
	  (do-marked (e shortest)
	    (let ((ebits (bits e))
		  (fbits (flags e)))
	      (declare (fixnum ebits))
	      (if (and (all-bits-on? ebits set-mask)
		       (all-bits-off? ebits clear-mask)
		       (all-bits-on? fbits flags-set)
		       (all-bits-off? fbits  flags-clear))
		  (fast-mark e m)))))
	;; Elements of interest may have no marks set, so we must scan
	;; all elements in the KB.
	(do-elements (e)
	  (let ((ebits (bits e))
		(fbits (flags e)))
	    (declare (fixnum ebits fbits))
	    ;; Don't check set-mask, since it is zero.
	    (if (and (all-bits-off? ebits clear-mask)
		     (all-bits-on? fbits flags-set)
		     (all-bits-off? fbits flags-clear))
		(fast-mark e m))))))
  (fast-marker-count m))

(defun unmark-boolean (m must-be-set must-be-clear
			 &optional
			 (flags-set 0)
			 (flags-clear 0))
  "M is a marker.  MUST-BE-SET and MUST-BE-CLEAR are lists of markers
   indicated by name or by number.  Scan all elements in the KB.  If an
   element has all the MUST-BE-SET bits on and all the MUST-BE-CLEAR bits
   off, remove marker M from that element.  If supplied the FLAGS-SET and
   FLAGS-CLEAR are integer masks indicating which flag bits in the element
   must be on or off."
  (declare (fixnum m flags-set flags-clear))
  (let ((set-mask (make-mask must-be-set))
	(clear-mask (make-mask must-be-clear))
	(unmark-me nil))
    (declare (fixnum set-mask clear-mask))
    ;; Just scan the set of elements marked with M, looking for the other
    ;; markers and flags.
    (do-marked (e m)
      (when unmark-me
	(faster-unmark unmark-me m)
	(setq unmark-me nil))
      (let ((ebits (bits e))
	    (fbits (flags e)))
	(declare (fixnum ebits fbits))
	(when (and (all-bits-on? ebits set-mask)
		   (all-bits-off? ebits clear-mask)
		   (all-bits-on? fbits flags-set)
		   (all-bits-off? fbits flags-clear))
	  (setq unmark-me e))))
    (when unmark-me (faster-unmark unmark-me m)))
  (fast-marker-count m))

(defun mark-all (m)
  "Mark all elements with marker M."
  (do-elements (e)
    (fast-mark e m)))

;;; Split checking.

(defun splits-ok? (m &optional ignore-context)
  "Some set of elements has been marked with M and upscanned.  Look for
   splits that have M on more than one of the disjoint items.  If there are
   none, return T, meaning OK.  If we find an unhappy split, return NIL,
   returning the unhappy split element as the second return value."
  (declare (fixnum m))
  (check-legal-marker-pair m)
  (let ((blocking-mask (logior *context-cancel-mask*
			       (cancel-marker m))))
    (declare (fixnum blocking-mask))
    (do-marked (e m)
      (dolist (split (incoming-split-wires e))
	(when (active-unblocked-element? split)
	  (let ((count 0))
	    (declare (fixnum count))
	    (dolist (item (split-wires split))
	      (when (fast-marker-on? item m)
		(incf count)))
	    (when (> count 1)
	      (return-from splits-ok?
			   (values nil split)))))))
    t))

;;; ========================================================================
;;; Marker scans for relations and statements.

(defmacro cross-rel (m-source m-rel m-target reverse)
  "Cross active and marked REL-LINKs.  If is T, cross B-to-A; if NIL, cross
   A-to-B."
  `(let* ((blocking-mask 
	   (logior *context-cancel-mask*
		   (make-mask (list (cancel-marker ,m-source)
				    (cancel-marker ,m-rel)))))
	  (blocking-m-mask (logior blocking-mask (marker-bit ,m-target)))
	  (target nil))
     (declare (fixnum blocking-mask blocking-m-mask))
     ;; Scan all elements with source-marker.
     (do-marked (x ,m-source)
       ;; Cross statement links under REL in desired direction, marking
       ;; target with M-TARGET.
       (dolist (link (,(if reverse 'incoming-b-wires 'incoming-a-wires)
		      x))
	 (when (and (fast-marker-on? link ,m-rel)
		    (statement? link)
		    (active-unblocked-element? link)
		    (setq target (,(if reverse 'a-wire 'b-wire)
				  link))
		    (markable-element? target))
	   (fast-mark target ,m-target))))))

(defun mark-rel (a rel m &key (downscan t) (ignore-context nil))
  "A is any element.  REL is a relation node.  Place marker M on all
   elements B such that (A REL B).  Downscan the M markers unless :DOWNSCAN
   is explicitly NIL.  Returns the number of elements ultimately marked
   with M."
  (mark-rel-internal a rel m t nil downscan ignore-context))

(defun mark-rel-inverse (m rel B &key (downscan t) (ignore-context nil))
  "B is any element.  REL is a relation node.  Place marker M on all
   elements A such that (A REL B).  Downscan the M markers unless :DOWNSCAN
   is explicitly NIL.  Returns the number of elements ultimately marked
   with M."
  (mark-rel-internal b rel m nil t downscan ignore-context))

(defun mark-rel-internal (a rel m fwd rev downscan ignore-context)
  (check-legal-marker-pair m)
  (clear-marker-pair m)
  (setq a (lookup-element-test a))
  (setq rel (lookup-element-test rel))
  (when (symmetric? rel)
    (setq fwd t)
    (setq rev t))
  ;; We need at least two new markers for the basic scan.
  (unless (not-enough-markers
	   2
	   "No free markers for MARK-REL.  No elements marked.")
    ;; Allocate the markers.
    (with-temp-markers (m-a m-rel)
      ;; Do a full downscan from REL using M-REL.
      (downscan rel m-rel :ignore-context ignore-context)
      ;; Simple upscan from A using M-A.
      (mark a m-a)
      (upscan-internal m-a nil nil nil ignore-context)
      ;; Cross any marked rel-links A-to-B.
      (when fwd (cross-rel m-a m-rel m nil))
      ;; Cross any marked rel-links B-to-A.
      (when rev (cross-rel m-a m-rel m t))
      ;; Now explore the descriptions in which A plays a role.  We need 3
      ;; new markers for this.
      (unless (not-enough-markers
	       3
	       "Not enough markers for full search of relations.  Continuing...")
	;; Allocate the markers.
	(with-temp-markers (m-desc m-source m-target)
	  (do-marked (source m-a)
	    ;; Look for map-links attached above X.
	    (dolist (link (incoming-a-wires source))
	      (when (active-map-link? link)
		;; Found one.  Focus on the description.
		(let ((description (c-wire link)))
		  (when description
		    ;; Do a full upscan with M-DESC.  Might recurse.
		    (upscan description m-desc :ignore-context ignore-context)
		    ;; Now upscan from SOURCE within this focus, using M-SOURCE.
		    (mark source m-source)
		    (upscan-internal m-source m-desc nil nil ignore-context)
		    ;; Cross any marked rel-links A-to-B.
		    (when fwd (cross-rel m-source m-rel m-target nil))
		    ;; Cross any marked rel-links B-to-A.
		    (when rev (cross-rel m-source m-rel m-target t))
		    ;; Now eq-scan or downscan M-TARGET with M-DESC as focus.
		    (if downscan
			(downscan-internal m-target m-desc nil nil ignore-context)
			(eq-scan-internal m-target m-desc nil nil ignore-context))
		    ;; Convert M-TARGET to M and clear M-SOURCE.
		    (convert-marker m-target m)
		    (clear-marker m-source))))))))
      ;; Final eq-scan or downscan of M.
      (when (> (fast-marker-count m) 0)
	(if downscan
	    (downscan nil m :augment t :ignore-context ignore-context)
	    (eq-scan nil m :augment t :ignore-context ignore-context)))))
  (fast-marker-count m))

;;; &&& Add scans for transitive relations.

;;; ***************************************************************************
;;; Queries and Predicates
;;; ***************************************************************************

;;; These operations must be as fast as possible.
(declaim (optimize (speed 3) (space 0) (safety 0)))

(defun is-x-a-y? (x y)
  "Predicate to determine whether type or individual X is known to be of
   type Y in the current context."
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (with-temp-markers (m1)
    (upscan x m1)
    (marker-on? y m1)))

(defun is-x-eq-y? (x y)
  "Predicate to determine whether node X is known to be identical to
   node Y in the current context."
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (with-temp-markers (m1)
    (eq-scan x m1)
    (marker-on? y m1)))

(defun can-x-be-a-y? (x y)
  "Predicate to determine whether it would be legal to state that X is a Y.
   That is, would this violate any splits or other constraints?  If it is
   OK, return T.  If it is not, return NIL, with the unhappy split or
   constraint as the second return value."
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (with-temp-markers (m1)
    ;; Mark both X and Y with M1, then upscan.
    (mark x m1)
    (upscan y m1 :augment t)
    ;; If Y is blocked by X or vice vesra, the answer must be no.
    (cond ((marker-on? y (cancel-marker m1))
	   (values nil y))
	  ((marker-on? x (cancel-marker m1))
	   (values nil x))
	  ;; Else, see if all the splits are OK.
	  (t (splits-ok? m1)))))

;;; NOTE: For now, CAN-X-EQ-Y? is the same as CAN-X-BE-A-Y?, but this might
;;; change once we start checking more constraints, such as cardinality.

(defun can-x-eq-y? (x y)
  "Predicate to determine whether it would be legal to put an EQ link
   between X and Y.  That is, would this violate any splits or other
   constraints?  If this is OK, return T.  If not, return NIL, with the
   unhappy split or contraint as the second return value."
  (can-x-be-a-y? x y))


;;; ***************************************************************************
;;; Mark, List, and Show Functions
;;; ***************************************************************************

;;; Use normal compilation policy, where safety counts as much as speed.
(declaim (optimize (speed 1) (space 1) (safety 1)))

;;; In general, these functions take any valid specifier for element
;;; arguments and check the marker arguments.

;;; In most cases, we have a MARK function and corresponding LIST and SHOW
;;; functions.  The MARK function does the real work, marking whatever set
;;; the user wants to see.

;;; The corresponding LIST function just returns the specified set in the
;;; form of a list, useful for passing to other programs.

;;; ========================================================================
;;; Basic List and Show machinery.

(defun list-elements ()
  "Return a list of all elements in the KB."
  (let ((l nil))
    (do ((e *first-element* (next-element e))
	 (i 0 (1+ i)))
	((null e))
      (push e l))
    (nreverse l)))

(defun list-marked (m)
  "Return a list of all elements marked with marker M."
  (check-legal-marker m)
  (let ((l nil))
    (do ((e (svref *first-marked-element* m)
	    (svref (next-marked-element e) m)))
	((null e))
      (push e l))
    (nreverse l)))

(defun list-not-marked (m)
  "Return a list of all elements marked with marker M."
  (with-temp-markers (m1)
    (mark-boolean m1 () (list m))
    (list-marked m1)))

;;; The SHOW function prints out the set in a human-readable form.  We
;;; don't want these functions to just stupidly print out thousands of
;;; elements unless the user explicitly asks for that.  So in general,
;;; these functions default to printing the first 100 elements in the
;;; requested set, with a notation indicating that there are more.

;;; The :N keyword argument, an integer, specifies the maximum number of
;;; elements to be printed.  The default value is 100.

;;; The :ALL keyword, followed by T, forces printing of *all* the selected
;;; elements.

;;; The :LAST keyword argument, an integer, says to print the last n
;;; selected elements instead of the first n.

;;; Normally you supply only one of :N, :ALL, or :LAST.

;;; The :HEADING keyword is a string to print at the start of the list.

(defun show-elements (&key (n 100)
			   (all nil)
			   (last nil))
  "Print out a table of all elements in the KB."
  (format *show-stream* "~%Elements in the Knowledge Base:~2%")
  (if (and last (integerp last))
      (setq n last))
  (if (or all (null n))
      (setq n *n-elements*))
  (let ((skip 0))
    (when (and last (< n *n-elements*))
      (setq skip (- *n-elements* n))
      (setq n *n-elements*)
      (format *show-stream* "Skipping ~D elements...~%" skip))
    (do ((e *first-element* (next-element e))
	 (i 0 (1+ i)))
	((or (null e) (>= i n))
	 (unless (null e)
	   (format *show-stream* "... and ~D more."
		   (- *n-elements* n))))
      (if (>= i skip)
	  (format *show-stream* "~S~%" e)))
    (values)))

(defun show-marked (m &key (n 100)
		      (all nil)
		      (last nil)
		      (heading nil))
  "Print out a table of all elements with marker M."
  (check-legal-marker m)
  (if heading 
      (format *show-stream* "~%~A~2%" heading)
      (format *show-stream* "~%"))
  ;;; If nothing to print, say so.
  (when (= (marker-count m) 0)
    (format *show-stream* "No elements to print.~%")
    (return-from show-marked (values)))
  (if (and last (integerp last))
      (setq n last))
  (if (or all (null n))
      (setq n *n-elements*))
  (let ((skip 0))
    (when (and last (< n (fast-marker-count m)))
      (setq skip (- (fast-marker-count m) n))
      (setq n *n-elements*)
      (format *show-stream* "Skipping ~D marked elements...~%" skip))
    (do ((e (svref *first-marked-element* m)
	    (svref (next-marked-element e) m))
	 (i 0 (1+ i)))
	((or (null e) (>= i n))
	 (unless (null e)
	   (format *show-stream* "... and ~D more."
		   (- (fast-marker-count m) n))))
      (if (>= i skip)
	  (format *show-stream* "~S~%" e)))
    (values)))

(defun show-not-marked (m &key (n 100)
			  (all nil)
			  (last nil))
  "Print out a table of all elements not marked with marker M."
  (with-temp-markers (m1)
    (mark-boolean m1 () (list m))
    (show-marked m1 :n n :all all :last last)))

;;; ========================================================================
;;; Mark, List, and Show operations relating to the IS-A hierarchy.

(defun mark-children (e m)
  "Mark the immediate descendents of element E."
  (downscan e m :one-step t)
  (unmark e m)
  (fast-marker-count m))

(defun list-children (e)
  "List the immediate child nodes of element E."
  (with-temp-markers (m1)
    (mark-children e m1)
    (list-marked m1)))

(defun show-children (e &key
			(n 100)
			(all nil)
			(last nil))
  "Show the immediate child nodes of element E."
  (with-temp-markers (m1)
    (mark-children e m1)
    (show-marked
     m1 :n n :all all :last last
     :heading (format nil "Immediate Inferiors of ~A:" e))))

(defun mark-parents (e m)
  "Mark the immediate ancestors of element E."
  (upscan e m :one-step t)
  (unmark e m)
  (fast-marker-count m))

(defun list-parents (e)
  "List the immediate parent nodes of element E."
  (with-temp-markers (m1)
    (mark-parents e m1)
    (list-marked m1)))

(defun show-parents (e &key
		       (n 100)
		       (all nil)
		       (last nil))
  "Show the immadiate parent nodes of element E."
  (with-temp-markers (m1)
    (mark-parents e m1)
    (show-marked
     m1 :n n :all all :last last
     :heading (format nil "Immediate Superiors of ~A:" e))))

(defun mark-supertypes (e m)
  "Mark the supertypes of element E in the current context with marker M,
   not including E itself."
  (setq e (lookup-element-test e))
  (upscan e m)
  (unmark e m)
  (fast-marker-count m))

(defun list-supertypes (e)
  "E is an element.  Return a list of all the types containing E, directly
   or indirectly, in the current context."
  (with-temp-markers (m1)
    (mark-supertypes e m1)
    (list-marked m1)))

(defun show-supertypes (e &key
			  (n 100)
			  (all nil)
			  (last nil))
  "E is an element.  Show all the types containing E, directly
   or indirectly, in the current context."
  (with-temp-markers (m1)
    (mark-supertypes e m1)
    (show-marked
     m1 :n n :all all :last last
     :heading (format nil "Supertypes of ~A:" e))))

(defun mark-inferiors (e m)
  "Mark the inferiors of element E in the current context with marker M,
   including both subtypes and individuals, but not E itself."
  (setq e (lookup-element-test e))
  (downscan e m)
  (unmark e m)
  (fast-marker-count m))

(defun list-inferiors (e)
  "E is an element.  Return a list of all the inferiors of E, directly
   or indirectly, in the current context."
  (with-temp-markers (m1)
    (mark-inferiors e m1)
    (list-marked m1)))

(defun show-inferiors (e &key
			 (n 100)
			 (all nil)
			 (last nil))
  "E is an element.  Show all the inferiors of E, directly
   or indirectly, in the current context."
  (with-temp-markers (m1)
    (mark-inferiors e m1)
    (show-marked
     m1 :n n :all all :last last
     :heading (format nil "Inferiors of ~A:" e))))

(defun mark-subtypes (e m)
  "Mark the subtypes of element E in the current context with marker M, not
   including any individuals, or E itself."
  (setq e (lookup-element-test e))
  (with-temp-markers (m1)
    (downscan e m1)
    (unmark e m1)
    (do-marked (x m1)
      (when (type-node? x)
	(mark x m))))
  (fast-marker-count m))

(defun list-subtypes (e)
  "E is an element.  Return a list of all the subtypesof E, directly
   or indirectly, in the current context."
  (with-temp-markers (m1)
    (mark-subtypes e m1)
    (list-marked m1)))

(defun show-subtypes (e &key
			  (n 100)
			  (all nil)
			  (last nil))
  "E is an element.  Show all the subtypes of E, directly
   or indirectly, in the current context."
  (with-temp-markers (m1)
    (mark-subtypes e m1)
    (show-marked
     m1 :n n :all all :last last
     :heading (format nil "Subtypes of ~A:" e))))

(defun mark-instances (e m)
  "Mark the instances of element E in the current context with marker M."
  (setq e (lookup-element-test e))
  (with-temp-markers (m1)
    (downscan e m1)
    (unmark e m1)
    (do-marked (x m1)
      (when (or (proper-indv-node? x)
		(statement? x))
	(mark x m)))
    (fast-marker-count m)))

(defun list-instances (e)
  "E is an element.  Return a list of all the instances of E in the current
   context."
  (with-temp-markers (m1)
    (mark-instances e m1)
    (list-marked m1)))

(defun show-instances (e &key
			 (n 100)
			 (all nil)
			 (last nil))
  "E is an element.  Show all the instances of E, directly
   or indirectly, in the current context."
  (with-temp-markers (m1)
    (mark-instances e m1)
    (show-marked
     m1 :n n :all all :last last
     :heading (format nil "Instances of ~A:" e))))

(defun mark-lowermost (m1 m2)
  "From the set of elements currently marked with M1, look for those that
   have no immediate descendants marked with M1.  Mark these with M2."
  (check-legal-marker-pair m1)
  (check-legal-marker-pair m2)
  (clear-marker-pair m2)
  (do-marked (x m1)
    (when (lowermost? x m1)
      (fast-mark x m2)))
  (fast-marker-count m2))

(defun list-lowermost (m)
  "From the set of elements currently marked with M, list the ones that are
   lowermost in the type hierarchy."
  (with-temp-markers (m1)
    (mark-lowermost m m1)
    (list-marked m1)))

(defun show-lowermost (m &key
			 (n 100)
			 (all nil)
			 (last nil))
  "From the set of elements currently marked with M, showthe ones that are
   lowermost in the type hierarchy."
  (with-temp-markers (m1)
    (mark-lowermost m m1)
    (show-marked
     m1 :n n :all all :last last)))

(defun mark-proper (m1 m2)
  "From the set of elements currently marked with M1, look for the lowermost
   elements that are proper individuals.  Mark these with M2."
  (check-legal-marker-pair m1)
  (check-legal-marker-pair m2)
  (clear-marker-pair m2)
  (do-marked (x m1)
    (when (and (lowermost? x m1)
	       (proper-indv-node? x))
      (fast-mark x m2)))
  (fast-marker-count m2))

(defun list-proper (m)
  "From the set of elements currently marked with M, list any proper
   individual nodes that are lowermost in the type hierarchy."
  (with-temp-markers (m1)
    (mark-proper m m1)
    (list-marked m1)))

(defun show-proper (m &key
		      (n 100)
		      (all nil)
		      (last nil))
  "From the set of elements currently marked with M, show any proper
   individual nodes that are lowermost in the type hierarchy."
  (with-temp-markers (m1)
    (mark-proper m m1)
    (show-marked
     m1 :n n :all all :last last)))

(defun mark-most-specific (m1 m2)
  "From the set of elements currently marked with M1, mark with M2 the ones
   that are the most specific.  That is, mark any proper individual nodes
   and, if none of those are present, the lowermost type or generic nodes."
  (check-legal-marker-pair m1)
  (check-legal-marker-pair m2)
  (clear-marker-pair m2)
  (when (> (fast-marker-count m1) 0)
    (with-temp-markers (m3)
      (mark-lowermost m1 m3)
      (cond
       ((> (mark-boolean m2 (list m3) () proper-indv-mask 0) 0))
       (t (mark-boolean m2 (list m3) nil)))))
  (fast-marker-count m2))

(defun most-specific (m)
  "Return one element that is the most specific one marked with M.  If
   there are more than one equally good candidates, pick one arbitrarily."
  (with-temp-markers (m1)
    (mark-most-specific m m1)
    (svref *first-marked-element* m1)))

(defun list-most-specific (m)
  "From the set of elements currently marked with M1, list the ones that
   are the most specific.  That is, any named individual nodes and, if none
   of those are present, lowermost type or generic nodes."
  (with-temp-markers (m1)
    (mark-most-specific m m1)
    (list-marked m1)))

(defun show-most-specific (m &key
                             (n 100)
                             (all nil)
                             (last nil)
			     heading)
  "From the set of elements currently marked with M, print the ones that
   are the most specific.  That is, any named individual nodes and, if none
   of those are present, lowermost type or map nodes."
  (with-temp-markers (m1)
    (mark-most-specific m m1)
    (show-marked m1 :n n :all all :last last :heading heading)))


;;; ========================================================================
;;; Some additional display functions that print a table of counts
;;; rather than a list of elements.

(defun show-element-counts ()
  "Print a table showing the number of elements of each type."
  (let ((n-loaded-elements 0)
	(n-number 0)
	(n-string 0)
	(n-function 0)
	(n-struct 0)
	(n-proper-indv 0)
	(n-generic-indv 0)
	(n-indv 0)
	(n-type 0)
	(n-is-a 0)
	(n-eq 0)
	(n-map 0)
	(n-cancel 0)
	(n-relation 0)
	(n-statement 0)
	(n-complete-split 0)
	(n-other-split 0)
	(n-negation 0)
	(i 0))
    (do-elements (e)
      (incf i)
      (when (eq e (car *last-loaded-elements*))
	(setq n-loaded-elements i))
      (cond
       ((number-node? e)
	(incf n-number)
	(incf n-indv))
       ((string-node? e)
	(incf n-string)
	(incf n-indv))
       ((function-node? e)
	(incf n-function)
	(incf n-indv))
       ((struct-node? e)
	(incf n-struct)
	(incf n-indv))
       ((proper-indv-node? e)
	(incf n-proper-indv)
	(incf n-indv))
       ((generic-indv-node? e)
	(incf n-generic-indv)
	(incf n-indv))
       ((type-node? e)
	(incf n-type))
       ((is-a-link? e)
	(incf n-is-a))
       ((eq-link? e)
	(incf n-eq))
       ((map-link? e)
	(incf n-map))
       ((cancel-link? e)
	(incf n-cancel))
       ((relation? e)
	(incf n-relation))
       ((statement? e)
	(incf n-statement))
       ((is-not-a-link? e)
	(incf n-negation))
       ((not-eq-link? e)
	(incf n-negation))
       ((not-statement? e)
	(incf n-negation))
       ((complete-split? e)
	(incf n-complete-split))
       ((split? e)
	(incf n-other-split))))
    (format *show-stream*
"~&Knowledge Base Element Counts
	       
Elements: ~20T~10:D
New Elements: ~20T~10:D

Indv Nodes: ~20T~10:D
  Number Nodes: ~20T~10:D
  String Nodes: ~20T~10:D
  Function Nodes: ~20T~10:D
  Struct Nodes: ~20T~10:D
  Proper Indv: ~20T~10:D
  Generic Indv: ~20T~10:D
Type Nodes: ~20T~10:D

Is-A Links: ~20T~10:D
EQ Links: ~20T~10:D
Map Links: ~20T~10:D
Cancel Links: ~20T~10:D
Relations: ~20T~10:D
Statements: ~20T~10:D

Comp Splits: ~20T~10:D
Other Splits: ~20T~10:D

Negations: ~20T~10:D

English Names: ~20T~10:D
"
	    *n-elements*
	    (- *n-elements* n-loaded-elements)
	    n-indv
	    n-number
	    n-string
	    n-function
	    n-struct
	    n-proper-indv
	    n-generic-indv
	    n-type
	    n-is-a
	    n-eq
	    n-map
	    n-cancel
	    n-relation
	    n-statement
	    n-complete-split
	    n-other-split
	    n-negation
	    (hash-table-count *english-dictionary*))
  (values)))

(defun show-marker-counts ()
  "Prints the number of elements marked with each marker."
  (format *show-stream* "~&Marker counts:~%")
  (dotimes (i n-markers)
    (format *show-stream* "~2D: ~D~%" i (fast-marker-count i)))
  (values))


;;; ************************************************************************
;;; Roles, Relations, and Statements
;;; ************************************************************************

;;; Simple access functions.

(defun mark-the-x-of-y (x y m &key ignore-context downscan)
  "X and Y are elements, M is a marker.  Mark with M all the nodes
   collectively representing the X-role of Y.  Return the number of marked
   elements.  If :DOWNSCAN, do a downscan from the role-players instead of
   an eq-scan, marking all instances of the role."
  (declare (fixnum m))
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (check-legal-marker-pair m)
  (clear-marker-pair m)
  ;; Check whether we have an additional marker for the basic scan.
  (unless (not-enough-markers
	   1
	   "No available marker for MARK-THE-X-OF-Y. Returning NIL.")
    ;; Allocate the marker.
    (with-temp-markers (m1)
      ;; Use M1 to upscan from Y.
      (upscan y m1 :ignore-context ignore-context)
      ;; Complain if X is not a direct role of Y or a superior of Y.
      (unless (and (context-wire x)
		   (fast-marker-on? (context-wire x) m1))
	(commentary
	 "~&~S is not a role of ~S.~%" x y)
	(return-from mark-the-x-of-y nil))
      ;; Now mark the X-role with M1 as focus.
      ;; If :ALL, do a downscan.  Else, do an EQ-SCAN.
      (mark x m)
      (if downscan
	  (downscan-internal m m1 nil nil ignore-context)
	  (eq-scan-internal m m1 nil nil ignore-context)))))

(defun mark-all-x-of-y (x y m &key ignore-context)
  "This is just MARK-THE-X-OF-Y with the :DOWNSCAN argument."
  (mark-the-x-of-y x y m :ignore-context ignore-context :downscan t))

(defun mark-the-x-owner-of-y (x y m &key ignore-context)
  "X and Y are elements, M is a marker.  Mark with M all the nodes
   representing elements whose X-role is Y.  Return the number of marked
   elements."
  (declare (fixnum m))
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (check-legal-marker-pair m)
  (clear-marker-pair m)
  ;; Check whether we have the additional markers we need.
  (unless (not-enough-markers
	   3
	   "Not enough markers for MARK-THE-X-OWNER-OF-Y. Returning NIL.")
    ;; Allocate the markers.
    (with-temp-markers (m1 m2 m3)
      ;; Mark all superiors of Y with M1.
      (upscan y m1 :ignore-context ignore-context)
      ;; Complain if X is not a superior of Y.
      (unless (marker-on? x m1)
	(commentary
	 "~&~S is not known to play the ~S role of anything.~%" y x)
	(return-from mark-the-x-owner-of-y nil))
      ;; Now downscan M2 from X, marking only nodes that already have M1.
      ;; Cross active map-wires downward, leaving M3 on the owner.
      (mark x m2)
      (role-owner-downscan m1 m2 m3 ignore-context)
      ;; Lowermost M3-marked nodes are our answer.
      (mark-lowermost m3 m))))

(defun mark-all-x-owners-of-y (x y m &key ignore-context)
  "This is a synonym for MARK-THE-X-OWNER-OF-Y."
  (mark-the-x-owner-of-y x y m :ignore-context ignore-context))

(defun find-the-x-of-y (x y)
  "Find a specific proper individual node representing the X of Y, and
   return that node if it exists.   Else return NIL."
  (with-temp-markers (m)
    (mark-the-x-of-y x y m)
    (let ((winner (most-specific m)))
      (if (and winner (proper-indv-node? winner))
	  winner
	  nil))))

;;; This is just a synonym.
(defun get-the-x-of-y (x y)
  "Find a specific proper individual node representing the X of Y, and
   return that node if it exists.  Else return NIL."
  (find-the-x-of-y x y))

(defun x-is-the-y-of-what? (x y)
  "Find and return one most specific element whose Y-role is equivalent to X.
   If we don't know of any such element, return NIL."
  (with-temp-markers (m)
    (mark-the-x-owner-of-y y x m )
    (let ((winner (most-specific m)))
      (if (and winner (proper-indv-node? winner))
	  winner
	  nil))))

(defun list-all-x-of-y (x y)
  "Return a list with all X of Y that are proper individuals."
  (with-temp-markers (m)
    (mark-all-x-of-y x y m)
    (list-proper m)))

(defun show-all-x-of-y (x y &key (n 100) (all nil) (last nil))
  "Print out all the X of Y that are proper individuals."
  (with-temp-markers (m)
    (mark-all-x-of-y x y m)
    (show-proper m :n n :all all :last last)))

(defun list-all-x-owners-of-y (x y)
  "Return a list with all proper individuals Z such that the X of Z
   is Y."
  (with-temp-markers (m)
    (mark-all-x-owners-of-y x y m)
    (list-proper m)))

(defun show-all-x-owners-of-y (x y &key (n 100) (all nil) (last nil))
  "Print all proper individuals Z such that the X of Z
   is Y."
  (with-temp-markers (m)
    (mark-all-x-owners-of-y x y m)
    (show-proper m :n n :all all :last last)))

;;; &&& Add argument (or separate function) to mark only roles defined
;;; directly for E.  Ditto for roles inherited by E, but mapped and
;;; modified or given a value at the E level.

(defun mark-roles (e m1)
  "Mark all the roles defined for element E, whether defined directly or by
   inheritance."
  (setq e (lookup-element-test e))
  (check-legal-marker-pair m1)
  (clear-marker-pair m1)
  (with-temp-markers (m2)
    (upscan e m2)
    ;; Scan all marked nodes, looking for attached roles.
    (do-marked (x m2)
      ;; Look for role-relations connected to the marked nodes.
      (dolist (y (incoming-context-wires x))
	(when (or (indv-node? y)
		  (type-node? y))
	  (mark y m1)))))
  (fast-marker-count m1))

(defun list-roles (e)
  "List all the roles attached to element E in the current context,
   directly or by inheritance."
  (with-temp-markers (m1)
    (mark-roles e m1)
    (list-marked m1)))

(defun show-roles (e &key
                     (n 100)
                     (all nil)
                     (last nil))
  "Print out the roles attached to element E in the current context.  Uses
   TEMP-MARKER to mark the owner.  Leaves RESULT-MARKER on the role-nodes
   actually selected."
  (with-temp-markers (m1)
    (mark-roles e m1)
    (show-marked
     m1 :n n :all all :last last
     :heading (format nil "Roles defined for ~A:" e))))

(defun find-the-x-role-of-y (x y)
  "Find and return the role-node or map-node directly representing the
   X-role of Y.  If this does not exist, return NIL.  Never creates a new
   node."
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (if (eq (context-wire x) y)
      ;; X itself is the desired role.
      x
      ;; Look for a map of X.
      (with-temp-markers (m1)
	;; Mark the X of Y with M1.
	(mark-the-x-of-y x y m1)
	;; Scan M1-marked nodes looking for the defined node we want.
	(let ((def nil))
	  (do-marked (e m1)
	    (when (and (defined? e)
		       (setq def (definition e))
		       (equal def `(:role ,x ,y)))
	      ;; Found it.
	      (return-from find-the-x-role-of-y e)))))))

(defun create-the-x-role-of-y (x y &key (parent *thing*) iname english)
  "Creates a defined node representing, by definition, the X role of Y.
   This is used to attach properties that are inherent in the role-player,
   rather than incidental properties of some specific role filler."
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (setq parent (lookup-element-test parent))
  (unless (or *no-checking*
	      (can-x-have-a-y? y x))
    (commentary
     "~S has no defined ~S role.  Continuing..." x y)
    (return-from create-the-x-role-of-y nil))
  (let* ((iname
	  ;; Generate an iname if caller didn't supply one.
	  (or iname
	      (gen-iname (format nil "~A of ~A"
				 (internal-name x)
				 (internal-name y)))))
	 ;; Create the defined type or individual node.
	 (node (if (type-node? x)
		   (new-type iname
			     parent
			     :context (context-wire y)
			     :definition `(:role ,x ,y)
			     :english english)
		   (new-indv iname
			     parent
			     :context (context-wire y)
			     :definition `(:role ,x ,y)
			     :english english))))
    ;; Create the map link: NODE is the X of Y.
    (new-map node x y :context (context-wire y))
    ;; Return the node.
    node))
		   
(defun get-the-x-role-of-y (x y &key (parent *thing*) iname english)
  "Find and return the node that is, by definition, the X of Y.  Create it
   if necessary." 
  (or (find-the-x-role-of-y x y)
      (create-the-x-role-of-y x y :parent parent :iname iname :english english)))


;;; ========================================================================
;;; Predicates

;;; &&& Replace this with a more efficient version that uses a modified
;;; upscan for A and B.  Also, it is useful if this function returns a
;;; second value, which is the specific REL we should cancel if we want to
;;; cancel this.

(defun statement-true? (a rel b &optional ignore-context)
  "Predicate to determine if there is a REL relationship between elements A
   and B."
  (setq a (lookup-element-test a))
  (setq rel (lookup-element-test rel))
  (setq b (lookup-element-test b))
  (with-temp-marker (m)
    (mark-rel a rel m :ignore-context ignore-context)
    (marker-on? b m)))

(defun can-x-have-a-y? (x y)
  "Predicate to determine whether element X can have a Y role in the
   current context.  That is, return T if Y is a role of X or some node
   above X, else NIL."
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (with-temp-markers (m1)
    (upscan x m1)
    (and (context-wire y)
	 (marker-on? (context-wire y) m1))))

(defun can-x-be-the-y-of-z?  (x y z)
  "Predicate to determine whether it would be legal to state that X is the
   Y of Z.  That is, would any splits or other constraints be violated?  If
   it is OK, return T.  If it is not, return NIL with the unhappy split or
   constraint as the second return value.  If there are multiple problems,
   we just return the first one we hit."
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (setq z (lookup-element-test z))
  (if (can-x-have-a-y? z y)
      (with-temp-markers (m1)
	(mark y m1)
	(upscan x m1 :augment t)
	(splits-ok? m1))
      (values nil :role-undefined)))

(defun can-x-be-a-y-of-z?  (x y z)
  "Predicate to determine whether it would be legal to state that X is a Y
   of Z.  That is, would any splits or other constraints be violated?  If
   it is OK, return T.  If it is not, return NIL with the unhappy split or
   constraint as the secodn return value.  If there are multiple problems,
   we just return the first one we hit."
  (can-x-be-the-y-of-z? x y z))

;;; ========================================================================
;;; Functions to add new roles or fillers.

;;; Note: X-IS-THE-Y-OF-Z and THE-X-OF-Y-IS-Z do the same thing with the
;;; args in different order.  I include both functions since if I include
;;; only one, I can never remember which version is implemented.

(defun x-is-the-y-of-z (x y z)
  "If it is legal for X to fill the Y-role of Z, create a statement link to
   make it so.  Return the link."
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (setq z (lookup-element-test z))
  (unless (or *no-checking*
	      (can-x-be-the-y-of-z? x y z))
    (commentary
     "~S cannot be the ~S of ~S.  Continuing..." x y z)
    (return-from x-is-the-y-of-z nil))
  (new-map x y z))

(defun the-x-of-y-is-z (x y z)
  "If it is legal for Z to fill the X-role of Y, create a statement link to
   make it so.  Return the link."
  (x-is-the-y-of-z z x y))

(defun x-is-a-y-of-z (x y z)
  "Y of Z should be a type-role.  If it is legal for X to be of this type,
   create and return a statement-link that makes it so."
  (setq x (lookup-element x))
  (setq y (lookup-element-test y))
  (setq z (lookup-element-test z))
  (unless (or (eq x :create)
	      (can-x-be-a-y-of-z? x y z))
    (error "~A cannot be a ~A of ~A"  x y z))
  (let* ((role-node (get-the-x-role-of-y y z)))
    (new-is-a x role-node)))

(defun the-x-of-y-is-a-z (x y z)
  "Find or create the node for the X of Y.  Then, if it is legal to do so,
   add a new IS-A link from this node to Z."
  (setq x (lookup-element-test x))
  (setq y (lookup-element-test y))
  (setq z (lookup-element-test z))
  (let ((role-node (get-the-x-role-of-y x y)))
    (new-is-a role-node z)))

;;; ========================================================================
;;; LIST and SHOW functions for statements and relations.

(defun list-rel (a rel)
  "List all individual elements B for which A REL B is true."
  (with-temp-markers (m1)
    (mark-rel a rel m1)
    (list-most-specific m1)))

(defun show-rel (a rel
                   &key
                   (n 100)
                   (all nil)
                   (last nil))
  "Show all individual elements B such that A REL B."
  (with-temp-markers (m1)
    (mark-rel a rel m1)
    (show-most-specific
     m1 :n n :all all :last last
     :heading (format nil "Individual X such that ~A ~A X is true."
		      a rel))))

(defun list-rel-inverse (rel b)
  "List all individual elements A for which A REL B is true."
  (with-temp-markers (m1)
    (mark-rel-inverse m1 rel b)
    (list-most-specific m1)))

(defun show-rel-inverse (rel b
                             &key
                             (n 100)
                             (all nil)
                             (last nil))
  "Show all individual elements A such that A REL B."
  (with-temp-markers (m1)
    (mark-rel-inverse m1 rel b)
    (show-most-specific
     m1 :n n :all all :last last
     :heading (format nil "Individual X such that X ~A ~A is true."
		      rel b))))


;;; ***************************************************************************
;;; Cardinality Functions
;;; ***************************************************************************

(defun does-x-have-a-y? (x y)
  "Predicate to determine whether element X definitely has a Y role in the
   current context.  This is indicated by an explicit {exists for}
   statement from Y to X, either direct or inherited.  Y must be a
   role-node.  If true, return three values: T, the {exists for} statement,
   and the C-node of the link, representing the number of Ys per X.  The
   C-node may be NIL, indicating that we know nothing about the
   cardinality.  If X has no Y, we just return NIL."
  ;; Easy, because a has-link is just a statement.
  (statement-true? x *has* y))

;;; &&& More to come.

;;; ***************************************************************************
;;; Internal Names and Namespaces
;;; ***************************************************************************

;;; Use normal compilation policy, where safety counts as much as speed.
(declaim (optimize (speed 1) (space 1) (safety 1)))


;;; The syntax {iname} designates the internal name of a Scone element.

;;; If the open-curly-brace is immediately followed by a string, as in
;;; {"Santo Domingo"}, this designates a STRING element.  This element
;;; created immediately at read-time.  If a string-element representing the
;;; identical string already exists, it will be returned and no new element
;;; will be created.

;;; Similarly, if the curly braces surround a Lisp number, function, or
;;; defstruct, we create the corresponding NUMBER or FUNCTION or STRUCT
;;; element at read-time.  Again, an existing element with the same value
;;; may be returned instead.

;;; Otherwise, we check whether the named element already exists, and
;;; return it if so.  If the element doesn't yet exist, we create and
;;; return an ELEMENT-INAME structure.

;;; If the iname string contains a colon, the part before the colon
;;; identifies the namespace, while the part after the colon is the
;;; element's name, which must be unique within its namespace.

;;; If the string does not contain a colon, we assume that the namespace is
;;; the one specified by *namespace*, which is set up by the IN-NAMESPACE
;;; function.

;;; Internal names are matched in a case-insensitive way, but the case of
;;; the internal name string is preserved for more attractive printing.
;;; Exception: Internal names of STRING nodes are always case sensitive.
;;; Strings identifying namespaces are always case-insensitive.

;;; NOTE: For efficiency, the ELEMENT-INAME structure was defined earlier,
;;; just after ELEMENT itself.

(defun print-element-iname (e stream depth)
  "The ELEMENT-INAME structure prints in the original curly-brace
   notation."
  (declare (ignore depth))
  (format stream "{~A}" (element-iname-string e)))

(defun read-element (s c)
  "This function implements the character macro for open-curly-brace.  If
   the next char is a double quote, create and return a String element.  If
   the curly braces surround the Lisp syntax for a number or function or
   defstruct, create and return the corresponding NUMBER or FUNCTION or
   STRUCT element.  Otherwise, return the form (ELEMENT <string>), where
   the string is everything between the curly barces."
  (declare (ignore c))
  (if (char= (peek-char t s) #\")
      ;; If the first char after the curly-open-bracket is a
      ;; double-quote, this is a string node.  Read the string itself,
      ;; then create and return the string-node.
      (let ((string (read s))
	    (next-char (read-char s)))
	;; If there is anything after the string but before the
	;; close-brace, signal an error.
	(if (char= next-char #\} )
	    (new-string string)
	    (error "Ill-formed String element.")))
      ;; It's not a string.  Read in everything up to the closing brace
      ;; into a string.
      (let* ((char-list nil)
	     (string nil))
	(do ((char (read-char s) (read-char s)))
	    ((char= char #\} )
	     (setq string (coerce (nreverse char-list) 'string)))
	  (push char char-list))
	;; If the first two characters are sharp-quote, it's a function
	;; element.  Read the function name or lambda expression.
	(cond ((and (char= (aref string 0) #\# )
		    (char= (aref string 1) #\' ))
	       (new-function
		(eval (read-from-string string))))
	      ;; Similarly, if the two leading chars are #S, it's a struct.
	      ((and (char= (aref string 0) #\# )
		    (char= (aref string 1) #\S ))
	       (new-struct
		(eval (read-from-string string))))
	      ;; If there's a colon in the string, it's a vanilla element.
	      ((position #\: string)
	       (return-from read-element
			    (make-element-iname :string string)))
	      (t
	       ;; Now it's safe to try a Lisp READ of the string contents.
	       ;; This is the easiest way to see if we have a legal number.
	       (multiple-value-bind (value end)
				    (read-from-string string)
		 (cond 
		  ;; If there's extraneous stuff in the string, it's a
		  ;; vanilla element.
		  ((not (= end (length string)))
		   (make-element-iname :string string))
		  ;; If we have a number, create and return the element.
		  ((typep value 'number)
		   (new-number value))
		  ;; None of the above.  Just a simple element.
		  (t (make-element-iname :string string)))))))))

(set-macro-character #\{
		     #'read-element)

(defun lookup-element-pred (name &optional (syntax-tag nil))
  "Given an element name, look up and return the corresponding element.  If
   the element doesn't yet exist, return NIL.  The SYNTAX-TAG argument, if
   supplied, is used to look up a string as an English name.  If an element
   is found from a string argument, return the syntax tag as the second
   value."
  (typecase name
    ;; If it's already an element, return it.
    (element name)
    ;; If it's an iname-structure, look up the element.
    (element-iname
     (let* ((string (element-iname-string name))
	    (colon-pos)
	    (namespace-name)
	    (namespace)
	    (iname-string))
       (if (setq colon-pos  (position #\: string))
	   ;; It's an iname with an explicit namespace, followed by a
	   ;; colon.  Pick it apart and look it up. Return NIL on failure.
	   (progn 
	     (setq namespace-name  (subseq string 0 colon-pos))
	     (setq namespace
		   (gethash (string-upcase namespace-name) *namespaces*))
	     (when namespace
	       (setq iname-string (subseq string (+ colon-pos 1) nil))
	       ;; Return the iname if it exists in the specified
	       ;; namespace.
	       (lookup-string-in-namespace iname-string namespace)))
	   ;; It's a vanilla iname.  Look it up in default namespace.
	   (lookup-string-in-namespace string *namespace*))))
    ;; If it's a string, look it up as an English word or phrase.
    (string
     (let ((defs (lookup-definitions name syntax-tag)))
       (cond ((null defs) nil)
	     ((null (cdr defs))
	      (values (car (car defs))
		      (cdr (car defs))))
	     (t (disambiguate name defs syntax-tag)))))
    ;; If it's a number or function, create and return 
    ;; the element.
    (number (new-number name))
    (function (new-function name))
    (structure-object (new-struct name))))

;;; Define a synonym, since I seem to use "pred" and "predicate" equally often.

(defun lookup-element-predicate (name &optional (syntax-tag nil))
  "Given an element name, look up and return the corresponding element.  If
   the element doesn't yet exist, return NIL.  The SYNTAX-TAG argument, if
   supplied, is used to look up a string as an English name.  If an element
   is found from a string argument, return the syntax tag as the second
   value."
  (lookup-element-pred name syntax-tag))

(defun lookup-element (name &optional (syntax-tag nil))
  "Given an element name, look it up and return the resulting element.  If
   the argument is an element-iname and the element doesn't yet exist,
   return the argument.  If an element is found from a string argument,
   return the syntax tag as the second return value."
  (multiple-value-bind (element tag)
		       (lookup-element-pred name syntax-tag)
    (if element
	(values element tag)
	name)))

(defun lookup-element-test (name &optional (syntax-tag nil))
  "Given an element name, look it up and return the resulting element.  If
   the element doesn't yet exist, signal an error.  If an element is found
   from a string argument, return the syntax tag as the second return
   value."
  (multiple-value-bind (element tag)
		       (lookup-element-pred name syntax-tag)
    (if element
	(values element tag)
	(error "Element ~S does not exist." name))))

(defun extract-namespace (string)
  "Return the part of STRING before the colon, or nil if there is no colon."
  (let ((pos (position #\: string)))
    (if pos
	(subseq string 0 pos)
	nil)))

(defun extract-name (string)
  "Return the part of the string after the colon, if it
   exists.  Otherwise, return the whole string."
   (let ((pos (position #\: string))
	 (posL (position #\{ string))
	 (posR (position #\} string)))
     (if pos
	 (subseq string (+ pos 1) posR)
       (if posL 
	   (subseq string (+ posL 1) posR)
	 (subseq string 0 posR)))))

;;; Define the NAMESPACE structure.

(defstruct (namespace
   (:constructor internal-make-namespace)
   (:print-function print-namespace))
  ;; A string that is the name of the namespace.
  (name)
  ;; The hashtable associating names with elements.
  (nametable (make-hash-table :test #'equal))
  ;; A namespace may optionally include another namespace.
  (include nil))

(defun print-namespace (e stream depth)
  (declare (ignore depth))
  (format stream "#<namespace ~S>" (namespace-name e)))

(defun make-namespace (namespace-name &key (include nil))
  "Make a new empty namespace with STRING as the name.  If :include is
   specified, that namespace is included in the one we are creating.
   :include may be either a namespace object of a string naming one."
  (check-type namespace-name string)
  ;; Coerce the :include argtument to a namespace object or nil.
  (typecase include
    (null)
    (namespace)
    (string (setq include (get-namespace include)))
    (t (error "The :include argument must be a namespace or string.")))
  ;; Create the namespace structure.
  (let ((ns (internal-make-namespace
	     :name namespace-name
	     :include include)))
    ;; Register the namespace-name for later lookup.
    (setf (gethash (string-upcase namespace-name) *namespaces*) ns)
    ns))

(defun get-namespace (namespace-name &key (include nil))
  "Given a string identifying a namespace, return the corresponding
   namespace object, or create it if necessary.  The lookup is
   case-insensitive."
  (or (gethash (string-upcase namespace-name) *namespaces*)
      (make-namespace namespace-name :include include)))

(defun full-iname (e)
  "E is an element.  Get the long form of the iname, including the
   namespace followed by a colon.  Surround this with curly braces."
  (let ((namespace (namespace e))
	(iname (internal-name e)))
    (if namespace
	(format nil "~A:~A"
		(namespace-name namespace)
		iname)
	 (format nil "~A" iname))))

(defun in-namespace (namespace-name &key (include nil))
  "The namespace designated by NAMESPACE-NAME becomes the new
   default namespace.  Create this namespace if it doesn't already
   exist."
  (check-type namespace-name string)
  ;; Look up the string in *NAMESPACES*.
  (let ((ns (get-namespace namespace-name :include include)))
    ;; Set up *NAMESPACE*
    (setq *namespace* ns)))

(defun lookup-string-in-namespace (string namespace)
  "Internal function.  Lookup the STRING as an iname in the specified
   NAMESPACE object.  If it isn't there, follow the chain of included
   namespaces as far as it goes.  Return NIL is the name is not found."
  (setq string (string-upcase string))
  (and namespace
       (or (gethash string (namespace-nametable namespace))
	   (lookup-string-in-namespace
	    string
	    (namespace-include namespace)))))

(defun register-internal-name (iname-struct e)
  "The INAME-STRUCT argument is an ELEMENT-INAME structure.  E is an
   element.  Extract the string and register it as an internal name for
   later lookup.  Assume we have already checked that the name is not in
   use."
  (when iname-struct
    (let* ((string (element-iname-string iname-struct))
	   (namespace-name (extract-namespace string))
	   (namespace))
      (cond (namespace-name
	     ;; If the string included a namespace, use it.
	     ;; Convert the string to a namespace structure.
	     (setq namespace (get-namespace namespace-name)))
	    ;; If the string didn't have an namespace, use
	    ;; use *NAMESPACE*.
	    (t (setq namespace *namespace*)))
      ;; Register the name.
      (setf (gethash (string-upcase (extract-name string))
		     (namespace-nametable namespace))
	    e)
      ;; Save the namespace and iname string in the element.
      (setf (namespace e) namespace)
      (setf (internal-name e) (extract-name string))
      e)))

(defun unregister-internal-name (e)
  "Remove the entry for an element's internal name."
  (setq e (lookup-element-test e))
  (let* ((in (internal-name e)))
    (setq in (string-upcase in))
    (let ((ns (namespace e)))
      (remhash in (namespace-nametable ns)))))


;;; ***************************************************************************
;;; English Names
;;; ***************************************************************************

;;; Use normal compilation policy, where safety counts as much as speed.
(declaim (optimize (speed 1) (space 1) (safety 1)))

;;; Scone does not (yet) attempt to provide a serious natural-language
;;; interface.  We assume that some external package does the parsing of NL
;;; inputs and queries.  However, Scone does collect and make available a
;;; dictionary (hashtable) of natural-language words and phrases (Common
;;; Lisp strings) that refer directly to Scone elements.  It is convenient
;;; to include these natural-langauge labels in the same KB files that
;;; create the elements.

;;; For now, Scone supports only one natural language: English.  We hope to
;;; support additional natural languages in the future.  However, the
;;; choice of languages may be limited because few Common Lisp
;;; implementations support the full Unicode character set.

;;; The association from English strings to Scone elements is stored in the
;;; *ENGLISH-DICTIONARY* hash-table.

;;; For each association we also store a syntax tag, such as :NOUN or :ADJ.
;;; The idea is that an external NL front-end will do the parsing or
;;; part-of-speech assignment, and then will use the Scone dictionary to
;;; find one or more elements corresponding to the sting/tag combination.

;;; The syntax tags currently allowed are stored in the variable
;;; *LEGAL-SYNTAX-TAGS*.  The following tags are currently defined:

;;; :NOUN -- Either a common name or a proper name for the entities
;;; represented by the element.  "Clyde" or "elephant".

;;; :ADJ -- An adjective like "blue", which is associated with a type-node.
;;; Gramatically, you normally combine the adjective with some noun,
;;; perhaps obtained from some superior of the type-node in question.  So
;;; we would say "a blue object" rather than just "a blue".

;;; :ADJ-NOUN -- A word like "solid" that can be used either as a noun or
;;; as an adjective.

;;; :ROLE -- A word like "population" that designates a role under some
;;; type node.  Normally used with "of" or a possessive form like "India's".

;;; :INVERSE-ROLE -- Name given to the inverse of a role relation.  For
;;; example, if the role is "PARENT", the inverse-role may be "CHILD".  So
;;; if we are told that "X is the CHILD of Y", we can convert this to "Y is
;;; the PARENT of X".

;;; :RELATION -- A word like "employs" or "hates" that represents some
;;; relation that is not an of-role or possessive.

;;; :INVERSE-RELATION -- Name given to the inverse of a relation.  For
;;; example, if the relation is "TALLER THAN", the inverse-role may be
;;; "SHORTER THAN".  So if we are told that "X is taller than Y", we can
;;; conver this to "Y is shorter than X".

;;; :ACTION -- An action verb such as "hit".

(defun english (element &rest r)
  "Define one or more English names for the specified ELEMENT. The
   arguments after the element are strings or syntax tags. Initially we
   assume that each English name receives the syntax-tag :NOUN, but the
   default changes if a different syntax-tag is encountered in the R list.
   The special tag :INAME is used to indicate that ELEMENT's internal name
   becomes an English name with the current syntax tag.  However, if
   :NO-INAME is present in the R list, that cancels any :INAME tag.  Log
   this KB change."
  (setq element (lookup-element-test element))
  (english-internal element r)
  (kb-log "(english-internal ~S '~S)~%" element r)
  element)

(defun english-internal (element namelist)
  "Internal call to do the work of the user-level ENGLISH function.
   This is called directly by MAKE-ELEMENT.  Assumes that any KB logging is done
   by the caller."
  ;; The default syntax-tag is :NOUN until someone changes it.
  (let ((syntax-tag :noun))
    ;; Scan the list of arguments.
    (dolist (x namelist)
      (cond ((eq x :no-iname))
	    ((eq x :iname)
	     (unless (member :no-iname namelist)
	       ;; :INAME is equivalent to a string with the elemnt's internal
	       ;; name.
	       (let ((iname (internal-name element)))
		 (unless (typep iname 'string)
		   (error "Element ~S has no internal name."
			  element))
		 (register-definition iname element syntax-tag))))
	    ((typep x 'string)
	     ;; If it's a string, register the definition.
	     (register-definition x element syntax-tag))
	    ((and (typep x 'keyword)
		  (member x *legal-syntax-tags*))
	     ;; If it's a syntax tag, use this tag for subsequent args.
	     (setq syntax-tag x))
	    (t (error "~S unknown argument to ENGLISH." x))))
    element))

(defun register-definition (string
			    element
			    syntax-tag)
  "Store an association from STRING to (ELEMENT . SYNTAX-TAG) in the
   *ENGLISH-DICTIONARY* hashtable.  Since a string may have multiple
   definitions, the hashtabl entry is a list if it exists at all.  Also
   store (STRING . SYNTAX-TAG) in the ELEMENT.  Assume that this KB change
   has already been logged."
  (setq element (lookup-element-test element))
  (let* ((up-string (string-upcase string))
	 (dictionary *english-dictionary*)
	 (definitions (gethash up-string dictionary))
	 (element-names
	  (get-element-property element :english-names))
	 (new1 (cons string syntax-tag))
	 (new2 (cons element syntax-tag)))
    (unless (member new2 definitions :test #'equal)
      (setf (gethash up-string dictionary)
	    (nconc definitions (list new2)))
      (set-element-property element
			    :english-names
			    (nconc element-names (list new1))
			    t))))

(defun unregister-definition (string
			      element)
  "Remove all mentions of ELEMENT from the entry for STRING in
   *ENGLISH-DICTIONARY*.  Assume that this KB change has already been
   logged."
  (setq element (lookup-element-test element))
  (let* ((dictionary *english-dictionary*)
	 (up-string (string-upcase string))
	 (new-definitions
	  (remove-if
	   #'(lambda (x) (equal (car x) element))
	   (gethash up-string dictionary))))
    (if new-definitions
	(setf (gethash up-string dictionary) new-definitions)
	(remhash up-string dictionary))))

(defun unregister-all-names (element)
  "Unregister all English names associated with the specified ELEMENT."
  (setq element (lookup-element-test element))
  (let ((names (get-element-property element :english-names)))
    (dolist (n names)
      (unregister-definition (car n)
                             element)))
  element)

(defun lookup-definitions (string &optional (syntax-tag nil))
  "Return all the elemnts that are definitions of STRING.  Each value will
   be of form (ELEMENT . SYNTAX-TAG).  If the SYNTAX-TAG argument is
   supplied, return a list containing only names with that tag."
  (let ((values (gethash (string-upcase string) *english-dictionary*)))
    (if syntax-tag
	(setq values
	      (remove-if-not
	       #'(lambda (x) (eq (cdr x) syntax-tag))
	       values))
	values)))

(defun mark-named-elements (string m &optional (syntax-tag nil))
  "Mark all the elements whose external name is STRING with marker M.  If
   the SYNTAX-TAGargument is supplied, mark only elements whose definition
   is of that type."
  (check-legal-marker m)
  (let ((elements (lookup-definitions string syntax-tag)))
    (dolist (e elements)
      (when (typep (car e) 'element)
        (mark (car e) m))))
  (fast-marker-count m))

(defun get-english-names (element &optional (syntax-tag nil))
  "Get the list of names for ELEMENT.  A name is of the form (string . .
   syntax-tag).  If the SYNTAX-TAG argument is supplied, return a list with
   only names of that type."
  (setq element (lookup-element-test element))
  (let ((names (get-element-property element :english-names)))
    (if syntax-tag
        (setq names
	      (remove-if-not
	       #'(lambda (x) (eq (cdr x) syntax-tag))
	       names))
	names)))

(defun disambiguate (name definition-list &optional syntax-tag)
  "Given a list of possible definitions (elements) for an English word,
   describe each and ask the user to choose one.  Return the chosen
   element."
  (if syntax-tag
      (format t "~%The English word ~S with syntax ~S is ambiguous.  ~
		 Possible meanings:~2%"
	      name syntax-tag)
      (format t "~%The English word ~S is ambiguous.  ~
		 Possible meanings:~2%"
	      name))
  (do ((defs definition-list (cdr defs))
       (n 0 (1+ n)))
      ((null defs))
    (let ((def (car defs)))
      (format t "~D: ~S, of element-type ~S, syntax ~S~%"
	      n (car def) (element-type-label (car def)) (cdr def))
      (format t "    Parent: ~A~2%" (parent-wire (car def)))))
  (format t "Type the number of the meaning you want: ")
  (force-output t)
  (let ((n (read t)))
    (terpri t)
    (values (car (nth n definition-list))
	    (cdr (nth n definition-list)))))

(defun element-type-label (e)
  "Return s string describing what type of element E is."
  (cond
   ((integer-node? e) "Integer")
   ((number-node? e) "Number")
   ((string-node? e) "String")
   ((function-node? e) "Function")
   ((struct-node? e) "Struct")
   ((primitive-node? e) "Primitive-Indv")
   ((proper-indv-node? e) "Proper-Indv")
   ((indv-node? e) "Generic Indv")
   ((is-a-link? e) "Is-A")
   ((is-not-a-link? e) "Is-Not-A")
   ((eq-link? e) "Eq")
   ((not-eq-link? e) "Not-Eq")
   ((map-link? e) "Map")
   ((not-map-link? e) "Not-Map")
   ((cancel-link? e) "Cancel")
   ((complete-split? e) "CSplit")
   ((split? e) "Split")
   ((relation? e) "Relation")
   ((statement? e) "Statement")
   ((not-statement? e) "Not-Statement")
   (t "Unknown")))

;;; ========================================================================
;;; LIST and SHOW functions for the natural-language interface.

(defun list-synonyms (string &optional (syntax-tag nil))
  "Find all the synonyms of STRING in the KB.  Returns a list of synonym
   sets, each os which is itself a list.  The first element of the
   synonym-set list is an element, followed by alternative names for that
   element.  If the SYNTAX-TAG argument is supplied, consider only
   definitions of STRING that correspond to that tag."
  (let ((defs (lookup-definitions string syntax-tag))
        (result nil))
    ;; For every definition of name-string...
    (dolist (d defs)
      (let ((synset nil)
            (element (car d)))
        (when (typep element 'element)
          ;; Scan the list of that element's names.
          (dolist (n (get-element-property element :english-names))
            ;; Filter out STRING itself.
            (unless (equal string (car n))
              ;; For the rest, extract the string and save it.
              (push (car n) synset)))
          (push (cons element (nreverse synset)) result))))
    (nreverse result)))

(defun show-synonyms (string &optional (syntax-tag nil))
  "Print out the synonyms of STRING in human-readable form."
  (let ((syns (list-synonyms string syntax-tag)))
    (cond ((null syns)
	   (format t "~%The name ~S is not defined in the KB." string))
	  ((null (cdr syns))
	   (if (null (cdr (car syns)))
	       (format t "~%The name ~S has only one meaning and no synonyms."
		       string)
	       (format t "~%Synonyms of ~S:~{ ~S~}"
		       string (cdr (car syns)))))
	  (t (format t "~%The name ~S has multiple synonym sets:~%"
		     string)
	     (dolist (syn syns)
	       (format t "~%  Meaning: ~S~%" (car syn))
	       (if (null (cdr syn))
		   (format t "   No synonyms.~%")
		   (format t "  Synonyms:~{ ~S~}~%" (cdr syn))))))
    (values)))

(defun list-dictionary ()
  "List all entries in the dictionary.  This is primarily for debugging."
  (let ((dl nil))
    (maphash
     #'(lambda (x y) (push (list x y) dl))
     *english-dictionary*)
    dl))

(defun show-dictionary ()
  "Print all entries in the dictionary.  This is primarily for debugging."
  (maphash
   #'(lambda (x y) (format t "~S => ~S~2%" x y))
   *english-dictionary*))


;;; ***************************************************************************
;;; Loading KB Files
;;; ***************************************************************************

;;; Use normal compilation policy, where safety counts as much as speed.
(declaim (optimize (speed 1) (space 1) (safety 1)))

(defun load-kb (filename &key (verbose nil))
  "Read in a knowledge base in text format.  This is basically just a
   wrapper around the Common Lisp LOAD function, but it does some essential
   housekeeping. Loads and uses the dictionary file (if there is one) to
   preserve permanently assigned element IDs."
  (let* ((*verbose-loading* (or *verbose-loading* verbose))
	 (*no-checking* *no-checking*)
	 (pathname
	  (merge-pathnames (pathname filename)
			   *default-kb-pathname*))
	 ;; When reading a KB file, set this variable.
	 (*reading-kb-file* t)
	 ;; The file may set *DEFER-UNKNOWN-CONNECTIONS*.  Make sure that
	 ;; this stays local.
	 (*defer-unknown-connections* nil))
    (commentary "~&;; Loading file \"~A\" into KB...~%" pathname)
    (force-output *terminal-io*)
    ;; Push last-loaded-file info now so we can roll back if we get an
    ;; error during loading.
    (load pathname
	  :verbose *verbose-loading*
	  :print *verbose-loading*)
    (process-deferred-connections)
    (push pathname *loaded-files*)
    (push *last-element* *last-loaded-elements*)
    (commentary "~&Load completed.  ~D elements total.~2%" *n-elements*)
    (force-output *terminal-io*)
    (values)))

(defun process-deferred-connections ()
  "Go through the *DEFERRED-CONNECTIONS* list and connect all the wires
   that can be connected at this time.  Leave the rest on the list."
  (when *deferred-connections*
    (when *verbose-loading*
      (commentary "~&Deferred connections: ~S~%" *deferred-connections*))
    (let ((still-deferred nil)
          (connected 0)
          (remaining 0))
      (dolist (c *deferred-connections*)
        (let ((wire (first c))
              (source-element (second c))
              (target-element (lookup-element (third c))))
          (cond ((not (typep target-element 'element))
                 (push c still-deferred)
                 (incf remaining))
		(t (connect-wire wire source-element target-element)
		   (incf connected)))))
      (setq *deferred-connections* still-deferred)
      (commentary "~&~D deferred connections fixed, ~D remain.~%"
		  connected remaining))))

(defun defer-connection (tag source target)
  "If a wire cannot be connected now because the target element doesn't
   exist yet, we may defer the connection and make it later.  This allows
   for creation of circularities in the KB."
  (when target
    (unless *defer-unknown-connections*
      (error "Element ~S is not (yet) defined." target))
    (push (list tag source target) *deferred-connections*)))


;;; ***************************************************************************
;;; KB Checkpointing and Persistence
;;; ***************************************************************************

;;; Machinery for checkpointing a KB file that has been modified by
;;; addition of new elements.  The resulting files can be loaded by
;;; LOAD-KB.

;;; NOTE: At present the checkpoint format is text rather than some
;;; optimized binary fasload format.  So dumping and loading are not
;;; super-fast, but at least we bypass most of the consistency checking
;;; that occurs when we create elements from scratch.

(defun checkpoint-kb (&optional (filename "checkpoint"))
  "Dump out all KB elements into a lisp file.  This can be loaded into a
   new Scone to recreate the KB state at the time of the checkpoint.  We do
   not dump the elements that came from the BOOTSTRAP file, so the new Lisp
   must already have loaded that file.  Any missing fields in the FILENAME
   argument are filled in from the corresponding fields in
   *DEFAULT-KB-PATHNAME*."
  (let* ((pathname (merge-pathnames filename *default-kb-pathname*))
	 (skip t))
    (with-open-file (dump pathname
			  :direction :output
			  :if-exists :supersede)
      (format dump "~&;;; Scone Checkpoint File ~A~%" pathname)
      (format dump ";;; Dumped on ~A.~2%" (current-time-string))
      (format dump "(setq *defer-unknown-connections* t)~%")
      (format dump "(check-loaded-files '(\"bootstrap\"))~%")
      (format dump "(new-iname-prefix ~A)~%" *iname-prefix*)
      (do-elements (e)
	(unless skip
	  (dump-element e dump))
	(when (eq e *last-bootstrap-element*)
	  (setq skip nil))))
    (commentary "Checkpointed KB to ~S." pathname)
    (values)))

(defun checkpoint-new (&optional (filename "checkpoint-new"))
  "Dump into a lisp file all the elements created since completion of the
   last LOAD-KB.  Use LOAD-KB to read this dumped file into a Lisp that
   already has the same set of files loaded.  Any missing fields in the
   FILENAME argument are filled in from the corresponding fields in
   *DEFAULT-KB-PATHNAME*."
  (let* ((pathname (merge-pathnames filename *default-kb-pathname*))
	 (skip t))
    (with-open-file (dump pathname
			  :direction :output
			  :if-exists :supersede)
      (format dump ";;; Scone New-Element Checkpoint File ~A~%" pathname)
      (format dump ";;; Dumped on ~A.~2%" (current-time-string))
      (format dump "(setq *defer-unknown-connections* t)~%")
      ;; Note what files are loaded, so that when the log file is read
      ;; back in, this will be checked.
      (format dump "~&(check-loaded-files '~S)~%"
	      (mapcar #'pathname-name *loaded-files*))
      (format dump "~&(new-iname-prefix ~A)~%" *iname-prefix*)
      (do-elements (e)
	(unless skip
	  (dump-element e dump))
	(when (eq e (car *last-loaded-elements*))
	  (setq skip nil)))
      (format dump
	      "(setq *iname-counter* ~A)~%"
	      (+ *iname-counter* 1)))
    (commentary "Checkpointed KB to ~S." pathname)
    (values)))

(defun dump-element (e stream)
  (format
   stream
   "(make-element ~S ~S ~S ~S ~S ~S ~S '~S NIL ~A '~S '~S)~%"
   (flags e)
   (if (primitive-node? e) (internal-name e) e)
   (parent-wire e)
   (context-wire e)
   (a-wire e)
   (b-wire e)
   (c-wire e)
   (unravel-english-names e)
   ;; Make sure the element name ends up in the right hashtable.
   (cond ((namespace e) nil)
	 ((number-node? e) "*number-hashtable*")
	 ((string-node? e) "*string-hashtable*")
	 ((function-node? e) "*function-hashtable*")
	 ((struct-node? e) "*struct-hashtable*")
	 (t "nil"))
   (definition e)
   ;; Save the element's property list, but not the :ENGLISH
   ;; property, which we handle separately.
   (remf (properties e) :english)))

(defun unravel-english-names (e)
  (let ((list nil))
    (dolist (name (get-english-names e))
      (push (cdr name) list)
      (push (car name) list))
    (nreverse list)))

;;; Function to check whether the expected set of files have been loaded.

(defun check-loaded-files (file-list)
  "Check whether all the files in FILE-LIST are currently loaded in Scone.
   Check only the filename itself, not the directory or extension.  Don't
   worry about load order or any extra files that have been loaded.  If we
   fail the test, signal a continuable error."
  (do ((list1 file-list (cdr list1)))
      ((null list1) t)
    (let ((name (pathname-name (car list1))))
      (unless (member name *loaded-files*
		      :test #'same-filename)
	(cerror 
	 "Continue despite the missing file."
	 "File ~S should be loaded, but is not." name)))))

(defun same-filename (x y)
  (string-equal x (pathname-name y)))

;;; Update *INAME-PREFIX* to a new value.

(defun new-iname-prefix (n)
  (setq *iname-prefix*
	(+ 1 (max *iname-prefix* n)))
  (setq *iname-counter* 0))

;;; Small utility function to print the current time.

(defvar *dow-vector*
  (vector "Monday" "Tuesday" "Wednesday" "Thursday" "Friday"
	  "Saturday" "Sunday"))

(defun current-time-string ()
  "Create a human-readable string representing the current time in this time
   zone."
  (multiple-value-bind (sec min hr d m y dow dst zone)
		       (get-decoded-time)
    (format nil
	    "~A, ~A/~A/~A at ~A:~A:~A, Time Zone: ~@D~A"
	    (aref *dow-vector* dow)
	    m d y
	    hr min sec
	    zone
	    (if dst " (DST)" ""))))

(defun start-kb-logging (&optional (filename "kb-log"))
  "Starting now, for each change to the KB, write out a log entry to a
   kb-log file.  If the system dies unexpectedly, we can restore the
   current state of the system by reading in this log file using LOAD-KB.
   We do not attempt to restore the state of the markers, which is
   considered ephemeral.  If the system dies while writing out a log entry,
   this mechanism still should work, though the last partial entry may be
   lost.  Use *DEFAULT-KB-PATHNAME* to complete any unsupplied
   components of the FILENAME."
  (if *kb-logging-stream*
      (commentary "Scone is already logging KB changes.")
      ;;; Open and initialize the KB logging file.
      (let* ((pathname
	      (merge-pathnames filename *default-kb-pathname*)))
	(setq *kb-logging-stream*
	      (open pathname
		    :direction :output
		    :if-exists :supersede))
	(kb-log "~&;;; Scone KB Logging File ~S~%" pathname)
	(kb-log "~&;;; Opened on ~S.~2%" (current-time-string))
	;; Note what files are loaded, so that when the log file is read
	;; back in, this will be checked.
	(kb-log "~&(check-loaded-files '~S)~%" 
		(mapcar #'pathname-name *loaded-files*))
	(kb-log "~&(new-iname-prefix ~A)~%" *iname-prefix*)
	;; If there are new elements since the last LOAD-KB, dump them to the
	;; log file.
	(unless (eq (car *last-loaded-elements*) *last-element*)
	  (commentary "~&Dumping all newly created elements to log file.~%")
	  (do-elements (e (car *last-loaded-elements*))
	    (dump-element e *kb-logging-stream*)))
	(commentary "~&Scone is now logging KB changes.~%")
	(values))))

(defun end-kb-logging ()
  (when *kb-logging-stream*
    (kb-log "~%;;; End of KB logging file.  ~S~%" (current-time-string))
    (close *kb-logging-stream*)
    (commentary ";;; KB logging terminated.")
    (values)))

;;; ***************************************************************************
;;; Removing Elements from the KB
;;; ***************************************************************************

;;; Use normal compilation policy, where safety counts as much as speed.
(declaim (optimize (speed 1) (space 1) (safety 1)))

;;; Removing an element can leave things in an odd state, but we still need
;;; some mechanism to do this.

(defun remove-element (e)
  "Disconnect element E from the network and remove it."
  (setq e (lookup-element-test e))
  ;; Can only do this if all markers are clear.
  (clear-all-markers)
  ;; Unhook all the wires coming into E.
  (disconnect-wire :parent e)
  (disconnect-wire :context e)
  (disconnect-wire :a e)
  (disconnect-wire :b e)
  (disconnect-wire :c e)
  ;; Remove E from the chain of elements.
  (cond ((eq e *first-element*)
	 (setq *first-element* (next-element e)))
	(t (let ((prev (find-previous-element e)))
	     (setf (next-element prev)
		   (next-element e))
	     (when (eq e *last-element*)
	       (setq *last-element* prev)))))
  ;; Fix up count.
  (decf *n-elements*)
  ;; Remove E from name hashtables.
  (unregister-internal-name e)
  (unregister-all-names e)
  *n-elements*)

(defun remove-last-element (&optional (n 1))
  "Remove the last element created.  If N argument is supplied, remove the
   last N elements."
  (dotimes (i n)
    (remove-element *last-element*))
  *n-elements*)

(defun remove-elements-after (e)
  "Given some element E, remove all elements added later than E, starting
   with the last." 
  (setq e (lookup-element-test e))
  (loop
    (when (or (null *last-element*)
	      (eq *last-element* e))
      (return nil))
    (remove-element *last-element*)))

(defun remove-last-loaded-file ()
  "Remove all the elements, starting with the last-created, until we have
   removed all elements created by the last loaded file (plus any created
   by hand after that).  Pops the last file off *LOADED-FILES* and
   *LAST-LOADED-ELEMENTS*.  Returns the number of elements remaining in the
   KB."
  (cond ((car *last-loaded-elements*)
         (let ((new-last (cadr *last-loaded-elements*)))
           (do ((e *last-element* *last-element*))
               ((or (null e) (eq e new-last)))
             (remove-element e)))
         (pop *loaded-files*)
         (pop *last-loaded-elements*)
         *n-elements*)
        (t (error "No last loaded file to remove."))))

(defun remove-currently-loading-file ()
  "Remove all the elements, starting with the last-created, until we have
   removed all elements created by the file that is currently being loaded.
   This is useful for backing out of a file-load if an error aborts the
   load.  Returns the number of elements remaining in the KB."
  (let ((new-last (car *last-loaded-elements*)))
    (do ((e *last-element* *last-element*))
        ((or (null e) (eq e new-last)))
      (remove-element e)))
  (pop *loaded-files*)
  *n-elements*)


;;; ***************************************************************************
;;; Server Machinery
;;; ***************************************************************************

;;; Use normal compilation policy, where safety counts as much as speed.
(declaim (optimize (speed 1) (space 1) (safety 1)))


;;; A Common Lisp process with Scone loaded can be run as a server,
;;; communicating with other software via character-stream input and
;;; output, implemented on top of the TCP socket mechanism.  For now, this
;;; is single-client, single-threaded, and synchronous.

;;; The file "client.lisp" contains a Common Lisp client side that will
;;; talk to this.  That is mostly for testing. Real connections will be
;;; from Java or C code.

;;; NOTE: This code is specific to CMU Common Lisp on Linux/Intel.

;;; NOTE: Something deep in Linux insists on buffering socket-stream
;;; printout until a newline is sent.  So we can't use prompts that print a
;;; partial line and then wait for input.  Grrrrr...

#+cmu
(defvar *this-host* (unix:unix-gethostname)
  "Get the host name of the machine we are running on.")

#+cmu
(defvar *default-port* 6517
  "If the caller doesn't supply a port number to use, use this one.  This
   is an arbitrary number chosen because it doesn't seem to be is use for
   anything else, and because it looks a bit like the word LISP if you
   stand on your head.")

#+cmu
(defvar *server-stream* nil
  "Character-stream communication to/from the server socket ends up looking
   like stream I/O within the Lisp.  This is the stream.")

#+cmu
(defun start-server (&optional (port *default-port*))
  "Open the specified PORT on this host.  When a client connects to it,
   create a two-way stream for this port and put it into *SERVER-STREAM*."
  (let ((server (create-inet-listener port)))
    (setq *prompt* "[PROMPT]
")
    (setq debug::*debug-prompt* #'server-debug-prompt)
    (setq *server-stream*
	  (when (sys:wait-until-fd-usable server :input)
	    (sys:make-fd-stream (accept-tcp-connection server)
				:buffering :none
				:input t
				:output t
				:element-type 'character))))
  (commentary ";;; Lisp server is now running on port ~D.~%" port)
  (force-output t)
  (setq *standard-input* *server-stream*)
  (setq *standard-output* *server-stream*)
  (setq *error-output* *server-stream*)
  (setq *query-io* *server-stream*)
  (setq *debug-io* *server-stream*))

#+cmu
(defun server-debug-prompt ()
  "A variant of the built-in DEBUG-PROMPT function that is used by the
   debugger.  This one prints a recognizable marker and prints a newline
   after the prompt."
  (let ((*standard-output* *debug-io*))
    (format t "~%[DEBUG PROMPT]~%~
	       ~A]~%"
	    (debug-internals::frame-number
	     debug::*current-frame*))
    (force-output)))

