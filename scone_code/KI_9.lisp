;; (load "/home/mjsantof/repo/mariajose.santofimia/tesis/CMU/Scone/files/pathToFiles")
;; (load-kb "/home/mjsantof/repo/mariajose.santofimia/tesis/CMU/Scone/files/keyIssues/KI_9.lisp")
(in-context {general})

(new-type-role {animateLocation} {animate} {Location})

(new-event-type {walk} '({event})
		:roles
		((:type {walker} {animate}) ;;though it might also be an animal
		 (:type {from} {place})
		 (:type {walkerLocation} {animateLocation}))
    :before
   ((in-context (new-context {walk bc}))
    (new-statement {walker} {is located at} {from})
    (the-x-of-y-is-z {walkerLocation} {walk} {from}))

   :after
   ((in-context (new-context {walk ac}))
    (new-not-statement {walker} {is located at} {from})))

(in-context {general})
(new-event-type {walk to} '({action} {walk})
		:roles
		((:type {to} {place}))
  :throughout		
  ((new-split '({from} {to})))
   :before
   ((in-context (new-context {walk to bc}))
    (new-statement {walker} {is located at} {from})
    (the-x-of-y-is-z {walkerLocation} {walk to} {from}))

   :after
   ((in-context (new-context {walk to ac}))
    (new-not-statement {walker} {is located at} {from})
    (new-statement {walker} {is located at} {to})))

(in-context {general})
(new-event-type {walk into} '({action} {walk} {walk to})
		:roles
		((:type {enteringRoom} {room}))
	     
   :throughout		
   ((new-is-a {enteringRoom} {enclosed space}))
   ; (new-eq {to} {enteringRoom})NOTA AQUÍ ESTABA EL FALLO DEL STATEMENT-TRUE! TENER EN CUENTA
    ;(new-split '({from} {enteringRoom})))
   :before
   ((in-context (new-context {walk into bc}))
    (new-statement {walker} {is located at} {from})
    (new-statement {from} {is connected to} {enteringRoom})
    (the-x-of-y-is-z {walkerLocation} {walk into} {from}))

   :after
   ((in-context (new-context {walk into ac}))
    (new-statement {walker} {crosses across} (get-the-x-role-of-y {doorway} {enteringRoom}))
    (new-statement {walker} {is in} {enteringRoom})
    (new-statement {walker} {is located at} {enteringRoom})
    (the-x-of-y-is-z {walkerLocation} {walk into} {enteringRoom})))


(in-context {general})
;; Descriptions of some rooms that can be used as places and locations
(new-indv {bedroom} {room})
(new-indv {kitchen} {room})
(new-indv {living room} {room})
(new-indv {office} {room})
(new-not-eq {kitchen} {living room})
(new-not-eq {living room} {bedroom})
(new-not-eq {living room} {kitchen})
(new-split '({living room} {kitchen} {bedroom} {office}))

(new-indv {kitchen door} {door})
(new-indv {living room door} {door})
(new-indv {bedroom door} {door})
(new-indv {office door} {door})
(new-split '({kitchen door} {living room door} {bedroom door}{office door}))
(the-x-of-y-is-z {doorway} {kitchen} {kitchen door})
(the-x-of-y-is-z {doorway} {living room} {living room door})
(the-x-of-y-is-z {doorway} {bedroom} {bedroom door})
(the-x-of-y-is-z {doorway} {office} {office door})


;; 1. We must represent Lisa as the walker in this event
(new-indv {Lisa} {person})
(new-is-a {Lisa} {animate})
(new-is-a {Lisa} {female})

;; 2. We create an instance of the walk into event, to represent the event of
;;    "Lisa walking into the room" and probing that it is not possible to walk 
;;    into two rooms at the same time"

(new-event-indv {Lisa walks into} {walk into})
;(new-eq {Lisa} {walker}) NOTA AQUÍ ESTABA EL FALLO DEL STATEMENT-TRUE! TENER EN CUENTA
(the-x-of-y-is-z {walker} {Lisa walks into} {Lisa})
;(new-eq {kitchen} {enteringRoom})NOTA AQUÍ ESTABA EL FALLO DEL STATEMENT-TRUE! TENER EN CUENTA

(in-context {walk into bc})
(the-x-of-y-is-z {from} {Lisa walks into} {bedroom})
(in-context {walk into ac})
(the-x-of-y-is-z {enteringRoom} {Lisa walks into} {kitchen})
(in-context {general})

;; Some contexts
(the-x-of-y-is-z {after context} {Lisa walks into} {walk into ac})
(the-x-of-y-is-z {before context} {Lisa walks into} {walk into bc})

(defun statement-true? (a rel b)
  "Predicate to determine if there is a REL relationship between elements A
   and B."
  (setq a (lookup-element-test a))
  (setq rel (lookup-element-test rel))
  (setq b (lookup-element-test b))
  (with-temp-markers (m m-rel)
    ;; this marks all possible Bs and relations that are crossed over
    (mark-rel a rel m :m-rel m-rel)
    ;;  relations that are crossed
    (loop for x in (list-marked m-rel)
	 ;; when mark is b there is a statement such a rel b
	 ;; Furthermore, we check if such relation is in the context we are interested in
	 ;; We get the contex-wire, and check if equal actual context
       when(and (and (marker-on? b m) (eq (context-wire (lookup-element x)) *context*)))
		;;Also, we have to check that b can be the b-wire of the just selected relation
		;; for instance if split moveOrigin moveDestination, and kitchen is moveOrigin, 
		;; it cannot be a moveDestination
       do (progn
	    (loop for j in (incoming-b-wires (lookup-element (b-wire (lookup-element x))))
	       when(eq (a-wire (lookup-element j)) (lookup-element b))
	       do(progn
		   (if(incoming-parent-wires (lookup-element(parent-wire (lookup-element x))))
		      (progn
			(loop for i in (incoming-parent-wires (lookup-element(parent-wire (lookup-element x))))
			   do (if (and (and (and (not-statement? i) (is-x-a-y? (a-wire  (lookup-element i)) (a-wire (lookup-element x))))  (b-wire  (lookup-element i)) (b-wire (lookup-element x))) (eq (context-wire  (lookup-element i)) *context*))
				  (progn
				    (format t "It cannot be predicted because: ~A and " i)
				    (return NIL))))))
	
		   (return-from statement-true? x)))))))
	     