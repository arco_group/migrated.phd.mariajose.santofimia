;; (load "/home/mjsantof/repo/mariajose.santofimia/tesis/CMU/Scone/files/pathToFiles")
;; (load-kb "/home/mjsantof/repo/mariajose.santofimia/tesis/CMU/Scone/files/keyIssues/KI_13.lisp")

(in-context {general})
(in-namespace "events" :include "common")


;; 1. We must represent Lisa, the newspaper, and the living room
 (new-indv {Lisa} {person})
 (new-is-a {Lisa} {female})
 (new-indv {living room} {room})
 (new-indv {bedroom} {room})
 (new-indv {kitchen} {room})
 (new-indv {office} {room})
 (new-indv {lisas bedroom} {room})
 (new-type {newspaper} {thing})
 (new-indv {Lisa newspaper} {newspaper})
 (new-not-eq {kitchen} {living room})
 (new-not-eq {living room} {bedroom})
 (new-not-eq {living room} {kitchen})
 (new-split '({living room} {kitchen} {bedroom}))
;; 2. We create two instances of event types
(new-event-indv {Lisa moves} {move})
(new-event-indv {Lisa takes up} {take up})

;; 3. We instanciate the roles of both event instances
(the-x-of-y-is-z {movingObject} {lisa moves} {lisa})
(the-x-of-y-is-z {moveOrigin} {lisa moves} {kitchen})
(the-x-of-y-is-z {movingObjectLocation} {lisa moves} {kitchen})
(the-x-of-y-is-z {moveDestination} {lisa moves} {living room})

(new-eq {lisa newspaper} {pickedObject})
(the-x-of-y-is-z {pickedObject} {lisa takes up} {lisa newspaper})
(the-x-of-y-is-z {picker} {lisa takes up} {lisa})
(the-x-of-y-is-z {pickedObjectLocation} {lisa takes up} {living room})

(the-x-of-y-is-z {after context} {lisa takes up}  {take up ac})
(the-x-of-y-is-z {before context} {lisa takes up}  {take up bc})
(the-x-of-y-is-z {after context} {lisa moves}   {move ac})
(the-x-of-y-is-z {before context} {lisa moves}  {move bc})


(new-type {container} {thing})
(new-type-role {contents} {container} {physical object})
(new-indv-role {inside} {container} {place})
(new-statement {contents} {located at} {inside})



(new-type {faucet} {thing})
(new-type {valve} {thing})
(new-is-a {sink} {container})
(new-type {tap status} {property})
(new-indv {opened tap} {tap status})
(new-indv {closed tap} {tap status})

(new-type {pipe} {thing})
(new-type-role {sinkTapStatus} {sink} {tap status})
(new-type-role {sinkPipe} {sink} {pipe})


(new-relation {is being dropped through}
	      :a-inst-of {liquid}
	      :b-inst-of {pipe})

(new-relation {gets overflowed with}
	      :a-inst-of {container}
	      :b-inst-of {liquid})

(new-relation {is contained in}
	      :a-inst-of {thing}
	      :b-inst-of {container}
	      :inverse '(:relation "contains"))


(new-relation {is dropping}
	      :a-inst-of {container}
	      :b-inst-of {liquid}
	      :inverse '(:relation "contains"))


(new-event-type {turn off} '({event}))
(new-event-type {turn on} '({event}))


(new-event-type {turn off faucet} '({event} {turn off})
				    :roles
				    ((:indv {turnedOffFaucet} {faucet})
				     (:indv {faucetValve} {valve})
				     (:indv {faucetSink} {sink})			     
				     (:indv {faucetLiquid} {liquid}))
   :before
   ((in-context (new-context {turn off faucet bc}))
    (new-statement {turnedOffFaucet} {status} {on}))
   :after
   ((in-context (new-context {turn off faucet ac}))
    (new-statement {turnedOffFaucet} {status} {off})))				   


(new-event-type {turn on faucet} '({event} {turn on} {turn off faucet})
				    :roles
				    ((:indv {turnedOnFaucet} {faucet}))
   :before
   ((in-context (new-context {turn on faucet bc}))
    (new-statement {turnedOnFaucet} {status} {off}))
   :after
   ((if(eq (get-the-x-of-y {sinkTapStatus} {faucetSink}) {opened tap})
       (progn    
	 (in-context (new-context {turn on faucet ac}))
	 (new-statement {turnedOnFaucet} {status} {on})
	 (new-statment {faucetLiquid} {is being dropped through} (get-the-x-of-y {sinkPipe} {faucetSink})))
       (progn
	 (in-context (new-context {turn on faucet ac}))
	 (new-statement {faucetLiquid} {is contained in} {faucetSink})
	 (in-context (new-context {turn on faucet DE}))
	 (new-statement {faucetSink} {gets overflowed with} {faucetLiquid})
	 (new-statement {faucetSink} {is dropping} {faucetLiquid})))))

				   