;; Now we define the INDRA building context. This building is where
;; services and devices have been deployed and where the intelligent 
;; room is located. So we describe the context and the events that
;; might take place in this context

;; We start describing the INDRA building, from outside

(in-namespace "events" :include "common")
(in-context {general})

(new-indv {INDRA-building} {builiding})
(new-indv-role {INDRA-roof} {ceiling} {INDRA-building})
(new-indv-role {INDRA-front-wall} {wall} {INDRA-building}) 
(new-indv-role {INDRA-back-wall} {wall} {INDRA-building}) 
(new-indv-role {INDRA-east-wall} {wall} {INDRA-building}) 
(new-indv-role {INDRA-west-wall} {wall} {INDRA-building})
(new-indv-role {INDRA-floor} {floor} {INDRA-building})
(new-indv-role {INDRA-doorway} {doorway} {INDRA-building})

(new-indv-role {INDRA-intelligent-room} {room} {INDRA-building})
(new-indv-role {INDRA-intelligent-room-back-wall} {wall} {INDRA-intelligent-room}) 
(new-indv-role {INDRA-intelligent-room-front-wall} {wall} {INDRA-intelligent-room}) 
(new-indv-role {INDRA-intelligent-room-east-wall} {wall} {INDRA-intelligent-room}) 
(new-indv-role {INDRA-intelligent-room-west-wall} {wall} {INDRA-intelligent-room}) 
(new-indv-role {INDRA-intelligent-room-floor} {floor} {INDRA-intelligent-room}) 
(new-indv-role {INDRA-intelligent-room-doorway} {doorway} {INDRA-intelligent-room}) 
(new-indv-role {INDRA-intelligent-room-door} {door} {INDRA-intelligent-room})

(new-indv-role {INDRA-corridor} {corridor} {INDRA-building})


(new-statement {INDRA-intelligent-room-doorway} {connects} {INDRA-intelligent-room} :c {INDRA-corridor})
(new-statement {INDRA-intelligent-room-door} {closes} {INDRA-intelligent-room})
