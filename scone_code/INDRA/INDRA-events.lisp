;;;*****************************************************************************
;;;** ------------------------------------------------------------------------**
;;;**      SERVICES AND DEVICES DEPLOYED AT THE INDRA BUILDING                **
;;;** This file describes the different set of devices and services deployed  **
;;;** in the context of the INDRRA building and its intelligent room.         **
;;;** The types defined here count on the types defined at service-kb.lisp    **
;;;** ------------------------------------------------------------------------**
;;;*****************************************************************************