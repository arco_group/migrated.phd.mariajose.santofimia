(defun planner (actions)
  "Plan the performable task that perform same task as action"
  (dolist (alpha actions)
    ;; for each action, decompose those that are unperformable
    (unless (x-is-the-y-of-what? alpha {performs-action})
    ;; if there is no service performing the current action
    ;; then look for the after contexts elements
      (setq events (list-events-causing-x alpha))
      (SUBST events alpha actions)
     ; (planner actions)
      (return actions))))
	
	
      