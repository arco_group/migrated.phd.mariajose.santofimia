	
(in-namespace "events" :include "common")
(in-context {general})

;;; This file necessarily contains some forward references.
(setq *defer-unknown-connections* t)

;; In the context of the INDRA building we consider the following devices	

;; Therefore, we group services into four groups, depending on the sort of
;; device that provides the service

;;FIXME: We have to fill the "performs-actions" for each of the services, so that we can map
;; services to the action the can perform. 

(new-type {sensor-service} {service})
(new-type {biometric-service} {service})
(new-type {authentication-service} {service})
(new-type {biometric-authentication-service} {authentication-service})
(new-type {electronic-authentication-service} {authentication-service})
(new-type {identification-service} {service})
(new-type-role {measurement} {sensor-service} {measure})
(new-type {presence-detection-service} {sensor-service})
(new-type {sound-measurement-service} {sensor-service})
(new-type {temperature-measurement-service} {sensor-service})
(new-type {light-detection-service} {sensor-service})
(new-type {face-detection-service} {biometric-service})
(new-type {face-recognition-service} {biometric-authentication-service})
(new-is-a {face-recognition-service} {authentication-service})
(new-type {distance-measurement-service} {sensor-service})
(new-type {media-content-service} {service})
(new-type {image-capture-service} {media-content-service})
(new-type {video-capture-service} {media-content-service})
(new-type {face-capture-service} {biometric-service})
(new-type {iris-capture-service} {biometric-service})
(new-type {iris-recognition-service} {biometric-authentication-service})
(new-is-a {iris-recognition-service} {authentication-service})
(new-type {fingerprint-capture-service} {biometric-service})
(new-type {fingerprint-recognition-service} {biometric-authentication-service})
(new-is-a {fingerprint-recognition-service} {authentication-service})
(new-type {image-display-service} {media-content-service})
(new-type {audio-play-service} {media-content-service})
(new-type {audio-display-service} {media-content-service})
(new-type {video-play-service} {media-content-service})
(new-type {video-display-service} {media-content-service})
(new-type {image-play-service} {media-content-service})
(new-type {audio-capture-service} {media-content-service})
(new-type {actuator-service} {service})
(new-type {turning-on-service} {actuator-service})
(new-type {turning-off-service} {actuator-service})
(new-type {openning-service} {actuator-service})
(new-type {closing-service} {actuator-service})
(new-type-role {actuator-object} {actuator-service} {thing}) ;the thing that is activated by the actuator

