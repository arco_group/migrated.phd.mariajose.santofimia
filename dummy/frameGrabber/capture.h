#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/ioctl.h>
#include<fcntl.h>
#include<linux/videodev.h>
#include<linux/videodev2.h>
#ifndef _CAPTURE_H
#define _CAPTURE_H

#include<sys/mman.h>
#include<assert.h>
#include<iostream>
#include<jpeglib.h>

#define OPTIONS	"hx:y:D:p:j:c:n:v:d:q"
  

  
  static int		width = 320;
  static int		height = 240;
  static int		depth = 3;
  static int		palette = V4L2_PIX_FMT_BGR24;
  static v4l2_std_id	video_standard = V4L2_STD_PAL; 
  static char		*video_std = "PAL";
  static int		jpeg_quality = 80;
  static int		channel = 0;
  static int		n_frames = 3;
  static char 		*filename;

class Capture {
 public:
    struct v4l2_capability	vcap;
  struct v4l2_requestbuffers reqbuf;
  
  struct vbuffer
  {
    void *start;
    size_t length;
  } *buffers;
  void usage( char **argv);
  
  int query_device( int video_fd) ;
  int get_input_channel( int video_fd );
  int set_input_channel( int video_fd );
  int get_supported_video_standard( int video_fd ); // Same as above
  int get_current_video_standard( int video_fd ); // I should pass the index of the input channel
  int set_video_standard( int video_fd );
  int set_capture_buffer(int video_fd);
  int set_picture(int video_fd);
  void process_image( void *frame );
  void main_loop(int video_fd);
  int read_frame(int fd);
  int frameGrab(char* file_device);
  int create_jpeg( char *fileout, unsigned char *img, int lx, int ly, int lw);

};

#endif
