#include <Ice/Ice.h>
#include <Video.h>

#include <capture.h>

using namespace std;
using namespace Video;


class GrabberI : public Grabber {
public:

  virtual void frameGrabber(const Ice::Current&);
  virtual string getImageFile(const Ice::Current&);
};

void GrabberI::frameGrabber(const Ice::Current&){
  char* dev = "/dev/video0";
  Capture* c = new Capture();
  c->frameGrab(dev);
  
}

string GrabberI::getImageFile(const Ice::Current& c){
	frameGrabber(c);
	return "/home/mjsantof/home/mariajose.santofimia/tesis/CMU/Scone/dummy/frameGrabber/capture.jpg";
}


int main(int argc, char* argv[]){
  int status = 0;
  Ice::CommunicatorPtr ic;
  try {
    ic = Ice::initialize(argc, argv);
    Ice::ObjectAdapterPtr adapter
      = ic->createObjectAdapterWithEndpoints("SimplePrinterAdapter", "default -p 9000");
    Ice::ObjectPtr object = new GrabberI;
    adapter->add(object, ic->stringToIdentity("SimplePrinter"));
    adapter->activate();
    ic->waitForShutdown();
  } catch(const Ice::Exception& e) {cerr << e << endl;
    status = 1;
  } catch (const char* msg) {
    cerr << msg << endl;
    status = 1;
  }
  if (ic) {
    try {
      ic->destroy();
    } catch (const Ice::Exception& e) {
      cerr << e << endl;
      status = 1;
    }
  }
  return status;
}
