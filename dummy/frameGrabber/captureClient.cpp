#include <Ice/Ice.h>
#include <Video.h>
using namespace std;
using namespace Video;
int main(int argc, char* argv[])
{
  int status = 0;
  Ice::CommunicatorPtr ic;
  try {
    ic = Ice::initialize(argc, argv);
    Ice::ObjectPrx base = ic->stringToProxy(
					    "SimplePrinter:default -p 9000");
    GrabberPrx grabber = GrabberPrx::checkedCast(base);
    if (!grabber)
      throw "Invalid proxy";
    grabber->frameGrabber();
  } catch (const Ice::Exception& ex) {
    cerr << ex << endl;
    status = 1;
  } catch (const char* msg) {
    cerr << msg << endl;
    status = 1;
  }
  if (ic)
    ic->destroy();
  return status;
}
