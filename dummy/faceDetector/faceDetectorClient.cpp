#include <Ice/Ice.h>
#include <Face.h>
using namespace std;
using namespace Face;
int main(int argc, char* argv[])
{
  int status = 0;
  Ice::CommunicatorPtr ic;
  try {
  
    ic = Ice::initialize(argc, argv);
    Ice::ObjectPrx base = ic->stringToProxy(
					    "SimpleDetector:default -p 11000");
    DetectorPrx detector = DetectorPrx::checkedCast(base);
    if (!detector)
      throw "Invalid proxy";
    detector->faceDetector();
  } catch (const Ice::Exception& ex) {
    cerr << ex << endl;
    status = 1;
  } catch (const char* msg) {
    cerr << msg << endl;
    status = 1;
  }
  if (ic)
    ic->destroy();
  return status;
}
