#define CV_NO_BACKWARD_COMPATIBILITY

#include "cv.h"
#include "highgui.h"

#include <iostream>
#include <cstdio>

#ifdef _EiC
#define WIN32
#endif

using namespace std;
using namespace cv;
#include <Ice/Ice.h>
#include <Face.h>

using namespace std;
using namespace Face;

String cascadeName = "haarcascade_frontalface_default.xml";
String nestedCascadeName = "haarcascade_eye_tree_eyeglasses.xml";
class DetectorI : public Detector {
public:

  virtual void faceDetector(const Ice::Current&);
  void detectAndDraw( Mat& img, CascadeClassifier& cascade, CascadeClassifier& nestedCascade, double scale, const char *filename);
};


void DetectorI::detectAndDraw( Mat& img,
                   CascadeClassifier& cascade, CascadeClassifier& nestedCascade,
		    double scale, const char *filename)
{
    int i = 0;
    double t = 0;
    vector<Rect> faces;
    IplImage* im;

    const static Scalar colors[] =  { CV_RGB(0,0,255),
        CV_RGB(0,128,255),
        CV_RGB(0,255,255),
        CV_RGB(0,255,0),
        CV_RGB(255,128,0),
        CV_RGB(255,255,0),
        CV_RGB(255,0,0),
        CV_RGB(255,0,255)} ;
    Mat gray, smallImg( cvRound (img.rows/scale), cvRound(img.cols/scale), CV_8UC1 );

    cvtColor( img, gray, CV_BGR2GRAY );
    resize( gray, smallImg, smallImg.size(), 0, 0, INTER_LINEAR );
    equalizeHist( smallImg, smallImg );

    t = (double)cvGetTickCount();
    cascade.detectMultiScale( smallImg, faces,
        1.1, 2, 0
        //|CV_HAAR_FIND_BIGGEST_OBJECT
        //|CV_HAAR_DO_ROUGH_SEARCH
        |CV_HAAR_SCALE_IMAGE
        ,
        Size(30, 30) );
    t = (double)cvGetTickCount() - t;
    for( vector<Rect>::const_iterator r = faces.begin(); r != faces.end(); r++, i++ )
      {
	cout<<"Dentro del faces"<<endl;
        Mat smallImgROI;
        vector<Rect> nestedObjects;
        Point center;
        Scalar color = colors[i%8];
        int radius;

        center.x = cvRound((r->x + r->width*0.5)*scale);
        center.y = cvRound((r->y + r->height*0.5)*scale);
        radius = cvRound((r->width + r->height)*0.25*scale);
        circle( img, center, radius, color, 3, 8, 0 );

	im = cvLoadImage(filename );
	cvSetImageROI(im, cvRect(center.x-radius, center.y-radius, radius*2,radius*2));
	IplImage* im_resize;
	im_resize = cvCreateImage(cvSize(92,112),im->depth,im->nChannels);
	cvResize(im,im_resize);
	cvSaveImage("result.pgm", im_resize);
	//cvShowImage("result", img);
	
        if( nestedCascade.empty() )
            continue;
        smallImgROI = smallImg(*r);

       
        nestedCascade.detectMultiScale( smallImgROI, nestedObjects,
					1.1, 2, 0
					//|CV_HAAR_FIND_BIGGEST_OBJECT
					//|CV_HAAR_DO_ROUGH_SEARCH
					//|CV_HAAR_DO_CANNY_PRUNING
					|CV_HAAR_SCALE_IMAGE
					,
					Size(30, 30) );
        for( vector<Rect>::const_iterator nr = nestedObjects.begin(); nr != nestedObjects.end(); nr++ )
	  {
            center.x = cvRound((r->x + nr->x + nr->width*0.5)*scale);
            center.y = cvRound((r->y + nr->y + nr->height*0.5)*scale);
            radius = cvRound((nr->width + nr->height)*0.25*scale);
            circle( img, center, radius, color, 3, 8, 0 );
	    
	  }
	
	
      }  
    cv::imshow( "result", img );    
}

void DetectorI::faceDetector(const Ice::Current&){
  CvCapture* capture = 0;
    Mat frame, frameCopy, image;
    const String scaleOpt = "--scale=";
    size_t scaleOptLen = scaleOpt.length();
    const String cascadeOpt = "--cascade=";
    size_t cascadeOptLen = cascadeOpt.length();
    const String nestedCascadeOpt = "--nested-cascade";
    size_t nestedCascadeOptLen = nestedCascadeOpt.length();
    String inputName;// ="capture.jpg";
 
    CascadeClassifier cascade, nestedCascade;
    double scale = 1;
    if( !cascade.load( cascadeName ) )
    {
      cerr << "ERROR: Could not load classifier cascade" << endl;
        cerr << "Usage: facedetect [--cascade=\"<cascade_path>\"]\n"
	  "   [--nested-cascade[=\"nested_cascade_path\"]]\n"
	  "   [--scale[=<image scale>\n"
	  "   [filename|camera_index]\n" ;
    }

    if(inputName.length()==0){
      capture = cvCaptureFromCAM(-1);
    }
    else if(inputName.length()){
      image = imread(inputName, 1);
      if(image.empty())
	capture = cvCaptureFromAVI(inputName.c_str());
    }
    else
        image = imread("capture.jpg", 1);

    cvNamedWindow( "result", 1 );
    if(capture){
      // for(;;)
      //{
	  IplImage* iplImg = cvQueryFrame( capture );
	  frame = iplImg;
	  // if( frame.empty() )
	  //break;
	  if( iplImg->origin == IPL_ORIGIN_TL )
	    frame.copyTo( frameCopy );
	  else
	    flip( frame, frameCopy, 0 );
	  cvSaveImage("capture.jpg", iplImg);
	  detectAndDraw( frameCopy, cascade, nestedCascade, scale, "capture.jpg");
	  
	    
	  if( waitKey( 10 ) >= 0 )
	    goto _cleanup_;
	  //}
      
      waitKey(0);
    _cleanup_:
      cvReleaseCapture( &capture );
    }
    else
      {
	if( !image.empty() )
	 {
	   detectAndDraw( image, cascade, nestedCascade, scale, "capture.jpg");
	   waitKey(0);
	 }
       else if( !inputName.empty() )
	 {
	   /* assume it is a text file containing the
	      list of the image filenames to be processed - one per line */
	   FILE* f = fopen( inputName.c_str(), "rt" );
	   if( f )
	     {
	       char buf[1000+1];
	       while( fgets( buf, 1000, f ) )
		 {
		   int len = (int)strlen(buf), c;
		   while( len > 0 && isspace(buf[len-1]) )
		     len--;
		   buf[len] = '\0';
		   cout << "file " << endl;
		   image = imread( buf, 1 );
		   if( !image.empty() )
		     {
		       
		       detectAndDraw( image, cascade, nestedCascade, scale, "capture.jpg" );
		       
		       c = waitKey(0);
		       if( c == 27 || c == 'q' || c == 'Q' )
			 break;
		     }
		 }
	       fclose(f);
	     }
	 }
    }


    cvDestroyWindow("result"); 
  
}

int main(int argc, char* argv[]){
  int status = 0;
  Ice::CommunicatorPtr ic;
  try {
    ic = Ice::initialize(argc, argv);
    Ice::ObjectAdapterPtr adapter
      = ic->createObjectAdapterWithEndpoints("SimpleDetectorAdapter", "default -p 11000");
    Ice::ObjectPtr object = new DetectorI;
    adapter->add(object, ic->stringToIdentity("SimpleDetector"));
    adapter->activate();
    ic->waitForShutdown();
  } catch(const Ice::Exception& e) {cerr << e << endl;
    status = 1;
  } catch (const char* msg) {
    cerr << msg << endl;
    status = 1;
  }
  if (ic) {
    try {
      ic->destroy();
    } catch (const Ice::Exception& e) {
      cerr << e << endl;
      status = 1;
    }
  }
  return status;
}
